<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use Redirect;
use Session;
use Carbon\Carbon;
use App\off_day;
use stdClass;
use auth;


class PayrollController extends Controller
{
    //Payroll geade view page
    public function payroll_grade(){
        return view('payroll.grade');
    }
    //Payroll geade data store
    public function payroll_grade_store(Request $request){
        $current_time = Carbon::now()->toDateTimeString();
        $payroll_grade=DB::table('payroll_grade')->insert([
            'grade_name' =>$request->grade_name,
            'basic_salary' =>$request->basic_amount,
            'house_rant' =>$request->house_amount,
            'transport' =>$request->transport_amount,
            'medical' =>$request->medical_amount,
            'food' =>$request->food_amount,
            'others' =>$request->others_amount,
            'total_salary'=>$request->basic_amount+$request->house_amount+$request->transport_amount+$request->medical_amount+$request->food_amount+$request->others_amount,
            'created_by' =>$request->user_name,
            'modified_by' =>$request->user_name,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if($payroll_grade){
            Session::flash('message', 'Grade Store Successful!');
            return redirect('grade');
        }
    }
    //Payroll grade data view method
    public function payroll_grade_show(){
        $data=DB::table('payroll_grade')->paginate(20);
        return view('payroll.grade_show',compact('data'));
    }
    //id wise grade show
    public function show($id){
        $data=DB::table('payroll_grade')->WHERE('id',$id)->get();
        return response()->json($data);
    }
    //Payroll grade data update method
    public function payroll_grade_update(Request $request){
        $id=$request->grade_id;
        $current_time = Carbon::now()->toDateTimeString();
        $data=DB::table('payroll_grade')->WHERE('id',$id)->update([
            'grade_name' =>$request->grade_name,
            'basic_salary' =>$request->basic_amount,
            'house_rant' =>$request->house_amount,
            'transport' =>$request->transport_amount,
            'medical' =>$request->medical_amount,
            'food' =>$request->food_amount,
            'others' =>$request->others_amount,
            'total_salary' =>$request->basic_amount+$request->house_amount+$request->transport_amount+$request->medical_amount+$request->food_amount+$request->others_amount,
            'created_by' =>$request->user_name,
            'modified_by' =>$request->user_name,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if($data){
            Session::flash('message', 'Grade Update Successful!');
            return redirect()->back();
        }
    }
    //Payroll grade data delete method
    public function grade_delete($id){
        $grade_id=DB::table('payroll_salary')->where(['grade_id'=>$id])->get();
        if(count($grade_id)){
            Session::flash('failedMessage',"Deletion failed. There are already some employees under this grade." );
            return back();
        }else{
            $data=DB::table('payroll_grade')->WHERE('id',$id)->delete();
            if($data){
                Session::flash('message', 'Grade Delete Successful!');
                return redirect()->back();
            }
        }
    }
    //Payroll salary view page
    public function payroll_salary(){
        $data=DB::table('payroll_grade')->latest()->get();$data=DB::table('payroll_grade')->latest()->get();
        return view('payroll.salary',compact('data'));
    }
    //Normal Bonus store multiple employee
    public function bonus_emloyee(Request $request){
        $bonus=$request->bonus_amount_employees;
        $emp_id=$request->emp_ids;
        $date=date('Y-m');
        $current_time = Carbon::now()->toDateTimeString();
        for($i=0;$i<count($request->bonus_amount_employees);$i++){
            $data=DB::table('employees_bonus')->insert([
                'emp_id' => $request->emp_ids[$i],
                'emp_bonus' => $bonus[$i],
                'date' => $date,
                'created_at' =>$current_time,
                'updated_at' =>$current_time,
            ]);
        }
        if($data){
            Session::flash('message', 'Bonus added Successful!');
            return redirect()->back();
        }
    }
    //employee bonus settings view
    public function employee_bonus_view(){
        return view('payroll.bonus');
    }
    //increment bonus view page
    public function incrementBonusview(){
        $designation=DB::table('designations')->get();
        $department=DB::table('departments')->get();
        return view('payroll.empincrement',compact('designation','department'));
    }
    //increment bonus designation all employee
    public function incdesignationall(){
      $alldesignation=DB::SELECT("SELECT employees.id,employees.empFirstName,employees.empLastName,employees.empJoiningDate,designations.designation,payroll_salary.total_employee_salary as gross_salary,employees.probation_period,NOW() as current_month,TIMESTAMPDIFF(MONTH,empJoiningDate,NOW())+1 as month_duration FROM employees
      LEFT JOIN payroll_salary ON payroll_salary.emp_id = employees.id
      LEFT JOIN  designations ON employees.empDesignationId=designations.id 
      WHERE TIMESTAMPDIFF(MONTH,empJoiningDate,NOW())+1 >employees.probation_period");
      return response()->json($alldesignation);
    }

    //employee wise gross show
    public function empGrossShow($id){
        $data=DB::SELECT("SELECT * FROM `payroll_salary` WHERE emp_id=$id");
        return response()->json($data);
    }
    //festival bonus view page
    public function FestivalBonuspage(){
        $month=date('Y-m');
        $festivalBonus=DB::table('festival_bonus')->where('month',$month)->get();
        $designation=DB::table('designations')->get();
        $department=DB::table('departments')->get();
        return view('payroll.empfestival',compact('designation','department','festivalBonus'));
    }
    // all designation employee
    public function alldesignationemployee(){
      $alldesignation=DB::SELECT("SELECT employees.id,employees.empFirstName,designations.designation,
        payroll_salary.total_employee_salary from employees
        LEFT JOIN designations ON employees.empDesignationId = designations.id 
        LEFT JOIN payroll_salary ON payroll_salary.emp_id = employees.id 
        WHERE employees.empAccStatus=1");
      return response()->json($alldesignation);
    }
    // all department employee
    public function alldepartmentemployee(){
        $alldepartment=DB::SELECT("SELECT employees.id,employees.empFirstName,departments.departmentName,
        payroll_salary.total_employee_salary from employees
        LEFT JOIN departments ON employees.empDepartmentId = departments.id 
        LEFT JOIN payroll_salary ON payroll_salary.emp_id = employees.id
        WHERE employees.empAccStatus=1");
        return response()->json($alldepartment);
    }
    //department id wise employe show
    public function Departmentwiseemployeefestival($id){
        $individiualdepartment=DB::SELECT("SELECT employees.id,employees.empFirstName,departments.departmentName,
        payroll_salary.total_employee_salary from employees
        LEFT JOIN departments ON employees.empDepartmentId = departments.id 
        LEFT JOIN payroll_salary ON payroll_salary.emp_id = employees.id 
        WHERE employees.empDepartmentId=$id AND employees.empAccStatus=1");
        return response()->json($individiualdepartment);
    }

    //designation id wise employee show for festival bonus
    public function Designationwiseemployeefestival($id){
        $individiualdesignation=DB::SELECT("
        SELECT employees.id,employees.empFirstName,designations.designation,
        payroll_salary.total_employee_salary from employees
        LEFT JOIN designations ON employees.empDesignationId = designations.id 
        LEFT JOIN payroll_salary ON payroll_salary.emp_id = employees.id 
        WHERE employees.empDesignationId=$id AND employees.empAccStatus=1");
        return response()->json($individiualdesignation);
    }

    //designation wise employee bonus store for festival bonus
    public function designationBonusStore(Request $request){
        $i=0;
        foreach($request->emp_id as $key=>$value)
        {
            $empid = $value;
                $empgross =  $_POST['total_salary'][$key];
                $emp_total_percent=$_POST['percent'][$key];
                $emp_total_amount=$_POST['amount'][$key];
                $amount = $_POST['amount'][$key];
                $totals_salary=$_POST['total_salary'][$key];
            $emp_bonus = (int)$totals_salary/100*(int)$emp_total_percent;
            $insert[]=[
                'emp_id' =>$empid,
                'emp_gross' =>$empgross,
                'emp_bonus' =>$emp_bonus,
                'emp_amount' =>$amount,
                'emp_total_percent' =>$emp_total_percent,
                'emp_total_amount' =>$emp_total_amount,
                'bonus_given_id' =>auth()->user()->id,
                'bonus_title' =>$request->festival_title,
                'month' =>date('Y-m'),
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ];
        }
        if($i%550==0){
            if(!empty($insert)){
                $insertData = DB::table('festival_bonus')->insert($insert);
                unset($insert);
            }
        }
        $i++;
        Session::flash('fbonus', 'Festival Bonus added successful!');
        return redirect()->back();
    }

    //increment or year employee multiple bonus store
    public function incBonusStore(Request $request){
        $i=0;
        foreach($request->emp_id as $key=>$value)
        {
            $empid = $value;

                $percent=$_POST['percent'][$key];
                $amount=$_POST['amount'][$key];
            $total_salary = $_POST['total_salary'][$key];
            $percent_bonus= (int)$_POST['total_salary'][$key]/100*(int)$percent;
            $insert[]=[
                'emp_id' =>$empid,
                'emp_gross' =>$_POST['total_salary'][$key],
                'emp_bonus' =>$percent_bonus,
                'emp_amount' =>$amount,
                'emp_total_percent' =>$percent,
                'emp_total_amount' =>$amount,
                'bonus_given_id' =>auth()->user()->id,
                'date' =>date('Y-m'),
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ];
        }
        if($i%550==0){
            if(!empty($insert)){
                $insertData = DB::table('employees_bonus')->insert($insert);
                unset($insert);
            }
        }
        $i++;

      if($insertData){
          for($i=0;$i<count($request->emp_id);$i++){
              $id=$request->emp_id[$i];
                  $percent=$request->percent[$i];
                  $amount=$request->amount[$i];
              $total_salary=$request->total_salary[$i];
              $percent_wise=$total_salary/100*$percent;
              $amount_wise=$amount;
              $totals=$percent_wise+$amount_wise;
              $update=DB::table('payroll_salary')->where('emp_id',$id)->update([
                 'total_employee_salary' =>$totals+$total_salary
            ]);
          }
      }

        Session::flash('inbonus', 'Increment Bonus added successful!');
        return redirect()->back();

    }
    //payslip generate view
    public function payslipGenerate(){
        return view('report.payroll.payslip.index');
    }
    //payslip report
    public function payslipReportemployees(Request $request){
            $monthyear=$request->payslip_generate_report;
            $year=date('Y',strtotime($request->payslip_generate_report));
            $monthwerwer=date('m',strtotime($request->payslip_generate_report));
              $data=DB::SELECT("SELECT tb_salary_history.id,tb_salary_history.total,tb_salary_history.normal_deduction,tb_salary_history.advanced_deduction,tb_salary_history.month,tb_salary_history.basic_salary,tb_salary_history.emp_id,tb_salary_history.grade_id,tb_salary_history.designation_id,tb_salary_history.house_rant,tb_salary_history.medical,tb_salary_history.transport,tb_salary_history.food,tb_salary_history.other,tb_salary_history.gross,tb_salary_history.overtime,tb_salary_history.overtime_rate,tb_salary_history.overtime_amount,tb_salary_history.present,tb_salary_history.absent,tb_salary_history.absent_deduction_amount,tb_salary_history.gross_pay,tb_salary_history.one_year_bonus,tb_salary_history.festival_bonus_amount,tb_salary_history.festival_bonus_percent,tb_salary_history.increment_bonus_amount,tb_salary_history.increment_bonus_percent,tb_salary_history.working_day,tb_salary_history.weekend,tb_salary_history.holiday,tb_salary_history.net_amount,employees.empFirstName,employees.empLastName,employees.employeeId,employees.empJoiningDate,employees.empSection,payroll_grade.grade_name,designations.designation,sum(total) as total_leave
              FROM tb_salary_history
              LEFT JOIN employees ON tb_salary_history.emp_id = employees.id
              LEFT JOIN payroll_grade ON tb_salary_history.grade_id = payroll_grade.id
              LEFT JOIN designations ON tb_salary_history.designation_id = designations.id
              WHERE YEAR(tb_salary_history.month)=$year AND MONTH(tb_salary_history.month)=$monthwerwer GROUP BY tb_salary_history.emp_id");
              return view('report.payroll.payslip.payslip_view',compact('data'));
        }
    }