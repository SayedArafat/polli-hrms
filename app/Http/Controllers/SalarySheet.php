<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use DB;
use redirect;
use Session;
use stdClass;

class SalarySheet extends Controller
{
    public function salary_data(Request $request){
        $s=$request->salary_sheet_month;
        $start = date('Y-m-01',strtotime("05/25/2018"));
        return response()->json($s);
//        $end = date('Y-m-t',strtotime($request->salary_sheet_month));
//        $leave_results = DB::select( DB::raw("select employee_id, employees.empFirstName,leave_type_id as leave_id, SUM(actual_days) as total_leave FROM employees,tb_leave_application WHERE tb_leave_application.employee_id=employees.id  AND  tb_leave_application.created_at BETWEEN '$start' AND '$end' AND status=1 GROUP BY employee_id") );
//        return response()->json($leave_results);
    }
    //salary sheet view page
    public function view_salary_sheet(){
        return view('report.payroll.salarysheet.index');
    }
    //condition wise salary sheet
    public function employee_SalarySheet(Request $request){
        $date=date('m-Y');
        $current_time = Carbon::now()->toDateTimeString();
        $current_month_first=date('Y-m-01',strtotime($request->salary_sheet_month));
        $current_month_last=date('Y-m-t',strtotime($request->salary_sheet_month));
        $month=date('Y-m-d',strtotime($request->salary_sheet_month));
        $workingmonth=$request->salary_sheet_month;
        $setup=DB::table('attendance_setup')->first();
        $exit_time=$setup->exit_time;
        $date = strtotime($exit_time);
        $office_exit_time=date('H',$date);
        $overtime_result = DB::select( DB::raw("SELECT employees.empFirstName,empLastName, emp_id,GROUP_CONCAT(DATE_FORMAT(attendance.out_time,\"%H\")) as out_time,SUM(abs(TIMEDIFF('$office_exit_time', DATE_FORMAT( attendance.out_time,\"%H\")))) as overtime FROM employees,attendance WHERE attendance.emp_id=employees.id  AND  date BETWEEN '$current_month_first' AND '$current_month_last' AND DATE_FORMAT(attendance.out_time,\"%H\") >'$office_exit_time' GROUP BY emp_id") );
        $present = DB::select( DB::raw("SELECT emp_id,empFirstName,count(date) as present from attendance,employees WHERE attendance.emp_id=employees.id AND date BETWEEN '$current_month_first' AND '$current_month_last' GROUP BY emp_id") );
        $leave_result=DB::SELECT("SELECT employee_id, leave_type_id, SUM(actual_days) as total FROM `tb_leave_application` WHERE status=1 AND (leave_ending_date>='$current_month_first' AND leave_ending_date<='$current_month_last' OR leave_starting_date >= '$current_month_first' AND leave_starting_date<='$current_month_last') GROUP BY employee_id");
        $leave_result_two=DB::SELECT("SELECT employee_id, leave_type_id, SUM(actual_days) as total FROM `tb_leave_application` WHERE status=1 AND (leave_ending_date>='$current_month_first' AND leave_ending_date<='$current_month_last' OR leave_starting_date >= '$current_month_first' AND leave_starting_date<='$current_month_last') GROUP BY leave_type_id");
        foreach ($overtime_result as $or){
            DB::table('employee_overtime')->insert([
                'emp_ids'=>$or->emp_id,
                'overtime_hour'=>$or->overtime,
                'month'=>$month,
            ]);
        }
        foreach ($present as $presents){
            DB::table('tb_total_present')->insert([
                'emp_id'=>$presents->emp_id,
                'total_present'=>$presents->present,
                'month'=>$month,
            ]);
        }
        foreach ($leave_result as $leaves){
            DB::table('total_employee_leave')->insert([
                'emp_idss'=>$leaves->employee_id,
                'total_leaves_taken'=>$leaves->total,
                'month'=>$month,
            ]);
        }
        foreach ($leave_result_two as $leaves_two){
            DB::table('leave_of_employee')->insert([
                'emp_id'=>$leaves_two->employee_id,
                'leave_type_id'=>$leaves_two->leave_type_id,
                'total'=>$leaves_two->total,
                'month'=>$month,
            ]);
        }
        $start = date('Y-m-01',strtotime($request->salary_sheet_month));
        $end = date('Y-m-t',strtotime($request->salary_sheet_month));
        $monthscheck=date('Y-m-d',strtotime($request->salary_sheet_month));
        $bonusmonth=date('Y-m',strtotime($request->salary_sheet_month));
        $data=DB::table('payroll_salary')
            ->leftJoin('employees', function ($query) {
                $query->on('payroll_salary.emp_id', '=', 'employees.id');
                $query->where('employees.empAccStatus','=',1);
            })
            ->leftJoin('payroll_grade', function ($query) {
                $query->on('payroll_salary.grade_id', '=', 'payroll_grade.id');
            })
            ->leftJoin('units', function ($query) {
                $query->on('employees.unit_id', '=', 'units.id');
            })
            ->leftJoin('total_employee_leave', function ($query) use($monthscheck) {
                $query->on('payroll_salary.emp_id', '=', 'total_employee_leave.emp_idss');
                $query->where('total_employee_leave.month','=',$monthscheck);
            })
            ->leftJoin('leave_of_employee', function ($query) use($monthscheck) {
                $query->on('payroll_salary.emp_id', '=', 'leave_of_employee.emp_id');
                $query->where('leave_of_employee.month','=',$monthscheck);
            })
            ->leftJoin('tb_leave_type', function ($query) use($start,$end) {
                $query->on('leave_of_employee.leave_type_id', '=', 'tb_leave_type.id');
            })
            ->leftJoin('tb_total_present', function ($query) use($monthscheck) {
                $query->on('payroll_salary.emp_id', '=', 'tb_total_present.emp_id');
                $query->where('tb_total_present.month','=',$monthscheck);
            })
            ->leftJoin('employee_overtime', function ($query) use($monthscheck) {
                $query->on('payroll_salary.emp_id', '=', 'employee_overtime.emp_ids');
                $query->where('employee_overtime.month','=',$monthscheck);
            })
            ->leftJoin('employees_bonus', function ($query) use($bonusmonth) {
                $query->on('payroll_salary.emp_id', '=', 'employees_bonus.emp_id');
                $query->where('employees_bonus.date','=',$bonusmonth);
            })
            ->leftJoin('festival_bonus', function ($query) use($bonusmonth) {
                $query->on('payroll_salary.emp_id', '=', 'festival_bonus.emp_id');
                $query->where('festival_bonus.month','=',$bonusmonth);
            })
            ->leftJoin('tb_deduction', function ($query) use($bonusmonth) {
                $query->on('payroll_salary.emp_id', '=', 'tb_deduction.emp_id');
                $query->where('tb_deduction.month','=',$bonusmonth);
            })
            ->leftJoin('tb_loan_deduction', function ($query) use($bonusmonth) {
                $query->on('payroll_salary.emp_id', '=', 'tb_loan_deduction.emp_id');
                $query->where('tb_loan_deduction.month','=',$bonusmonth);
            })
            ->leftJoin('tb_production_bonus', function ($query) use($bonusmonth) {
                $query->on('payroll_salary.emp_id', '=', 'tb_production_bonus.emp_id');
                $query->where('tb_production_bonus.month','=',$bonusmonth);
            })
            ->leftJoin('departments', function ($query) {
                $query->on('employees.empDepartmentId', '=', 'departments.id');
            })
            ->join('designations', function ($query) {
                $query->on('designations.id', '=', 'employees.empDesignationId');
            })
            ->select('payroll_salary.*','employees.empDesignationId','employees.empFirstName','employees.empLastName','employees.empJoiningDate','employees.empSection','employees.employeeId','payroll_grade.grade_name','designations.designation','employees.unit_id','units.name','employees.empDepartmentId','departments.departmentName','employees.work_group','employees.payment_mode','total_employee_leave.total_leaves_taken','employee_overtime.overtime_hour as overtime','employees_bonus.emp_bonus as special_bonus','employees_bonus.emp_amount','festival_bonus.emp_bonus as f_bonus','festival_bonus.emp_amount as f_amount','tb_total_present.total_present','leave_of_employee.leave_type_id','leave_of_employee.total','tb_deduction.normal_deduction_amount','tb_loan_deduction.month_wise_deduction_amount')
            ->selectraw("CONCAT(GROUP_CONCAT( DISTINCT(tb_leave_type.leave_type),leave_of_employee.total)) as leave_name_value")
            ->selectraw("GROUP_CONCAT(DISTINCT(leave_of_employee.leave_type_id)SEPARATOR ',') as leave_with_id")
            ->selectraw("GROUP_CONCAT(DISTINCT(leave_of_employee.total)SEPARATOR ',') as leave_with_total")
            ->groupBy('payroll_salary.emp_id')
            ->get();
        //return response()->json($data);
        if($data){
            $month=date('Y-m-d',strtotime($request->salary_sheet_month));
            $check=DB::table('tb_salary_history')->where('month',$month)->count();
            if($check>0){
                $delete=DB::table('tb_salary_history')->where('month',$month)->delete();
            }
            function dayCalculator($d1,$d2){
                $date1=strtotime($d1);
                $date2=strtotime($d2);
                $interval1=1+round(abs($date1-$date2)/86400);
                $festivalLeave = DB::table('tb_festival_leave')->get();
                $dcount=0;
                foreach($festivalLeave as $fleave){
                    if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                        $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
                    }
                    else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                        $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
                    }
                    else{
                        continue;
                    }
                }
                $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
                $start_date=date('Y-m-d', $date1);
                $end_date=date('Y-m-d', $date2);
                $key=0;
                for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
                    $dates = $i->format('Y-m-d');
                    $timestamp = strtotime($dates);
                    $day = date('l', $timestamp);
                    for($j=0;$j<count($weekends);$j++)
                    {
                        if($day==$weekends[$j]){
                            $key++;
                        }
                    }
                }
                $interval=(int)$interval1-((int)$dcount+$key);
                return $interval;
            }
            function holiday($d1,$d2){
                $date1=strtotime($d1);
                $date2=strtotime($d2);
                $interval1=1+round(abs($date1-$date2)/86400);
                $festivalLeave = DB::table('tb_festival_leave')->get();
                $dcount=0;
                foreach($festivalLeave as $fleave){
                    if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                        $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
                    }
                    else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                        $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
                    }
                    else{
                        continue;
                    }
                }
                return $dcount;
            }
            function weekdayCalculator($d1,$d2){
                $date1=strtotime($d1);
                $date2=strtotime($d2);
                $interval1=1+round(abs($date1-$date2)/86400);
                $festivalLeave = DB::table('tb_festival_leave')->get();
                $dcount=0;
                foreach($festivalLeave as $fleave){
                    if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                        $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
                    }
                    else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                        $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
                    }
                    else{
                        continue;
                    }
                }
                $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
                $start_date=date('Y-m-d', $date1);
                $end_date=date('Y-m-d', $date2);
                $key=0;
                for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
                    $dates = $i->format('Y-m-d');
                    $timestamp = strtotime($dates);
                    $day = date('l', $timestamp);
                    for($j=0;$j<count($weekends);$j++)
                    {
                        if($day==$weekends[$j]){
                            $key++;
                        }
                    }
                }
                $interval=(int)$interval1-((int)$dcount+$key);
                return $key;
            }
            $current_month_first=date('Y-m-01',strtotime($workingmonth));
            $current_month_last=date('Y-m-t',strtotime($workingmonth));
            $working_day=dayCalculator($current_month_first,$current_month_last);
            $weekend=weekdayCalculator($current_month_first,$current_month_last);
            $holiday=holiday($current_month_first,$current_month_last);
            foreach($data as $testdata){
                $current_month_day=date('t',strtotime($workingmonth));
                $overtime_rate=$testdata->basic_salary/208*2;
                $actual_overtime_rate=round($overtime_rate,2);
                $overtime_amount=$actual_overtime_rate*$testdata->overtime;
                $absentday=$working_day-$testdata->total_leaves_taken-$testdata->total_present;
                $deduction=round($testdata->basic_salary/$current_month_day*$absentday,2);
                $gross_pay=$testdata->total_employee_salary-$deduction-$testdata->normal_deduction_amount-$testdata->month_wise_deduction_amount;
                $month=date('Y-m-d',strtotime($request->salary_sheet_month));
                $monthss=date('Y-m',strtotime($request->salary_sheet_month));
                $net_wages=$gross_pay+$overtime_amount+$testdata->f_bonus+$testdata->f_amount;
                $actual_net_wages=round($net_wages,2);
                DB::table('tb_salary_history')->insert([
                    'emp_id' => $testdata->emp_id,
                    'grade_id' => $testdata->grade_id,
                    'designation_id' => $testdata->empDesignationId,
                    'basic_salary' => $testdata->basic_salary,
                    'house_rant' => $testdata->house_rant,
                    'medical' => $testdata->medical,
                    'transport' => $testdata->transport,
                    'food' => $testdata->food,
                    'other' => $testdata->other,
                    'gross' => $testdata->total_employee_salary,
                    'gross_pay' => $gross_pay,
                    'present' => $testdata->total_present,
                    'absent' => $absentday,
                    'absent_deduction_amount' => $deduction,
                    'festival_bonus_amount' => $testdata->f_amount,
                    'festival_bonus_percent' => $testdata->f_bonus,
                    'increment_bonus_amount' => $testdata->emp_amount,
                    'increment_bonus_percent' => $testdata->special_bonus,
                    'normal_deduction' => $testdata->normal_deduction_amount,
                    'advanced_deduction' => $testdata->month_wise_deduction_amount,
                    'working_day' =>$working_day,
                    'weekend' =>$weekend,
                    'holiday' =>$holiday,
                    'overtime' =>$testdata->overtime,
                    'overtime_rate' =>$actual_overtime_rate,
                    'overtime_amount' =>$overtime_amount,
                    'leave' =>$testdata->leave_name_value,
                    'total' =>$testdata->total_leaves_taken,
                    'net_amount' =>$actual_net_wages,
                    'month' => $month,
                    'dates' => $monthss,
                    'status' => 0,
                    'created_at' =>Carbon::now()->toDateTimeString(),
                    'updated_at' =>Carbon::now()->toDateTimeString(),
                ]);
            }
        }
        $delete_leave_of_employee_leave=DB::table('leave_of_employee')->delete();
        $delete_total_employee_leave=DB::table('total_employee_leave')->delete();
        $delete_total_employee_present=DB::table('tb_total_present')->delete();
        $delete_total_employee_overtime=DB::table('employee_overtime')->delete();
        return redirect('report/payroll/employee/salary/sheet');
//        return view('report.payroll.salarysheet.salary_sheet_view',compact('data','month','workingmonth','leave_result_two'));
    }
    //salary sheet view page
    public function monthwisesalarysheetview(){
        return view('report.payroll.salarysheet.salarysheetmonth');
    }

    //generate employee month wise salary sheet
    public function Monthwiseemployeesalarysheet(Request $request){
                $monthyear=$request->emp_sal_month;
                $year=date('Y',strtotime($request->emp_sal_month));
                $monthwerwer=date('m',strtotime($request->emp_sal_month));
                $paydate=$request->salary_pay_date;
                $paymonth=DB::SELECT("select month FROM tb_salary_history WHERE YEAR(month)='$year' AND MONTH(month)='$monthwerwer' LIMIT 1");
                $employees_count_department_wise=DB::SELECT("select count(*) as count,departments.departmentName
                from employees
                inner join departments on employees.empDepartmentId = departments.id
                group by departments.departmentName");
                    $departmentwise_total=DB::SELECT("select departments.departmentName,SUM(tb_salary_history.basic_salary) as total_basic,SUM(tb_salary_history.house_rant) as total_house,
                SUM(tb_salary_history.medical) as total_medical,SUM(tb_salary_history.transport) as total_trasport,SUM(tb_salary_history.food) as total_food,SUM(tb_salary_history.other) as total_other,SUM(tb_salary_history.gross) as total_gross,SUM(tb_salary_history.absent_deduction_amount) as total_absent_deduction_amount,SUM(tb_salary_history.gross_pay) as total_gross_pay,SUM(tb_salary_history.overtime) as total_overtime_hour,SUM(tb_salary_history.overtime_amount) as total_overtime_amount,SUM(tb_salary_history.one_year_bonus) as total_one_Year_bonus,SUM(tb_salary_history.net_amount) as total_net_amount from employees inner join tb_salary_history on tb_salary_history.emp_id = employees.id inner join departments on employees.empDepartmentId = departments.id
                WHERE  YEAR( tb_salary_history.month)='$year' AND MONTH( tb_salary_history.month)='$monthwerwer'
                group by departments.departmentName");
                    $data=DB::table('tb_salary_history')
                        ->select('tb_salary_history.id','tb_salary_history.increment_bonus_amount','tb_salary_history.increment_bonus_percent','tb_salary_history.festival_bonus_amount','tb_salary_history.festival_bonus_percent','tb_salary_history.basic_salary','tb_salary_history.emp_id','tb_salary_history.grade_id','tb_salary_history.designation_id','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.other','tb_salary_history.gross','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.total','tb_salary_history.present','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.gross_pay','tb_salary_history.one_year_bonus','tb_salary_history.leave','tb_salary_history.working_day','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.net_amount','employees.empFirstName','employees.empLastName','employees.employeeId','employees.empJoiningDate','employees.empSection','employees.empDepartmentId','departments.departmentName','payroll_grade.grade_name','designations.designation','normal_deduction','advanced_deduction')
                        ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations', 'tb_salary_history.designation_id', '=', 'designations.id')
                        ->leftJoin('departments', 'employees.empDepartmentId', '=', 'departments.id')
                        ->whereYear('month', '=', $year)
                        ->whereMonth('month', '=', $monthwerwer)
                        ->orderBy('departments.departmentName', 'desc')
                        ->get();
                    return view('report.payroll.salary_month_wise_show',compact('data','employees_count_department_wise','departmentwise_total','paymonth'));
               }

    //daily employee salary sheet
    public function employeeDailysalary(){
        return view('report.payroll.dailyemployeesalary');
    }
    //employee daily salary show report
    public function employeeDailysalaryshow(Request $request){
        return view('report.payroll.dailyemployeesalaryview');
    }
}
