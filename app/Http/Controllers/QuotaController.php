<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class QuotaController extends Controller
{
    function __construct()
    {

    }

    public function district_list(){
        $country_population=DB::table('country_population')->first(['population']);
//        return $country_population->population;
        $district_quota=DB::table('quota_settings')->get();
        $store_vacancy=DB::table('store_vacancy_temp')->get();
        return view('quota.district_quota',compact('country_population','district_quota','store_vacancy'));
//        return $district_quota;
//        return "list";
    }
    public function store_vacancy(Request $request){
        $store_vacancy=DB::table('store_vacancy_temp')->get();
        if(count($store_vacancy)!=0) {
            DB::table('store_vacancy_temp')->truncate();

        }
        DB::table('store_vacancy_temp')->insert([
            'designation'=>$request->designation,
            'vacancy_no'=>$request->vacancy_no,

        ]);
        return redirect(route('quota.district_list'));

    }
    public function quota_settings(){
        $quota_settings=DB::table('quota_settings')->get();
        return view('quota.quota_settings',compact('quota_settings'));
    }
    public function store_district(Request $request){
        $this->validate($request,[
            'district_name'=>'required',
            'population'=>'required',
        ]);
        DB::table('quota_settings')->insert([
            'district_name'=>$request->district_name,
            'population'=>$request->population
        ]);
        Session::flash('message',"District Information Added Successfully");
        return redirect(route('quota.settings'));

    }
    public function update_district(Request $request,$id){
        $this->validate($request,[
            'district_name'=>'required',
            'population'=>'required',
        ]);
        DB::table('quota_settings')->where('id','=',$id)->update([
            'district_name'=>$request->district_name,
            'population'=>$request->population

        ]);
        Session::flash('message',"Successfully updated district information");
        return redirect(route('quota.settings'));

    }

    public function population(){
        $population=DB::table('country_population')->get();
//        return $population;
        return view('quota.population',compact('population'));
    }

    public function store_population(Request $request){
        $this->validate($request,[
            'population'=>'required',
        ]);
        DB::table('country_population')->insert([
            'country_name'=>'Bangladesh',
            'population'=>$request->population,
        ]);
        Session::flash('message',"Population information inserted");
        return redirect(route('quota.population'));

//        return $request->all();
    }

    public function update_population(Request $request,$id){
        $this->validate($request,[
            'population'=>'required',
        ]);
        DB::table('country_population')->where('id','=',$id)->update([
            'population'=>$request->population,

        ]);
        Session::flash('message','Population Updated');
        return redirect(route('quota.population'));

    }
}
