<?php

namespace App\Http\Controllers;

use App\Helper\AppHelper;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\Constraint\Count;
use Redirect;
use Session;
use PDF;
use mPDF;
use Carbon\Carbon;
use Excel;
use stdClass;
use Yajra\DataTables\DataTables;


class ReportController extends Controller
{
    //Start Employee Reports Area 
     public function employeeReportDashboard(){
        return view('report.employee.index');
     }

     public function search_employee(){
         $employees = DB::table('employees')
             ->select(DB::raw("CONCAT(empFirstName,' ',empLastName,' , ID : ',employeeId) AS full_name, id"))
             ->pluck('full_name','id')->all();
//         return $employees;
         return view('report.employee.employee_search',compact('employees'));
     }

     public function showEmployee(Request $request){
         return redirect(route('employee.show',$request->empId));
     }

    public function employeeListReport(){
        $units=DB::table('units')->distinct()->groupBy('name')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.employee.employee_list',compact('units','departments','floors','sections','designations'));
    }

    public function floor_line($id)
    {

        $lines=DB::table('tblines')->where('floor_id','=',$id)->pluck('line_no','id')->all();
        return view('report.floor_line',compact('lines'));
    }

    public function viewEmployeeListReport(Request $request){

        $query=@"SELECT employees.*,designations.designation,departments.departmentName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->reportType)=="0") {

            $query=@"SELECT employees.*,designations.designation,departments.departmentName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";
            
            if (($request->unitId)!=0) {
                $query.=" AND employees.unit_id='".$request->unitId."'";
            }

            if (!empty($request->sectionName)) {
                $query.=" AND employees.empSection='".$request->sectionName."'";
            }

            if (($request->departmentId)!=0) {
                $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
            }

            if (($request->designationId)!=0) {
                $query.=" AND employees.empDesignationId='".$request->designationId."'";
            }

            if (($request->empGenderId)!=0) {
                $query.=" AND employees.empGenderId='".$request->empGenderId."'";
            }

            $query.=" AND employees.empAccStatus='".$request->accStatus."'  ORDER BY employees.employeeId ASC ";
            // $query.=" GROUP BY 'unit_id'";
//            return $query;

            $employees= DB::select(DB::raw($query));


            if (($request->viewType)=="Preview") {
                return view('report.employee.view_employee_list',compact('employees','request'));
            }elseif(($request->viewType)=="Generate PDF"){
                $pagesize=$request->pagesize;
                $pageOrientation=$request->pageOrientation;


                return view('print/employee_list',compact('employees','request'));

                // $pdf=PDF::loadView('report.pdf.employee_list_pdf',compact('employees','request'));
                // $pdf->setPaper($pagesize, $pageOrientation);
                // $name=time()."_employee_list.pdf";
                // return $pdf->download($name);
                
            }else{
                Session::flash('error_message', "Invalid Request");
                return view('404');
            }
       }elseif(($request->reportType)=="1"||($request->reportType)=="2") {
            
            $query=@"SELECT employees.*,designations.designation,departments.departmentName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";
            
            if (($request->unitId)!=0) {
                $query.=" AND employees.unit_id='".$request->unitId."'";
            }

            if (!empty($request->sectionName)) {
                $query.=" AND employees.empSection='".$request->sectionName."'";
            }

            if (($request->departmentId)!=0) {
                $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
            }

            if (($request->designationId)!=0) {
                $query.=" AND employees.empDesignationId='".$request->designationId."'";
            }

            if (($request->empGenderId)!=0) {
                $query.=" AND employees.empGenderId='".$request->empGenderId."'";
            }

            $query.=" AND employees.empAccStatus='".$request->accStatus."'  ORDER BY employees.line_id ASC";
            // $query.=" GROUP BY 'unit_id'";

//            return $query;

            $employees= DB::select(DB::raw($query));


            if (($request->viewType)=="Preview") {
                return view('report.employee.view_employee_list1',compact('employees','request'));
            }elseif(($request->viewType)=="Generate PDF"){
                // return view('report.pdf.employee_list_pdf_line_section',compact('employees','request'));
                $pagesize=$request->pagesize;
                $pageOrientation=$request->pageOrientation;
                if(($request->reportType)=="1"){
                    $pdf=PDF::loadView('report.pdf.employee_list_pdf_line_section',compact('employees','request'));
                }elseif(($request->reportType)=="2"){
                    $pdf=PDF::loadView('report.pdf.employee_list_pdf_line_section1',compact('employees','request'));

                }else{

                }

                $pdf->setPaper($pagesize, $pageOrientation);
                $name=time()."_employee_list.pdf";
                return $pdf->download($name);
            }else{
                Session::flash('error_message', "Invalid Request");
                return view('404');
            }

       }else{
            return 'error occoured.';
       }


    }


    public function employeeListByStatus(){
       return view('report.employee.employee_list_by_status');
    }
    public function viewEmployeeListByStatus(Request $request){
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";
      
        $query.=" AND employees.empAccStatus='".$request->accStatus."'  ORDER BY employees.employeeId ASC";
       
        $employees= DB::select(DB::raw($query));


        if (($request->viewType)=="Preview") {
            return view('report.employee.view_employee_list_status',compact('employees','request'));
        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.employee_list_pdf',compact('employees','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_employee_list.pdf";
            return $pdf->download($name);
        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
       

    }


    public function employeeListByGender(){
       return view('report.employee.employee_list_by_gender');
    }
    
    public function viewEmployeeListByGender(Request $request){
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";
      
       
        if (($request->empGenderId)!=0) {
            $query.=" AND employees.empGenderId='".$request->empGenderId."'";
        }
        $query.=" ORDER BY employees.employeeId ASC";
       
        $employees= DB::select(DB::raw($query));


        if (($request->viewType)=="Preview") {
            return view('report.employee.view_employee_list_gender',compact('employees','request'));
        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.employee_list_pdf',compact('employees','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_employee_list.pdf";
            return $pdf->download($name);
        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
       

    }


    public function employeeListByDepartment(){
       $departments=DB::table('departments')->get(['departmentName','id']);
       return view('report.employee.employee_list_by_department',compact('departments'));
    }
    
    public function viewEmployeeListByDepartment(Request $request){
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";
      
       
        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }
       
        $query.=" ORDER BY employees.employeeId ASC";
        $employees= DB::select(DB::raw($query));


        if (($request->viewType)=="Preview") {
            return view('report.employee.view_employee_list_department',compact('employees','request'));
        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.employee_list_pdf',compact('employees','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_employee_list.pdf";
            return $pdf->download($name);
        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }


    public function employeeListByDesignation(){
       $designations=DB::table('designations')->get(['designation','id']);
       return view('report.employee.employee_list_by_designation',compact('designations'));
    }
    
    public function viewEmployeeListByDesignation(Request $request){
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";
      
       
        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }
       
        $query.=" ORDER BY employees.employeeId ASC";

        $employees= DB::select(DB::raw($query));


        if (($request->viewType)=="Preview") {
            return view('report.employee.view_employee_list_designation',compact('employees','request'));
        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.employee_list_pdf',compact('employees','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_employee_list.pdf";
            return $pdf->download($name);
        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function employeeListByFloorLine(){
       $floors=DB::table('floors')->get(['floor','id']);
       return view('report.employee.employee_list_by_floor_line',compact('floors'));
    }
    
    public function viewEmployeeListByFloorLine(Request $request){
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";
      
       
        if (($request->floorId)!=0) {
            $query.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (($request->line_id)!=0) {
            $query.=" AND employees.line_id='".$request->line_id."'";
        }

       
        $query.=" ORDER BY employees.employeeId ASC";

        $employees= DB::select(DB::raw($query));


        if (($request->viewType)=="Preview") {
            return view('report.employee.view_employee_list_by_floor_line',compact('employees','request'));
        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.employee_list_pdf',compact('employees','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_employee_list.pdf";
            return $pdf->download($name);
        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }


    public function employeeListBySection(){
       $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
       return view('report.employee.employee_list_by_section',compact('sections'));
    }
    
    public function viewEmployeeListBySection(Request $request){
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";
       
        if (!empty($request->sectionName)) {
            $query.=" AND employees.empSection='".$request->sectionName."'";
        }

        $query.=" ORDER BY employees.employeeId ASC";

        $employees= DB::select(DB::raw($query));


        if (($request->viewType)=="Preview") {
            return view('report.employee.view_employee_list_by_section',compact('employees','request'));
        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.employee_list_pdf',compact('employees','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_employee_list.pdf";
            return $pdf->download($name);
        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    //Start Payroll Reports Area 
     public function payrollReportDashboard(){
        return view('report.payroll.index');
    }
    //End Payroll Reports Area 


    //Start Training Reports Area 
     public function trainingReportDashboard(){
        return view('report.training.index');
    }
    //End Training Reports Area 



    //Start Recruitment Reports Area 
     public function recruitmentReportDashboard(){
        return view('report.recruitment.index');
    }
    //End Recruitment Reports Area 


    // Start Attendance report area
    public function attendanceReportDashboard(){
        return view('report.attendance.index');
    }

    public function report_daily_attendance(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.attendance.report_daily_attendance',compact('units','departments','floors','sections','designations'));

    }

    public function view_report_daily_attendance(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->report_daily_attendance();
        }

        $date=Carbon::parse($request->date)->toDateString();
        $ld=Carbon::parse($request->date)->toDateString();
        $lateTime=AttendanceController::late_time();

        $query=@"SELECT employees.*,designations.designation,departments.departmentName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

        $queryLeave=@"SELECT employees.*,designations.designation,departments.departmentName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

        $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
            $queryLeave.=" AND employees.unit_id='".$request->unitId."'";
            $queryPresent.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->sectionName)) {
            $query.=" AND employees.empSection='".$request->sectionName."'";
            $queryLeave.=" AND employees.empSection='".$request->sectionName."'";
            $queryPresent.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
            $queryLeave.=" AND employees.empDepartmentId='".$request->departmentId."'";
            $queryPresent.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
            $queryLeave.=" AND employees.empDesignationId='".$request->designationId."'";
            $queryPresent.=" AND employees.empDesignationId='".$request->designationId."'";
        }
        if (($request->empGenderId)!=0) {
            $query.=" AND employees.empGenderId='".$request->empGenderId."'";
            $queryLeave.=" AND employees.empGenderId='".$request->empGenderId."'";
            $queryPresent.=" AND employees.empGenderId='".$request->empGenderId."'";
        }

        $query.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryLeave.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $query.=" AND employees.id not in 
                          (SELECT * FROM (SELECT emp_id FROM attendance GROUP BY date,emp_id HAVING date='$date') AS subquery)";
        $query.=" AND employees.id not in 
                          (SELECT * FROM (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1') AS subquery)";
//        return $query;
        $queryPresent.=" AND attendance.date='$date'";
        $queryLeave.=" AND employees.id in
                            (SELECT * FROM (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1') AS subquery)";

//        return $queryLeave;

        $absent=DB::select($query);
        $present=DB::select($queryPresent);
        $leave=DB::select($queryLeave);
        $countAbsent=count($absent);
        $countPresent=count($present);
        $totalL=count($leave);

        foreach ($leave as $l){
            $l->leave='Leave';
        }

        $allData=array_merge($present, $absent);
        $allData=array_merge($allData, $leave);



        usort($allData, function($a, $b) {
            return $a->empFirstName <=> $b->empFirstName;
        });
        if (($request->viewType)=="Preview") {
            return view('report.attendance.view_report_daily_attendance',compact('allData','request','lateTime','countPresent','countAbsent','totalL'));

        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.daily_attendance_pdf',compact('allData','request','lateTime','countAbsent','countPresent','totalL'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_admin_daily_attendance.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Daily_Attendance_Report_Admin";
            Excel::create("$excelName", function($excel) use ($countAbsent, $lateTime, $countPresent, $totalL, $request, $allData) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Attendance Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Attendance Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($countAbsent, $totalL, $countPresent, $lateTime, $request, $allData) {
                    $sheet->loadView('report.excel.daily_attendance_excel')
                        ->with('allData',$allData)
                        ->with('request',$request)
                        ->with('lateTime',$lateTime)
                        ->with('countPresent',$countPresent)
                        ->with('totalL',$totalL)
                        ->with('countAbsent',$countAbsent);
                });

            })->download($request->extensions);
        }



        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }


    public function public_report_daily_attendance(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.attendance.public_report_daily_attendance',compact('units','departments','floors','sections','designations'));

    }
    public function public_report_daily_attendance_index(Request $request)
    {
        if (($request->viewType) == "Preview") {
            session($request->all());
            return view('report.attendance.public_report_daily_attendance_index', compact('request'));
        } else {
            $helper = AppHelper::instance();
            if (!$helper->checkIsAValidDate($request->date)) {
                Session::flash('message', 'Incorrect date format.');
                return $this->public_report_daily_attendance();
            }
            $date = Carbon::parse($request->date)->toDateString();
            $ld = Carbon::parse($request->date)->toDateString();
            $lateTime = AttendanceController::late_time();

//        return $lateTime;
            $query = @"SELECT employees.*,designations.designation,departments.departmentName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";
            $queryLeave = @"SELECT employees.*,designations.designation,departments.departmentName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";
            $queryPresent = @"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $query .= " AND employees.unit_id='" . $request->unitId . "'";
                $queryLeave .= " AND employees.unit_id='" . $request->unitId . "'";
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (!empty($request->sectionName)) {
                $query .= " AND employees.empSection='" . $request->sectionName . "'";
                $queryLeave .= " AND employees.empSection='" . $request->sectionName . "'";
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $query .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
                $queryLeave .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $query .= " AND employees.empDesignationId='" . $request->designationId . "'";
                $queryLeave .= " AND employees.empDesignationId='" . $request->designationId . "'";
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $query .= " AND employees.empGenderId='" . $request->empGenderId . "'";
                $queryLeave .= " AND employees.empGenderId='" . $request->empGenderId . "'";
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }


            $query .= " AND employees.empAccStatus='" . $request->accStatus . "'";
            $queryLeave .= " AND employees.empAccStatus='" . $request->accStatus . "'";
            $queryPresent .= " AND employees.empAccStatus='" . $request->accStatus . "'";
            $query .= " AND employees.id not in 
                          (SELECT * FROM (SELECT emp_id FROM attendance GROUP BY date, emp_id HAVING date='$date') AS subquery)";
            $query .= " AND employees.id not in 
                          (SELECT * FROM (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1') AS subquery)";
            $queryPresent .= " AND attendance.date='$date'";
            $queryLeave .= " AND employees.id in
                            (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1')";
            $absent = DB::select($query);
            $present = DB::select($queryPresent);
//        return $queryLeave;
            $leave = DB::select($queryLeave);

            $countAbsent = count($absent);
            $countPresent = count($present);
            $totalL = count($leave);
//        return $totalL;

            foreach ($leave as $l) {
                $l->Status = 'Leave';
            }

            $allData = array_merge($present, $absent);
            $allData = array_merge($allData, $leave);

            $excelData = [];

            usort($allData, function ($a, $b) {
                return $a->empFirstName <=> $b->empFirstName;
            });

            $excelName = time() . "_Daily_Attendance_Report";

            if (($request->viewType) == "Generate PDF") {
                return view('print.daily_attendance_report_public', compact('allData', 'request', 'lateTime', 'countAbsent', 'countPresent', 'totalL'));
            } elseif (($request->viewType) == "Generate Excel") {
                Excel::create("$excelName", function ($excel) use ($countAbsent, $lateTime, $countPresent, $totalL, $request, $allData, $excelData) {

                    // Set the title
                    $name = Auth::user()->name;
                    $excel->setTitle("Daily Attendance Report");


                    // Chain the setters
                    $excel->setCreator($name);

                    $excel->setDescription('Daily Attendance Report');

                    $excel->sheet('Sheet 1', function ($sheet) use ($countAbsent, $totalL, $countPresent, $lateTime, $request, $allData) {
                        $sheet->loadView('report.excel.public_daily_attendance_excel')
                            ->with('allData', $allData)
                            ->with('request', $request)
                            ->with('lateTime', $lateTime)
                            ->with('countPresent', $countPresent)
                            ->with('totalL', $totalL)
                            ->with('countAbsent', $countAbsent);
                    });

                })->download($request->extensions);
            }
//        return $request->all();
        }
    }
    public function public_report_daily_attendance_data(){
//        dd(session()->all());
        $request= new Request();
        $request->date=session()->pull('date');
        $request->unitId=session()->pull('unitId');
        $request->sectionName=session()->pull('sectionName');
        $request->departmentId=session()->pull('departmentId');
        $request->designationId=session()->pull('designationId');
        $request->empGenderId=session()->pull('empGenderId');
        $request->accStatus=session()->pull('accStatus');
        $request->viewType=session()->pull('viewType');
        $request->extensions=session()->pull('extensions');
        $request->coldepartment=session()->pull('coldepartment');
        $request->coldesignation=session()->pull('coldesignation');
        $request->collateTime=session()->pull('collateTime');
        $helper=AppHelper::instance();

        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->public_report_daily_attendance();
        }
        $date=Carbon::parse($request->date)->toDateString();
        $ld=Carbon::parse($request->date)->toDateString();
        $lateTime=AttendanceController::late_time();

//        return $lateTime;
        $query=@"SELECT employees.*,designations.designation,departments.departmentName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";
        $queryLeave=@"SELECT employees.*,designations.designation,departments.departmentName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";
        $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

//        return $query;
        if (($request->unitId)!=0) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
            $queryLeave.=" AND employees.unit_id='".$request->unitId."'";
            $queryPresent.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->sectionName)) {
            $query.=" AND employees.empSection='".$request->sectionName."'";
            $queryLeave.=" AND employees.empSection='".$request->sectionName."'";
            $queryPresent.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
            $queryLeave.=" AND employees.empDepartmentId='".$request->departmentId."'";
            $queryPresent.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
            $queryLeave.=" AND employees.empDesignationId='".$request->designationId."'";
            $queryPresent.=" AND employees.empDesignationId='".$request->designationId."'";
        }
        if (($request->empGenderId)!=0) {
            $query.=" AND employees.empGenderId='".$request->empGenderId."'";
            $queryLeave.=" AND employees.empGenderId='".$request->empGenderId."'";
            $queryPresent.=" AND employees.empGenderId='".$request->empGenderId."'";
        }


        $query.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryLeave.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $query.=" AND employees.id not in 
                          (SELECT * FROM (SELECT emp_id FROM attendance GROUP BY date, emp_id HAVING date='$date') AS subquery)";
        $query.=" AND employees.id not in 
                          (SELECT * FROM (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1') AS subquery)";
        $queryPresent.=" AND attendance.date='$date'";
        $queryLeave.=" AND employees.id in
                            (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1')";
//        return $query;
        $absent=DB::select($query);
        $present=DB::select($queryPresent);
//        return $queryLeave;
        $leave=DB::select($queryLeave);

        foreach ($leave as $l){
            $l->Status='Leave';
        }

        $allData=array_merge($present, $absent);
        $allData=array_merge($allData, $leave);

        $excelData=[];

        usort($allData, function($a, $b) {
            return $a->empFirstName <=> $b->empFirstName;
        });
//        $out_time=AttendanceController::out_time();

//        return $allData;

        $excelName=time()."_Daily_Attendance_Report";
        if (($request->viewType)=="Preview") {
            return datatables($allData)
                ->addColumn('name', function ($allData) {
                    return $allData->empFirstName . " " . $allData->empLastName;

                })
                ->addColumn('in_time', function ($allData) {
                    if (isset($allData->in_time)) {
                        return $allData->in_time;
                    } else {
                        return "";
                    }

                })
                ->addColumn('out_time', function ($allData) {
                    if (isset($allData->out_time)) {
                        if ($allData->in_time==$allData->out_time){
                            return "Not Given";
                        }
                        return $allData->out_time;
                    }


                    return "";
                })
                ->addColumn('late_time', function ($allData) {
                    $lateTime = AttendanceController::late_time();
                    if (isset($allData->in_time)) {
                        if ($allData->in_time > $lateTime) {
                            return gmdate('H:i:s', (strtotime($allData->in_time) - strtotime($lateTime)));
                        }
                    }
                    return "";

                })
                ->addColumn('action', function ($allData) {
                    if (isset($allData->in_time)) {
                        $lateTime = AttendanceController::late_time();
                        if ($allData->in_time > $lateTime) {
                            return "<span class='late-text'>Late</span>";

                        } else {
                            return "<span class='green-text'>Present</span>";
                        }
                    } else {
                        if (isset($allData->Status)) {
                            return "<span class='late-text'>On Leave</span>";
                        } else {
                            return "<span class='red-text'>Absent</span>";
                        }

                    }


                })
                ->make('true');
        }
//        return Datatables::of($allData)->make(true);

    }

    public function public_view_report_daily_attendance(Request $request){
//        return "kk";
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->public_report_daily_attendance();
        }
        $date=Carbon::parse($request->date)->toDateString();
        $ld=Carbon::parse($request->date)->toDateString();
        $lateTime=AttendanceController::late_time();

//        return $lateTime;
        $query=@"SELECT employees.*,designations.designation,departments.departmentName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";
        $queryLeave=@"SELECT employees.*,designations.designation,departments.departmentName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";
        $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
            $queryLeave.=" AND employees.unit_id='".$request->unitId."'";
            $queryPresent.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->sectionName)) {
            $query.=" AND employees.empSection='".$request->sectionName."'";
            $queryLeave.=" AND employees.empSection='".$request->sectionName."'";
            $queryPresent.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
            $queryLeave.=" AND employees.empDepartmentId='".$request->departmentId."'";
            $queryPresent.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
            $queryLeave.=" AND employees.empDesignationId='".$request->designationId."'";
            $queryPresent.=" AND employees.empDesignationId='".$request->designationId."'";
        }
        if (($request->empGenderId)!=0) {
            $query.=" AND employees.empGenderId='".$request->empGenderId."'";
            $queryLeave.=" AND employees.empGenderId='".$request->empGenderId."'";
            $queryPresent.=" AND employees.empGenderId='".$request->empGenderId."'";
        }


        $query.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryLeave.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $query.=" AND employees.id not in 
                          (SELECT * FROM (SELECT emp_id FROM attendance GROUP BY date, emp_id HAVING date='$date') AS subquery)";
        $query.=" AND employees.id not in 
                          (SELECT * FROM (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1') AS subquery)";
        $queryPresent.=" AND attendance.date='$date'";
        $queryLeave.=" AND employees.id in
                            (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1')";
        $absent=DB::select($query);
        $present=DB::select($queryPresent);
//        return $queryLeave;
        $leave=DB::select($queryLeave);

        $countAbsent=count($absent);
        $countPresent=count($present);
        $totalL=count($leave);
//        return $totalL;

        foreach ($leave as $l){
            $l->Status='Leave';
        }

        $allData=array_merge($present, $absent);
        $allData=array_merge($allData, $leave);

        $excelData=[];

        usort($allData, function($a, $b) {
            return $a->empFirstName <=> $b->empFirstName;
        });

//        return $allData;

        $excelName=time()."_Daily_Attendance_Report";


        if (($request->viewType)=="Preview") {
            return view('report.attendance.public_view_report_daily_attendance',compact('allData','request','lateTime','countPresent','countAbsent','totalL'));

        }elseif(($request->viewType)=="Generate PDF"){
            return view('print.daily_attendance_report_public',compact('allData','request','lateTime','countAbsent','countPresent','totalL'));

//            $pagesize=$request->pagesize;
//            $pageOrientation=$request->pageOrientation;
//            $pdf=PDF::loadView('report.pdf.public_daily_attendance_pdf',compact('allData','request','lateTime','countAbsent','countPresent','totalL'));
//            $pdf->setPaper($pagesize, $pageOrientation);
//            $name=time()."_daily_attendance.pdf";
//            return $pdf->download($name);
        }
        elseif(($request->viewType)=="Generate Excel"){
            Excel::create("$excelName", function($excel) use ($countAbsent, $lateTime, $countPresent, $totalL, $request, $allData,$excelData) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Attendance Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Attendance Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($countAbsent, $totalL, $countPresent, $lateTime, $request, $allData) {
                    $sheet->loadView('report.excel.public_daily_attendance_excel')
                            ->with('allData',$allData)
                            ->with('request',$request)
                            ->with('lateTime',$lateTime)
                            ->with('countPresent',$countPresent)
                            ->with('totalL',$totalL)
                            ->with('countAbsent',$countAbsent);
                });

            })->download($request->extensions);
        }
        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }

    }



    // End Attendance report area

    //Report attendance view method
    public function attendance_report_view(){
        $employees=DB::table('employees')
            ->select(DB::raw("concat(empFirstName,' ', empLastName, ' (',employeeId,')' ) as name,id"))
            ->pluck('name','id')->all();
        $departments=DB::table('departments')->pluck('departmentName','id');
        return view('report.attendance.date_wise_report',compact('employees','departments'));
    }

    //Attendance Report show method
    public function attendance_report_show(Request $request)
    {
        $start = $request->start_date;
        $end = $request->end_date;
        $sdate = date_create($start);
        $edate = date_create($end);
        $start_date = date_format($sdate, "Y-m-d");
        $end_date = date_format($edate, "Y-m-d");
        if ($request->checkStatus == 'ae') {
            $data = DB::table('attendance')
                ->leftJoin('employees', function ($query) {
                    $query->on('attendance.emp_id', '=', 'employees.id');
                })
                ->join('designations', function ($query) {
                    $query->on('designations.id', '=', 'employees.empDesignationId');
                })
                ->select('attendance.*', 'employees.empFirstName', 'employees.id', 'employees.employeeId', 'employees.empLastName', 'designations.designation')
                ->selectRaw('attendance.emp_id')->whereBetween('attendance.date', [$start_date, $end_date])
                ->orderBy('employees.empFirstName', 'ASC')
                ->get();
            return response()->json($data);

        }

        else if($request->checkStatus=='bd'){
            $data = DB::table('attendance')
                ->leftJoin('employees', function ($query) {
                    $query->on('attendance.emp_id', '=', 'employees.id');
                })
                ->join('designations', function ($query) {
                    $query->on('designations.id', '=', 'employees.empDesignationId');
                })
                ->select('attendance.*', 'employees.empFirstName','employees.empDepartmentId', 'employees.id', 'employees.employeeId', 'employees.empLastName', 'designations.designation')
                ->selectRaw('attendance.emp_id')->whereBetween('attendance.date', [$start_date, $end_date])
                ->where('employees.empDepartmentId', $request->dept_id)
                ->orderBy('employees.empFirstName', 'ASC')
                ->get();
            return response()->json($data);

        }
    }

    public function attendanceCardReport(Request $request){
        $start = $request->start_date;
        $end = $request->end_date;
        $sdate = date_create($start);
        $edate = date_create($end);
        $start_date = date_format($sdate, "Y-m-d");
        $end_date = date_format($edate, "Y-m-d");
        $weekends = [];
        $weekend = DB::table('week_leave')->where(['status' => 1])->get();
        $festivalLeave = DB::table('tb_festival_leave')->get();
        $approvedLeave=DB::table('tb_leave_application')->where(['employee_id'=>$request->emp_id, 'status'=>1])->get();
        foreach ($weekend as $w) {
            $weekends[] = $w->day;
        }


        $dates = [];
        $datas = [];
        $abs=0;

        for ($i = Carbon::createFromFormat('Y-m-d', $start_date); $i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
            $key=0;
            $fes=0;
            $lev=0;
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);
            $day = date('l', $timestamp);
            foreach ($festivalLeave as $fe){
                $fesDayStart=strtotime($fe->start_date);
                $fesDayEnd=strtotime($fe->end_date);
                if($timestamp >=$fesDayStart && $timestamp<=$fesDayEnd){
                    $fes=1;
                    break;
                }
            }
            foreach ($approvedLeave as $al){
                if($timestamp >=strtotime($al->leave_starting_date) && $timestamp<=strtotime($al->leave_ending_date))
                {
                    $lev=1;
                    break;
                }
            }
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $key=1;
                    break;
                }
            }
            if($key!=1) {

                if ($fes != 1) {

                    if ($lev != 1) {
                        $data = DB::select("select attendance.*, employees.id as eid, employees.empFirstName,
                        employees.empLastName, employees.empJoiningDate, employees.employeeId,designations.designation from attendance,
                        employees, designations where attendance.emp_id=employees.id and
                        employees.empDesignationId=designations.id HAVING employees.id =
                        " . $request->emp_id . " and date='$dates'");
                        if (count($data)) {
                            $datas[] = array_merge($data, [$dates, $abs]);
                        } else {
                            $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName,
                                employees.employeeId, designations.designation FROM employees, designations
                                where employees.empDesignationId=designations.id HAVING employees.id =" . $request->emp_id);
                            $datas[] = array_merge($data, [$dates, ++$abs]);

                        }
                    } else {
                        $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName,
                                employees.employeeId, designations.designation FROM employees, designations
                                where employees.empDesignationId=designations.id HAVING employees.id in (" . $request->emp_id . ")");
                        $datas[] = array_merge($data, [$dates, "On Leave"]);


                    }
                } else {
                    $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName,
                                employees.employeeId, designations.designation FROM employees, designations
                                where employees.empDesignationId=designations.id HAVING employees.id in (" . $request->emp_id . ")");
                    $datas[] = array_merge($data, [$dates, "Festival Holiday"]);

                }
            }
            else{

                $data = DB::select("SELECT employees.id,employees.empFirstName,employees.empJoiningDate, employees.empLastName,
                                employees.employeeId, designations.designation FROM employees, designations
                                where employees.empDesignationId=designations.id HAVING employees.id in (" . $request->emp_id . ")");
                $datas[] = array_merge($data, [$dates, "weekend"]);


            }
        }
        return $datas;

    }
  

    public function pdfAttendanceCard( Request $request){
//        return $request->emp_id;
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->start_date)){
            Session::flash('message','Incorrect date format.');
            return redirect('report/attendance/card');
        }
        elseif(!$helper->checkIsAValidDate($request->end_date)){
            Session::flash('message','Incorrect date format.');
            return redirect('report/attendance/card');
        }
//        return $request->all();
        $start = $request->start_date;
        $end = $request->end_date;
        $sdate = date_create($start);
        $edate = date_create($end);
        $start_date = date_format($sdate, "Y-m-d");
        $end_date = date_format($edate, "Y-m-d");

        if($start_date>$end_date){
            Session::flash('message','Start date must be less then end date.');
            return redirect('report/public/attendance/card');
        }

        $weekends = [];
        $weekend = DB::table('week_leave')->where(['status' => 1])->get();
        $festivalLeave = DB::table('tb_festival_leave')->get();
        $approvedLeave=DB::table('tb_leave_application')->where(['employee_id'=>$request->emp_id, 'status'=>1])->get();
//        return $approvedLeave;
        foreach ($weekend as $w) {
            $weekends[] = $w->day;
        }


        $dates = [];
        $datas = [];
        $abs=0;

        for ($i = Carbon::createFromFormat('Y-m-d', $start_date); $i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
            $key=0;
            $fes=0;
            $lev=0;
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);

            $day = date('l', $timestamp);
            foreach ($festivalLeave as $fe){
                $fesDayStart=strtotime($fe->start_date);
                $fesDayEnd=strtotime($fe->end_date);
                if($timestamp >=$fesDayStart && $timestamp<=$fesDayEnd){
                    $fes=1;
                    break;
                }

            }
            foreach ($approvedLeave as $al){
                if($timestamp >=strtotime($al->leave_starting_date) && $timestamp<=strtotime($al->leave_ending_date))
                {
                // return $al->leave_type_id;
                    $lev=1;
                    break;
                }
            }
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $key=1;
                    break;
                }
            }

            if($key!=1) {

                if ($fes != 1) {

                    if ($lev != 1) {
                        $data = DB::select("SELECT attendance.*, employees.id as eid, employees.empFirstName, employees.empLastName,employees.empSection, employees.empJoiningDate, employees.employeeId,designations.designation, departments.departmentName  FROM attendance,
                        employees, designations, departments WHERE attendance.emp_id=employees.id and
                        employees.empDesignationId=designations.id AND employees.empDepartmentId=departments.id  HAVING employees.id =
                        " . $request->emp_id . " and date='$dates'");
                        if (count($data)) {
                            $datas[] = array_merge($data, [$dates, $abs]);
                        } else {
                            $sql="SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName, employees.empSection,
                                employees.employeeId, designations.designation, departments.departmentName FROM employees, designations, departments
                                where employees.empDesignationId=designations.id AND employees.empDepartmentId=departments.id HAVING employees.id =$request->emp_id";
//                            return $sql;
                            $data = DB::select($sql);

                            $datas[] = array_merge($data, [$dates, ++$abs]);

                        }
                    } else {
                        $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName,employees.empSection,
                                employees.employeeId, designations.designation, departments.departmentName  FROM employees, designations, departments
                                WHERE employees.empDesignationId=designations.id AND employees.empDepartmentId=departments.id  HAVING employees.id in (" . $request->emp_id . ")");
                        $datas[] = array_merge($data, [$dates, "On Leave"]);


                    }
                } else {
                    $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName,employees.empSection,
                                employees.employeeId, designations.designation, departments.departmentName  FROM employees, designations, departments
                                where employees.empDesignationId=designations.id  AND employees.empDepartmentId=departments.id HAVING employees.id in (" . $request->emp_id . ")");
                    $datas[] = array_merge($data, [$dates, "Festival Holiday"]);

                }
            }
            else{

                $data = DB::select("SELECT employees.id,employees.empFirstName,employees.empJoiningDate, employees.empLastName,employees.empSection,
                                employees.employeeId, designations.designation, departments.departmentName FROM employees, designations, departments
                                where employees.empDesignationId=designations.id AND employees.empDepartmentId=departments.id HAVING employees.id in (" . $request->emp_id . ")");
                $datas[] = array_merge($data, [$dates, "weekend"]);


            }
        }

//        return $datas;

        if ($request->viewType == "Download Excel") {
//            return $request->all();

//            return view('report.excel.attendance_card_public',compact('datas','start_date','end_date'));
//            return "AA";
            $excelName=time()."_job_card";
            Excel::create("$excelName", function($excel) use ($datas, $start_date, $end_date) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("job card");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Employee Job Card');

                $excel->sheet('Sheet 1', function ($sheet) use ($datas, $end_date, $start_date) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.attendance_card')
                        ->with('datas',$datas)
                        ->with('start_date',$start_date)
                        ->with('end_date',$end_date);
                });

            })->download($request->extensions);

        }

        else if ($request->viewType == "Download PDF English") {

            $pdf = PDF::loadView('report.pdf.attendance_card', compact('datas', 'start_date', 'end_date'));
            $pdf->setPaper('A4', 'portrait');
            $name = time() . "_attendance_card_english.pdf";
            return $pdf->download($name);
        }
        else if ($request->viewType == "Download PDF Bangla") {

            $pdf = mPDF::loadView('report.pdf.attendance_card_bangla', compact('datas', 'start_date', 'end_date'));
//            $pdf->setPaper('A4', 'portrait');
            $name = time() . "_attendance_card_Bengali.pdf";
            return $pdf->download($name);
        }


    }

    public function pdfPublicAttendanceCard(Request $request)
    {
//        return $request->all();

        $helper = AppHelper::instance();
        if (!$helper->checkIsAValidDate($request->start_date)) {
            Session::flash('message', 'Incorrect date format.');
            return redirect('report/public/attendance/card');
        } elseif (!$helper->checkIsAValidDate($request->end_date)) {
            Session::flash('message', 'Incorrect date format.');
            return redirect('report/public/attendance/card');
        }
        $start = $request->start_date;
        $end = $request->end_date;
        $sdate = date_create($start);
        $edate = date_create($end);
        $start_date = date_format($sdate, "Y-m-d");
        $end_date = date_format($edate, "Y-m-d");
        if ($start_date > $end_date) {
            Session::flash('message', 'Start date must be less then end date.');
            return redirect('report/public/attendance/card');
        }
        $weekends = [];
        $weekend = DB::table('week_leave')->where(['status' => 1])->get();
        $festivalLeave = DB::table('tb_festival_leave')->get();
        $approvedLeave = DB::table('tb_leave_application')->where(['employee_id' => $request->emp_id, 'status' => 1])->get();
//        return $approvedLeave;
        foreach ($weekend as $w) {
            $weekends[] = $w->day;
        }


        $dates = [];
        $datas = [];
        $abs = 0;

        for ($i = Carbon::createFromFormat('Y-m-d', $start_date); $i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
            $key = 0;
            $fes = 0;
            $lev = 0;
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);

            $day = date('l', $timestamp);
            foreach ($festivalLeave as $fe) {
                $fesDayStart = strtotime($fe->start_date);
                $fesDayEnd = strtotime($fe->end_date);
                if ($timestamp >= $fesDayStart && $timestamp <= $fesDayEnd) {
                    $fes = 1;
                    break;
                }

            }
            foreach ($approvedLeave as $al) {
                if ($timestamp >= strtotime($al->leave_starting_date) && $timestamp <= strtotime($al->leave_ending_date)) {
                    // return $al->leave_type_id;
                    $lev = 1;
                    break;
                }
            }
            for ($j = 0; $j < count($weekends); $j++) {
                if ($day == $weekends[$j]) {
                    $key = 1;
                    break;
                }
            }

            if ($key != 1) {

                if ($fes != 1) {

                    if ($lev != 1) {
                        $data = DB::select("SELECT attendance.*, employees.id as eid, employees.empFirstName, employees.empLastName,employees.empSection, employees.empJoiningDate, employees.employeeId,designations.designation, departments.departmentName  FROM attendance,
                        employees, designations, departments WHERE attendance.emp_id=employees.id and
                        employees.empDesignationId=designations.id AND employees.empDepartmentId=departments.id  HAVING employees.id =
                        " . $request->emp_id . " and date='$dates'");
                        if (count($data)) {
                            $datas[] = array_merge($data, [$dates, $abs]);
                        } else {
                            $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName, employees.empSection,
                                employees.employeeId, designations.designation, departments.departmentName FROM employees, designations, departments
                                where employees.empDesignationId=designations.id AND employees.empDepartmentId=departments.id HAVING employees.id =" . $request->emp_id);
                            $datas[] = array_merge($data, [$dates, ++$abs]);

                        }
                    } else {
                        $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName,employees.empSection,
                                employees.employeeId, designations.designation, departments.departmentName  FROM employees, designations, departments
                                WHERE employees.empDesignationId=designations.id AND employees.empDepartmentId=departments.id  HAVING employees.id in (" . $request->emp_id . ")");
                        $datas[] = array_merge($data, [$dates, "On Leave"]);


                    }
                } else {
                    $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName,employees.empSection,
                                employees.employeeId, designations.designation, departments.departmentName  FROM employees, designations, departments
                                where employees.empDesignationId=designations.id  AND employees.empDepartmentId=departments.id HAVING employees.id in (" . $request->emp_id . ")");
                    $datas[] = array_merge($data, [$dates, "Festival Holiday"]);

                }
            } else {

                $data = DB::select("SELECT employees.id,employees.empFirstName,employees.empJoiningDate, employees.empLastName,employees.empSection,
                                employees.employeeId, designations.designation, departments.departmentName FROM employees, designations, departments
                                where employees.empDesignationId=designations.id AND employees.empDepartmentId=departments.id HAVING employees.id in (" . $request->emp_id . ")");
                $datas[] = array_merge($data, [$dates, "weekend"]);


            }
        }

        if ($request->viewType == "Download PDF Bangla") {
            $pdf = mPDF::loadView('report.pdf.attendance_card_public_bangla', compact('datas', 'start_date', 'end_date'));
            //$pdf->setPaper('A4', 'portrait');
            $name = time() . "_attendance_card_bangla.pdf";
            return $pdf->download($name);
        }
        if ($request->viewType == "Download PDF English") {
            $pdf = PDF::loadView('report.pdf.attendance_card_public', compact('datas', 'start_date', 'end_date'));
            $pdf->setPaper('A4', 'portrait');
            $name = time() . "_attendance_card_english.pdf";
            return $pdf->download($name);
        }

        else if ($request->viewType == "Download Excel") {
//            return $request->all();

//            return view('report.excel.attendance_card_public',compact('datas','start_date','end_date'));
//            return "AA";
            $excelName=time()."_job_card";
            Excel::create("$excelName", function($excel) use ($datas, $start_date, $end_date) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("job card");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Employee Job Card');

                $excel->sheet('Sheet 1', function ($sheet) use ($datas, $end_date, $start_date) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.attendance_card_public')
                        ->with('datas',$datas)
                        ->with('start_date',$start_date)
                        ->with('end_date',$end_date);
                });

            })->download($request->extensions);

        }
    }

    public function attendanceCard(){
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('report.attendance.attendance_card',compact('employees'));

    }

    public function attendanceCardPublic(){
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('report.attendance.attendance_card_public',compact('employees'));

    }



    //Daily employee attendance method
    public function daily_employee_attendance(Request $request){
        echo "data hs comming";
    }

    public function daily_late_present(){

        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.attendance.daily_late_present',compact('units','departments','sections','designations'));

//        return view('report.attendance.daily_late_present');
    }

    public function daily_late_present_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->daily_late_present();
        }
        $lateTime=AttendanceController::late_time();
        $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $queryPresent.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->sectionName)) {
            $queryPresent.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $queryPresent.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $queryPresent.=" AND employees.empDesignationId='".$request->designationId."'";
        }
        if (($request->empGenderId)!=0) {
            $queryPresent.=" AND employees.empGenderId='".$request->empGenderId."'";
        }
        $date=Carbon::parse($request->date)->toDateString();

        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND attendance.date='$date' AND attendance.in_time>'$lateTime' ORDER BY employees.empFirstName";
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.daily_late_present_data',compact('present','request','lateTime','countPresent'));

        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.daily_late_pdf',compact('present','request','lateTime','countPresent'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_daily_late_attendance.pdf";
//            return view('report.pdf.daily_late_pdf',compact('present','request','lateTime','countPresent'));
            return $pdf->download($name);


        }
        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Daily_Late_Present";
            Excel::create("$excelName", function($excel) use ($present, $lateTime, $countPresent, $request) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Late Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Late Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $countPresent, $lateTime, $request) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.daily_late_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('lateTime',$lateTime)
                        ->with('countPresent',$countPresent);
                });

            })->download($request->extensions);
        }
        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function daily_present_report(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.attendance.daily_present_report',compact('units','departments','floors','sections','designations'));


        return view('report.attendance.daily_present_report');
    }

    public function daily_present_report_data(Request $request)
    {
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->daily_present_report();
        }
        $lateTime=AttendanceController::late_time();
        $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $queryPresent.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->sectionName)) {
            $queryPresent.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $queryPresent.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $queryPresent.=" AND employees.empDesignationId='".$request->designationId."'";
        }
        if (($request->empGenderId)!=0) {
            $queryPresent.=" AND employees.empGenderId='".$request->empGenderId."'";
        }
        $date=Carbon::parse($request->date)->toDateString();

        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND attendance.date='$date' ORDER BY employees.empFirstName";
//        return $queryPresent;
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.daily_present_report_data',compact('present','request','lateTime','countPresent'));

        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.daily_present_pdf',compact('present','request','lateTime','countPresent'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_daily_present_report.pdf";
//            return view('report.pdf.daily_late_pdf',compact('present','request','lateTime','countPresent'));
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Daily_Late_Present";
            Excel::create("$excelName", function($excel) use ($present, $lateTime, $countPresent, $request) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Present Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Present Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $countPresent, $lateTime, $request) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.daily_present_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('lateTime',$lateTime)
                        ->with('countPresent',$countPresent);
                });

            })->download($request->extensions);
        }


        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function daily_absent_report(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.attendance.daily_absent_report',compact('units','departments','sections','designations'));
//        return view('report.attendance.daily_absent_report');
    }
    public function daily_absent_report_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->daily_absent_report();
        }


        $query=@"SELECT employees.*,designations.designation,departments.departmentName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->sectionName)) {
            $query.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }
        if (($request->empGenderId)!=0) {
            $query.=" AND employees.empGenderId='".$request->empGenderId."'";
        }
        $date=Carbon::parse($request->date)->toDateString();

        $query.=" AND employees.empAccStatus='".$request->accStatus."'";
        $query.=" AND employees.id not in 
                          (SELECT emp_id FROM attendance WHERE date='$date' ORDER BY employees.empFirstName)";
        $query.=" AND employees.id not in 
                          (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1')";
        $absent=DB::select($query);
        $countAbsent=count($absent);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.daily_absent_report_data',compact('absent','request','countAbsent'));

        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.daily_absent_pdf',compact('absent','request','countAbsent'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_daily_absent.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Daily_Absent_Present";
            Excel::create("$excelName", function($excel) use ($absent, $countAbsent, $request) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Absent Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Absent Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($absent, $countAbsent, $request) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.daily_absent_excel')
                        ->with('absent',$absent)
                        ->with('request',$request)
                        ->with('countAbsent',$countAbsent);
                });

            })->download($request->extensions);
        }


        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function date_wise_late(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('report.attendance.date_wise_late',compact('employees','units','departments','floors','sections','designations'));
    }

    public function date_wise_late_data(Request $request){

        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_late();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_late();
        }


        $lateTime=AttendanceController::late_time();
        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();
        if($request->emp_id!=0){
            $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id='".$request->emp_id."'";
        }

        else {
            $queryPresent = @"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (($request->floorId) != 0) {
                $queryPresent .= " AND employees.floor_id='" . $request->floorId . "'";
            }

            if (($request->line_id) != 0) {
                $queryPresent .= " AND employees.line_id='" . $request->line_id . "'";
            }

            if (!empty($request->sectionName)) {
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }


            $queryPresent .= " AND employees.empAccStatus='" . $request->accStatus . "'";
        }


        $queryPresent.=" AND attendance.in_time>'$lateTime' AND attendance.date BETWEEN '$date' and '$endDate' ORDER BY employees.empFirstName";

        $present=DB::select($queryPresent);
        $countPresent=count($present);
//        return $present;

        if (($request->viewType)=="Preview") {
            return view('report.attendance.date_wise_late_data',compact('present','request', 'lateTime', 'countPresent'));

        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.date_wise_late_pdf',compact('present','request','lateTime','countPresent'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_date_wise_late.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Date_Wise_Late";
            Excel::create("$excelName", function($excel) use ($present, $lateTime, $request, $countPresent) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Date Wise Late");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Date Wise Late Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $lateTime, $request,$countPresent) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.date_wise_late_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('countPresent',$countPresent)
                        ->with('lateTime',$lateTime);
                });

            })->download($request->extensions);
        }

        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }

    }
    public function date_wise_present(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->get(['empFirstName','empLastName','id']);
        return view('report.attendance.date_wise_present',compact('employees','units','departments','sections','designations'));

    }

    public function date_wise_present_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_present();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_present();
        }
        $lateTime=AttendanceController::late_time();
        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();
        if($request->emp_id!=0){
            $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id='".$request->emp_id."'";
        }
        else {
            $queryPresent = @"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (!empty($request->sectionName)) {
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }


            $queryPresent .= " AND employees.empAccStatus='" . $request->accStatus . "'";
        }

        $queryPresent.=" AND attendance.date BETWEEN '$date' and '$endDate' ORDER BY employees.empFirstName";
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.date_wise_present_data',compact('present','request', 'lateTime', 'countPresent'));

        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.date_wise_present_pdf',compact('present','request','lateTime','countPresent'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_admin_date_wise_present.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."Admin_Date_Wise_Present";
            Excel::create("$excelName", function($excel) use ($present, $lateTime, $request, $countPresent) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Date Wise Present");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Date Wise Present Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $lateTime, $request,$countPresent) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.date_wise_present_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('countPresent',$countPresent)
                        ->with('lateTime',$lateTime);
                });

            })->download($request->extensions);
        }



        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function public_date_wise_present(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('report.attendance.public_date_wise_present',compact('employees','units','departments','floors','sections','designations'));

    }

    public function public_date_wise_present_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->public_date_wise_present();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->public_date_wise_present();
        }


        $lateTime=AttendanceController::late_time();
        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();
        if($request->emp_id!=0){
            $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id='".$request->emp_id."'";
        }
        else {
            $queryPresent = @"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (!empty($request->sectionName)) {
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }


            $queryPresent .= " AND employees.empAccStatus='" . $request->accStatus . "'";
        }

        $queryPresent.=" AND attendance.date BETWEEN '$date' and '$endDate' ORDER BY employees.empFirstName";
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.public_date_wise_present_data',compact('present','request', 'lateTime', 'countPresent'));

        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.public_date_wise_present_pdf',compact('present','request','lateTime','countPresent'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_date_wise_present.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."Date_Wise_Present";
            Excel::create("$excelName", function($excel) use ($present, $lateTime, $request, $countPresent) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Date Wise Present");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Date Wise Present Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $lateTime, $request,$countPresent) {
                    $sheet->loadView('report.excel.public_date_wise_present_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('countPresent',$countPresent)
                        ->with('lateTime',$lateTime);
                });

            })->download($request->extensions);
        }



        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function date_wise_absent(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('report.attendance.date_wise_absent',compact('employees','units','departments','floors','sections','designations'));
    }

    public function date_wise_absent_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_absent();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_absent();
        }
        $weekends = [];
        $weekend = DB::table('week_leave')->where(['status' => 1])->get();
        $festivalLeave = DB::table('tb_festival_leave')->get();
        $approvedLeave=DB::table('tb_leave_application')->where(['employee_id'=>$request->emp_id, 'status'=>1])->get();
//        return $approvedLeave;
        foreach ($weekend as $w) {
            $weekends[] = $w->day;
        }


        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();
        $absentData=[];

        if($request->emp_id!=0){
            $queryPresent=@"SELECT employees.*,designations.designation,departments.departmentName,units.name unitName FROM employees  LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id='".$request->emp_id."'";
        }
        else {
            $queryPresent = @"SELECT employees.*,designations.designation,departments.departmentName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (($request->floorId) != 0) {
                $queryPresent .= " AND employees.floor_id='" . $request->floorId . "'";
            }

            if (($request->line_id) != 0) {
                $queryPresent .= " AND employees.line_id='" . $request->line_id . "'";
            }

            if (!empty($request->sectionName)) {
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }
            $queryPresent .= " AND employees.empAccStatus='" . $request->accStatus . "'";
        }

        $rightQuery=$queryPresent;

        for ($i = Carbon::createFromFormat('Y-m-d', $date); $i->lte(Carbon::createFromFormat('Y-m-d', $endDate)); $i->addDay(1)) {
//            $i=Carbon::parse($i)->toDateString();
            $key=0;
            $fes=0;
            $lev=0;
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);

            $day = date('l', $timestamp);
            foreach ($festivalLeave as $fe){
                $fesDayStart=strtotime($fe->start_date);
                $fesDayEnd=strtotime($fe->end_date);
                if($timestamp >=$fesDayStart && $timestamp<=$fesDayEnd){
                    $fes=1;
                    break;
                }

            }
            foreach ($approvedLeave as $al){
                if($timestamp >=strtotime($al->leave_starting_date) && $timestamp<=strtotime($al->leave_ending_date))
                {
                    $lev=1;
                    break;
                }
            }
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $key=1;
                    break;
                }
            }

            if($fes !=1 && $lev !=1 && $key!=1) {
                $ld=Carbon::parse($i)->toDateString();
                $queryPresent .= " AND employees.id NOT IN (select emp_id from attendance WHERE date ='" . $ld . "')";
                $queryPresent .= " AND employees.id NOT IN 
                          (SELECT employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1')";
                $absent = DB::select($queryPresent);
                foreach ($absent as $a){
                    $a->date=$dates;
                }
                $absentData = array_merge($absent, $absentData);
                $queryPresent = $rightQuery;
            }

        }

        $countAbsent=count($absentData);

        if (($request->viewType)=="Preview") {
//            return
            return view('report.attendance.date_wise_absent_data',compact('absentData','request'));

        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.date_wise_absent_pdf',compact('absentData','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_date_wise_absent.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Date_Wise_Absent";
            Excel::create("$excelName", function($excel) use ($absentData, $request,$countAbsent) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Date Wise Absent");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Date Wise Absent Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($absentData, $request,$countAbsent) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.date_wise_absent_excel')
                        ->with('absentData',$absentData)
                        ->with('request',$request)
                        ->with('countAbsent',$countAbsent);
                });

            })->download($request->extensions);
        }


        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function daily_overtime_report(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->get(['empFirstName','empLastName','id']);
        return view('report.attendance.daily_overtime_report',compact('employees','units','departments','floors','sections','designations'));

    }

    public function daily_overtime_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->daily_overtime_report();
        }


        $lateTime=AttendanceController::late_time();
        $out_time=AttendanceController::out_time();
        $overtime_start=OvertimeController::overtime_start();
        $overtime_end=OvertimeController::overtime_end();
        $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $queryPresent.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $queryPresent.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (($request->line_id)!=0) {
            $queryPresent.=" AND employees.line_id='".$request->line_id."'";
        }

        if (!empty($request->sectionName)) {
            $queryPresent.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $queryPresent.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $queryPresent.=" AND employees.empDesignationId='".$request->designationId."'";
        }
        if (($request->empGenderId)!=0) {
            $queryPresent.=" AND employees.empGenderId='".$request->empGenderId."'";
        }
        $date=Carbon::parse($request->date)->toDateString();

        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND attendance.date='$date' AND attendance.out_time>'$overtime_start' ORDER BY employees.empFirstName";
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.daily_overtime_data',compact('present','request','lateTime','countPresent','out_time','overtime_end'));

        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.daily_overtime_pdf',compact('present','request','lateTime','countPresent','out_time','overtime_end'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_daily_overtime_report.pdf";
            return $pdf->download($name);
        }
        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Daily_Overtime_Report";
            Excel::create("$excelName", function($excel) use ($present, $request,$lateTime,$countPresent,$out_time,$overtime_end) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Overtime Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Overtime Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $lateTime, $request,$countPresent,$out_time,$overtime_end) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.daily_overtime_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('lateTime',$lateTime)
                        ->with('countPresent',$countPresent)
                        ->with('out_time',$out_time)
                        ->with('overtime_end',$overtime_end);
                });

            })->download($request->extensions);
        }

        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function date_wise_overtime(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('report.attendance.date_wise_overtime',compact('employees','units','departments','floors','sections','designations'));

    }
    public function date_wise_overtime_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_overtime();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_overtime();
        }


        $lateTime=AttendanceController::late_time();
        $out_time=AttendanceController::out_time();
        $overtime_start=OvertimeController::overtime_start();
        $overtime_end=OvertimeController::overtime_end();
        if($request->emp_id!=0){
            $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id='".$request->emp_id."'";
        }
        else {
            $queryPresent = @"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (!empty($request->sectionName)) {
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }
        }
        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();

        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND attendance.date BETWEEN '$date' and '$endDate' AND attendance.out_time>'$overtime_start' ORDER BY employees.empFirstName";
//        return $queryPresent;
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.date_wise_overtime_data',compact('present','request','lateTime','countPresent','out_time','overtime_end'));

        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.date_wise_overtime_pdf',compact('present','request','lateTime','countPresent','out_time','overtime_end'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_date_wise_overtime.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_date_wise_overtime";
            Excel::create("$excelName", function($excel) use ($present, $request,$countPresent,$lateTime, $out_time, $overtime_end) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Date Wise Overtime");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Date Wise Overtime Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $lateTime, $request,$countPresent, $out_time, $overtime_end) {
                    $sheet->loadView('report.excel.date_wise_overtime_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('lateTime',$lateTime)
                        ->with('out_time',$out_time)
                        ->with('overtime_end',$overtime_end)
                        ->with('countPresent',$countPresent);
                });

            })->download($request->extensions);
        }


        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }

    }

    public function admin_date_wise_overtime(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->get(['empFirstName','empLastName','id']);
        return view('report.attendance.admin_date_wise_overtime',compact('employees','units','departments','floors','sections','designations'));

    }

    public function admin_date_wise_overtime_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->admin_date_wise_overtime();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->admin_date_wise_overtime();
        }

        $lateTime=AttendanceController::late_time();
        $out_time=AttendanceController::out_time();
        $overtime_start=OvertimeController::overtime_start();
        $overtime_end=OvertimeController::overtime_end();
        if($request->emp_id!=0){
            $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,
designations.designation,departments.departmentName, units.name unitName 
FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations 
ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id 
LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id='".$request->emp_id."'";
        }
        else {
            $queryPresent = @"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (!empty($request->sectionName)) {
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }
        }
        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();

        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND attendance.date BETWEEN '$date' and '$endDate' AND attendance.out_time>'$overtime_start' ORDER BY employees.empFirstName";
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.admin_date_wise_overtime_data',compact('present','request','lateTime','countPresent','out_time','overtime_end'));

        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.admin_date_wise_overtime_pdf',compact('present','request','lateTime','countPresent','out_time','overtime_end'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_admin_date_wise_overtime.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_date_wise_overtime_admin";
            Excel::create("$excelName", function($excel) use ($present, $request,$countPresent,$lateTime, $out_time, $overtime_end) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Date Wise Overtime Private");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Date Wise Overtime Private');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $lateTime, $request,$countPresent, $out_time, $overtime_end) {
                    $sheet->loadView('report.excel.admin_date_wise_overtime_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('lateTime',$lateTime)
                        ->with('out_time',$out_time)
                        ->with('overtime_end',$overtime_end)
                        ->with('countPresent',$countPresent);
                });

            })->download($request->extensions);
        }


        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function attendance_summary(){
        return view('report.attendance.attendance_summary');
    }

    public function attendance_summary_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->attendance_summary();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->attendance_summary();
        }
        $lateTime=AttendanceController::late_time();
        $weekends = [];
        $weekend = DB::table('week_leave')->where(['status' => 1])->get();
        $festivalLeave = DB::table('tb_festival_leave')->get();
        foreach ($weekend as $w) {
            $weekends[] = $w->day;
        }

        $totalAttendance=[];
        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();
        for ($i = Carbon::createFromFormat('Y-m-d', $date); $i->lte(Carbon::createFromFormat('Y-m-d', $endDate)); $i->addDay(1)) {
            $key=0;
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);

            $day = date('l', $timestamp);
            foreach ($festivalLeave as $fe){
                $fesDayStart=strtotime($fe->start_date);
                $fesDayEnd=strtotime($fe->end_date);
                if($timestamp >=$fesDayStart && $timestamp<=$fesDayEnd){
                    $key=1;
                    break;
                }

            }
//            foreach ($approvedLeave as $al){
//                if($timestamp >=strtotime($al->leave_starting_date) && $timestamp<=strtotime($al->leave_ending_date))
//                {
//                    $key=2;
//                    break;
//                }
//            }
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $key=2;
                    break;
                }
            }

            $date=Carbon::parse($i)->toDateString();
            $d=Carbon::parse($i)->toDateString();
//            return $d;
            $attendance=DB::select("select COUNT(*) as attendance FROM attendance WHERE date='$date'");
            $late=DB::select("select COUNT(*) as late FROM attendance WHERE date='$date' and in_time>'$lateTime'");
            $query="SELECT COUNT(*) AS leaves FROM `tb_leave_application` WHERE '$d' BETWEEN leave_starting_date and leave_ending_date AND status=1";
            $leaves=DB::select($query);
//            return $leaves;
//            return $query;
            foreach ($attendance as $a)
            {
                $a->date=$date;
                foreach ($late as $l){
                    $a->late=$l->late;
                    $a->key=$key;
                }
                $a->leaves=$leaves[0]->leaves;
            }
            $totalAttendance=array_merge($attendance,$totalAttendance);
        }
        if (($request->viewType)=="Preview") {
            return view('report.attendance.attendance_summery_data', compact('totalAttendance', 'request'));
        }
        elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
//            return view('report.pdf.attendance_summery_pdf',compact('totalAttendance','request'));
            $pdf=PDF::loadView('report.pdf.attendance_summery_pdf',compact('totalAttendance','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_attendance.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_attendance_summery";
            Excel::create("$excelName", function($excel) use ($totalAttendance, $request) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Attendance Summery");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Attendance Summery');

                $excel->sheet('Sheet 1', function ($sheet) use ($totalAttendance, $request) {
                    $sheet->loadView('report.excel.attendance_summery_excel')
                        ->with('totalAttendance',$totalAttendance)
                        ->with('request',$request);
                });

            })->download($request->extensions);
        }

        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function daily_attendance_exception(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->get(['empFirstName','empLastName','id']);
        return view('report.attendance.daily_attendance_exception',compact('employees','units','departments','floors','sections','designations'));
    }

    public function daily_attendance_exception_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->daily_attendance_exception();
        }


        $lateTime=AttendanceController::late_time();
        $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id  WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $queryPresent.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->sectionName)) {
            $queryPresent.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $queryPresent.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $queryPresent.=" AND employees.empDesignationId='".$request->designationId."'";
        }
        if (($request->empGenderId)!=0) {
            $queryPresent.=" AND employees.empGenderId='".$request->empGenderId."'";
        }
        $date=Carbon::parse($request->date)->toDateString();

        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND attendance.date='$date' AND attendance.in_time=attendance.out_time ORDER BY employees.empFirstName";
//        return $queryPresent;
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.daily_attendance_exception_data',compact('present','request','lateTime','countPresent'));

        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.daily_attendance_exception_pdf',compact('present','request','countPresent','lateTime'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_daily_attendance_exception.pdf";
            return $pdf->download($name);
        }
        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Daily_Attendance_Exception";
            Excel::create("$excelName", function($excel) use ($present, $request,$countPresent,$lateTime) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Overtime Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Overtime Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $lateTime, $request,$countPresent) {
                    $sheet->loadView('report.excel.daily_attendance_exception_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('lateTime',$lateTime)
                        ->with('countPresent',$countPresent);
                });

            })->download($request->extensions);
        }

        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function date_wise_attendance_exception(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('report.attendance.date_wise_attendance_exception',compact('employees','units','departments','floors','sections','designations'));
    }

    public function date_wise_attendance_exception_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_overtime();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_overtime();
        }

        $lateTime=AttendanceController::late_time();
        if($request->emp_id!=0){
            $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id='".$request->emp_id."'";
        }
        else {
            $queryPresent = @"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (!empty($request->sectionName)) {
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }
        }
        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();

        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND attendance.date BETWEEN '$date' and '$endDate' AND attendance.in_time=attendance.out_time ORDER BY employees.empFirstName";
//        return $queryPresent;
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.date_wise_attendance_exception_data',compact('present','request','lateTime','countPresent'));

        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.date_wise_attendance_exception_pdf',compact('present','request','lateTime','countPresent'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_date_wise_attendance_exception.pdf";
            return $pdf->download($name);
        }
        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Date_Wise_Attendance_Exception";
            Excel::create("$excelName", function($excel) use ($present, $request,$countPresent,$lateTime) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Attendance Exception Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Attendacne Exception Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $lateTime, $request,$countPresent) {
                    $sheet->loadView('report.excel.date_wise_attendance_exception_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('lateTime',$lateTime)
                        ->with('countPresent',$countPresent);
                });

            })->download($request->extensions);
        }

        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }

    }

    public function daily_attendance_summery(){
        return view('report.attendance.daily_attendance_summary');
    }

    public function daily_attendance_summary_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->daily_attendance_summery();
        }

        if(!empty($request->date)) {
            $date = Carbon::parse($request->date)->toDateString();
            $ld=Carbon::parse($request->date)->toDateString();
            $late_time=AttendanceController::late_time();
            $totalE=DB::table('employees')->where('empAccStatus','=','1')->count();
            $totalP=DB::table('employees')->join('attendance','employees.id','=','attendance.emp_id')->where('employees.empAccStatus','=','1')->where(['attendance.date'=>$date])->count();
            $totalL=DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1')");
            $totalL=$totalL[0]->leaveCount;
            $Tlate=DB::table('employees')->join('attendance','employees.id','=','attendance.emp_id')->where('employees.empAccStatus','=','1')->where(['attendance.date'=>$date])->where('attendance.in_time','>',$late_time)->count();

//            return $totalL;

        }

        else{
            return redirect(route('report.daily_attendance_summery'));
        }

        if($request->viewType=='Preview') {

            if ($request->reportType == 1) {
                $data = [];
                $departments = DB::table('departments')->select('id', 'departmentName')->get();
                foreach ($departments as $id => $department) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->where(['attendance.date' => $date])->count();
                    $late = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->where(['attendance.date' => $date])->where('attendance.in_time', '>', $late_time)->count();
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empDepartmentId=" . $department->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $department->id;
                    $data[$id]['designationName'] = $department->departmentName;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }


                return view('report.attendance.daily_attendance_summary_data', compact('request', 'data', 'totalE', 'totalP', 'totalL', 'Tlate'));


            } else if ($request->reportType == 2) {
                $data = [];
                $sections = DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
//            return $sections;
                foreach ($sections as $id => $section) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empSection', '=', $section->empSection)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empSection', '=', $section->empSection)->where(['attendance.date' => $date])->count();
                    $late = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empSection', '=', $section->empSection)->where(['attendance.date' => $date])->where('attendance.in_time', '>', $late_time)->count();
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empSection='" . $section->empSection . "'");
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $section->empSection;
                    $data[$id]['designationName'] = $section->empSection;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }

                return view('report.attendance.daily_attendance_summary_data', compact('request', 'data', 'totalE', 'totalP', 'totalL', 'Tlate'));

            } else if ($request->reportType == 3) {
                $data = [];
                $lines=DB::select("SELECT id, line_no, floor_id FROM tblines WHERE id in (SELECT line_id FROM employees WHERE empAccStatus=1)");

                foreach ($lines as $id => $line) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('line_id', '=', $line->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('line_id', '=', $line->id)->where(['attendance.date' => $date])->count();
                    $late = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('line_id', '=', $line->id)->where(['attendance.date' => $date])->where('attendance.in_time', '>', $late_time)->count();
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and line_id=" . $line->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $line->floor_id;
                    $data[$id]['designationName'] = $line->line_no;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }
                return view('report.attendance.daily_attendance_summary_data', compact('request', 'data', 'totalE', 'totalP', 'totalL', 'Tlate'));

            } else if ($request->reportType == 4) {
                $data = [];
                $designations = DB::table('designations')->select('id', 'designation')->get();
                foreach ($designations as $id => $designation) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empDesignationId', '=', $designation->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDesignationId', '=', $designation->id)->where(['attendance.date' => $date])->count();
                    $late = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDesignationId', '=', $designation->id)->where(['attendance.date' => $date])->where('attendance.in_time', '>', $late_time)->count();
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empDesignationId=" . $designation->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
//                return $leaveCount;
                    $data[$id]['designationId'] = $designation->id;
                    $data[$id]['designationName'] = $designation->designation;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }

                return view('report.attendance.daily_attendance_summary_data', compact('request', 'data', 'totalE', 'totalP', 'totalL', 'Tlate'));
            }
        }
        elseif(($request->viewType)=="Generate PDF"){
            if ($request->reportType == 1) {
                $data = [];
                $departments = DB::table('departments')->select('id', 'departmentName')->get();
                foreach ($departments as $id => $department) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->where(['attendance.date' => $date])->count();
                    $late = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->where(['attendance.date' => $date])->where('attendance.in_time', '>', $late_time)->count();
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empDepartmentId=" . $department->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $department->id;
                    $data[$id]['designationName'] = $department->departmentName;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }

            } else if ($request->reportType == 2) {
                $data = [];
                $sections = DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
//            return $sections;
                foreach ($sections as $id => $section) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empSection', '=', $section->empSection)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empSection', '=', $section->empSection)->where(['attendance.date' => $date])->count();
                    $late = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empSection', '=', $section->empSection)->where(['attendance.date' => $date])->where('attendance.in_time', '>', $late_time)->count();
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empSection='" . $section->empSection . "'");
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $section->empSection;
                    $data[$id]['designationName'] = $section->empSection;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }

            } else if ($request->reportType == 3) {
                $data = [];
                $lines=DB::select("SELECT id, line_no, floor_id FROM tblines WHERE id in (SELECT line_id FROM employees WHERE empAccStatus=1)");
                foreach ($lines as $id => $line) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('line_id', '=', $line->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('line_id', '=', $line->id)->where(['attendance.date' => $date])->count();
                    $late = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('line_id', '=', $line->id)->where(['attendance.date' => $date])->where('attendance.in_time', '>', $late_time)->count();
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and line_id=" . $line->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $line->floor_id;
                    $data[$id]['designationName'] = $line->line_no;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }
                $floors=DB::select("SELECT floor_id FROM employees WHERE empAccStatus=1 AND floor_id in (SELECT id FROM floors) GROUP BY floor_id");

//                return view('report.pdf.daily_attendance_summary_floor_line_wise_pdf',compact('request','data','totalE','totalP','totalL','Tlate','floors'));
                $pagesize=$request->pagesize;
                $pageOrientation=$request->pageOrientation;

                $name=time()."_Attendance_Summary.pdf";
                return view('report.pdf.daily_attendance_summary_floor_line_wise_pdf',compact('request','data','totalE','totalP','totalL','Tlate','floors'));
                $pdf->setPaper($pagesize, $pageOrientation);
                return $pdf->download($name);


            } else if ($request->reportType == 4) {
                $data = [];
                $designations = DB::table('designations')->select('id', 'designation')->get();
                foreach ($designations as $id => $designation) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empDesignationId', '=', $designation->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDesignationId', '=', $designation->id)->where(['attendance.date' => $date])->count();
                    $late = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDesignationId', '=', $designation->id)->where(['attendance.date' => $date])->where('attendance.in_time', '>', $late_time)->count();
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empDesignationId=" . $designation->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $designation->id;
                    $data[$id]['designationName'] = $designation->designation;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }
            }
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;

            $name=time()."_Attendance_Summary.pdf";
            $pdf=PDF::loadView('report.pdf.daily_attendance_summary_pdf',compact('request','data','totalE','totalP','totalL','Tlate'));
            $pdf->setPaper($pagesize, $pageOrientation);
            return $pdf->download($name);

        }

    elseif(($request->viewType)=="Generate Excel"){
        $data = [];
        if ($request->reportType == 1) {
            $departments = DB::table('departments')->select('id', 'departmentName')->get();
            foreach ($departments as $id => $department) {
                $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->count();
                $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->where(['attendance.date' => $date])->count();
                $late = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->where(['attendance.date' => $date])->where('attendance.in_time', '>', $late_time)->count();
                $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empDepartmentId=" . $department->id);
                $leaveCount = $leaveCount[0]->leaveCount;
                $data[$id]['designationId'] = $department->id;
                $data[$id]['designationName'] = $department->departmentName;
                $data[$id]['total'] = $total;
                $data[$id]['present'] = $present;
                $data[$id]['late'] = $late;
                $data[$id]['leave'] = $leaveCount;
            }

        } else if ($request->reportType == 2) {
            $sections = DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
//            return $sections;
            foreach ($sections as $id => $section) {
                $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empSection', '=', $section->empSection)->count();
                $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empSection', '=', $section->empSection)->where(['attendance.date' => $date])->count();
                $late = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empSection', '=', $section->empSection)->where(['attendance.date' => $date])->where('attendance.in_time', '>', $late_time)->count();
                $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empSection='" . $section->empSection . "'");
                $leaveCount = $leaveCount[0]->leaveCount;
                $data[$id]['designationId'] = $section->empSection;
                $data[$id]['designationName'] = $section->empSection;
                $data[$id]['total'] = $total;
                $data[$id]['present'] = $present;
                $data[$id]['late'] = $late;
                $data[$id]['leave'] = $leaveCount;
            }

        }
        else if ($request->reportType == 4) {
            $designations = DB::table('designations')->select('id', 'designation')->get();
            foreach ($designations as $id => $designation) {
                $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empDesignationId', '=', $designation->id)->count();
                $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDesignationId', '=', $designation->id)->where(['attendance.date' => $date])->count();
                $late = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDesignationId', '=', $designation->id)->where(['attendance.date' => $date])->where('attendance.in_time', '>', $late_time)->count();
                $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empDesignationId=" . $designation->id);
                $leaveCount = $leaveCount[0]->leaveCount;
                $data[$id]['designationId'] = $designation->id;
                $data[$id]['designationName'] = $designation->designation;
                $data[$id]['total'] = $total;
                $data[$id]['present'] = $present;
                $data[$id]['late'] = $late;
                $data[$id]['leave'] = $leaveCount;
            }
        }
        $excelName=time()."_Daily_Attendance_Summery";
        Excel::create("$excelName", function($excel) use ($request,$data,$totalL,$totalE,$totalP,$Tlate) {

            // Set the title
            $name=Auth::user()->name;
            $excel->setTitle("Daily Attendance Summary");


            // Chain the setters
            $excel->setCreator($name);

            $excel->setDescription('Daily Attendance Summary');


            $excel->sheet('Sheet 1', function ($sheet) use ($request,$data,$Tlate,$totalP,$totalL,$totalE) {
                $sheet->loadView('report.excel.daily_attendance_summary_excel')
                    ->with('data',$data)
                    ->with('request',$request)
                    ->with('Tlate',$Tlate)
                    ->with('totalP',$totalP)
                    ->with('totalL',$totalL)
                    ->with('totalE',$totalE);
            });

        })->download($request->extensions);
    }
    }

    //leave report view method
    public function leave_report(){
        return view('report.leave.index');
    }
    public function training_wise_employee_report(){
        $training_name=DB::table('tb_training')->pluck('training_name','id')->all();
        return view('report.training.trainingwiseEmployeereport',compact('training_name'));
    }
    public function dateWiseLeaveReport(){
        //dd('hello');
        return view('report.leave.dateWiseLeaveReport');
    }
    //Leave Report show
    public function showDateWiseLeaveReport(Request $request){
        $start=$request->leave_start;
        $end=$request->leave_end;
        $sdate=date_create($start);
        $edate=date_create($end);
        $start_date=date_format($sdate,"Y-m-d");
        $end_date=date_format($edate,"Y-m-d");
        //return($end_date);
        $tb_leave_type=DB::table('tb_leave_type')->get();
        $data=DB::table('tb_employee_leave');
        $tb_leave_type=DB::table('tb_leave_type')->get();
        $data=DB::table('tb_leave_application')
            ->leftJoin('employees', function ($query) {
                $query->on('tb_leave_application.employee_id', '=', 'employees.id');
            })
            ->leftJoin('tb_leave_type', function ($query) {
                $query->on('tb_leave_application.leave_type_id', '=', 'tb_leave_type.id');
            })
            ->join('designations', function ($query){
                $query->on('designations.id', '=', 'employees.empDesignationId');
            })
            ->select('tb_leave_application.*', 
            DB::raw('group_concat(tb_leave_type.total_days)as leave_amount_days'),
            'employees.empFirstName','employees.employeeId','employees.empLastName',
            'designations.designation','tb_leave_type.leave_type',
            'tb_leave_application.leave_starting_date','tb_leave_application.leave_ending_date',
            DB::raw('group_concat(tb_leave_type.leave_type,DATEDIFF(tb_leave_application.leave_starting_date,
            tb_leave_application.leave_ending_date)) as names'),
            DB::raw('group_concat(tb_leave_application.leave_starting_date) as leave_sdays'),
            DB::raw('group_concat(tb_leave_application.leave_ending_date) as leave_edays'),  
            DB::raw(' group_concat(DATEDIFF(tb_leave_application.leave_starting_date,tb_leave_application.leave_ending_date))as distance_value'),
            DB::raw('group_concat(tb_leave_type.leave_type,abs(tb_leave_type.total_days)  -  
            abs(DATEDIFF(tb_leave_application.leave_starting_date,tb_leave_application.leave_ending_date))) 
            as leave_available'))
            ->selectRaw('tb_leave_application.created_at')->whereBetween('tb_leave_application.created_at',[$start_date,$end_date])
            ->selectRaw('tb_leave_application.status as sta')->WHERE('tb_leave_application.status','=',1)
            ->groupBy('tb_leave_application.employee_id')
            ->orderBy('tb_leave_application.created_at','ASC')
            ->orderBy('tb_leave_application.leave_type_id','ASC')
            ->get();
            //dd($data);
            $current_date=date("m/d/Y");
            //dd($request->tests);
            if (($request->tests)=="leave_report_generate") {
                return view('report.leave.showDateWiseLeaveReport',compact('data','tb_leave_type'));
            }elseif(($request->tests)==null){
                $pdf=PDF::loadView('report.pdf.Employee_datewise_report',compact('data','tb_leave_type','request'));
                $pdf->setPaper('A4', 'Landscape');
                $name=$current_date."Employee_datewise_report.pdf";
                return $pdf->download($name);
            }else{
                Session::flash('error_message', "Invalid Request");
                return view('404');
            }  
       
    }

    public function onLeaveReport(){
        $current_date=date("Y-m-d");
        $onLeaveEmployee=DB::table('tb_leave_application')
            ->join('tb_leave_type', 'tb_leave_type.id','=','tb_leave_application.leave_type_id')
            ->join('employees', 'employees.id','=','tb_leave_application.employee_id')
            ->join('designations', 'designations.id','=','employees.empDesignationId')
            ->select('tb_leave_application.leave_starting_date','tb_leave_type.leave_type','tb_leave_application.leave_ending_date', 'employees.empFirstName', 'employees.empLastName','employees.employeeId','designations.designation')
            ->where('tb_leave_application.leave_starting_date','<=',$current_date)
            ->where('tb_leave_application.leave_ending_date','>=',$current_date)
            ->where(['tb_leave_application.status'=>1])
            //->groupBy('tb_training.id')
            //->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->get();
            //dd($onLeaveEmployee);
            return view('report.leave.onLeaveReport',compact('onLeaveEmployee'));
    }

    public function onLeaveListPDF(){
        $current_date=date("Y-m-d");
        $onLeaveEmployee=DB::table('tb_leave_application')
            ->join('tb_leave_type', 'tb_leave_type.id','=','tb_leave_application.leave_type_id')
            ->join('employees', 'employees.id','=','tb_leave_application.employee_id')
            ->join('designations', 'designations.id','=','employees.empDesignationId')
            ->select('tb_leave_application.leave_starting_date','tb_leave_type.leave_type','tb_leave_application.leave_ending_date', 'employees.empFirstName', 'employees.empLastName','employees.employeeId','designations.designation')
            ->where('tb_leave_application.leave_starting_date','<=',$current_date)
            ->where('tb_leave_application.leave_ending_date','>=',$current_date)
            ->where(['tb_leave_application.status'=>1])
            //->groupBy('tb_training.id')
            //->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->get();

            $pdf=PDF::loadView('report.pdf.onLeaveEmployeePDF',compact('onLeaveEmployee'));
            $pdf->setPaper('A4', 'Potrait');
            $name=$current_date."onLeaveEmployee.pdf";
            return $pdf->download($name);
            //dd($onLeaveEmployee);
            
    }
    //Employee all types bonus report
    public function employee_month_wise_bonus_Report(){
        return view('report.payroll.bonus.bonus_report');
    }

    //increment bonus view page
    public function incrementBonus(){
       return view('report.payroll.bonus.empincrementreport');
    }

    //festival bonus view page
    public function festivalBonus(){
        return view('report.payroll.bonus.empfestivalreport');
    }

    //increment bonus report month wise
    public function incrementEmployeeBonusReport(Request $request){
        $month=$request->year_bonus_month;
        $data=DB::SELECT("select employees_bonus.emp_id,employees.empFirstName,employees.empLastName,users.name,users.email,users.is_permission,
              employees_bonus.emp_gross,employees_bonus.emp_bonus,employees_bonus.emp_amount,employees_bonus.emp_total_percent,employees_bonus.emp_total_amount,employees_bonus.created_at
              FROM employees_bonus
              LEFT JOIN employees ON employees_bonus.emp_id = employees.id
              LEFT JOIN users ON employees_bonus.bonus_given_id = users.id 
              WHERE date='$month'
             ");
          return response()->json($data);
        }
    //festival bonus report month wise
    public function festivalEmloyeeBonusReport(Request $request){
        $month=$request->year_bonus_month;
        $data=DB::SELECT("select festival_bonus.emp_id,designations.designation,departments.departmentName,employees.empFirstName,employees.empLastName,users.name,users.is_permission,
              festival_bonus.emp_gross,festival_bonus.bonus_title,festival_bonus.emp_bonus,festival_bonus.emp_amount,festival_bonus.emp_total_percent,festival_bonus.emp_total_amount,festival_bonus.created_at
              FROM festival_bonus
              LEFT JOIN employees ON festival_bonus.emp_id = employees.id
              LEFT JOIN departments ON employees.empDepartmentId = departments.id
              LEFT JOIN designations ON employees.empDesignationId = designations.id
              LEFT JOIN users ON festival_bonus.bonus_given_id = users.id 
              WHERE month='$month'");
        return response()->json($data);
    }

    public function ongoing_training_report_show(){
        $current_date=date("Y-m-d");
        //dd($current_date);

        $training=DB::table('tb_employee_training')
            ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
            ->join('designations', 'designations.id','=','employees.empDesignationId')
            ->select('tb_training.training_name', 
            DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,
            '(',employees.employeeId,')'  SEPARATOR ' , ')) as `employee_name`"),
            'tb_training.id as tid','tb_employee_training.training_starting_date',
            'tb_employee_training.training_ending_date','employees.employeeId',
            'designations.designation')
            ->where('tb_employee_training.training_starting_date','<=',$current_date)
            ->where('tb_employee_training.training_ending_date','>=',$current_date)
            ->groupBy('tb_training.id')
            ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->get();
        
            //dd($training);
        return view('report.training.onGoing',compact('training'));
    }

    public function completed_training_report_show(){
        $current_date=date("Y-m-d");
        //dd($current_date);

        $training=DB::table('tb_employee_training')
            ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
            ->join('designations', 'designations.id','=','employees.empDesignationId')
            ->select('tb_training.training_name', 
            DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,
            '(',employees.employeeId,')'  SEPARATOR ' , ')) as `employee_name`"),
            'tb_training.id as tid','tb_employee_training.training_starting_date',
            'tb_employee_training.training_ending_date','employees.employeeId',
            'designations.designation')
            ->where('tb_employee_training.training_starting_date','<',$current_date)
            ->where('tb_employee_training.training_ending_date','<',$current_date)
            ->groupBy('tb_training.id')
            ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->get();
        //dd($training);
        return view('report.training.completed',compact('training'));
    }

    public function completedTrainingPDF(){
        $current_date=date("Y-m-d");
        //dd($current_date);

        $training=DB::table('tb_employee_training')
            ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
            ->join('designations', 'designations.id','=','employees.empDesignationId')
            ->select('tb_training.training_name', 
            DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,
            '(',employees.employeeId,')'  SEPARATOR ' , ')) as `employee_name`"),
            'tb_training.id as tid','tb_employee_training.training_starting_date',
            'tb_employee_training.training_ending_date','employees.employeeId',
            'designations.designation')
            ->where('tb_employee_training.training_starting_date','<',$current_date)
            ->where('tb_employee_training.training_ending_date','<',$current_date)
            ->groupBy('tb_training.id')
            ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->get();
        //dd($training);

        $pdf=PDF::loadView('report.pdf.completedTrainingPDF',compact('training'));
        $pdf->setPaper('A4', 'portrait');
        $name=$current_date."completedTrainingPDF.pdf";
        return $pdf->download($name);
       
    }


    public function employee_training_history_report(){
      

        $training=DB::table('employees')
            ->leftjoin('tb_employee_training', 'employees.id','=','tb_employee_training.employee_id')
            ->leftjoin('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->leftjoin('designations', 'designations.id','=','employees.empDesignationId')
            ->select('employees.employeeId','employees.empFirstName','employees.empLastName', 
            DB::raw("(GROUP_CONCAT(tb_training.training_name  SEPARATOR ' , ')) as `training_name`"),
            'designations.designation','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->where('tb_employee_training.training_ending_date',"<",Carbon::now())
            ->groupBy('employees.id')
           ->get();

        $training2=DB::table('employees')
            ->leftjoin('tb_employee_training', 'employees.id','=','tb_employee_training.employee_id')
            ->leftjoin('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->leftjoin('designations', 'designations.id','=','employees.empDesignationId')
            ->select('employees.employeeId','employees.empFirstName','employees.empLastName', 
            'designations.designation','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->whereNotIn('employees.id',function($query) {
                $query->select('employee_id')->from('tb_employee_training')
                ->where('tb_employee_training.training_ending_date',"<",Carbon::now());
            })
            ->groupBy('employees.id') 
            ->get();
        //dd($training,$training2);
        return view('report.training.employeeTrainingHistoryReport',compact('training','training2'));
    }
    public function employeeTrainingPDFReport(){
        $training=DB::table('employees')
        ->leftjoin('tb_employee_training', 'employees.id','=','tb_employee_training.employee_id')
        ->leftjoin('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
        ->leftjoin('designations', 'designations.id','=','employees.empDesignationId')
        ->select('employees.employeeId','employees.empFirstName','employees.empLastName', 
        DB::raw("(GROUP_CONCAT(tb_training.training_name  SEPARATOR ' , ')) as `training_name`"),
        'designations.designation','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
        ->where('tb_employee_training.training_ending_date',"<",Carbon::now())
        ->groupBy('employees.id')
       ->get();

    $training2=DB::table('employees')
        ->leftjoin('tb_employee_training', 'employees.id','=','tb_employee_training.employee_id')
        ->leftjoin('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
        ->leftjoin('designations', 'designations.id','=','employees.empDesignationId')
        ->select('employees.employeeId','employees.empFirstName','employees.empLastName', 
        'designations.designation','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
        ->whereNotIn('employees.id',function($query) {
            $query->select('employee_id')->from('tb_employee_training')
            ->where('tb_employee_training.training_ending_date',"<",Carbon::now());
        })
        ->groupBy('employees.id') 
        ->get();
        $current_date=date("d-m-Y");
        $pdf=PDF::loadView('report.pdf.employeeTrainingPDFReport',compact('training','training2'));
        $pdf->setPaper('A4', 'portrait');
        $name=$current_date."_enployee_training.pdf";
        return $pdf->download($name);

    }

    public function training_report_show(Request $request){
        // return $request->all();
        $start=date("Y-m-d",strtotime($request->training_start));
        $end=date("Y-m-d",strtotime($request->training_end));
        $training_id=$request->training_id;
        //dd($start,$end,$training_id);
        if($training_id==0){
            $trainingwiseEmployee=DB::table('tb_employee_training')
            ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
            ->join('designations', 'designations.id','=','employees.empDesignationId')
            ->select('tb_training.training_name', DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,'(',employees.employeeId,')'  SEPARATOR ' , '))
             as `employee_name`"),'tb_training.id as tid','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date','employees.employeeId','designations.designation')
            ->whereBetween('tb_employee_training.training_starting_date',[$start, $end])
            
            ->groupBy('tb_training.id')
            ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->get();  
        }
        else{
        $trainingwiseEmployee=DB::table('tb_employee_training')
            ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
            ->join('designations', 'designations.id','=','employees.empDesignationId')
            ->select('tb_training.training_name', DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,'(',employees.employeeId,')'  SEPARATOR ' , '))
             as `employee_name`"),'tb_training.id as tid','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date','employees.employeeId','designations.designation')
            ->whereBetween('tb_employee_training.training_starting_date',[$start, $end])
            ->where(['tb_training.id'=>$training_id])
            ->groupBy('tb_training.id')
            ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->get();
        }
        return view('report.training.ajaxtable1',compact('trainingwiseEmployee'));
    }

    public function trainingWiseEmployeePDF(Request $request){
        // return $request->all();
        $start=date("Y-m-d",strtotime($request->training_start));
        $end=date("Y-m-d",strtotime($request->training_end));
        $training_id=$request->training_id;
        //dd($start,$end,$training_id);
        if($training_id==0){
            $trainingwiseEmployee=DB::table('tb_employee_training')
            ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
            ->join('designations', 'designations.id','=','employees.empDesignationId')
            ->select('tb_training.training_name', DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,'(',employees.employeeId,')'  SEPARATOR ' , '))
             as `employee_name`"),'tb_training.id as tid','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date','employees.employeeId','designations.designation')
            ->whereBetween('tb_employee_training.training_starting_date',[$start, $end])
            
            ->groupBy('tb_training.id')
            ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->get();  
        }
        else{
        $trainingwiseEmployee=DB::table('tb_employee_training')
            ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
            ->join('designations', 'designations.id','=','employees.empDesignationId')
            ->select('tb_training.training_name', DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,'(',employees.employeeId,')'  SEPARATOR ' , '))
             as `employee_name`"),'tb_training.id as tid','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date','employees.employeeId','designations.designation')
            ->whereBetween('tb_employee_training.training_starting_date',[$start, $end])
            ->where(['tb_training.id'=>$training_id])
            ->groupBy('tb_training.id')
            ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->get();
        }
      
        $current_date=date("d-m-Y");
        $pdf=PDF::loadView('report.pdf.trainingwiseEmployeePDF',compact('trainingwiseEmployee','request'));
        $pdf->setPaper('A4', 'portrait');
        $name=$current_date."_trainingwiseEmployee.pdf";
        return $pdf->download($name);
    }



    public function pdfOngoingTrainingCard(){
        $current_date=date("Y-m-d");
        //dd($current_date);

        $training=DB::table('tb_employee_training')
            ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
            ->join('designations', 'designations.id','=','employees.empDesignationId')
            ->select('tb_training.training_name', 
            DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,
            '(',employees.employeeId,')'  SEPARATOR ' , ')) as `employee_name`"),
            'tb_training.id as tid','tb_employee_training.training_starting_date',
            'tb_employee_training.training_ending_date','employees.employeeId',
            'designations.designation')
            ->where('tb_employee_training.training_starting_date','<=',$current_date)
            ->where('tb_employee_training.training_ending_date','>=',$current_date)
            ->groupBy('tb_training.id')
            ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->get();
        //dd($training);
        $pdf=PDF::loadView('PDF.ongoingTrainingCard',compact('training'));
        $pdf->setPaper('A4', 'portrait');
        $name=time()."_ongoing_training_card.pdf";
        return $pdf->download($name);

    }
    public function vacancyListFirst(){
        return view('report.recruitment.vacancyList');
    }

    public function vacancyList(Request $request){
        $vacancy_start=date("Y-m-d",strtotime($request->vacancy_start));
        $vacancy_end=date("Y-m-d",strtotime($request->vacancy_end));
        //dd($vacancy_end);
        $vacancyList=DB::table('vacancies')
        ->where('VacAnnounceStartingDate','>',$vacancy_start)
        ->where('vacAnnounceEndingDate','<',$vacancy_end)
        ->get();
        //dd($vacancyList);
        

        $current_date=date("m/d/Y");
        //dd($request->tests);
        if (($request->viewType)=="Preview") {
            return view('report.recruitment.vacancyListReport',compact('vacancyList'));
        }
        elseif(($request->viewType)=="pdfenglish"){
            $pdf=PDF::loadView('report.pdf.vacancyListPDF',compact('vacancyList','request'));
            $pdf->setPaper('A4', 'Potrait');
            $name=$current_date."Vacancy_list.pdf";
            return $pdf->download($name);
        
        }

        elseif(($request->viewType)=="pdfbangla"){
            $pdf=mPDF::loadView('report.pdf.vacancyListPDFBangla',compact('vacancyList','request'));
            //$pdf->setPaper('A4', 'Potrait');
            $name=$current_date."Vacancy_list.pdf";
            return $pdf->download($name);
        
        }
        
        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }  
    }
    public function applicantList(){
        $applicantList=DB::table('vacancies')
        ->select('vacTitle','id as vID')
        ->get();
        //dd($applicantList);
        return view('report.recruitment.applicantList',compact('applicantList'));
    }
    public function applicantListShow(Request $request){
        $query=@"SELECT vacancy_application.*,vacancies.* 
        FROM vacancy_application JOIN vacancies ON 
        vacancy_application.vacancy_id=vacancies.id  ";
        
        if (($request->vacId)!=0) {
            $query.=" AND vacancies.id='".$request->vacId."'";
        }

        if (($request->priority)!=0) {
            $query.=" AND vacancy_application.priorityStatus='".$request->priority."'";
        }

        if (($request->elgInterview)!=0) {
            $query.=" AND vacancy_application.interviewStatus='".$request->elgInterview."'";
        }

        if (!empty($request->jobStatus)) {
            $query.=" AND vacancy_application.jobStatus='".$request->jobStatus."'";
        }

        if (($request->vacStatus)!=3) {
            $query.=" AND vacancies.vacStatus='".$request->vacStatus."'";
        }


        $query.=" ORDER BY vacancy_application.submittedDate ASC";
        // $query.=" GROUP BY 'unit_id'";

        $applicantListShow= DB::select(DB::raw($query));


        if (($request->viewType)=="Preview") {
            return view('report.recruitment.viewApplicantList',compact('applicantListShow','request'));
        }
        elseif(($request->viewType)=="pdfenglish"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.ApplicantListPDF',compact('applicantListShow','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."ApplicantList.pdf";
            return $pdf->download($name);
        }
        elseif(($request->viewType)=="pdfbangla"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=mPDF::loadView('report.pdf.ApplicantListPDFBangla',compact('applicantListShow','request'));
            //$pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."ApplicantList.pdf";
            return $pdf->download($name);
        }
        
        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function acceptedList(){
        $acceptedList=DB::table('vacancy_application')
        ->join('vacancies', 'vacancies.id','=','vacancy_application.vacancy_id')
        ->select('name','phone','vacTitle','interviewDate','submittedDate','priorityStatus','vacStatus')
        ->where(['jobStatus'=>'Confirm'])
        ->get();
        //dd($applicantList);
        return view('report.recruitment.accepted',compact('acceptedList'));
    }

    public function acceptedListPDF(){
        
        $acceptedList=DB::table('vacancy_application')
        ->join('vacancies', 'vacancies.id','=','vacancy_application.vacancy_id')
        ->select('name','phone','vacTitle','interviewDate','submittedDate','priorityStatus','vacStatus')
        ->where(['jobStatus'=>'Confirm'])
        ->get();
        //dd($applicantList);
        $current_date=date("m/d/Y");
        $pdf=PDF::loadView('report.pdf.ApplicantAcceptedListPDF',compact('acceptedList'));
        $pdf->setPaper('A4','Potrait');
        $name=$current_date."ApplicantAcceptedList.pdf";
        return $pdf->download($name);
    }

    public function acceptedListPDFBangla(){
        
        $acceptedList=DB::table('vacancy_application')
        ->join('vacancies', 'vacancies.id','=','vacancy_application.vacancy_id')
        ->select('name','phone','vacTitle','interviewDate','submittedDate','priorityStatus','vacStatus')
        ->where(['jobStatus'=>'Confirm'])
        ->get();
        //dd($applicantList);
        $current_date=date("m/d/Y");
        $pdf=mPDF::loadView('report.pdf.ApplicantAcceptedListPDFBangla',compact('acceptedList'));
        //$pdf->setPaper('A4','Potrait');
        $name=$current_date."ApplicantAcceptedList.pdf";
        return $pdf->download($name);
    }

    public function dailyRecruitmentList(){
        $current_date=date("Y-m-d");
        $dailyRecruitment=DB::table('vacancy_application')
        ->join('vacancies', 'vacancies.id','=','vacancy_application.vacancy_id')
        ->select('name','phone','vacTitle','interviewDate','submittedDate','priorityStatus','vacStatus')
        ->where(['submittedDate'=>$current_date])
        ->get();
        //dd($dailyRecruitment);
        return view('report.recruitment.dailyRecruitmentList',compact('dailyRecruitment'));
    }

    public function dailyRecruitmentListPDF(){
        $current_date=date("Y-m-d");
        $dailyRecruitment=DB::table('vacancy_application')
        ->join('vacancies', 'vacancies.id','=','vacancy_application.vacancy_id')
        ->select('name','phone','vacTitle','interviewDate','submittedDate','priorityStatus','vacStatus')
        ->where(['submittedDate'=>$current_date])
        ->get();
        //dd($dailyRecruitment);
        $current_date=date("d/m/Y");
        $pdf=PDF::loadView('report.pdf.dailyRecruitmentListPDF',compact('dailyRecruitment'));
        $pdf->setPaper('A4','Potrait');
        $name=$current_date."dailyRecruitment.pdf";
        return $pdf->download($name);
       
    }

    public function earnLeave(){
        $units=DB::table('units')->distinct()->groupBy('name')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.leave.earnLeaveReport',compact('units','departments','sections','designations'));

        //return view('report.leave.earnLeaveReport',compact('designation'));
    }

    public function maternityLeave(){
        $units=DB::table('units')->distinct()->groupBy('name')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.leave.maternityLeaveReport',compact('units','departments','sections','designations'));

        //return view('report.leave.earnLeaveReport',compact('designation'));
    }

    public function viewMaternityReport(Request $request)
    {
        $query=@"SELECT tb_maternity_allowance.*, employees.empFirstName,employees.empLastName,employees.employeeId,
        employees.empSection,employees.id,employees.empJoiningDate,designations.designation,
        departments.departmentName, units.name unitName
        FROM tb_maternity_allowance
        LEFT JOIN employees ON tb_maternity_allowance.employee_id=employees.id  
        LEFT JOIN designations ON employees.empDesignationId=designations.id 
        LEFT JOIN departments ON employees.empDepartmentId=departments.id 
        LEFT JOIN units ON employees.unit_id=units.id
        WHERE tb_maternity_allowance.employee_id>0";

        if (($request->method) != '01') {
            $query.=" AND tb_maternity_allowance.other3=".$request->method."";
            //dd($query);
        }
        if (($request->unitId)!=0) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->sectionName)) {
            $query.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        $maternityLeave= DB::select(DB::raw($query));

        if (($request->viewType)=="Preview") {
            return view('report.leave.view_maternityLeave_report',compact('maternityLeave','request'));
        }
        elseif(($request->viewType)=="pdfbangla"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=mPDF::loadView('report.pdf.maternityLeave_pdf',compact('maternityLeave','request'));
            //$pdf->setPaper($pagesize, $pageOrientation);
            $name=date("d-M-Y")."_maternityLeave.pdf";
            return $pdf->download($name);
        }
        elseif(($request->viewType)=="pdfenglish"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.maternityLeave_pdfenglish',compact('maternityLeave','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=date("d-M-Y")."_maternityLeave.pdf";
            return $pdf->download($name);
        }
        elseif(($request->viewType)=="Generate Excel"){
            // $maternityLeaveArray = [];
            // $maternityLeaveArray[] = ['id', 'Name','Designation','Department','Leave Period',
            // 'Previous 3 Month Salary','Previous 3 Month Working Days','Total Payable amount',
            // 'Per Installment','First Installment Date','Second Installment date','Status','Approved By'];

            // foreach ($maternityLeave as $maternityLeave) {
            //     $maternityLeaveArray[] = $maternityLeave;
            // }
            // Excel::create('users', function ($excel) use ($maternityLeaveArray) {
 
            //     // Build the spreadsheet, passing in the users array
            //     $excel->sheet('sheet1', function ($sheet) use ($maternityLeaveArray) {
            //         $sheet->fromArray($maternityLeaveArray);
            //     });
        
            // })->download('xlsx');
        Excel::create('Report2016', function($excel) use ($maternityLeave) {

            // Set the title
            $excel->setTitle('My awesome report 2016');

            // Chain the setters
            $excel->setCreator('Me')->setCompany('Our Code World');

            $excel->setDescription('A demonstration to change the file properties');


            $data = $maternityLeave;

            $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                $sheet->setOrientation('landscape');
                $sheet->fromArray($data, NULL, 'A3');
            });

        })->download('xlsx');
        }
        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }  
    }

    public function viewEarnReport(Request $request)
    {
        $query=@"SELECT employees.empFirstName,employees.empLastName,employees.employeeId,employees.empSection,
        employees.empJoiningDate,tb_leave_type.total_days,tb_salary_history.month,
        tb_salary_history.net_amount,tb_employee_leave.leave_taken,tb_employee_leave.leave_available,
        designations.designation,departments.departmentName, units.name unitName 
        FROM employees 
        LEFT JOIN designations ON employees.empDesignationId=designations.id 
        LEFT JOIN tb_leave_application ON employees.id=tb_leave_application.employee_id
        LEFT JOIN tb_salary_history ON employees.id=tb_salary_history.emp_id
        LEFT JOIN tb_employee_leave ON employees.id=tb_employee_leave.employee_id
        LEFT JOIN tb_leave_type ON tb_leave_type.id=tb_employee_leave.leave_type_id
        LEFT JOIN departments ON employees.empDepartmentId=departments.id 
        LEFT JOIN units ON employees.unit_id=units.id
        WHERE employees.id>0 AND tb_leave_application.leave_type_id=4 AND tb_leave_application.status=1
        AND tb_employee_leave.leave_type_id=4 AND tb_employee_leave.year=YEAR(CURRENT_DATE) AND YEAR(CURDATE())-YEAR(empJoiningDate)>=1 
        AND MONTH(tb_salary_history.month)=MONTH(CURRENT_DATE - INTERVAL 1 MONTH)" ;

//        return $query;

        if (($request->unitId)!=0) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->sectionName)) {
            $query.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (($request->empGenderId)!=0) {
            $query.=" AND employees.empGenderId='".$request->empGenderId."'";
        }

        $query.="GROUP BY employees.id ORDER BY employees.employeeId ASC";
        // $quer.=" GROUP BY 'unit_id'";

        $employee_benefit= DB::select(DB::raw($query));

        $query2=@"SELECT employees.empFirstName,employees.empLastName,employees.employeeId,employees.empSection,
                employees.empJoiningDate,tb_salary_history.month,
                tb_salary_history.net_amount,designations.designation,departments.departmentName, 
                units.name unitName 
                FROM employees 
                LEFT JOIN designations ON employees.empDesignationId=designations.id 
                LEFT JOIN tb_salary_history ON employees.id=tb_salary_history.emp_id
                LEFT JOIN departments ON employees.empDepartmentId=departments.id 
                LEFT JOIN units ON employees.unit_id=units.id 
                WHERE employees.id>0 
                AND employees.id NOT IN (SELECT employee_id
                                    FROM `tb_leave_application`
                                    WHERE tb_leave_application.leave_type_id=4 
                                    and tb_leave_application.status=1)
                AND YEAR(CURDATE())-YEAR(empJoiningDate)>=1 
                AND MONTH(tb_salary_history.month)=MONTH(CURRENT_DATE - INTERVAL 1 MONTH)" ;

        if (($request->unitId)!=0) {
            $query2.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->sectionName)) {
            $query2.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query2.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query2.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (($request->empGenderId)!=0) {
            $query2.=" AND employees.empGenderId='".$request->empGenderId."'";
        }

        $query2.="ORDER BY employees.employeeId ASC";
        // $quer.=" GROUP BY 'unit_id'";

        $employee_without_leave_benefit= DB::select(DB::raw($query2));

        $earnLeave=DB::table("tb_leave_type")
            ->where(['id'=>4])
            ->get();

        //dd($employee_benefit);
        if (($request->viewType)=="Preview") {
            return view('report.leave.view_earnLeave_report',compact('employee_benefit','earnLeave','employee_without_leave_benefit','request'));
        }
        elseif(($request->viewType)=="pdfenglish"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.earnLeave_pdf',compact('employee_benefit','earnLeave','employee_without_leave_benefit','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."earnLeave.pdf";
            return $pdf->download($name);
        }
        elseif(($request->viewType)=="pdfbangla"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=mPDF::loadView('report.pdf.earnLeave_bangla_pdf',compact('employee_benefit','earnLeave','employee_without_leave_benefit','request'));
            //$pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."earnLeave.pdf";
            return $pdf->download($name);
        }
        
        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }  
        
    }

    public function availableLeaveReport(){
        $units=DB::table('units')->distinct()->groupBy('name')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees= DB::table('employees')
            ->select(DB::raw("CONCAT(empFirstName,' ',empLastName,' , ID : ',employeeId)AS full_name, id"))
            ->get(['full_name','id']);
        return view('report.leave.employeeAvailableLeaveReport',compact('employees','units','departments','floors','sections','designations'));
    }

    public function viewEmployeeAvailableLeave(Request $request){
        $query=@"SELECT employees.empFirstName,employees.empLastName,employees.employeeId,employees.empSection,
        employees.empJoiningDate,
        GROUP_CONCAT(DISTINCT tb_leave_type.leave_type SEPARATOR ', ') as leave_name,
        GROUP_CONCAT(DISTINCT tb_leave_type.total_days SEPARATOR ', ') as total_days,
        GROUP_CONCAT(DISTINCT tb_employee_leave.leave_type_id SEPARATOR ', ') as leave_id,
        GROUP_CONCAT(tb_employee_leave.leave_taken SEPARATOR ', ') as leave_taken,
        GROUP_CONCAT(tb_employee_leave.leave_available SEPARATOR ', ') as leave_available,
        designations.designation,departments.departmentName, floors.floor floorName, units.name unitName 
        FROM employees 
        LEFT JOIN designations ON employees.empDesignationId=designations.id
        LEFT JOIN tb_employee_leave ON employees.id=tb_employee_leave.employee_id
        LEFT JOIN tb_leave_type ON tb_leave_type.id=tb_employee_leave.leave_type_id
        LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units 
        ON employees.unit_id=units.id LEFT JOIN floors ON employees.floor_id=floors.id 
        WHERE employees.id>0 AND tb_employee_leave.year=YEAR(CURDATE())";

        if (($request->empId)!=0) {
            $query.=" AND employees.unit_id='".$request->empId."'";
        }    

        if (($request->unitId)!=0) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $query.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (!empty($request->sectionName)) {
            $query.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (($request->empGenderId)!=0) {
            $query.=" AND employees.empGenderId='".$request->empGenderId."'";
        }

        $query.="GROUP BY tb_employee_leave.employee_id ORDER BY employees.employeeId ASC";
        // $quer.=" GROUP BY 'unit_id'";

        $leaveAvailable= DB::select(DB::raw($query));
        dd($leaveAvailable);

        $query2=@"SELECT employees.empFirstName,employees.empLastName,employees.employeeId,employees.empSection,
                employees.empJoiningDate,tb_salary_history.month,
                tb_salary_history.net_amount,designations.designation,departments.departmentName, 
                floors.floor floorName, units.name unitName 
                FROM employees 
                LEFT JOIN designations ON employees.empDesignationId=designations.id 
                LEFT JOIN tb_salary_history ON employees.id=tb_salary_history.emp_id
                LEFT JOIN departments ON employees.empDepartmentId=departments.id 
                LEFT JOIN units ON employees.unit_id=units.id 
                LEFT JOIN floors ON employees.floor_id=floors.id 
                WHERE employees.id>0 
                AND employees.id NOT IN (SELECT employee_id
                                    FROM `tb_leave_application`
                                    WHERE tb_leave_application.leave_type_id=4 
                                    and tb_leave_application.status=1)
                AND YEAR(CURDATE())-YEAR(empJoiningDate)>=1 
                AND MONTH(tb_salary_history.month)=MONTH(CURRENT_DATE - INTERVAL 1 MONTH)" ;

        if (($request->unitId)!=0) {
            $query2.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $query2.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (!empty($request->sectionName)) {
            $query2.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query2.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query2.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (($request->empGenderId)!=0) {
            $query2.=" AND employees.empGenderId='".$request->empGenderId."'";
        }

        $query2.="ORDER BY employees.employeeId ASC";
        // $quer.=" GROUP BY 'unit_id'";

        $employee_without_leave_benefit= DB::select(DB::raw($query2));

        $earnLeave=DB::table("tb_leave_type")
            ->where(['id'=>4])
            ->get();

        //dd($employee_benefit);
        if (($request->viewType)=="Preview") {
            return view('report.leave.view_earnLeave_report',compact('employee_benefit','earnLeave','employee_without_leave_benefit','request'));
        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.earnLeave_pdf',compact('employee_benefit','earnLeave','employee_without_leave_benefit','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."earnLeave.pdf";
            return $pdf->download($name);
        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }  
    } 
    public function personWiseMaternityLeaveBenefit($id){

        $employee=DB::table('tb_maternity_allowance')
        ->leftjoin('employees','tb_maternity_allowance.employee_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
        ->leftjoin('nominees', 'nominees.emp_id','=','employees.id')
        ->leftjoin('units', 'employees.unit_id','=','units.id')
        ->select('employees.*','nominees.*','units.name as unitName','tb_maternity_allowance.*','designations.designation','departments.departmentName')
        ->where('employees.id','=',$id)
        ->first();

        $current_month=Carbon::now()->format('m-Y');
        $dayCount=cal_days_in_month(CAL_GREGORIAN,Carbon::now()->subMonth()->format('m'),Carbon::now()->subMonth()->format('Y'));
        $present_salary=DB::table('payroll_salary')->where('emp_id','=',$id)->where('current_month','=',$current_month)->select('total_employee_salary','basic_salary')->first();
    
        //dd($employee);

        $pdf=mPDF::loadView('report.pdf.personWiseMaternityLeaveBenefitPDF',compact('employee','present_salary'));
        //$pdf->setPaper($pagesize, $pageOrientation);
        $name=date("d-M-Y")."_maternityLeave.pdf";
        return $pdf->download($name); 

    }
}
