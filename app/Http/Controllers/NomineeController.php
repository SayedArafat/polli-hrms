<?php
namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class NomineeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['middleware'=>'check-permission:admin|hr']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function showDelete($id){
        $data=DB::table('nominees')->where(['id'=>$id])->first();
        return view('employee.modal.Nominee.deleteForm',compact('data'));

    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nominee_name'=>'required',
            'nominee_address'=>'required',
        ]);
        if($file=$request->file('nominee_attachments')){
            if($file->getClientSize()<5000000) {
                $name = time() . "_" . $request->emp_id . "_" . "Nominee." . $file->getClientOriginalExtension();
                $file->move('Employee_Nominee_Attachments', $name);
            }
            else{
                Session::flash('fileSize','File size limit exceeded');
                return redirect(route('employee.show',$request->emp_id));
            }
        }
        else{
            $name=null;
        }
        DB::table('nominees')->insert([
            'emp_id'=>$request->emp_id,
            'nominee_name'=>$request->nominee_name,
            'nominee_phone'=>$request->nominee_phone,
            'nominee_address'=>$request->nominee_address,
            'priority'=>$request->priority,
            'nominee_attachments'=>$name,
            'nominee_details'=>$request->nominee_details,
            'created_at'=>Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon::now()->toDateTimeString(),
        ]);
        Session::flash('message','New Nominee Added');
        return redirect(route('employee.show',$request->emp_id));
//        return $request->all();

    }


    public function edit($id)
    {
        $nominee=DB::table('nominees')->where('id','=',"$id")->first();
        return view('employee.modal.Nominee.editForm',compact('nominee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nominee_name'=>'required',
            'nominee_address'=>'required',
        ]);

        $table=DB::table('nominees')->where(['id'=>$id]);

        if($file=$request->file('nominee_attachments'))
        {
            if($file->getClientSize()<5000000) {
                $name = time() . "_" . session('emp_id') . "_" . "Nominee." . $file->getClientOriginalExtension();
                $file->move('Employee_Nominee_Attachments', $name);
                if($table->first()->nominee_attachments) {
                    $path = public_path() . "/Employee_Nominee_Attachments/" . $table->first()->nominee_attachments;
                    if (file_exists($path)) {
//                    return $path;
                        unlink($path);
                    }
                }
            }
            else{
                Session::flash('fileSize','File size limit exceeded');
                return redirect('employee/'.session('emp_id'));
            }
        }
        else{
            $name=$table->first()->nominee_attachments;

        }
        $store=DB::table('nominees')->where('id','=',$id)->update([
            'emp_id'=>session('emp_id'),
            'nominee_name'=>$request->nominee_name,
            'nominee_phone'=>$request->nominee_phone,
            'nominee_address'=>$request->nominee_address,
            'priority'=>$request->priority,
            'nominee_attachments'=>$name,
            'nominee_details'=>$request->nominee_details,
            'updated_at'=>Carbon::now()->toDateTimeString(),
        ]);
        if($store){
            Session::flash('message',"Nominee Updated Successfully");
        }

        return redirect('employee/'.session('emp_id'));
//        return $request->all();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dlt=DB::table('nominees')->where(['id'=>$id]);
        if($dlt->first()->nominee_attachments) {
            $name = public_path() . "/Employee_Nominee_Attachments" . "/" . $dlt->first()->nominee_attachments;
            if (file_exists($name)) {
                unlink($name);
            }
        }

        $dlt=$dlt->delete();
        if($dlt)
        {
            Session::flash('delete', 'Educational Info Successfully Deleted');
            return redirect("/employee/".session('emp_id'));
        }

    }
}
