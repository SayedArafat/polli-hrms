<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UnitController extends Controller
{

    function __construct()
    {
        $this->middleware([
            'middleware'=>'check-permission:admin|hr'
        ]);
    }

    public function index()
    {
        $units=DB::table('units')->orderBy('id','asc')->get();
//        return $units;

        return view('settings.unit.index',compact('units'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'phone_no'=>'required',
            'email'=>'required',
            'address'=>'required',
        ]);
        $id=DB::table('units')->insert([
            'name'=>$request->name,
            'phone_no'=>$request->phone_no,
            'email'=>$request->email,
            'address'=>$request->address,
        ]);
        Session::flash('message','Unit Inserted Successfully');
        return redirect('settings/unit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unit=DB::table('units')->where(['id'=>$id])->first();
        return view('settings.unit.show',compact('unit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unit=DB::table('units')->where(['id'=>$id])->first();
        return view('settings.unit.editForm',compact('unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        return $request->all();

        $this->validate($request,[
            'name'=>'required',
            'phone_no'=>'required',
            'email'=>'required',
            'address'=>'required',
        ]);


        DB::table('units')->where(['id'=>$id])->update([
            'name'=>$request->name,
            'phone_no'=>$request->phone_no,
            'email'=>$request->email,
            'address'=>$request->address,
        ]);

        Session::flash('edit','Unit information updated');
        return redirect('settings/unit');

    }

    public function showDelete($id){
        $unit=DB::table('units')->where(['id'=>$id])->first();
        return view('settings.unit.deleteForm',compact('unit'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('units')->where(['id'=>$id])->delete();
        Session::flash('delete', 'Unit successfully deleted');
        return redirect('settings/unit');
    }
}
