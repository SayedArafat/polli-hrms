<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Redirect;
use Session;
use auth;
use Symfony\Component;
class TrainingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $designation = DB::table('designations')->pluck('designation','id')->all();
        //$employee = Component::select(DB::raw("CONCAT('empFirstName','empLastName','employeeId') AS display_name"),'id')->get()->pluck('display_name','id');
        $employee = DB::table('employees')->pluck('empFirstName','id','employeeId')->all();
        $training_data=DB::table('tb_training')->latest()->get();
        return view('training.index',compact('training_data','designation','employee'));
    }

    public function storeTraining(Request $request){
        $current_time = Carbon::now()->toDateTimeString();
        if($file=$request->file('attachment')) {
            $name = time() . "_" .$request->emp_id. "_".$request->emp_name ;
            $file->move('Training_Attachment', $name);
        }
        else{
            $name="No Attachment";
        }
        $store=DB::table('tb_training')->insert([
            'training_name'=>$request->training_name,
            'duration'=>$request->duration,
            'description'=>$request->description,
            'attachment'=>$name,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
            if($store){
                Session::flash('msg','Training Added');
                return redirect()->back();
            }
    }
    public function training_delete($id){
        $data=DB::table('tb_training')->WHERE('id',$id)->delete();
        if($data){
            Session::flash('message', 'Training Deleted Successful!');
            return redirect()->back();
        }
    }

    public function trainingUpdate(Request $request){
        $id=$request->training_id;
        $current_time = Carbon::now()->toDateTimeString();
        if($file=$request->file('attachment')) {
            $name = time() . "_" .$request->emp_id. "_".$file->getClientOriginalName() ;
            $file->move('Training_Attachment', $name);
        }
//        else if($request->attachment != "No Attachment"){
//            $name = DB::table('tb_training')
//                ->select('attachment')
//                ->where('id','=', $request->training_id)
//                ->first();
//        }
        else{
            $name="No Attachment";

        }
        $training_update=DB::table('tb_training')->WHERE('id',$id)->update([
            'training_name'=>$request->training_name,
            'duration'=>$request->duration,
            'description'=>$request->description,
            'attachment'=>$name,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if( $training_update){
            Session::flash('message', 'Leaves Store Successful!');
            return redirect()->back();
        }
    }

   

    public function training_history(){
        $history = DB::table('tb_employee_training')
        ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
        ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
        ->join('designations', 'designations.id','=','employees.empDesignationId')
        ->select('tb_training.training_name','tb_training.id as tid','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date','employees.id','employees.empFirstName','employees.empLastName','designations.designation')
        ->groupBy('tb_training.id')
        ->groupBy('tb_employee_training.training_starting_date')
        ->groupBy('tb_employee_training.training_ending_date')
        //->groupBy('employees.id')
        ->get();

        //dd($history);
        
        return view('training.history',compact('history'));
    }
    public function AttendentList($id){
        $array=explode(",",$id);

        $employee = DB::table('tb_employee_training')
        ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
        ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
        ->join('designations', 'designations.id','=','employees.empDesignationId')
        ->select('employees.*','designations.designation')
        ->where(['tb_training.id'=>$array[2],'tb_employee_training.training_starting_date'=>$array[0],'tb_employee_training.training_ending_date'=>$array[1],])
        //->groupBy('employees.id')
        ->get();
        return response()->json($employee);
        //dd($employee);
        // return view('training.modal',compact('employee'));
    }
    
    public function training_assign(){
        $training = DB::table('tb_training')->pluck('training_name','id')->all();
        $designation = DB::table('designations')->pluck('designation','id')->all();
        $employees = DB::table('employees')
        ->select(DB::raw("CONCAT(empFirstName,' ',empLastName,' , ID : ',employeeId)AS full_name, id"))
        ->pluck('full_name','id')->all();
        //dd($training);

        return view('training.assign',compact('training','designation','employees'));
    }
    // public function training_assign_request(Request $request){
    //     $designation=$request->designation_id;
    //     $employee=$request->employee_id;
    //     $current_time = Carbon::now()->toDateTimeString();
        
    //     $training_id=$request->training_id;
    //     $training_starting_date=$request->startDate;

    //     $duration=DB::table('tb_training')
    //     ->select('tb_training.duration')
    //     ->where(['tb_training.id'=>$training_id])
    //     ->first()->duration;

    //     $next_date= date('d-m-Y', strtotime($training_starting_date. '+'.($duration-1).'days'));

    //     //dd($duration,$training_starting_date, $next_date);


    //     if($designation!=null){
    //         for ($i = 0; $i < count($request->designation_id); $i++) {
    //             // dd($designation[$i]);
    //             if($designation[$i]!=null){
    //                 $emp_id[]=DB::table('employees')
    //                 ->select('employees.id')
    //                 ->where(['employees.empDesignationId'=>$designation[$i]])
    //                 ->get()->toArray();

    //                 //dd($emp_id);

    //                 foreach($emp_id[$i] as $value){
    //                     //dd($emp_id,$value);
    //                     $insertData = DB::table('tb_employee_training')->insert([
    //                         'employee_id'=>$value->id,
    //                         'training_id'=>$request->training_id,
    //                         'training_starting_date'=>date('d-m-Y', strtotime($request->startDate)),
    //                         'training_ending_date'=>$next_date,
    //                         'created_at' =>$current_time,
    //                         'updated_at' =>$current_time,
    //                     ]);
    //                 }
                    
    //                     if ($insertData) {
    //                         Session::flash('success1', 'Training Succesfully Assigned to Selected Employees');
    //                     }  
    //                 }
    //             }  
    //             return redirect()->back();    
    //         }
            
    //     elseif($employee!=null){
           
            
    //         for ($i = 0; $i < count($request->employee_id); $i++) {
    //             $store=DB::table('tb_employee_training')->insert([
    //             'employee_id'=>$request->employee_id[$i],
    //             'training_id'=>$request->training_id,
    //             'training_starting_date'=>date('d-m-Y', strtotime($request->startDate)),
    //             'training_ending_date'=>$next_date,
    //             'created_at' =>$current_time,
    //             'updated_at' =>$current_time,
    //         ]);
    //         }
    //         //dd($store);
    //         if($store){
    //             Session::flash('success2','Training Succesfully Assigned to Selected Employees');
    //             return redirect()->back();
    //         }
    //     }
    // }


    public function training_assign_request(Request $request){
        
        $empId=DB::table('temporary_training')
        ->pluck('employee_id')->toArray();
        //dd(count($empId));

        $current_time = Carbon::now()->toDateTimeString();
        $training_starting_date=date("Y-m-d",strtotime($request->startDate));
        $training_id=$request->training_id;
        $duration=DB::table('tb_training')
        ->select('tb_training.duration')
        ->where(['tb_training.id'=>$training_id])
        ->first()->duration;
        $next_date= date("Y-m-d",strtotime($training_starting_date. '+'.($duration-1).'days'));
        //dd($training_starting_date,$next_date);
 
        for ($i = 0; $i < count($empId); $i++) {
            $store=DB::table('tb_employee_training')->insert([
            'employee_id'=>$empId[$i],
            'training_id'=>$request->training_id,
            'training_starting_date'=>$training_starting_date,
            'training_ending_date'=>$next_date,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        }
        
        if($store){
            DB::table('temporary_training')->delete();
            DB::table('temporary_training')->truncate();
            Session::flash('success2','Training Succesfully Assigned to Selected Employees');
            return redirect()->back();
        }
        
    }

   
    public function DesignationTable($designation){
        $array=explode(",",$designation);
        //dd($array);
        $desDelete=DB::table('temporary_training')
        ->pluck('designation_id')->unique()->toArray();

        //dd($desDelete,$array);

        $result = array_diff($desDelete, $array);

        if($result==null){
            for ($k = 0; $k<count($array); $k++){
                //dd($designation[$k]);
                $request1=DB::table('employees')
                ->select('employees.id','employees.empDesignationId')
                ->where(['empDesignationId'=>$array[$k]])->get();
    
                
                //dd($request1);
    
                $current_time = Carbon::now()->toDateTimeString();
                foreach($request1 as $value1){
                    $request2=DB::table('temporary_training')
                    ->select('temporary_training.employee_id')
                    ->where(['temporary_training.employee_id'=>$value1->id])->first();
                    //dd($request1, $request2);
    
                    if($request2==null){   
                        //dd($value1->id); 
                        $insert=DB::table('temporary_training')->insert([
                            'employee_id' =>$value1->id,
                            'designation_id' =>$value1->empDesignationId,
                            'created_at' =>$current_time,
                            'updated_at' =>$current_time,
                        ]);
                    }
                }
            }
        }
        else{
            DB::table('temporary_training')->where(['designation_id'=>$result])->delete();
        }

 
        $employeesByDesignation=DB::table('temporary_training')
        ->join('employees','employees.id','=','temporary_training.employee_id')
        ->join('designations','designations.id','=','employees.empDesignationId')
        ->select('temporary_training.id','employees.employeeId','employees.empFirstName','employees.empLastName','designations.designation')
        ->groupBy('temporary_training.id')
        ->get();

    // for ($k = 0; $k<count($array); $k++){
    //     $employeesByDesignation=DB::table('employees')
    //     ->join('designations','designations.id','=','employees.empDesignationId')
    //     ->select('employees.id','employees.employeeId','employees.empFirstName','employees.empLastName','designations.designation')
    //     ->where(['empDesignationId'=>$array[$k]])
    //     ->groupBy('employees.id')
    //     ->get();

    //     $empList=DB::table('employees')
    //     ->join('designations','designations.id','=','employees.empDesignationId')
    //     ->where(['empDesignationId'=>$array[$k]])
    //     ->pluck('employees.id')->toArray();
    // }
    //dd($empList);


        return view('training.employeeByDesignation',compact('employeesByDesignation'));

    }

    public function EmployeeTable($empID){
        $array=explode(",",$empID);

        $empDelete=DB::table('temporary_training')
        ->pluck('employee_id')->toArray();

        $result = array_diff($empDelete, $array);

        //dd($result);
        
        if($result!=null){
            DB::table('temporary_training')->where(['employee_id'=>$result])->delete();
        }
        else{
            for ($k = 0; $k<count($array); $k++){
                //dd($designation[$k]);
                $request1=DB::table('employees')
                ->select('employees.id','employees.empDesignationId')
                ->where(['id'=>$array[$k]])->first();

                $current_time = Carbon::now()->toDateTimeString();
                $request2=DB::table('temporary_training')
                ->select('temporary_training.employee_id')
                ->where(['temporary_training.employee_id'=>$array[$k]])->first();

                //dd($request1->id);

                if($request2==null){     
                    $insert=DB::table('temporary_training')->insert([
                        'employee_id' =>$request1->id,
                        'designation_id' =>$request1->empDesignationId,
                        'created_at' =>$current_time,
                        'updated_at' =>$current_time,
                    ]);
                }
                
            }
        }
        $employeesByDesignation=DB::table('temporary_training')
        ->join('employees','employees.id','=','temporary_training.employee_id')
        ->join('designations','designations.id','=','employees.empDesignationId')
        ->select('temporary_training.id','employees.employeeId','employees.empFirstName','employees.empLastName','designations.designation')
        ->groupBy('temporary_training.id')
        ->get();

        return view('training.employeeByDesignation',compact('employeesByDesignation'));
    }

    public function DeleteFromnTable($delete){
        //dd($delete);
        DB::table('temporary_training')->where(['id'=>$delete])->delete();

    }


}
