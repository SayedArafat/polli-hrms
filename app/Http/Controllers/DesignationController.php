<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DesignationController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'middleware'=>'check-permission:admin|hr',
        ]);
    }

    public function index()
    {
        $designations=DB::table('designations')->get();
        return view('settings.designation.index',compact('designations'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);
        $now=Carbon::now()->toDateTimeString();

        $des=DB::table('designations')->insert([
            'designation'=>$request->name,
            'description'=>$request->description,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);
        if($des){
            Session::flash('message','Data inserted Successfully');
            return redirect("settings/designation");
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);
        $now=Carbon::now()->toDateTimeString();
        $upd=DB::table('designations')->where(['id'=>$id])->update([
            'designation'=>$request->name,
            'description'=>$request->description,
            'updated_at'=>$now,
        ]);
        if($upd)
        {
            Session::flash('edit', "Designation Info Successfully Updated ");
        }
        return redirect("settings/designation");
    }

    public function destroy($id)
    {
        $check=DB::table('employees')->where(['empDesignationId'=>$id])->get();
        if(count($check)){
            Session::flash('edit',"Can't delete. There are already some employees bearing this designation." );
        }
        else {
            DB::table('designations')->where(['id'=>$id])->delete();
            Session::flash('delete', "Successfully Deleted Data");
        }
        return redirect("settings/designation");
    }
}
