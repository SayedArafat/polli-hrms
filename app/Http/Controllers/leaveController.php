<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
//use Illuminate\Support\Facades\DB;
use Redirect;
use Session;
use auth;

class leaveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){

        // $data=DB::table('tb_leave_application')->
        // join('employees', 'employees.id','=','tb_leave_application.employee_id')->
        // join('designations', 'designations.id','=','employees.empDesignationId')->
        // leftjoin('tb_employee_leave',
        // ['tb_employee_leave.employee_id'=>'tb_leave_application.employee_id', 
        // 'tb_employee_leave.leave_type_id'=>'tb_leave_application.leave_type_id'])->
        // join('tb_leave_type','tb_leave_type.id','=','tb_leave_application.leave_type_id')
        // ->select('employees.*','employees.id as eID','designations.designation','tb_employee_leave.leave_available',
        //         'tb_leave_type.leave_type','tb_leave_application.*')
        // //->select('leave_available')
        // ->where('tb_leave_application.status','=',0)
        // //->whereYear(['tb_employee_leave.year'=>'tb_leave_application.leave_ending_date'])
        // //->whereYear('leave_ending_date', '=',date("Y"))
        // ->groupBy('tb_leave_application.id')->get();
        // //dd($data);

        $query=@"SELECT employees.*,employees.id as eID,designations.designation,tb_employee_leave.leave_available,
                tb_leave_type.leave_type,tb_leave_application.*
                FROM tb_leave_application
                JOIN employees ON employees.id=tb_leave_application.employee_id 
                JOIN designations ON designations.id=employees.empDesignationId
                LEFT JOIN tb_employee_leave ON tb_employee_leave.employee_id=tb_leave_application.employee_id 
                AND tb_employee_leave.leave_type_id=tb_leave_application.leave_type_id 
                AND tb_employee_leave.year=YEAR(tb_leave_application.leave_ending_date)
                JOIN tb_leave_type ON tb_leave_type.id=tb_leave_application.leave_type_id
                WHERE tb_leave_application.status=0 
                GROUP BY tb_leave_application.id DESC" ;
        
        $data= DB::select(DB::raw($query));

        $data2=DB::table('tb_leave_application')->
        join('employees', 'employees.id','=','tb_leave_application.employee_id')->
        join('designations', 'designations.id','=','employees.empDesignationId')->
        leftjoin('tb_employee_leave',['tb_employee_leave.employee_id'=>'tb_leave_application.employee_id', 'tb_employee_leave.leave_type_id'=>'tb_leave_application.leave_type_id'])->
        join('tb_leave_type','tb_leave_type.id','=','tb_leave_application.leave_type_id')
        ->select('employees.*','employees.id as eID','designations.designation','tb_employee_leave.*',
                'tb_leave_type.leave_type','tb_leave_application.*')
        ->where('tb_leave_application.status','=',1)
        ->groupBy('tb_leave_application.id')->get();

        $data3=DB::table('tb_leave_application')->
        join('employees', 'employees.id','=','tb_leave_application.employee_id')->
        join('designations', 'designations.id','=','employees.empDesignationId')->
        leftjoin('tb_employee_leave',['tb_employee_leave.employee_id'=>'tb_leave_application.employee_id', 'tb_employee_leave.leave_type_id'=>'tb_leave_application.leave_type_id'])->
        join('tb_leave_type','tb_leave_type.id','=','tb_leave_application.leave_type_id')
        ->select('employees.*','designations.designation','tb_employee_leave.*',
                'tb_leave_type.leave_type','tb_leave_application.*')
        ->where('tb_leave_application.status','=',2)
        ->groupBy('tb_leave_application.id')->get();


       
        return view('leave.index',compact('data','data2','data3'));
    }

    public static function totalLeave($leaveID){
        $leave=DB::table('tb_leave_type')
                ->select('total_days')
                ->where(['id'=>$leaveID])
                ->first()->total_days;

        return($leave);
    }

    public function leave_settings(Request $request){
        $current_time = Carbon::now()->toDateTimeString();
        $leave_type=DB::table('tb_leave_type')->insert([
            'leave_type' =>$request->leave_type,
            'total_days' =>$request->total_days,
            'policy' =>$request->policy,
            'created_by' =>$request->user_name,
            'modified_by' =>$request->user_name,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if( $leave_type){
            Session::flash('message', 'Leaves Store Successful!');
            return redirect()->back();
        }
    }
    public function leave_sett(){
        $data=DB::table('tb_leave_type')->latest()->get();
        $festival_data=DB::table('tb_festival_leave')->latest()->get();

        $week2=DB::table('week_leave')->where(['status'=>1])
        ->select('day')->get();
       $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
       $placeholder = implode(', ', $weekends);
        return view('leave.leave_settings',compact('data','festival_data','week2','placeholder'));
    }


    public function leave_update(Request $request){
        $id=$request->leave_id;
        $current_time = Carbon::now()->toDateTimeString();
        $leave_type=DB::table('tb_leave_type')->WHERE('id',$id)->update([
            'leave_type' =>$request->leave_type,
            'total_days' =>$request->total_days,
            'policy' =>$request->policy,
            'created_by' =>$request->user_name,
            'modified_by' =>$request->user_name,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if( $leave_type){
            Session::flash('message', 'Leaves Store Successful!');
            return redirect()->back();
        }
    }
    public function festival_leave_update(Request $request){
        $id=$request->festival_leave_id;
        $current_time = Carbon::now()->toDateTimeString();
        $festival_leave_type=DB::table('tb_festival_leave')->WHERE('id',$id)->update([
            'year' =>$request->year,
            'start_date' =>$request->startDate,
            'end_date' =>$request->endDate,
            'purpose' =>$request->purpose,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if( $festival_leave_type){
            Session::flash('message', 'Leaves Store Successful!');
            return redirect()->back();
        }
    }
    public function leave_delete($id){
        $data=DB::table('tb_leave_type')->WHERE('id',$id)->delete();
        if($data){
            Session::flash('message', 'Grade Delete Successful!');
            return redirect()->back();
        }
    }
    public function festival_leave_delete($id){
        $data=DB::table('tb_festival_leave')->WHERE('id',$id)->delete();
        if($data){
            Session::flash('message', 'Grade Delete Successful!');
            return redirect()->back();
        }
    }
    public function add_festival_leave(Request $request){
        $current_time = Carbon::now()->toDateTimeString();
        $festive_leave=DB::table('tb_festival_leave')->insert([
            'year' =>$request->year,
            'start_date' =>$request->startDate,
            'end_date' =>$request->endDate,
            'purpose' =>$request->purpose,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if($festive_leave){
            Session::flash('message', 'Festival Leave Added');
        }
        return redirect('leave_sett');
    }


    public function leaveHistory(Request $request){
        $emp_id=auth::user()->emp_id;
        $leave_history=DB::table('tb_leave_application')
        ->join('tb_employee_leave','tb_employee_leave.employee_id','=','tb_leave_application.employee_id')
        ->join('tb_leave_type','tb_leave_application.leave_type_id','=','tb_leave_type.id')
        ->select('tb_leave_type.leave_type','tb_leave_application.id','tb_leave_application.leave_starting_date','tb_leave_application.leave_ending_date','tb_leave_application.created_at','tb_leave_application.status')
        ->WHERE('tb_leave_application.status','!=',0)
        ->Where('tb_employee_leave.employee_id','=',$emp_id)
        ->groupBy('tb_leave_application.id')
        ->orderBy('tb_leave_application.id', 'DESC')
        ->get();

        return view('leave.leave_history',compact('leave_history'));
    }
    public function employeeLeaveHistory(Request $request){
        $emp_id=$request->id;
        $leave_history=DB::table('tb_leave_application')
        ->join('tb_employee_leave','tb_employee_leave.employee_id','=','tb_leave_application.employee_id')
        ->join('tb_leave_type','tb_leave_application.leave_type_id','=','tb_leave_type.id')
        ->select('tb_leave_type.leave_type','tb_leave_application.id','tb_leave_application.leave_starting_date','tb_leave_application.leave_ending_date','tb_leave_application.created_at','tb_leave_application.status')
        ->WHERE('tb_leave_application.status','!=',0)
        ->Where('tb_employee_leave.employee_id','=',$emp_id)
        ->groupBy('tb_leave_application.id')
        ->orderBy('tb_leave_application.id', 'DESC')
        ->get();

        return view('leave.leave_history',compact('leave_history'));
    }


    public function request_leave(Request $request){
        $leaveType = DB::table('tb_leave_type')->select('leave_type','id')->get();
        
        $leaveType2 = DB::table('tb_leave_type')
        ->select('tb_leave_type.*')
        ->get();
        
        $emp_id=auth::user()->emp_id;
        $data=DB::table('tb_leave_type')
            ->leftjoin('tb_employee_leave', 'tb_employee_leave.leave_type_id','=','tb_leave_type.id')
            //->select('tb_employee_leave.*')
           ->WHERE('tb_employee_leave.employee_id','=',$emp_id)
            ->get();
            //dd($data);

        $emp_id=auth::user()->emp_id;
        $emp_gender=DB::table('employees')->WHERE(['id'=>$emp_id])->pluck('empGenderId')->toArray();
        //dd($emp_gender);

        $leave_history=DB::table('tb_leave_application')
        ->join('tb_leave_type','tb_leave_application.leave_type_id','=','tb_leave_type.id')
        ->select('tb_leave_type.leave_type','tb_leave_application.id','tb_leave_application.leave_starting_date','tb_leave_application.leave_ending_date','tb_leave_application.created_at','tb_leave_application.status')
        ->WHERE(['tb_leave_application.employee_id'=>$emp_id,'tb_leave_application.status'=>0])
        ->groupBy('tb_leave_application.id')
        ->orderBy('tb_leave_application.id', 'DESC')
        ->get();
        //dd($leave_history);

        return view('leave.leaveRequest',compact('emp_gender','leaveType','data','leaveType2','leave_history'));
    }



    public function store_leave_request(Request $request){
        $defaultStatus='Pending';
        
        if($file=$request->file('attachment')) {
            $name = time() . "_" .$request->emp_id. "_".$request->leave_type_id.$file->getClientOriginalName() ;
            $file->move('Leave_request_attachment', $name);
        }
        else{
            $name=null;
        }

        $date1=date("Y-m-d",strtotime($request->leave_starting_date));
        $date2=0;
        if(($request->leave_type_id)==1){
            $maternityLeaveDays=DB::table('tb_leave_type')->select('total_days')->where(['id'=>1])->first()->total_days;
            $date2=date("Y-m-d",strtotime($date1."+$maternityLeaveDays days"));
            $day=$maternityLeaveDays;
        }
        else{
            $date2=date("Y-m-d",strtotime($request->leave_ending_date));
            $day=$this->dayCalculator($date1,$date2);
        }
        dd($date2);

        $current_time = Carbon::now()->toDateTimeString();
        $store_leave_request=DB::table('tb_leave_application')->insert([
            'employee_id' =>$request->emp_id,
            'leave_type_id' =>$request->leave_type_id,
            'leave_starting_date' =>$date1,
            'leave_ending_date' =>$date2,
            'actual_days' =>$day,
            'status' =>0,
            'attachment'=>$name,
            'description' =>$request->description,
            'approved_by'=>$defaultStatus,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if( $store_leave_request){
            Session::flash('leavereq', "Your leave request added successful!");
            return redirect('request_leave');
        }
    }

    public function leaveRequestDelete($id){
        
        $data=DB::table('tb_leave_application')->WHERE('id',$id)->delete();
        if($data){
            Session::flash('leaveDelete', 'Your Application Deleted Succesfully!');
            return redirect()->back();
        }

    }

    public function leave_declined2($id){
        $current_time = Carbon::now()->toDateTimeString();
        $leave_declined=DB::table('tb_leave_application')->where(['id'=>$id])->update([
            'status'=>2,
            'approved_by'=>auth()->user()->name,
            'updated_at' =>$current_time,
        ]);
        if($leave_declined){
            Session::flash('message', "Leave Declined Successfully!");
           return redirect()->back();
        }
    }

    public static function dayCalculator($d1,$d2){
        //$request=DB::table('tb_leave_application')->where(['id'=>$id])->first();
        $date1=strtotime($d1);
        $date2=strtotime($d2);
        $interval1=1+round(abs($date1-$date2)/86400);
//        return $interval1;

        $festivalLeave = DB::table('tb_festival_leave')->get();
        $dcount=0;

        foreach($festivalLeave as $fleave){
            if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
               
            }
            else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
               
            }
            else{
                continue;
            }
        }
        $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();

        //dd($weekends);
        $start_date=date('Y-m-d', $date1);
        $end_date=date('Y-m-d', $date2);
       // dd($start_date,$end_date);
       $key=0;
        for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
            
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);
            $day = date('l', $timestamp);
            
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $key++;     
                }
            }
            
        }
        //dd($start_date,$end_date,$key);
        $interval=(int)$interval1-((int)$dcount+$key);
        //dd($start_date,$end_date,$key,$interval);
        return $interval;
    }

    public static function dayCalculator1($d1,$d2){
        //$request=DB::table('tb_leave_application')->where(['id'=>$id])->first();
        $date1=strtotime($d1);
        $date2=strtotime($d2);
        $interval1=1+round(abs($date1-$date2)/86400);

        $festivalLeave = DB::table('tb_festival_leave')->get();
        $dcount=0;

        foreach($festivalLeave as $fleave){
            if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
               
            }
            else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
               
            }
            else{
                continue;
            }
        }
        $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();

        //dd($weekends);
        $start_date=date('Y-m-d', $date1);
        $end_date=date('Y-m-d', $date2);
       // dd($start_date,$end_date);
       $key=0;
        for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
            
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);
            $day = date('l', $timestamp);
            
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $key++;     
                }
            }
            
        }
        //dd($start_date,$end_date,$key);
        $interval=(int)$interval1-((int)$dcount+$key);
        //dd($start_date,$end_date,$key,$interval);
        return $interval;
    }

    public function leave_declined($id){
         $current_time = Carbon::now()->toDateTimeString();
        $request=DB::table('tb_leave_application')->where(['id'=>$id])->first();
        $date1=$request->leave_starting_date;
        $date2=$request->leave_ending_date;
        $day=$this->dayCalculator($date1,$date2);

        $year=date("Y",strtotime($date2));

        if(($request->leave_type_id)==1){
            $delete=DB::table('tb_maternity_allowance')
            ->where(['other2'=>$id])->delete();
        }

        $request1=DB::table('tb_employee_leave')->
            join('tb_leave_application',['tb_employee_leave.employee_id'=>'tb_leave_application.employee_id', 'tb_employee_leave.leave_type_id'=>'tb_leave_application.leave_type_id'])
            ->groupBy('tb_employee_leave.id')
            ->where(['tb_leave_application.id'=>$id])
            ->where(['tb_employee_leave.year'=>$year])
            ->first();

    //     $request1 = DB::table('tb_leave_application')->
    //     join('tb_employee_leave',['tb_employee_leave.employee_id'=>'tb_leave_application.employee_id','tb_employee_leave.leave_type_id'=>'tb_leave_application.leave_type_id'])
    //    //->select('tb_employee_leave.*')
    //     ->where(['tb_leave_application.id'=>$id])
    //     ->where(['tb_employee_leave.year'=>$year])
    //     ->groupBy('tb_employee_leave.id')
    //     ->get();
        //dd($request1);

        $leaveTaken=$request1->leave_taken;
        $leaveAvailable=$request1->leave_available;
        
        $newLeaveTaken=$leaveTaken-$day;
        
        $newLeaveAvailable=$leaveAvailable+$day;

        //dd($newLeaveTaken,$newLeaveAvailable);

           $leave_declined=DB::table('tb_employee_leave')
           ->join('tb_leave_application',['tb_employee_leave.employee_id'=>'tb_leave_application.employee_id', 'tb_employee_leave.leave_type_id'=>'tb_leave_application.leave_type_id'])
            ->groupBy('tb_employee_leave.id')
            ->where(['tb_leave_application.id'=>$id])
            ->where(['tb_employee_leave.year'=>$year])
            ->update([
               'leave_taken' =>$newLeaveTaken,
               'leave_available' =>$newLeaveAvailable,
               'status'=>2,
                'approved_by'=>auth()->user()->name,
           ]);

        // $leave_declined2=DB::table('tb_leave_application')->where(['id'=>$id])->update([
        //     'status'=>2,
        //     'approved_by'=>auth()->user()->name,
        //     'updated_at' =>$current_time,
        // ]);
        if($leave_declined){
            Session::flash('message', "Leave Declined Successfully!");
           return redirect()->back();
        }
    }

    //approve after decline



    //admin approve leave request
    public function leave_accepted($id){

        $request=DB::table('tb_leave_application')->where(['id'=>$id])->first();
        $date1=$request->leave_starting_date;
        $date2=$request->leave_ending_date;

        if(($request->leave_type_id)==1){
            $interval=DB::table('tb_leave_type')->select('total_days')->where(['id'=>1])->first()->total_days;
        }
        else{
            $interval=$this->dayCalculator($date1,$date2);
        }
        $year=date("Y",strtotime($date2));

        $data = DB::table('tb_leave_application')->
        join('tb_employee_leave',['tb_employee_leave.employee_id'=>'tb_leave_application.employee_id','tb_employee_leave.leave_type_id'=>'tb_leave_application.leave_type_id'])
       //->select('tb_employee_leave.*')
        ->where(['tb_leave_application.id'=>$id])
        ->where(['tb_employee_leave.year'=>$year])
        ->groupBy('tb_employee_leave.id')
        ->get();
        $count = $data->count();

        
        
       if(($request->leave_type_id)==1){
        $current_time = Carbon::now()->toDateTimeString();
        $maternityLeaveDays=DB::table('tb_leave_type')->select('total_days')->where(['id'=>1])->first()->total_days;
        //dd($maternityLeaveDays);
        $empId=DB::table('tb_leave_application')->select('employee_id')->where(['id'=>$id])->first()->employee_id;
        $previousData=DB::table('tb_salary_history')->select('net_amount','working_day','month')
        ->where(['emp_id'=>$empId])
        ->where('month',">",Carbon::now()->subMonths(4))
        ->get();
        $ammount=0;
        $totalDay=0;
        //dd($previousData);
        if(count($previousData)){
        foreach($previousData as $previousData){
            $ammount=$ammount+($previousData->net_amount);
            $totalDay=$totalDay+($previousData->working_day);
        }

        $payPerDay=$ammount/$totalDay;
        $totalMaternityPay=$payPerDay*$maternityLeaveDays;
        $perInstalment=$totalMaternityPay/2;
        $payStatus='00';
        //dd($ammount,$totalDay,$payPerDay,(int)$totalMaternityPay,(int)$perInstalment,$date1,$date2);
        //dd($ammount,$totalDay,$payPerDay,$totalMaternityPay,$perInstalment);

        $maternityDataStore=DB::table('tb_maternity_allowance')->insert([
            'employee_id' =>$empId,
            'leave_starting_date' =>date("Y-m-d",strtotime($date1)),
            'leave_ending_date' =>date("Y-m-d",strtotime($date2)),
            'previous_three_month_salary' =>$ammount,
            'total_working_days' =>$totalDay,
            'total_payable_amount' =>$totalMaternityPay,
            'per_instalment' =>$perInstalment,
            'other2' =>$id,
            'other3' =>$payStatus,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        }
       }

       if($count==0){
          
           $totalLeave=DB::table('tb_leave_application')
            ->join('tb_leave_type','tb_leave_application.leave_type_id','=','tb_leave_type.id')
            ->select('tb_leave_type.total_days')
            ->where(['tb_leave_application.id'=>$id])
            ->first()->total_days;

           $leaveTaken=$interval;

           $leaveAvailable=$totalLeave-$leaveTaken;
           //dd($leaveAvailable);

            $current_time = Carbon::now()->toDateTimeString();
            $leave_accepted=DB::table('tb_employee_leave')->insert([
                'employee_id' =>$request->employee_id,
                'leave_type_id' =>$request->leave_type_id,
                'leave_taken' =>$leaveTaken,
                'leave_available' =>$leaveAvailable,
                'year' => $year,
                'created_at' =>$current_time,
                'updated_at' =>$current_time,
            ]);
           $leave_accepted2=DB::table('tb_leave_application')->where(['id'=>$id])->update([
               'status'=>1,
               'approved_by'=>auth()->user()->name,
               'updated_at' =>$current_time,
           ]);

           if($leave_accepted&&$leave_accepted2){
               Session::flash('message', "Leave approved successfully!");
              return redirect()->back();
           }

       }

       else{
            $day=$interval;


        $request1=DB::table('tb_employee_leave')->
            join('tb_leave_application',['tb_employee_leave.employee_id'=>'tb_leave_application.employee_id', 'tb_employee_leave.leave_type_id'=>'tb_leave_application.leave_type_id'])
                ->groupBy('tb_employee_leave.id')
                ->where(['tb_leave_application.id'=>$id])
                ->where(['tb_employee_leave.year'=>$year])
                ->first();

        $leaveTaken=$request1->leave_taken;
        $leaveAvailable=$request1->leave_available;
        
        $newLeaveTaken=$leaveTaken+$day;
        
        $newLeaveAvailable=$leaveAvailable-$day;

        //dd($newLeaveTaken,$newLeaveAvailable);

        $current_time = Carbon::now()->toDateTimeString();
           $leave_accepted=DB::table('tb_employee_leave')
           ->join('tb_leave_application',['tb_employee_leave.employee_id'=>'tb_leave_application.employee_id', 'tb_employee_leave.leave_type_id'=>'tb_leave_application.leave_type_id'])
            ->groupBy('tb_employee_leave.id')
            ->where(['tb_leave_application.id'=>$id])
            ->where(['tb_employee_leave.year'=>$year])  
            ->update([
               'leave_taken' =>$newLeaveTaken,
               'leave_available' =>$newLeaveAvailable,
               'status'=>1,
                'approved_by'=>auth()->user()->name,
           ]);
    

           if($leave_accepted){
               Session::flash('message', "Leave approved successfully!");
               return redirect()->back();
           }
        }
    }
    public function addweekLeave($weekleave){
        $leaveDay=explode(",",$weekleave);
        //dd(count($array));
        $tableStatus=DB::table('week_leave')->where(['status'=>1])
        ->pluck('day-id')->unique()->toArray();

        $result = array_values(array_diff($tableStatus, $leaveDay));
        //dd($leaveDay,$tableStatus,$result,count($result));
        $current_time = Carbon::now()->toDateTimeString();
        for ($i = 0; $i <count($leaveDay); $i++){
            $weeklv=DB::table('week_leave')->where(['day-id'=>$leaveDay[$i]])->update([
            'status'=>1,
            'updated_at' =>$current_time,
            ]);
        }
        for ($i = 0; $i <count($result); $i++){ 
            
            $weeklv=DB::table('week_leave')->where(['day-id'=>$result[$i]])->update([
            'status'=>0,
            'updated_at' =>$current_time,
            ]);
            
        }
        $week=DB::table('week_leave')->where(['status'=>1])
        ->select('day')->get();

        return view('leave.weekLeave',compact('week'));

    }
    public function maternityLeaveHistory(){
        $maternityLeaveHistory=DB::table('tb_maternity_allowance')->
        join('employees', 'employees.id','=','tb_maternity_allowance.employee_id')
        ->join('designations', 'designations.id','=','employees.empDesignationId')
        ->select('employees.employeeId','employees.empFirstName','employees.empLastName',
        'designations.designation','tb_maternity_allowance.*')->get();
        
        return view('leave.maternityLeaveHistory',compact('maternityLeaveHistory'));

        
    }

    public function pay_first_installment($id){
        $current_time = Carbon::now()->toDateTimeString();
        $first_installment=DB::table('tb_maternity_allowance')->where(['id'=>$id])->update([
            'other3'=>'10',
            'first_installment_pay'=>date("Y-m-d"),
            'approved_by'=>auth()->user()->name,
            'updated_at' =>$current_time,
        ]);

        if($first_installment){
            Session::flash('message1', "First installment paid successfully!");
           return redirect()->back();
        }
    }

    public function pay_second_installment($id){
        $current_time = Carbon::now()->toDateTimeString();
        $second_installment=DB::table('tb_maternity_allowance')->where(['id'=>$id])->update([
            'other3'=>'11',
            'second_installment_pay'=>date("Y-m-d"),
            'approved_by'=>auth()->user()->name,
            'updated_at' =>$current_time,
        ]);

        if($second_installment){
            Session::flash('message2', "Second installment paid successfully!");
           return redirect()->back();
        }
    }

    public function employeeHistoryModal($id){
        $leave_history_modal=DB::table('tb_leave_application')
        ->join('tb_leave_type','tb_leave_application.leave_type_id','=','tb_leave_type.id')
        // ->join('employees','employees.id','=','tb_leave_application.employee_id')
        // ->join('designations','designations.id','=','employees.empDesignationId')
        //->select('tb_leave_type.leave_type','tb_leave_application.id','tb_leave_application.leave_starting_date','tb_leave_application.leave_ending_date','tb_leave_application.created_at','tb_leave_application.status')
        ->WHERE(['tb_leave_application.status'=>1])
        ->Where(['tb_leave_application.employee_id'=>$id])
        ->groupBy('tb_leave_application.id')
        ->orderBy('tb_leave_application.id', 'DESC')
        ->get();
        //dd($leave_history_modal);
       // return view('leave.employee_leave_history_modal',compact('leave_history_modal'));
        return response()->json($leave_history_modal);
    }
    public function assignLeave(){
        $employees= DB::table('employees')
        ->select(DB::raw("CONCAT(empFirstName,' ',empLastName,' , ID : ',employeeId)AS full_name, id"))
        ->pluck('full_name','id')->all();
       
        $leaveType = DB::table('tb_leave_type')->select('leave_type','id')->get();

        return view('leave.assignLeave',compact('employees','leaveType'));

    }
    public function mannualAssignLeave(Request $request){
        $current_time = Carbon::now()->toDateTimeString();
        if($file=$request->file('attachment')) {
            $name = time() . "_" .$request->employee_id. "_".$request->leave_type_id.$file->getClientOriginalName() ;
            $file->move('Leave_request_attachment', $name);
        }
        else{
            $name=null;
        }


        $defaultStatus='Pending';
        
        $date1=date("Y-m-d",strtotime($request->startDate));
        $date2=0;
        
        
        if(($request->leave_type_id)==1){
            $maternityLeaveDays=DB::table('tb_leave_type')->select('total_days')->where(['id'=>1])->first()->total_days;
            $date2=date("m/d/Y",strtotime($date1."+$maternityLeaveDays days"));
            $day=$maternityLeaveDays;
        }
        else{
            $date2=$request->endDate;
            $day=$this->dayCalculator($date1,$date2);
        }
        //dd($day);
        //dd($date1,date("Y-m-d",strtotime($date2)));
        $addLeaveApplication=DB::table('tb_leave_application')->insert([
            'employee_id' =>$request->employee_id,
            'leave_type_id' =>$request->leave_type_id,
            'leave_starting_date' =>$date1,
            
            'leave_ending_date' =>date("Y-m-d",strtotime($date2)),
            'actual_days' =>$day,
            'status' =>0,
            'attachment'=>$name,
            'description' =>$request->leaveReason,
            'approved_by'=>$defaultStatus,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if( $addLeaveApplication){
            Session::flash('applySuccess', 'Leave Successfully Applied for Approval!');
            return redirect()->back();
        }
    }
    public function employeeLeaveLeft($id){
        $value=explode(",",$id);
        if($value[0]!=null&&$value[1]!=null){
        $data=DB::table('tb_leave_application')
            ->join('employees', 'employees.id','=','tb_leave_application.employee_id')
            ->join('designations', 'designations.id','=','employees.empDesignationId')
            ->leftjoin('tb_employee_leave',
            ['tb_employee_leave.employee_id'=>'tb_leave_application.employee_id',
            'tb_employee_leave.leave_type_id'=>'tb_leave_application.leave_type_id'])
            ->join('tb_leave_type','tb_leave_type.id','=','tb_leave_application.leave_type_id')
            ->select('employees.empFirstName','tb_employee_leave.leave_available',
            'employees.empLastName','employees.employeeId as eID','tb_leave_type.leave_type',
            'designations.designation','tb_leave_type.total_days')
            ->where('tb_leave_application.status','=',1)
            ->where(['tb_leave_application.employee_id'=>$value[0]])
            ->where(['tb_leave_application.leave_type_id'=>$value[1]])
            //->where(['tb_employee_leave.year'=>$year])
            ->groupBy('tb_leave_type.total_days')->get();

            $count = $data->count();
            //dd($count);
            if($count!=0){
                //dd($data);
            return view('leave.employeeLeaveAjazTable',compact('data'));
            }
            else{
                $leaveData=DB::table('tb_leave_type')
                ->where(['tb_leave_type.id'=>$value[1]])
                //->groupBy('tb_leave_type.total_days')
                ->get();

                $employeeData=DB::table('employees')
                ->join('designations', 'designations.id','=','employees.empDesignationId')
                ->where(['employees.id'=>$value[0]])
                ->get();

                //dd($leaveData,$employeeData);
                return view('leave.employeeLeaveAjazTable2',compact('leaveData','employeeData'));
            }
        }
        
    }
    public function earnLeave(){
        $employee_benefit=DB::table('employees')
        ->join('tb_leave_application','tb_leave_application.employee_id','=','employees.id')
        ->join('tb_salary_history','tb_salary_history.emp_id','=','employees.id')
        ->join('tb_employee_leave','tb_employee_leave.employee_id','=','employees.id')
        ->join('tb_leave_type','tb_leave_type.id','=','tb_employee_leave.leave_type_id')
        ->join('designations','designations.id','=','employees.empDesignationId')
        ->select('empFirstName','empLastName','employeeId','designation','empJoiningDate',
        'leave_taken as enjoyed','total_days','leave_available','month','net_amount')
        ->where(['tb_leave_application.leave_type_id'=>4, 'tb_leave_application.status'=>1])
        ->where(['tb_employee_leave.leave_type_id'=>4])
        ->where('empJoiningDate',"<",Carbon::now()->subMonths(12))
        //->where('month',">",Carbon::now()->subMonths(2))
        ->whereMonth('month', '=',date("m",strtotime("previous month")))
        ->groupBy('tb_leave_application.employee_id')
        ->get();
   //dd($employee_benefit);

    $employee_without_leave_benefit=DB::table("employees")
        ->leftjoin('tb_salary_history','tb_salary_history.emp_id','=','employees.id')
        ->join('designations','designations.id','=','employees.empDesignationId')
        
        ->whereNotIn('employees.id',function($query) {
            $query->select('employee_id')->from('tb_leave_application')
            ->where(['tb_leave_application.leave_type_id'=>4])
            ->where(['tb_leave_application.status'=>1]);
        })
        ->select('empFirstName','empLastName','employeeId','designation','empJoiningDate','month','net_amount')
        ->where('empJoiningDate',"<",Carbon::now()->subMonths(12))
        //->whereMonth('month',"=",Carbon::now()->subMonths(1))
        ->whereMonth('month', '=',date("m",strtotime("previous month")))
        ->get();
        //dd(date("m"));
        //dd($employee_without_leave_benefit);

    $earnLeave=DB::table("tb_leave_type")
            ->where(['id'=>4])
            ->get();

       // dd($employee_without_leave_benefit, $earnLeave);
        return view('leave.earnLeaveBenefit',compact('employee_benefit','employee_without_leave_benefit','earnLeave'));
    }
}

