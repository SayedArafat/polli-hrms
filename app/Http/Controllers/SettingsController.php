<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use Session;
use Carbon\Carbon;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings=DB::table('settings')->latest()->get();
        return view('settings.system_settings.index',compact('settings'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function settings_store(Request $request)
    {
        $file=$request->file('logo_name');
        $current_time = Carbon::now()->toDateTimeString();
        $csv_file=time()."_".$request->file('logo_name')->getClientOriginalName();
        $file->move(public_path('\logo'),time()."_".$file->getClientOriginalName());
        $store_logo=DB::table('settings')->insert([
            'logo' =>$csv_file,
            'copyright' =>$request->copyright,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if($store_logo){
            Session::flash('message', 'Store Successful!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function settings_update(Request $request)
    {
      $id=$request->settings_hidden_id;
        $file=$request->file('logo_name');
        $current_time = Carbon::now()->toDateTimeString();
        $csv_file=time()."_".$request->file('logo_name')->getClientOriginalName();
        $file->move(base_path('\file'),time()."_".$file->getClientOriginalName());
        $update_settingss=DB::table('settings')->WHERE('id',$id)->update([
            'logo' =>$csv_file,
            'copyright' =>$request->copyright,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if($update_settingss){
            Session::flash('message', 'Update Successful!');
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function settings_delete($id)
    {
        $data=DB::table('settings')->WHERE('id',$id)->delete();
        if($data){
            Session::flash('message', 'Delete Successful!');
            return redirect()->back();
        }
    }
}
