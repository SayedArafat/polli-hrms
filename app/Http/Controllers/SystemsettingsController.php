<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Redirect;
use Session;

class SystemsettingsController extends Controller
{
    //user view method
    public function index(){
        $userData=DB::table('users')->latest()->get();
        return view('settings.user.index',compact('userData'));
    }
	
	//user password update method
	public function user_pass_update(Request $request){
		$id=$request->user_id;
		$password= Hash::make($request['user_password']);
		$userdata=DB::table('users')->WHERE('id',$id)->update([
		 'password' => $password
		]);
		if($userdata){
		  Session::flash('message', 'User Password Update Successful!'); 
		  return redirect()->back();
		}
	}

	//company information view page
    public function companyInformationview(){
	    $data=DB::table('tbcompany_information')->get();
	    return view('settings.company.index',compact('data'));
    }
    //update company information
    public function companyInformationupdate(Request $request){
       $id=$request->company_hidden_id;
       $data=DB::table('tbcompany_information')->where('id',$id)->update([
           'company_name'     =>$request->company_name,
           'company_phone'    =>$request->company_phone,
           'company_email'    =>$request->company_email,
           'company_address1' =>$request->company_address_one,
           'company_address2' =>$request->company_address_two,
       ]);
       if($data){
           Session::flash('companymessage', 'Information Update Successful!');
           return redirect()->back();
       }
    }
}
