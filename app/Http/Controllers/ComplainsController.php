<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ComplainsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('complains.index');
    }


    public function alllist()
    {
         $complains = DB::table('tbcomplains')
            ->leftJoin('users', 'tbcomplains.createdBy', '=', 'users.id')
            ->select('tbcomplains.*', 'users.name as created_by')
            ->get();
        return datatables($complains)
        ->addColumn('complainDate', function ($complains) {
                return Carbon::parse($complains->complainDate)->format('d-M-Y');

        })
        ->addColumn('status', function ($complains) {
            if($complains->complainStatus==0){ 
                $data = 'Pending';
            }elseif($complains->complainStatus==1){ 
                $data = 'On Process';
            }elseif($complains->complainStatus==2){ 
                $data = 'Completed';
            }elseif($complains->complainStatus==3){ 
                $data = 'Rejected';
            }else{
                $data = 'Not defined';
            }
            return $data;
        })
        ->addColumn('action', function ($complains) {
            if($complains->complainStatus==0){ 
                $data = '<span style="font-size:13px;color:blue;"><b>Pending</b></span>';
            }elseif($complains->complainStatus==1){ 
                $data = '<span style="font-size:13px;color:#0087d6;"><b>On Process</b></span>';
            }elseif($complains->complainStatus==2){ 
                $data = '<span style="font-size:13px;color:green;"><b>Completed</b></span>';
            }elseif($complains->complainStatus==3){ 
                $data = '<span style="font-size:13px;color:red;"><b>Rejected</b></span>';
            }else{
                $data = '<span style="font-size:13px;color:yellow;">Not defined</b></span>';
            }
                return '<a href="#" data-toggle="modal" data-target="#viewmodel'.$complains->id.'" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> Details</a>

                 <div class="modal fade" id="viewmodel'.$complains->id.'" role="dialog">
                                <div class="modal-dialog">
                                
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title"><b>Complain Details</b></h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Complain Date:</label>
                                                <br />

                                                '.Carbon::parse($complains->complainDate)->format('d-M-Y').'
                                            </div>
                                            <br />
                                            <br />
                                            <div class="form-group">
                                                <label>Complainer Name:</label>
                                                <br />
                                                '.$complains->complainerName.'
                                            </div>
                                            <br />
                                            <br />

                                            <div class="form-group">
                                                <label>Complainer Phone:</label>
                                                <br />
                                                '.$complains->complainerPhone.'
                                            </div>
                                            <br />
                                            <br />

                                            <div class="form-group">
                                                <label>Complainer Address:</label>
                                                <br />
                                                '.$complains->complainerAddress.'
                                            </div>
                                            <br />
                                            <br />

                                            <div class="form-group">
                                                <label>Complain Message:</label>
                                                <br />
                                                <span style="font-size:13px;color:#9a0000;">'.$complains->complainMessage.'</span>
                                            </div>
                                            <br />
                                            <br /> 

                                            <div class="form-group">
                                                <label>Complain Status:</label>
                                                <br />'.$data.'
                                            </div>
                                            <br />
                                            <br />
                                            <div class="form-group">
                                                <label>Reason for Rejected:</label>
                                                <br />'.$complains->complainRejectReason.'
                                            </div>
                                            <br />
                                            <br />

                                            
                                      </div>
                                      <div class="modal-header">
                                          
                                      </div>
                                  
                                    </div>
                                </div>
                            </div>



                            <a href="'.route('complains.edit', $complains->id).'" class="btn btn-xs btn-danger"><i class="fa fa-refresh"></i> Change Status</a>

                          

                ';
            })
        ->make(true);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('complains.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
                
        $this->validate($request,[
            'complainDate'=>'required',
            'complainerName'=>'required',
            'complainerPhone'=>'required',
            'complainMessage'=>'required',
        ]);

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();


        $str = DB::table('tbcomplains')->insert([
            'complainDate'=>Carbon::parse($request->complainDate)->format('Y-m-d'),
            'complainerName'=>$request->complainerName,
            'complainerPhone'=>$request->complainerPhone,
            'complainerAddress'=>$request->complainerAddress,
            'complainMessage'=>$request->complainMessage,
            'complainStatus'=>'0',
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);

       if($str)
        {
            Session::flash('message','Complain has been successfully added and already sent confirmation message to complainer. ');
        }else{
            Session::flash('failedMessage','Complain information adding failed.');
        }
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $complains = DB::table('tbcomplains')
        ->leftJoin('users', 'tbcomplains.createdBy', '=', 'users.id')
        ->select('tbcomplains.*', 'users.name as created_by')
        ->where('tbcomplains.id', '=', $id)
        ->first();
      return view('complains.edit',compact('id','complains'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();


        $str = DB::table('tbcomplains')->where(['id'=>$id])->update([
            'complainStatus'=>$request->status,
            'complainRejectReason'=>$request->complainRejectReason,
            'createdBy'=>$user->id,
            'updated_at'=>$now,
          ]);

       if($str)
        {
            Session::flash('message','Complain status has been successfully changed');
        }else{
            Session::flash('failedMessage','Complain status has been failed to changed.');
        }
        return redirect('/complains');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
