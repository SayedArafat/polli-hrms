<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use Session;
use Carbon\Carbon;

class SalaryController extends Controller
{
    //salary view method
    public function index(){
   //employee salary view
        $grade_name=DB::table('payroll_grade')->get();
        $data = DB::table('payroll_salary')
            ->join('employees', 'employees.id', '=', 'payroll_salary.emp_id')
            ->join('payroll_grade', 'payroll_grade.id', '=', 'payroll_salary.grade_id')
            ->select('payroll_salary.*', 'employees.empFirstName','employees.employeeId','payroll_grade.grade_name')
            ->where('employees.empAccStatus','=',1)
            ->paginate(150);
            return view('payroll.salary_view',compact('data','grade_name'));
        }
    //salary store method
    public function store (Request $request){
      $current_time = Carbon::now()->toDateTimeString();
      $total=$request->basic+$request->houserant+$request->medical+$request->transport+$request->foods+$request->others;
      $data=DB::table('payroll_salary')->insert([
          'emp_id' =>$request->emp_select,
          'grade_id' =>$request->grade_select,
          'basic_salary' =>$request->basic,
          'house_rant' =>$request->houserant,
          'medical' =>$request->medical,
          'transport' =>$request->transport,
          'food' =>$request->foods,
          'other' =>$request->others,
          'total_employee_salary' =>$total,
          'current_month' =>$request->current_month,
          'created_at' =>$current_time,
          'updated_at' =>$current_time,
      ]);
      if($data){
          Session::flash('message', 'Salary Store Successful!');
          return redirect('/employee/'.$request->emp_select);
      }
    }

    //id wise grade select and json response
    public function gradeWisesalaryUpdate($id){
        $grade_wise_salary=DB::table('payroll_grade')->where('id',$id)->get();
        return response()->json($grade_wise_salary);
    }

    //payroll_salary data edit with modal open
    public function employeeSalaryEditmodal($id){
        $data=DB::SELECT("SELECT payroll_salary.id as default_id,payroll_salary.emp_id,payroll_salary.grade_id,payroll_salary.basic_salary,payroll_salary.house_rant,payroll_salary.medical,payroll_salary.transport,payroll_salary.food,payroll_salary.other as default_others,payroll_salary.total_employee_salary,payroll_grade.id as grades_id,payroll_grade.grade_name,employees.employeeId,employees.empFirstName,employees.empLastName
        FROM payroll_salary
        LEFT JOIN payroll_grade ON payroll_salary.grade_id = payroll_grade.id
        LEFT JOIN employees ON payroll_salary.emp_id = employees.id
        WHERE payroll_salary.id=$id");
        return response()->json($data);
    }

    //salary update method
    public function update(Request $request ){
        $current_time = Carbon::now()->toDateTimeString();
        $total=$request->basic+$request->house_rant+$request->medical+$request->transport+$request->foods+$request->others;
        $id=$request->salary_hidden_id;
        $data=DB::table('payroll_salary')->WHERE('id',$id)->update([
           'basic_salary' =>$request->basic,
           'emp_id' =>$request->emp_ac_id,
           'grade_id' =>$request->emp_grade_id,
           'house_rant' =>$request->house_rant,
           'medical' =>$request->medical,
           'transport' =>$request->transport,
           'food' =>$request->foods,
           'other' =>$request->others,
           'total_employee_salary' =>$total,
           'created_at' =>$current_time,
           'updated_at' =>$current_time,
        ]);
        if($data){
            Session::flash('message', 'Salary Update Successful!');
            return redirect('/salary_view');
        }
    }
}
