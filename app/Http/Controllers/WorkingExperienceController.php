<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class WorkingExperienceController extends Controller
{
    function __construct()
    {
        $this->middleware(['middleware'=>'check-permission:admin|hr']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'empCompanyName'=>'required',
            'empJobTile'=>'required',
        ]);

        if($file=$request->file('empWHattachment'))
        {
            if($file->getClientSize()<5000000) {
                $name = time() . "_" . session('emp_id') . "_" . $file->getClientOriginalName();
                $file->move('Employee_Working_Experience', $name);
            }
            else{
                Session::flash('fileSize','File size limit exceeded');
                return redirect('employee/'.session('emp_id'));
            }
        }
        else{
            $name=null;
        }

        $workExp=DB::table('employee_working_histories')->insert([
            'emp_id'=>session('emp_id'),
            'empCompanyName'=>$request->empCompanyName,
            'empJobTile'=>$request->empJobTile,
            'empJoiningDate'=>Carbon::parse($request->empJoiningDate)->format('Y-m-d H:i:s'),
            'empLeaveDate'=>Carbon::parse($request->empLeaveDate)->format('Y-m-d H:i:s'),
            'empWHDescription'=> $request->empWHDescription,
            'empWHattachment'=>$name,
            'created_by'=>Auth::user()->name,
            'modified_by'=>Auth::user()->name,
            'created_at'=>Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon::now()->toDateTimeString(),
        ]);
        if($workExp){
            Session::flash('message','Successfully inserted working Details');
        }
        return redirect('employee/'.session('emp_id'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        return "edit";
        $we=DB::table('employee_working_histories')->where(['id'=>$id])->first();
        return view("employee.modal.Working Experience.editForm",compact('we','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'empCompanyName'=>'required',
            'empJobTile'=>'required',
        ]);

        $table=DB::table('employee_working_histories')->where(['id'=>$id]);

        if($file=$request->file('empWHattachment'))
        {
            if($file->getClientSize()<5000000) {
                $name = time() . "_" . session('emp_id') . "_" . $file->getClientOriginalName();
                $file->move('Employee_Working_Experience', $name);
                if($table->first()->empWHattachment) {
                    $path = public_path() . "/Employee_Working_Experience/" . $table->first()->empWHattachment;
                    if (file_exists($path)) {
//                    return $path;
                        unlink($path);
                    }
                }
            }
            else{
                Session::flash('fileSize','File size limit exceeded');
                return redirect('employee/'.session('emp_id'));
            }
        }
        else{
            $name=$table->first()->empWHattachment;

        }
        $store=DB::table('employee_working_histories')->where(['id'=>$id])->update([
            'empCompanyName'=>$request->empCompanyName,
            'empJobTile'=>$request->empJobTile,
            'empJoiningDate'=>Carbon::parse($request->empJoiningDate)->format('Y-m-d H:i:s'),
            'empLeaveDate'=>Carbon::parse($request->empLeaveDate)->format('Y-m-d H:i:s'),
            'empWHDescription'=> $request->empWHDescription,
            'empWHattachment'=>$name,
            'modified_by'=>Auth::user()->name,
            'updated_at'=>Carbon::now()->toDateTimeString(),
        ]);
        if($store){
            Session::flash('message',"Working Experience Updated Successfully");
        }

        return redirect('employee/'.session('emp_id'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function showDelete($id){
        $we=DB::table('employee_working_histories')->where(['id'=>$id])->first();
        return view('employee.modal.Working Experience.deleteForm',compact('id','we'));
    }
    public function destroy($id)
    {
        $table=DB::table('employee_working_histories')->where(['id'=>$id]);
        if($table->first()->empWHattachment) {
            $path = public_path() . "/Employee_Working_Experience/" . $table->first()->empWHattachment;
            if (file_exists($path)) {
//                    return $path;
                unlink($path);
            }
        }
        $delete=$table->delete();
        if($delete){
            Session::flash('delete','Data successfully removed');
        }

        return redirect('employee/'.session('emp_id'));

    }
}
