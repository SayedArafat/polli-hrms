<?php

namespace App\Http\Controllers;

//use Illuminate\Contracts\Session\Session;
use App\Helper\AppHelper;
use Illuminate\Http\Request;
use App\attendancemodel;
//use DB;
use Excel;
//use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\DB;
use function MongoDB\BSON\toJSON;
use Redirect;
use Session;
use Carbon\Carbon;

class AttendanceController extends Controller
{
    public function index(){
        return view('attendance.index');
    }
    public function attendance_setup(){
        $data=DB::table('attendance_setup')->latest()->get();
        return view('attendance.attendance_setup',compact('data'));
    }
    public function settings_store(Request $request){
        //dd($request->entry_time,$request->exit_time,$request->max_entry_time,$request->min_exit_time);
        $current_time = Carbon::now()->toDateTimeString();
        $data=DB::table('attendance_setup')->insert([
            'entry_time' =>$request->entry_time,
            'exit_time'  =>$request->exit_time,
            'max_entry_time'=>$request->max_entry_time,
            'min_exit_time'=>$request->min_exit_time,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if($data){
            Session::flash('message', 'Settings Successful!');
            return redirect()->back();
        }
    }
    public function settings_update(Request $request){
        $current_time = Carbon::now()->toDateTimeString();
        $id=$request->settings_id;
        $data=DB::table('attendance_setup')->WHERE('id',$id)->update([
            'entry_time' =>   $request->entry_time,
            'exit_time'  =>   $request->exit_time,
            'max_entry_time'  =>   $request->max_entry_time,
            'min_exit_time'  =>   $request->min_exit_time,
            'created_at' =>   $current_time,
            'updated_at' =>   $current_time,
        ]);
        if($data){
            Session::flash('message', 'Settings Update Successful!');
            return redirect()->back();
        }
    }
    public function csv_upload(Request $request){
        $this->validate($request,[
            'upload-file'=>['required','mimes:xls,xlxs']
        ]);
        $file=$request->file('upload-file');
        $current_time = Carbon::now()->toDateTimeString();
        $csv_file=time()."_".$request->file('upload-file')->getClientOriginalName();
        $file->move(base_path('/file/'),time()."_".$file->getClientOriginalName());
//        return $csv_file;
        $store_scv=DB::table('attendance_file')->insert([
            'attachment' =>$csv_file,
            'date' =>$request->cur_date,
            'comment'=>$request->comment,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if($store_scv){
            Session::flash('message', 'File upload Successful!');
            return redirect('csv_view');
        }
    }
    public function csv_show(){
        $data=DB::table('attendance_file')->latest()->take(1)->get();
        return view('attendance.csv_view',compact('data'));
    }

    public function store(Request $request){
        $file_name=DB::table('attendance_file')->where(['id'=>$request->hidden_id])->first()->attachment;
        $path=base_path()."/file/$file_name";
//        $path=base_path()."/file/History Report 05-07.xls";
//        return $file_name;
        $data=Excel::load($path, function($reader){})->get();
        if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {
                $insert[] = [
                    'device_name' => $value->device_name,
                    'door' => $value->door,
                    'emp_no'=>$value->emp_no,
                    'card_no'=>$value->card_no,
                    'time'=>$value->time,
                    'event_explanation'=>$value->event_explanation,
                    'attendance_file_id'=>$request->hidden_id

                ];
            }
//            return $insert;
            if(!empty($insert)){
                $insertData = DB::table('attendance_machine_data')->insert($insert);
                if ($insertData) {
                    Session::flash('success', 'Your Data has successfully imported');
                }else {
                    Session::flash('error', 'Error inserting the data..');
                    return back();
                }
            }
        }

        $att=DB::select("SELECT attendance_machine_data.card_no, employees.id AS emp_id, MIN(TIME(attendance_machine_data.time)) as in_time, MAX(TIME(attendance_machine_data.time)) as out_time, DATE(attendance_machine_data.time) as date FROM attendance_machine_data JOIN employees ON employees.empCardNumber=attendance_machine_data.card_no GROUP BY attendance_machine_data.card_no, DATE(attendance_machine_data.time) HAVING attendance_machine_data.card_no is not null");
        $ids=[];
        foreach ($att as $key => $value) {
            $emp_id=$value->emp_id;
            if($emp_id!=null){
                $checkEmp = DB::table('attendance')->where(['emp_id' => $emp_id, 'date' => $value->date])->get();
                if(count($checkEmp)){
                    $id = $checkEmp[0]->id;
                    $ids[]=$id;
                    $ddate=$value->date;
                    $otime=$value->out_time;
                    if($checkEmp[0]->in_time>$otime)
                    {
                        $temp=$checkEmp[0]->in_time;
                        $checkEmp[0]->in_time=$otime;
                        $otime=$temp;
                    }
                    $updateInsert[]=[
                        'id'=>$id,
                        'emp_id'=>$emp_id,
                        'in_time'=>$checkEmp[0]->in_time,
                        'out_time'=>$otime,
                        'date'=>$ddate,

                    ];
                }
                else{
                    $secondInsert[] = [
                        'emp_id' => $emp_id,
                        'in_time' => $value->in_time,
                        'out_time' => $value->out_time,
                        'date' => $value->date,
                    ];
                }
            }
        }
//        return $ids;
//        return $secondInsert;
        if(!empty($ids))
        {
            DB::table('attendance')->whereIn('id',$ids)->delete();
//            return $updateInsert;
            DB::table('attendance')->insert($updateInsert);


        }

        if (!empty($secondInsert)) {
            DB::table('attendance')->insert($secondInsert);
        }
        DB::table('attendance_machine_data')->truncate();

        return "KK";

        $data=DB::table('attendance_file')->take(5)->latest()->get();
        return view('attendance.csv_view',compact('data'));

    }



    public function attendance_report_daily()
    {
//        return "hello World";
        return view('attendance.status');
    }

    public function absentStatus($date){
        $applications=DB::select("SELECT employees.id,employees.empFirstName,employees.empLastName,
                          employees.employeeId, designations.designation FROM employees, designations 
                          where employees.empDesignationId = designations.id AND employees.empAccStatus='1' AND employees.id not in 
                          (SELECT emp_id FROM attendance GROUP BY date, emp_id HAVING date='".$date."')
                           AND employees.id NOT IN 
                          (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1');");

        if(count($applications)){
            return view('attendance.statusData',compact('applications','date'));
        }
        else{
            return "";
        }

    }



    public function filesView(){
        $data=DB::table('attendance_file')->latest()->get();
        return view('attendance.fileIndex',compact('data'));

    }

    public function showDelete($id){
        $data=DB::table('attendance_file')->where(['id'=>$id])->first();
//        return $data;
        return view('attendance.modal.deleteForm',compact('data'));
    }

    public function destroyFile(Request $request,$id){
        $path=base_path()."/file/".DB::table('attendance_file')->where(['id'=>$id])->first()->attachment;
        if(file_exists($path)){
            unlink($path);
        }
        $dlt=DB::table('attendance_file')->where(['id'=>$id])->delete();
        if($dlt){
            Session::flash('message', 'File delete successful!');
        }
        $data=DB::table('attendance_file')->latest()->get();
        return view('attendance.fileIndex',compact('data'));

    }

    public function addEmployee($id,$date){
        $data=DB::table('employees')->where(['id'=>$id])->first();
        return view('attendance.modal.attendanceData',compact('data','date'));

    }

    public function storeAttendance(Request $request){
        $this->validate($request,[
            'in_time'=>'required',
            'out_time'=>'required',
        ]);
        $up=DB::table('attendance')->insert([
            'emp_id'=>$request->emp_id,
            'in_time'=>$request->in_time,
            'out_time'=>$request->out_time,
            'date'=>$request->date,

        ]);
        if($up){
            Session::flash('message','Attendance data inserted');
        }
        return redirect('/attendance/present/status');

    }

    public function presentStatus(){
        return view('report.attendance.daily_attendance_report');

    }
    public function presentAttendance($date){
        $data=DB::select("SELECT attendance.id as aid, employees.id,employees.empFirstName,
              employees.empLastName,employees.employeeId, designations.designation, attendance.in_time,
              attendance.out_time,attendance.date FROM employees, designations, 
              attendance where employees.empDesignationId=designations.id and employees.id=attendance.emp_id
              AND employees.empAccStatus='1' 
              AND attendance.date='$date'");
        $absentData=DB::select("SELECT employees.id,employees.empFirstName, employees.empLastName,employees.employeeId, designations.designation FROM employees, designations where employees.empDesignationId=designations.id AND employees.empAccStatus='1' and employees.id not in (SELECT emp_id FROM attendance where date='$date') AND employees.id NOT IN 
                          (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1')");

        $allData=array_merge($data,$absentData);
        return $allData;

    }

    public function dataAttendance($data){
        $ddata=explode(',',$data);
        $date=$ddata[0];
        $id=$ddata[1];
        $time=DB::table('attendance')->where('emp_id','=',$id)->where('date','=',$date)->select('id','in_time','out_time')->first();
        return view('attendance.editDataAttendance',compact('time'));

    }

    public function addEditAttendance($id, $date){
        $checkExist=DB::table('attendance')->where(['emp_id'=>$id,'date'=>$date])->get();
        if(count($checkExist)) {
            $data=DB::table('employees')
                ->join('designations','employees.empDesignationId','=','designations.id')
                ->join('attendance','employees.id','=','attendance.emp_id')
                ->select('employees.id as eid','attendance.id as aid','employees.empFirstName',
                    'employees.empLastName','employees.employeeId','designations.designation','attendance.date',
                    'attendance.in_time','attendance.out_time')
                ->where(['attendance.date'=>$date,'attendance.emp_id'=>$id])
                ->first();

            return view('attendance.modal.attendanceUpdate',compact('data'));

        }
        else{
            $data=DB::table('employees')
                ->join('designations','employees.empDesignationId','=','designations.id')
                ->select('employees.id','employees.empFirstName','employees.empLastName','employees.employeeId','designations.designation')
                ->where(['employees.id'=>$id])
                ->first();
//            $data=$employee;
            return view('attendance.modal.attendanceData',compact('data','date'));
        }

    }

    public function updateAttendance(Request $request,$id){
        if($id==0){
            $this->validate($request,[
                'emp_id'=>'required',
                'date'=>'required',
                'stime'=>'required',
                'etime'=>'required',
            ]);

            $k=DB::table('attendance')->where('id','=',$request->id)->update([
                'emp_id'=>$request->emp_id,
                'in_time'=>$request->stime,
                'out_time'=>$request->etime,
                'date'=>$request->date,
            ]);
            if($k){
                Session::flash('success','Attendance data updated');

            }
            return redirect(route('report.public.daily_attendance'));

        }
//        return $request->all();
        $this->validate($request,[
            'emp_id'=>'required',
            'in_time'=>'required',
            'out_time'=>'required',
            'date'=>'required',
        ]);
        $up=DB::table('attendance')->where(['id'=>$id])->update([
            'emp_id'=>$request->emp_id,
            'in_time'=>$request->in_time,
            'out_time'=>$request->out_time,
            'date'=>$request->date,

        ]);
        if($up){
            Session::flash('success','Attendance data updated');
        }
        return redirect(route('report.public.daily_attendance'));
    }

    public function createAttendance(){
        $employees=DB::table('employees')->where('empAccStatus','=',1)
            ->get(['empFirstName','employeeId','id']);
        return view('attendance.createAttendance',compact('employees'));
    }

    public function editAttendance(){
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('attendance.editAttendance',compact('employees'));

    }

    public function manualAttendance(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->createAttendance();
        }
        $this->validate($request,[
            'emp_id'=>'required',
            'date'=>'required',
            'stime'=>'required',
            'etime'=>'required',

        ]);

//        return count($request->emp_id);

        for($i=0;$i<1;$i++){
            $check=DB::table('attendance')->where(['emp_id'=>$request->emp_id[$i],'date'=>Carbon::parse($request->date)->toDateString()])->get();
            if(count($check)<1) {
                $input[] = [
                    'emp_id' => $request->emp_id[$i],
                    'date' => Carbon::parse($request->date)->toDateString(),
                    'in_time' => $request->stime,
                    'out_time' => $request->etime,
                ];
            }
            else{
                Session::flash('error','Failed!! Given employee attendance record already exist for '.Carbon::parse($request->date)->toDateString().'. Update Attendance.');
                return redirect(route('attendance.manual.edit'));

            }

        }
        if(!empty($input)) {
            $k = DB::table('attendance')->insert($input);
            if ($k) {
                Session::flash('success', 'Attendance Information Successfully Added');
            }
        }
        return redirect('report/public/daily_attendance');
    }

    public static function late_time(){
        $late_time=DB::table('attendance_setup')->
        select('max_entry_time')->first();
        return $late_time->max_entry_time;
    }

    public static function out_time(){
        $out_time=DB::table('attendance_setup')->
        select('exit_time')->first();
        return $out_time->exit_time;
    }
}