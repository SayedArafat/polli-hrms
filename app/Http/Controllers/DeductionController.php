<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use redirect;
use Session;

class DeductionController extends Controller
{
    //normal deduction view page
    public function index(Request $request){
      $month=date('Y-m');
      $data=DB::table('employees')->where('empAccStatus','=',1)->get();
      $deductiondata = DB::table('tb_deduction')
            ->leftJoin('employees', 'tb_deduction.emp_id', '=', 'employees.id')
            ->select('tb_deduction.*', 'employees.empFirstName','employees.employeeId')
            ->where('tb_deduction.month','=',$month)->
            latest()->get();
          return view('payroll.deduction',compact('data','deductiondata'));
    }
    //normal deduction store
    public function deductionStore(Request $request){
           $month=date('Y-m');
               $inseret=DB::table('tb_deduction')->insert([
                   'emp_id' => $request->emp_id,
                   'normal_deduction_amount' => $request->normal_deduction_amount,
                   'month' => $month,
                   'created_at' =>Carbon::now()->toDateTimeString(),
                   'updated_at' =>Carbon::now()->toDateTimeString(),
               ]);
        if($month){
            Session::flash('deductionnormalmessage', 'Operation Successful!');
            return redirect()->back();
        }
    }

    //Normal Deduction Update Multiple
    public function normalDeductionUpdate(Request $request){
        for($i=0;$i<count($request->emp_id);$i++){
            $emp_id=$request->emp_id[$i];
            $deduction=$request->deduction_amounts[$i];
            $data=DB::table('tb_deduction')->where('emp_id',$emp_id)->update([
                'emp_id' =>$emp_id,
                'normal_deduction_amount' =>$deduction,
            ]);
        }
        Session::flash('ndeduction', 'Deduction update successful!');
        return redirect()->back();
    }

    //advanced loan view
    public function advancedDeduction(){
        $data=DB::table('employees')->where('empAccStatus','=',1)->get();
        return view('payroll.advanced_deduction',compact('data'));
    }
    //advanced load store
    public function loanStore(Request $request){
        $incmonth = date('Y-m',strtotime('+1 month'));
        $complete_month=date('Y-m-d',strtotime('+1 month'));
        $time2=strtotime($complete_month);
        $time = strtotime($incmonth);
        $loan =$request->loan_amount;
        for($i=0;$i<$request->total_months;$i++){
            if($loan>$request->month_wise_deduction_amount){
                $ddamnt = $request->month_wise_deduction_amount;
                $loan= $loan-$ddamnt;
            }else{
                $ddamnt= $loan;
            }
            // convert timestamp back to date string
            $date = date('Y-m', $time);
            $date2 = date('Y-m-d', $time2);
            $actual_date= $due_dates[] = $date;
            $actual_date2= $due_dates[] = $date2;
            // move to next timestamp
            $time = strtotime('+1 month', $time);
            $time2 = strtotime('+1 month', $time2);
            $data=DB::table('tb_loan_deduction')->insert([
                'emp_id' =>$request->emp_id,
                'loan_amount' =>$request->loan_amount,
                'month_wise_deduction_amount' =>$ddamnt,
                'month' =>$actual_date,
                'complete_month' =>$actual_date2,
                'given_id' =>auth()->user()->id,
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ]);
        }
        if($data){
            Session::flash('loanme', 'Loan Added Successful!');
            return redirect()->back();
        }
    }

    //Employee loan deduction report view
    public function loanEmployee(){
       $employee=DB::SELECT("SELECT tb_loan_deduction.emp_id,employees.empFirstName,employees.empPhone
       FROM tb_loan_deduction
       LEFT JOIN employees ON tb_loan_deduction.emp_id=employees.id
       GROUP BY tb_loan_deduction.emp_id");
       return view('report.payroll.advancereport',compact('employee'));
    }

    //Employee loan report show
    public function Employeeloanreportshow($id){
        $data=DB::SELECT("SELECT tb_loan_deduction.emp_id,tb_loan_deduction.loan_amount,
       employees.empFirstName,employees.empLastName,
     designations.designation,
     users.name as advance_given,
    GROUP_CONCAT(tb_loan_deduction.month_wise_deduction_amount) as monthwise_deduction,
    COUNT(tb_loan_deduction.emp_id) as complete_month,
   GROUP_CONCAT(MONTHNAME(tb_loan_deduction.complete_month))as month_duration,
     GROUP_CONCAT(YEAR(tb_loan_deduction.complete_month))as year_duration
   FROM tb_loan_deduction
   LEFT JOIN employees ON tb_loan_deduction.emp_id=employees.id
  LEFT JOIN designations ON employees.empDesignationId=designations.id
  LEFT JOIN users ON tb_loan_deduction.given_id=users.id
  WHERE tb_loan_deduction.emp_id=$id");
      return response()->json($data);
    }

    //Employee penalty amount report view
    public function Employeedeductionreportview(){
        return view('report.payroll.empdeductionreport');
    }
    //Employee penalty report show
    public function Employeedeductionreportshow(Request $request){
        $month=$request->year_bonus_month;
        $data=DB::SELECT("SELECT tb_deduction.normal_deduction_amount,tb_deduction.created_at,employees.empFirstName,designations.designation FROM tb_deduction LEFT JOIN employees ON tb_deduction.emp_id=employees.id LEFT JOIN designations ON employees.empDesignationId=designations.id WHERE month='$month'");
        return response()->json($data);
    }


}
