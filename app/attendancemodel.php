<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class attendancemodel extends Model
{
    protected $table ='attendance';
    protected $fillable = [
        'emp_id','date','in','out',
    ];
}
