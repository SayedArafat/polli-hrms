@extends('layouts.master')
@section('title', 'Manual Bulk SMS')
@section('content')
{{ Html::script('hrm_script/js/bootstrap3-typeahead.js') }}
    <div class="page-content">
        @if(Session::has('notification'))
            <p id="alert_message" class="alert alert-success">{{Session::get('notification')}}</p>
        @endif
        @if(Session::has('sms'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('sms')}}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-envelope"></i> <strong>Manual Bulk SMS </strong></h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                      
                        <form action="{{url('/proccess_bulk_sms')}}" method="POST">
                          {{ csrf_field() }}
                          
                            
                              <div class="form-group">
                                <label for="departmentId">Employee Department</label>
                                <select name="departmentId" id="departmentId" class="form-control" data-search='true' >
                                  <option value="">All Department</option>
                                  @foreach($departments as $department)

                                  <?php if(isset($request)&&!empty($request)){ ?>
                                  <option <?php if($request->departmentId==$department->id){ echo "selected"; } ?> value="{{$department->id}}">{{$department->departmentName}}</option>
                                  <?php }else{ ?>
                                  <option value="{{$department->id}}">{{$department->departmentName}}</option>
                                  <?php } ?>

                                  @endforeach
                              </select>
                          </div>
                          <div class="form-group">
                            <label for="designationId">Employee Designation</label>
                            <select name="designationId" id="designationId" class="form-control"  data-search='true'  >
                              <option value="">All Designation</option>
                              @foreach($designations as $d)

                              <?php if(isset($request)&&!empty($request)){ ?>
                              <option <?php if($request->designationId==$d->id){ echo "selected"; } ?> value="{{$d->id}}">{{$d->designation}}</option>
                              <?php }else{ ?>
                              <option value="{{$d->id}}">{{$d->designation}}</option>
                              <?php } ?>
                              @endforeach
                              </select>
                          </div>

                          <div class="form-group">
                            <label for="unitId">Unit</label>
                            <select name="unitId" id="unitId" class="form-control"  data-search='true'  >
                              <option value="">All Unit</option>
                              @foreach($units as $unit)

                              <?php if(isset($request)&&!empty($request)){ ?>
                              <option <?php if($request->unitId==$unit->id){ echo "selected"; } ?> value="{{$unit->id}}">{{$unit->name}}</option>
                              <?php }else{ ?>
                              <option value="{{$unit->id}}">{{$unit->name}}</option>
                              <?php } ?>
                              @endforeach
                              </select>
                          </div>

                          <div class="form-group">
                            <label for="empSection">Employee Section</label>

                              <?php if(isset($request)&&!empty($request)){ ?>
                                  <input name="empSection" value="{{$request->empSection}}" id="empSection" class="form-control" >
                              <?php }else{ ?>
                                  <input name="empSection" placeholder="Employee Section" id="empSection" class="form-control" >
                              <?php } ?>
                          </div>
                          
                           <div>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-chevron-circle-down"></i> Load Employee</button>
                                <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Clear</button>
                           </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

<script type="text/javascript">

    var url = "{{ route('employee.section.autocomplete.ajax') }}";

    $('#empSection').typeahead({

        source:  function (query, process) {

            return $.get(url, { query: query }, function (data) {

                return process(data);

            });

        }

    });

</script>
    <script>


      $('#messageArea').keyup(function () {
        var max = 250;
        var len = $(this).val().length;
        if (len >= max) {
          $('#charNum').text(' You have reached the limit');
          $('#charNum').css({"color": "red"});
        } else {
          var char = max - len;
          $('#charNum').text(char + ' characters left');
          $('#charNum').removeAttr("style");
        }
      });

      setTimeout(function() {
          $('#alert_message').fadeOut('fast');
      }, 5000);
    </script>
@include('include.copyright')
@endsection