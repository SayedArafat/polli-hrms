@extends('layouts.master')
@section('title', 'Quota Settings')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <a data-toggle="modal" data-target="#usercreate" class="btn btn-default btn-sm"><i class="fa fa-plus"></i>Add</a>
                        <h3><i class="fa fa-table"></i> <strong>Quota </strong> Settings</h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>District Name</th>
                                <th>Population</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($quota_settings as $c)
                                <tr>
                                    <td>{{$c->id}}</td>
                                    <td>{{$c->district_name}}</td>
                                    <td>{{$c->population}}</td>
                                    <td>
                                        <a data-toggle="modal" title="Edit" data-target="#{{$c->id}}" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a>
                                        {{--<a data-toggle="modal" title="Delete Data" data-target="#{{"delete".$c->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>--}}
                                    </td>
                                </tr>

                                <!-- Edit Modal -->
                                <div class="modal fade" id="{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Update Info</strong></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                {!! Form::open(['method'=>'PATCH', 'action'=>['QuotaController@update_district', $c->id]]) !!}
                                                <div class="form-group">
                                                    {!! Form::label('district_name','District Name') !!}
                                                    {!! Form::text('district_name',$c->district_name, ['class'=>'form-control']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('population','Population') !!}
                                                    {!! Form::number('population',$c->population, ['class'=>'form-control']) !!}
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Update Changes</button>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!--End of Edit Model-->





                                <!-- Delete Modal -->
                                <div class="modal fade" id="{{"delete".$c->id}}" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Confirm Delete</strong></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Do you want to delete this maritial status? </p>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::open(['method'=>'DELETE', 'action'=>['MaritialStatusController@destroy', $c->id]]) !!}
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-danger">Confirm</button>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--Delete Modal Ended-->

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Add New Modal -->
    <div class="modal fade" id="usercreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>New District</strong>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>

                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST', 'action'=>'QuotaController@store_district']) !!}
                    <div class="form-group">
                        {!! Form::label('district_name','District Name:') !!}
                        {!! Form::text('district_name',null,['class'=>'form-control','required'=>'', 'placeholder'=>'']) !!}


                    </div>
                    <div class="form-group">
                        {!! Form::label('population','Population:') !!}
                        {!! Form::number('population',null,['class'=>'form-control', 'required'=>'', 'placeholder'=>'']) !!}


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="Submit" class="btn btn-primary">Add District</button>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>


    @include('include.copyright')
@endsection