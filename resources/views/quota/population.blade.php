@extends('layouts.master')
@section('title', 'Country Population')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        @if(count($population)==0)
                            <a data-toggle="modal" data-target="#usercreate" class="btn btn-default btn-sm"><i class="fa fa-plus"></i>Add</a>
                        @endif
                        <h3><i class="fa fa-table"></i> <strong>Population </strong> Status</h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>Country Name</th>
                                <th>Population</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            @if(count($population)!=0)
                                <tbody>
                                @foreach($population as $c)
                                    <tr>
                                        <td>{{$c->country_name}}</td>
                                        <td>{{$c->population}}</td>
                                        <td>
                                            <a data-toggle="modal" title="Edit" data-target="#{{$c->id}}" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a>
                                        </td>
                                    </tr>

                                    <!-- Edit Modal -->
                                    <div class="modal fade" id="{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel"><strong>Update Info</strong></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    {!! Form::open(['method'=>'PATCH', 'action'=>['QuotaController@update_population', $c->id]]) !!}
                                                    <div class="form-group">
                                                        {!! Form::label('country_name','Country') !!}
                                                        {!! Form::text('country_name',$c->country_name, ['class'=>'form-control','disabled'=>'']) !!}
                                                        <label for="population">Population</label>
                                                        <input type="text" class="form-control" required="" id="population"  name="population" value="{{$c->population}}">
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Update Population</button>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                    <!--End of Edit Model-->
                                @endforeach
                                </tbody>
                            @endif
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Add New Modal -->
    <div class="modal fade" id="usercreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>Add Population (Onetime)</strong>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>

                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST', 'action'=>'QuotaController@store_population']) !!}
                    <div class="form-group">
                        {!! Form::label('country_name','Population:') !!}
                        {!! Form::text('country_name',"Bangladesh",['class'=>'form-control','required'=>'', 'disabled'=>'']) !!}


                    </div>
                    <div class="form-group">
                        {!! Form::label('population','Population:') !!}
                        {!! Form::text('population',null,['class'=>'form-control','required'=>'', 'placeholder'=>'Population']) !!}


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="Submit" class="btn btn-primary">Save</button>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>


    @include('include.copyright')
@endsection