@extends('layouts.master')
@section('title', 'District Quota')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-table"></i> <strong>District Quota </strong> Status</h3>
                        <a data-toggle="modal" data-target="#myModal" class="btn btn-default btn-sm"><i class="fa fa-plus"></i>Add Vacancy Number</a>

                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>District</th>
                                <th>Population</th>
                                <th>Designation</th>
                                <th>Vacancy Number</th>
                                <th>Quota</th>
                                <th>Existing Employee</th>
                                <th>Difference</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($district_quota as $c)
                                <?php
                                        $quota=round(($store_vacancy[0]->vacancy_no/$country_population->population)*$c->population);
                                        $existing=\Illuminate\Support\Facades\DB::table('employees')
                                            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
                                            ->where('designations.designation','=',$store_vacancy[0]->designation)
                                            ->where('emergency_contact_relation','=',$c->district_name)->count();

                                ?>
                                <tr>
                                    <td>{{$c->district_name}}</td>
                                    <td>{{$c->population}}</td>
                                    <td>{{$store_vacancy[0]->designation}}</td>
                                    <td>{{$store_vacancy[0]->vacancy_no}}</td>
                                    <td>{{$quota}}</td>
                                    <td>{{$existing}}</td>
                                    <td>{{$quota-$existing}}</td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Employee Vacancy</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['method'=>'POST', 'action'=>'QuotaController@store_vacancy']) !!}
                        <div class="form-group">
                            {!! Form::label('designation','Designation:') !!}
                            {!! Form::text('designation',$store_vacancy[0]->designation,['class'=>'form-control','required'=>'']) !!}

                        </div>
                        <div class="form-group">
                            {!! Form::label('vacancy_no','Number of Vacancy:') !!}
                            {!! Form::number('vacancy_no',$store_vacancy[0]->vacancy_no,['class'=>'form-control','required'=>'']) !!}


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="Submit" class="btn btn-primary">Next</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>




    <script>
        $(document).ready(function() {
            $('#myModal').modal('show');
        });
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>


    @include('include.copyright')
@endsection