@extends('layouts.master')
@section('title', 'Vacancy Application')
@section('content')
    <div class="page-content">
        @if(Session::has('file_size'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('file_size')}}</p>
        @elseif(Session::has('up'))
                <p id="alert_message" class="alert alert-success">{{Session::get('up')}}</p>
        @endif
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
        @endif
        <h3><strong>Application </strong> Details</h3>
        <div class="row">
            <div  class="col-sm-4 text-end">
                <h5><strong>Name: </strong></h5>
            </div>
            <div class="col-sm-4">
                <h5><strong>{{$application->name}}</strong></h5>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 text-end">
                <h5><strong>Phone Number:</strong></h5>
            </div>
            <div class="col-sm-4">
                <h5><strong>{{$application->phone}}</strong></h5>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-4 text-end">
                <h5><strong>Applied Position:</strong></h5>
            </div>
            <div class="col-sm-4">
                <h5><strong>{{$application->vacTitle}}</strong></h5>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-4 text-end">
                <h5><strong>Applied Date:</strong></h5>
            </div>
            <div class="col-sm-4">
                <h5><strong>{{\Carbon\Carbon::parse($application->submittedDate)->format('j F Y')}}</strong></h5>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-4 text-end">
                <h5><strong>Interview Status:</strong></h5>
            </div>
            <div class="col-sm-4">
                <h5><strong>{{$application->interviewStatus}}</strong></h5>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-4 text-end">
                <h5><strong>Interview Date:</strong></h5>
            </div>
            <div class="col-sm-4">
                @if($application->interviewDate!=null)
                    <h5><strong>{{\Carbon\Carbon::parse($application->interviewDate)->format('j F Y')}}</strong></h5>
                @endif
            </div>

        </div>

        <div class="row">
            <div class="col-sm-4 text-end">
                <h5><strong>Job Status:</strong></h5>
            </div>
            <div class="col-sm-4">
                <h5><strong>{{$application->jobStatus}}</strong></h5>
            </div>
            {{--<div class="col-sm-4">--}}
                {{--<h5><strong>--}}
                        {{--@if($v->vacStatus==1)--}}
                            {{--Active--}}
                        {{--@endif--}}
                        {{--@if($v->vacStatus==0)--}}
                            {{--<span class="text-red">Inactive</span>--}}
                        {{--@endif--}}
                    {{--</strong></h5>--}}
            {{--</div>--}}

        </div>


        <div class="row">
            <div class="col-sm-4 text-end">
                <h5><strong>Interview Notes</strong></h5>
            </div>
            <div class="col-sm-4">
                <h5><strong>{{$application->interviewNote}}</strong></h5>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-4 text-end">
                <h5><strong>Priority Label:</strong></h5>
            </div>
            <div class="col-sm-4">
                <h5><strong>{{$application->priorityStatus}}</strong></h5>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-4 text-end">
                <h5><strong>Attachments: </strong></h5>
            </div>
            <div class="col-sm-4">
                <h5><a href="{{asset('Applications/'.$application->attachment)}}"><strong>Show Attachments</strong></a> </h5>
            </div>

        </div>
        <div class="row padTop30">
            <div class="col-sm-5">
                <a href="{{url('recruitment/application')}}"> <button class="btn btn-default"><i class="fa fa-arrow-left"></i> Back To List</button></a>
            </div>
            <div class="col-sm-5">
                <button class="btn btn-success" data-toggle="modal" data-target="#editApp"><i class="fa fa-edit"></i> Edit Info</button>
                <button type="button" data-toggle="modal" data-target="#deleteApp" class="btn btn-danger padLeft20"><i class="fa fa-trash-o"></i> Delete Record</button>
            </div>
        </div>

    </div>



    <div class="modal fade" id="deleteApp" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Confirm</h4>
                </div>
                <div class="modal-body">
                    <p>Do you want to delete job application of <strong>{{ $application->name }}</strong> for <strong>{{$application->vacTitle}}</strong> ??</p>
                </div>
                {!! Form::open(['method'=>'DELETE','action'=>['RecruitmentController@applicationDelete',$application->id]]) !!}
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Confirm Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>






    <div class="modal fade" id="editApp" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                    <h4 class="modal-title"><strong>Edit</strong> Application</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            {!! Form::open(['method'=>'PATCH','action'=>['RecruitmentController@applicationUpdate',$application->id],'files'=>true]) !!}

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="required form-group">
                                        {!! Form::label('name','Name',['class'=>'control-label']) !!}
                                        <div class="append-icon">
                                            <input type="text" name="name" value="{{$application->name}}" class="form-control" placeholder="Enter Name..." required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('phone','Phone Number',['class'=>'control-label']) !!}
                                        <div class="append-icon">
                                            <input type="text" name="phone" value="{{$application->phone}}" class="form-control" placeholder="Enter Phone Number">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="required form-label">Applying For </label>
                                        {{--{!! Form::label('vacancyId','Applying For',['class'=>'form-control']) !!}--}}
                                        <div class="option-group">
                                            {!! Form::select('vacancy_id', $vacancies,null,['class'=>'form-control','required'=>'']) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="required form-label">Submitted Date</label>
                                        <div class="prepend-icon">
                                            <input type="text" name="submittedDate" class="date-picker form-control" value="{{\Carbon\Carbon::parse($application->submittedDate)->format('j F Y')}}" required>
                                            <i class="icon-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="required form-label">Attachment (Max Size:5mb)</label>
                                        {!! Form::file('attachment', ['class'=>'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Interview Status</label>
                                        <div class="option-group">
                                            <select id="language" name="interviewStatus" class="form-control">
                                                <option value="">Select Status</option>
                                                <option {{ $application->interviewStatus=="Accepted" ? "selected" : "none" }} value="Accepted">Accepted</option>
                                                <option {{ $application->interviewStatus=="Rejected" ? "selected" : "none" }} value="Rejected">Rejected</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="form-label">Interview Date</label>
                                        <div class="prepend-icon">

                                            <input type="text" name="interviewDate" value=" {{ $application->interviewDate!=null ? \Carbon\Carbon::parse($application->interviewDate)->format('j F Y') : ''}}" class="date-picker form-control" placeholder="Select a date...">

                                            <i class="icon-calendar"></i>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Interview Details</label>
                                        <div class="append-icon">
                                            <textarea name="interviewNote" class="form-control" placeholder="Enter the interview details..." rows="4">{{ $application->interviewNote }}</textarea>
                                            <i class="icon-lock"></i>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">


                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Priority</label>
                                        <div class="option-group">
                                            <select id="language" name="priorityStatus" class="form-control">
                                                <option value="">Select Priority</option>
                                                <option {{ $application->priorityStatus=='High' ? "selected" :'none' }} value="High">High</option>
                                                <option {{ $application->priorityStatus=='Regular' ? "selected" :'none' }} value="Regular">Regular</option>
                                                <option {{ $application->priorityStatus=='Low' ? "selected" :'none' }} value="Low">Low</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Job Status</label>
                                        <div class="option-group">
                                            <select id="language" name="jobStatus" class="form-control">
                                                <option value="">Select Status</option>
                                                <option {{$application->jobStatus=='Confirm' ? 'selected':'none' }} value="Confirm">Confirm</option>
                                                <option {{$application->jobStatus=='Waiting' ? 'selected':'none' }} value="Waiting">Waiting</option>
                                                <option {{$application->jobStatus=='Reject' ? 'selected' :'none'}} value="Reject">Reject</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="text-center  m-t-20">
                                <button type="submit" class="btn btn-embossed btn-primary">Update Info</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                            {!! Form::close() !!}

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>



    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>


@include('include.copyright')
@endsection