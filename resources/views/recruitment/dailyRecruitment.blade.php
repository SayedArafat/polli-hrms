@extends('layouts.master')
@section('title', 'Daily Recruitment')
@section('content')
    <div class="page-content">
        <h2><strong>Daily Recruitment Status</strong></h2>
        {{--{!! Form::open(['method'=>'GET']) !!}--}}
        {!! Form::label('date', 'Select a date: ') !!}
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">

                    {{--{!! Form::text('date',null,['class'=>'form-control date-picker', 'id'=>'datePicker' ]) !!}--}}
                    <input type="text" class="form-control" id="date" name="date"/>
                    {{--<input type='text' class="form-control" id='date' />--}}
                </div>
            </div>
        </div>
        {{--{!! Form::close() !!}--}}

        {{--@elseif(Session::has('file_size'))--}}
        {{--<p id="alert_message" class="alert alert-danger">{{ Session::get('file_size') }}</p>--}}
        {{--@endif--}}
        <div id="hid" class="row">
            @if(isset($applications))
                @include('recruitment.recruitmentStatus')

            @endif
        </div>
    </div>


    <script>
        $(function() {
            var dateVariable;
            $("#date").datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function(dateText) {
                    dateVariable = dateText;
//                    alert($("#date").val())

                },
            
                onClose:function () {

                    $.ajax({
                        url:"status/"+dateVariable,
                        method:'GET',
                        dataType:'html',
                        success: function (response) {
                            $('#hid').html(response);

                        },
                        error:function () {
//                            alert('error')

                        }

                    });
//                    document.getElementById('hid').style.display='block';

                }

            });

        });
    </script>
@include('include.copyright')
@endsection