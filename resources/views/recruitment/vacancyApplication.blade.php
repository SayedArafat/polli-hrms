@extends('layouts.master')
@section('title', 'New Job Application')
@section('content')
    <div class="page-content">
        <div class="row">
            @if(Session::has('file_size'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('file_size')}}</p>
            @elseif(Session::has('message'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
            @endif
            <div class="col-md-12">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title">New <strong> Job Application</strong></h2>
                    </div>
                    <div class="panel-body bg-white">

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                {!! Form::open(['method'=>'POST','action'=>'RecruitmentController@storeVacancyApplication','files'=>true]) !!}

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="required form-group">
                                            {!! Form::label('name','Name',['class'=>'control-label']) !!}
                                            <div class="append-icon">
                                                <input type="text" name="name" class="form-control" placeholder="Enter Name..." required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('phone','Phone Number',['class'=>'control-label']) !!}
                                            <div class="append-icon">
                                                <input type="text" name="phone" class="form-control" placeholder="Enter Phone Number">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="required form-label">Applying For </label>
                                            {{--{!! Form::label('vacancyId','Applying For',['class'=>'form-control']) !!}--}}
                                            <div class="option-group">
                                                {!! Form::select('vacancy_id',[''=>'Select Vacancy Position']+ $vacancies,null,['class'=>'form-control','required'=>'']) !!}

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="required form-label">Submitted Date</label>
                                            <div class="prepend-icon">
                                                <input type="text" name="submittedDate" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required>
                                                <i class="icon-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="required form-label">Attachment (Max Size:5mb)</label>
                                            {!! Form::file('attachment', ['class'=>'form-control','required'=>'']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Interview Status</label>
                                            <div class="option-group">
                                                <select id="language" onchange="interviewCheck(this);" name="interviewStatus" class="form-control">
                                                    <option value="">Select Status</option>
                                                    <option value="Accepted">Accepted</option>
                                                    <option value="Rejected">Rejected</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="interviewDate" class="col-sm-6">
                                        <div class="form-group">
                                            <label class="form-label">Interview Date</label>
                                            <div class="prepend-icon">
                                                <input type="text" name="interviewDate" class="date-picker form-control" placeholder="Select a date...">
                                                <i class="icon-calendar"></i>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div id="conditionDisplay">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label">Interview Details</label>
                                                <div class="append-icon">
                                                    <textarea name="interviewNote" class="form-control" placeholder="Enter the interview details..." rows="4"></textarea>
                                                    <i class="icon-lock"></i>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">


                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Priority</label>
                                                <div class="option-group">
                                                    <select id="language" name="priorityStatus" class="form-control">
                                                        <option value="">Select Priority</option>
                                                        <option value="High">High</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Low">Low</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Job Status</label>
                                                <div class="option-group">
                                                    <select id="language" name="jobStatus" class="form-control">
                                                        <option value="">Select Status</option>
                                                        <option value="Confirm">Confirm</option>
                                                        <option value="Waiting">Waiting</option>
                                                        <option value="Reject">Reject</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>



                                <div class="text-center  m-t-20">
                                    <button type="submit" class="btn btn-embossed btn-primary">Save</button>
                                    <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                                </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('include.copyright')

    <script>
        function interviewCheck(that) {
            if(that.value=='Accepted'){
//                alert('accepted');
                document.getElementById('interviewDate').style.display="block";
                document.getElementById('conditionDisplay').style.display="block";
            }
            else{
//                alert('Rejected');
                document.getElementById('interviewDate').style.display="none";
                document.getElementById('conditionDisplay').style.display="none";
            }

        }

        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);

    </script>

@endsection