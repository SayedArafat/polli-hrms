@extends('layouts.master')
@section('title', 'Change Complain Status ')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>

    <div class="page-content">

        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-table"></i>Change <strong>Complain </strong> Status</h3>
                    </div>
                    <div class="panel-content pagination2">
                       {!! Form::open(['method'=>'PATCH', 'action'=>['ComplainsController@update', $id]]) !!}
                         <div class="form-group">
                            <label>Complain Date:</label>
                            <br />

                            {{\Carbon\Carbon::parse($complains->complainDate)->format('d-M-Y')}}
                        </div>

                        <div class="form-group">
                            <label>Complainer Name:</label>
                            <br />
                            {{$complains->complainerName}}
                        </div>
                        <div class="form-group">
                            <label>Complainer Phone:</label>
                            <br />
                            {{$complains->complainerPhone}}
                        </div>

                        <div class="form-group">
                            <label>Complainer Address:</label>
                            <br />
                            {{$complains->complainerAddress}}
                        </div>
                        <div class="form-group">
                            <label>Complain Message:</label>
                            <br />
                            <span style="font-size:14px;color:#9a0000;">{{$complains->complainMessage}}</span>
                        </div>

                        <div class="form-group">
                            <label>Complain Status:</label>
                            <br />
                            <select name="status" required onchange="statusFunc(this)" style="padding:5px 5px;min-width:300px;">
                                <option value="">Select status </pending>
                                <option <?php  if($complains->complainStatus==0){ echo "selected"; }?> value="0">Pending</pending>
                                <option <?php  if($complains->complainStatus==1){ echo "selected"; }?> value="1">On Process</pending>
                                <option <?php  if($complains->complainStatus==2){ echo "selected"; }?> value="2">Completed</pending>
                                <option <?php  if($complains->complainStatus==3){ echo "selected"; }?> value="3">Rejected</pending>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Reason for Rejected:</label>
                            <br />
                            <textarea rows="8" style="border: 1px solid #ddd;min-width:650px; padding:5px;" name="complainRejectReason" disabled required id="complainRejectReason" >{{$complains->complainRejectReason}}</textarea>
                        </div>
                        <br />
                        <br />

                        <div class="form-group">
                            <label></label>
                            <button type="submit" class="btn btn-danger "><i class="fa fa-refresh"></i>Change Status</button>
                        </div>
                        <br />
                        <br />
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>

        function statusFunc(that) {
            if(that.value==='3'){
                document.getElementById('complainRejectReason').disabled=false;
            }
            else {
                document.getElementById('complainRejectReason').disabled=true;
            }

        }

        function defaultStatus() {

                document.getElementById('complainRejectReason').disabled=true;
        }
        
        window.onload = defaultStatus;

    </script>
      @include('include.copyright')
@endsection
