@extends('layouts.master')
@section('title', 'Create New Notice')
@section('content')
	<div class="page-content">
		<div class="row">

            <div class="col-md-12">
                @if(Session::has('message'))
                    <p id="alert_message" class="alert alert-success">{{Session::get('message')}}</p>
                @endif
                @if(Session::has('failedMessage'))
                        <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
                @endif
            </div>
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="panel panel-default no-bd">
					<div class="panel-header bg-dark">
						<h2 class="panel-title">Add New <strong>Complain</strong></h2>
					</div>
					<div class="panel-body bg-white">
						<div class="">
                        {!! Form::open(['method'=>'POST','action'=>['ComplainsController@store']]) !!}
						
                            <div class="required form-group">
                                <label class="control-label">Complain Date</label>
                                <div class="append-icon">
                                    <input type="text" name="complainDate" class="date-picker form-control" required placeholder="Click here to select date" >
                                </div>
                            </div>

                            <div class="required form-group">
                                <label class="control-label">Complainer Name</label>
                                <div class="append-icon">
                                    <input type="text" name="complainerName" class="form-control" required placeholder="Enter Complainer Name Here " >
                                </div>
                            </div>

                            <div class="required form-group">
                                <label class="control-label">Complainer Phone</label>
                                <div class="append-icon">
                                    <input type="text" name="complainerPhone" class="form-control" required placeholder="Enter Complainer Phone Number Here " >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Complainer Address</label>
                                <div class="append-icon">
                                    <textarea rows="8" name="complainerAddress" class="form-control" placeholder="Enter Full Address " ></textarea>
                                </div>
                            </div>
                            
                            <div class="required form-group">
                                <label class="control-label">Complain Message</label>
                                <div class="append-icon">
                                    <textarea rows="8" name="complainMessage" class="form-control" placeholder="Enter complain message here " required ></textarea>
                                </div>
                            </div>

                            <div class="text-center modal-footer">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save Complain</button>
                            </div>

                        {!! Form::close() !!}
						
						</div>

                       

					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
<script>
    setTimeout(function() {
        $('#alert_message').fadeOut('fast');
    }, 5000);
</script>
@endsection
