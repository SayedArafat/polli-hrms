@extends('layouts.master')
@section('title', 'Complain List')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>

    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{Session::get('message')}}</p>
        @endif
        @if(Session::has('fmessage'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('fmessage')}}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-table"></i> <strong>Complain </strong> List</h3>
                    </div>
                    <div class="panel-content pagination2">
                        <table class="table table-hover table-bordered" id="datatable1">
                            <thead>
                            <tr>
                                <th>Complain Date</th>
                                <th>Complainer Name</th>
                                <th>Complainer Phone</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th width="16%">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>                     
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready( function () {
            $('#datatable1').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('complains.list') }}",
                "columns": [
                    { "data": "complainDate" },
                    { "data": "complainerName" },
                    { "data": "complainerPhone" },
                    { "data": 'status' },
                    { "data": "created_by" },
                    { "data": "action" }
                ],
            "pageLength": 20,
            "order": [[ 0, "desc" ]]
            });


        });


    </script>
      @include('include.copyright')
@endsection
