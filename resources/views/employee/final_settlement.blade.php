<?php
use App\Helper\AppHelper;

if($total_earn_leave-$leave_enjoyed<0)
{
    $payable_leave=0;
}

else{
    $payable_leave=$total_earn_leave-$leave_enjoyed;

}

$pay_per_salary=round($previous_salary/$dayCount,2);

$leave_benefit=$pay_per_salary*$payable_leave;

if($interval->y>=5 && $interval->y<10){
    $service_benefit_days=14;
}
else if($interval->y>=10){
    $service_benefit_days=30;
}
else{
    $service_benefit_days=0;
}

$total_service_benefit_days=$interval->y*$service_benefit_days;

$pay_per_salary_basic=round($previous_salary_basic/$dayCount,2);

$service_benefit_salary=round($total_service_benefit_days*$pay_per_salary_basic);

$total_payable=round($service_benefit_salary+$leave_benefit);
$helper=AppHelper::instance();
?>


<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Final Settlement Form</title>
    <style>
        @page { sheet-size: A4; }
        body{
            font-family: 'bangla', sans-serif;
            font-size: 11px;

        }
        p{
            line-height: 1px;
        }

        .basicInfo{
            width: 100%;
        }

        .basicInfo td{
            height: 25px;
        }


        .reportHeaderArea{
            text-align: center;
        }
        .reportHeader p{
            line-height: 10px;
        }
        .container{
            text-align: left;
        }
        .salary{
            text-align:left;
            width: 400px;
        }


        .tbl{
            width: 100%;
            border-collapse: collapse;
        }


        .tbl td{
            padding: 4px;
        } 

    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">Email: {{$companyInformation->company_email}}</p>
        <p class="reportHeader">Phone: {{$companyInformation->company_phone}}</p>
        <h2><b>FINAL SETTLEMENT FORM</b></h2>
    </div>
</div>
<div>
    <table  class="tbl">
      <tr>
          <td style="text-align:center;" width="50%">UNIT: <b>{{$employee->unitName}}</b></td>
          <td style="text-align:center;">DATE: <b>{{\Carbon\Carbon::now()->format('d M Y')}}</b></td>
      </tr>
    </table>
    <table class="tbl"  border="1"  >
        <tr>
            <td  colspan="6" style="background-color:#ccc;text-align:center;">EMPLOYEES IDENTIFICATION</td>
        </tr>
        <tr>
            <td width="25%">Name</td>
            <td style="text-align:center;" colspan="5">{{$employee->empFirstName." ".$employee->empLastName}}</td>
        </tr>
        <tr>
            <td>ID No</td>
            <td style="text-align:center;" colspan="5">{{$employee->employeeId}}</td>
        </tr>
        <tr>
            <td>Designation</td>
            <td style="text-align:center;" colspan="5">{{$employee->designation}}</td>
        </tr>
        <tr>
            <td>Department</td>
            <td style="text-align:center;" colspan="5">{{$employee->departmentName}}</td>
        </tr>
        <tr>
            <td>Joining Date</td>
            <td style="text-align:center;" colspan="5">{{$employee->empJoiningDate}}</td>
        </tr>
        <tr>
            <td>Resign Date</td>
            <td style="text-align:center;" colspan="5">{{$employee->date_of_discontinuation}}</td>
        </tr>
        <tr>
            <td>Section</td>
            <td style="text-align:center;" colspan="5">{{$employee->empSection}}</td>
        </tr>
        <tr>
            <td>Running Salary(Gross)</td>
            <td style="text-align:center;" colspan="5">{{$present_salary->total_employee_salary}}</td>
        </tr>
        <tr>
            <td>Running Salary(Basic)</td>
            <td style="text-align:center;" colspan="5">{{$present_salary->basic_salary}}</td>
        </tr>
        <tr>
            <td rowspan="2">Service Period</td>
            <td style="text-align:center;"> Days</td>
            <td colspan="2"  style="text-align:center;">Months</td>
            <td  colspan="2"  style="text-align:center;">Years</td>
        </tr>
        <tr>
            <td style="text-align:center;">{{$interval->d}}</td>
            <td  colspan="2" style="text-align:center;" >{{$interval->m}}</td>
            <td colspan="2"  style="text-align:center;">{{$interval->y}}</td>
        </tr>

        <tr>
            <td  colspan="6" style="background-color:#ccc;text-align:center;">EARN LEAVE BENEFIT</td>
        </tr>

        <tr>
            <td rowspan="2" style="text-align:center;">Previous Month Working Day</td>
            <td style="text-align: center;">Name of Month</td>
            <td colspan="2" style="text-align:center;">Total Days</td>
            <td  colspan="2" style="text-align:center;">Payable Salary Previous Month</td>
        </tr>
        <tr>
            <td style="text-align:center;">{{\Carbon\Carbon::now()->format('M-Y')}}</td>
            <td  colspan="2" style="text-align:center;" >{{$dayCount}}</td>
            <td colspan="2"  style="text-align:center;">{{round($previous_salary)}}</td>
        </tr>

        <tr>
            <td rowspan="2">Per Day Salary</td>
            <td colspan="4" style="text-align:center;">Previous Month's (Payable Salary / Total Days)</td>
            <td colspan="1" style="text-align:center;">Per Day Salary</td>
        </tr>
        <tr>
            <td style="text-align:center;">{{$previous_salary}}</td>
            <td  colspan="2"  style="text-align:center;">{{$dayCount}}</td>
            <td colspan="2"  style="text-align:center;">{{$pay_per_salary}}</td>
        </tr>

        <tr>
            <td rowspan="2">Payable Earn Leave Days</td>
            <td style="text-align:center;">Total Earn Leave</td>
            <td style="text-align:center;">Enjoyed</td>
            <td style="text-align:center;">Encashment</td>
            <td colspan="2"  style="text-align:center;">Payable Earn Leave Days</td>
        </tr>
        <tr>
            <td style="text-align:center;">

                {{$total_earn_leave}}

            </td>
            <td style="text-align:center;">{{$leave_enjoyed}}</td>
            <td style="text-align:center;">0</td>
            <td colspan="2" style="text-align:center;" >{{$payable_leave}}

            </td>
        </tr>

        <tr>
            <td rowspan="2">Payable Earn Leave Benefit</td>
            <td colspan="3" style="text-align:center;">Per Day Salary * Payable Earn Leave</td>
            <td colspan="2"  style="text-align:center;">Payable Earn Leave Benefit</td>
        </tr>
        <tr>
            <td style="text-align:center;">{{$pay_per_salary}}</td>
            <td colspan="2" style="text-align:center;">{{$payable_leave}}</td>
            <td colspan="2" style="text-align:center;" >{{$leave_benefit}}</td>
        </tr>

        <tr>
            <td  colspan="6" style="background-color:#ccc;text-align:center;">SERVICE BENEFIT</td>
        </tr>

        <tr>
            <td rowspan="2">Service Benefit (5 years to 10 years per years - 14 days & 10 years up per years - 30days)</td>
            <td colspan="3" style="text-align:center;">Total Service Years * Per Years Benefit</td>
            <td colspan="2"  style="text-align:center;">Payable Service Benefit Days</td>
        </tr>
        <tr>
            <td style="text-align:center;">{{$interval->y}}</td>
            <td colspan="2" style="text-align:center;">{{$service_benefit_days}}</td>
            <td colspan="2" style="text-align:center;" >{{$total_service_benefit_days}}</td>
        </tr>

        <tr>
            <td rowspan="2">Net Payable Service Benefit</td>
            <td colspan="3" style="text-align:center;">Per Day Salary(Basic) * Payable</td>
            <td colspan="2"  style="text-align:center;">Net Payable Service Benefit</td>
        </tr>
        <tr>
            <td style="text-align:center;">{{$pay_per_salary_basic}}</td>
            <td colspan="2" style="text-align:center;">{{$total_service_benefit_days}}</td>
            <td colspan="2" style="text-align:center;" >{{$service_benefit_salary}}</td>
        </tr>
        <tr>
            <td colspan="4">Total Payable Amount</td>
            <td colspan="3" style="text-align:center;">{{$total_payable}}</td>
        </tr>
        <tr>
            <td colspan="4">Deduction(stamp)</td>
            <td colspan="3" style="text-align:center;">10</td>
        </tr>
        <tr>
            <td colspan="4">Net Payable Amount </td>
            <td colspan="3" style="text-align:center;">{{$total_payable-10}}</td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <?php
        $after_deduction=$total_payable-10;

    ?>
    <p>Taka In Word:@if($after_deduction<0) (Mines) {{$helper->getBanglaCurrency(-$after_deduction)}}
        @else
            {{$helper->getBanglaCurrency($after_deduction)}}
        @endif </p>
    <br />
    <br />
    <br />
    <br />
    <br />
     <table class="tbl">
        <tr>
            <td style="text-align:center;"><hr>Received By</td>
            <td style="text-align:center;"><hr>Prepared By</td>
            <td style="text-align:center;"><hr>A.G.M(HR, Ad. & Com.)</td>
            <td style="text-align:center;"><hr>A.G.M(F.A)</td>
            <td style="text-align:center;"><hr>COO</td>
            <td style="text-align:center;"><hr>Director</td>
        </tr>
    </table>
</div>
</body>
</html>