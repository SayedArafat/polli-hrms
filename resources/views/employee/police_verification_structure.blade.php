@extends('layouts.master')
@section('title', 'Police Verification')
@section('content')
<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<style>
 .head{
    width: 350px;
 }   

 .head p{
    line-height: 10px;
 }

 .frmControl{
    width: 150px;
    /*padding: 5px;*/
 }

 .family_info{
    border: 1px solid #999;
    padding: 5px;
 }
 .thikana tr>td{
    padding: 5px;
 }
.pcmain{
    font-size: 15px;
}
</style>
<div class="page-content">
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 ><i class="fa fa-users "></i> <strong>Police Varification </strong> Form</h3>
                            
                        </div>
                        <div class="col-md-6" style="margin-top:8px;">
                        </div>


                    </div>

                </div>
                <div class="panel-content pagination2 table-responsive">
                  <div class="pcmain">
                    {!!  Form::open(array('url' => '/police_verification_data', 'method' => 'POST')) !!}
                    @foreach($employee as $emp)
                    <input value="{{$id}}" name="empId" hidden >
                    <div class="head">
                        <p>তারিখঃ <input type="text" name="pvDate" required class="date-picker frmControl"></p>
                        <br />
                        <p>বরাবর,</p>
                        <p>ভারপ্রাপ্ত কর্মকর্তা</p>
                        <p>থানা: <input type="text" name="vpkt" required class="frmControl"></p>
                        <p>জেলা: <input type="text" name="vpkz" required class="frmControl"></p>
                        <br />
                        <p>মাধ্যম,</p>
                        <p>পুলিশ সুপার</p>
                        <p>বিশেষ শাখা</p>
                        <p>জেলাঃ <input type="text" name="mpsz" required class="frmControl"></p>
                    </div><br />
                    <div class="body">
                        <p><b>বিষয়ঃ চরিত্র, তথ্য ও স্থায়ী ঠিকানা যাচাই প্রসঙ্গে ।</b></p>

                        <br />
                        <p>প্রিয় মহোদয়,<br />
                            অত্র গার্মেন্টস প্রতিষ্ঠানের, নামঃ <b>{{$companyInformation->company_name}}</b> কর্মকর্তা/ কর্মচারী / শ্রমিক এর পূর্ণ নামঃ  <b>{{$emp->empFirstName ." ".$emp->empLastName}}</b> কার্ড নং <b>{{en2bnNumber($emp->employeeId)}}</b> পদবীঃ <b>{{$emp->designation}} </b> হিসাবে কর্মরত আছেন।
                        </p>
                        <div style="padding-left:25px !important;" class="family_info">
                           <table width="500px" height="100px">
                              <tr>
                                  <td> ১। ক) পিতা/স্বামীর পূর্ণ নাম</td>
                                  <td>: <b>{{$emp->empFatherName}}</b></td>
                              </tr>
                              <tr>
                                  <td>&nbsp; &nbsp; &nbsp; খ) পিতা/স্বামী চাকরীতে থাকিলে পদের নাম</td>
                                  <td>: <input type="text" name="fathersJob" required class="frmControl"></td>
                              </tr>
                              <tr>
                                  <td>&nbsp; &nbsp; &nbsp; গ) জাতীয়তা</td>
                                  <td>: <input type="text" name="fathersNationality" required class="frmControl"></td>
                              </tr>
                            </table>
                           
                        </div>
                        <div style="padding-left:25px !important;padding-top:3px !important;" class="family_info">
                           <p>২। স্থায়ী ঠিকানাঃ {{$emp->empParAddress}}</p>
                        </div>
                        <div style="padding-left:25px !important;padding-top:3px !important;" class="family_info">
                           <p>৩। বর্তমান বাসস্থানের ঠিকানাঃ {{$emp->empCurrentAddress}}</p>
                        </div>
                        <p style="padding:25px 26px !important;" >৪। প্রার্থী যে সব স্থানে পাঁচ বৎসরে ছয় মাসের অধিক অবস্থান করিয়াছেন সেই সব স্থানের ঠিকানাঃ  </p>
                        <div >
                          <table class="thikana" style="width:100%;text-align:center;" border="1px">
                            <tr style="font-weight:bold;">
                                <td>ঠিকানা </td>
                                <td>তারিখ হইতে </td>
                                <td>তারিখ পর্যন্ত </td>
                                <td width="20%"></td>
                                <td width="5%"></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" name="address[]"></td>
                                <td><input type="text" class="date-picker form-control" name="fromAddress[]"></td>
                                <td><input type="text" class="date-picker form-control" name="toAddress[]"></td>
                                <td><input type="text" class="form-control" name="remarks[]"></td>
                                <td></td>
                            </tr>

                          </table>
                          <br />
                          <span class="add_button btn btn-sm btn-success"><i class="fa fa-plus"></i> Add New Row</span>
                        </div>
                        <br />
                        <br />
                        <p>জন্ম তারিখ (মাধ্যমিক স্কুল পরীক্ষার সার্টিফিকেট অনুযায়ী ) <?php echo en2bnNumber(date("d-m-Y", strtotime($emp->empDOB))); ?> গার্মেন্টস শিল্পের বিশেষ নিরাপত্তার কারনে পুলিশী তদন্তের জন্য পত্রটি আপনার থানায় প্রেরন করা হল। </p>

                        <br />
                        <p>অতএব, সরজমিনে তদন্ত সাপেক্ষে নিম্ন স্বাক্ষরকারীর বরাবরে লিখিত তদন্ত প্রতিবেদন প্রেরণ করার জন্য অনুরোধ করা হল। </p>



                    </div>
                    <hr>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save Information </button>
                    @endforeach
                    
                    {!! Form::close() !!}

                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    var x = 1; //Initial field counter is 1
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.thikana'); //Input field wrapper

    var fieldHTML = '<tr><td><input type="text" class="form-control" name="address[]"></td> <td><input type="text" class="form-control" name="fromAddress[]"></td><td><input type="text" class="form-control" name="toAddress[]"></td><td><input type="text" class="form-control" name="remarks[]"></td><td><button class="remove_button btn btn-sm btn-danger"><i class="fa fa-close"></i></button></td></tr>'; //New input field html 
    
    //Once add button is clicked
    $('.add_button').click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter 
            $(wrapper).append(fieldHTML); //Add field html
        }else{
            alert("You can add maximum 10 rows.");
        }
    });

     $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('td').parent('tr').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>
@include('include.copyright')
@endsection