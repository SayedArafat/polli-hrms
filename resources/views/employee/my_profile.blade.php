@php
    $expectedDate=\Carbon\Carbon::now()->subMonths($employee->probation_period);
    $joiningDate=\Carbon\Carbon::parse($employee->empJoiningDate);
    $difference = $joiningDate->diffInMonths($expectedDate);
@endphp
@section('title', 'My Profile')
@extends('layouts.master')

@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-user"></i> Personal  <strong>Details</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="tab-pane fade active in" id="tab2_1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img class="img-responsive" height="200" width="200" src="
                                            @if($employee->empPhoto ==null)
                                        @if($employee->empGenderId==1 || $employee->empGenderId==3)
                                        {{asset("Employee_Profile_Pic/male.jpg")}}
                                        @endif
                                        @if($employee->empGenderId==2)
                                        {{asset("Employee_Profile_Pic/female.jpg")}}
                                        @endif

                                        @endif
                                        @if($employee->empPhoto!=null)
                                        {{asset("Employee_Profile_Pic/".$employee->empPhoto)}}
                                        @endif
                                        ">

                                    </div>
                                    <div class="col-md-9">
                                        <h2><strong>{{$employee->empFirstName." ".$employee->empLastName }}</strong></h2>
                                        <p><i class="fa fa-phone"></i>{{$employee->empPhone}}<span class="padLeft20"><i class="fa fa-envelope"></i></span><span>{{$employee->empEmail}}</span></p>
                                        <hr>
                                        <span>
                                            Status: <span class="employee-profile-text">
                                                @if($employee->empAccStatus=='0')
                                                    <span class="red-text">Inactive</span>

                                                @else
                                                    <span>Active</span>

                                                @endif
                                            </span>
                                        </span>
                                        <span class="padLeft20">
                                            Employee Id:<span class="employee-profile-text">
                                                {{ $employee->employeeId }}
                                            </span>
                                        </span>
                                        <span class="padLeft20">
                                            Department: <span class="employee-profile-text">
                                                {{ $employee->departmentName }}
                                            </span>
                                        </span>
                                        <span class="padLeft20">
                                            Designation: <span class="employee-profile-text">
                                                {{$employee->designation}}
                                            </span>
                                        </span>
                                        <hr>
                                    </div>
                                </div>

                                <div class="profile-body row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="panel panel-info">
                                                <div class="panel-heading"><h3><strong>Personal Information</strong></h3></div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <span>
                                                                National ID:<span class="employee-profile-text">
                                                                    {{$employee->empNid }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Nationality:<span class="employee-profile-text">
                                                                    {{$employee->nationalitiesName }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Date of Birth:
                                                                <span class="employee-profile-text">
                                                                    @if($employee->empDOB!=null)
                                                                        {{\Carbon\Carbon::parse($employee->empDOB)->format('j F Y') }}
                                                                    @endif
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>


                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Gender:<span class="employee-profile-text">
                                                                    @if($employee->empGenderId==1)
                                                                        Male
                                                                    @endif
                                                                    @if($employee->empGenderId==2)
                                                                        Female
                                                                    @endif
                                                                    @if($employee->empGenderId==3)
                                                                        Others
                                                                    @endif
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Marital Status:<span class="employee-profile-text">
                                                                    {{$employee->ms_name }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Joining Date:
                                                                <span class="employee-profile-text">
                                                                    {{\Carbon\Carbon::parse($employee->empJoiningDate)->format('j F Y') }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>


                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Current Address:<br><span class="employee-profile-text">
                                                                    {{$employee->empCurrentAddress}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Permanent Address:<br><span class="employee-profile-text">
                                                                    {{$employee->empParAddress }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Religion:
                                                                <span class="employee-profile-text">
                                                                    {{ $employee->empReligion }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>



                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Father's Name:<span class="employee-profile-text">
                                                                    {{$employee->empFatherName}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Mother's Name:<span class="employee-profile-text">
                                                                    {{$employee->empMotherName }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Biometric ID:
                                                                <span class="employee-profile-text">
                                                                    {{ $employee->empGlobalId}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>



                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Unit:<span class="employee-profile-text">
                                                                    {{$employee->unit_name}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Section:<span class="employee-profile-text">
                                                                    {{$employee->empSection}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Salary Type:<span class="employee-profile-text">
                                                                    {{$employee->salary_type }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Payment Mode:<span class="employee-profile-text">
                                                                    {{$employee->payment_mode }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                    </div>

                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Bank Account:<span class="employee-profile-text">
                                                                    {{$employee->bank_account}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Bank Information:<span class="employee-profile-text">
                                                                    {{$employee->bank_info }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Emergency Contact:<span class="employee-profile-text">
                                                                    {{$employee->empEcontactName}}
                                                                </span>
                                                            </span>
                                                        </div>

                                                    </div>

                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Emergency Contact Relation:<span class="employee-profile-text">
                                                                    {{$employee->emergency_contact_relation}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Emergency Contact Phone:<span class="employee-profile-text">
                                                                    {{$employee->emergencyPhone }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Emergency Contact Address:<br>
                                                                <span class="employee-profile-text">
                                                                    {{ $employee->emergencyAddress }}
                                                                    {{--{{\Carbon\Carbon::parse($employee->empDOB)->format('l j F Y') }}--}}

                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <?php $empId = $employee->id; ?>
                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Reference Type:<span class="employee-profile-text">
                                                                    {{$employee->reference_type }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Reference Details:<span class="employee-profile-text"><br>
                                                                    {{$employee->reference_description }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Skill Level:<span class="employee-profile-text">
                                                                    {{$employee->skill_level}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Working Group:<span class="employee-profile-text">
                                                                    {{$employee->work_group}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Blood Group:<span class="employee-profile-text">
                                                                    {{$employee->empBloodGroup }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Card:<span class="employee-profile-text">
                                                                    {{$employee->empCardNumber }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>


                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Remaining Probation Period:<span class="employee-profile-text">
                                                                @if($expectedDate<$joiningDate)
                                                                    <span class="red-text">{{$difference}}</span> Month/s</span>
                                                                @else
                                                                    <span class="green-text">Completed</span>
                                                                @endif
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Employee Status:<span class="employee-profile-text">
                                                                    @if($employee->empAccStatus==1)
                                                                        Active
                                                                    @else
                                                                        <span class="red-text">Inactive</span>

                                                                    @endif
                                                                </span>
                                                            </span>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <span>
                                                                Date of Discontinuation:<span class="employee-profile-text">
                                                                    @if($employee->date_of_discontinuation !=null)
                                                                        {{\Carbon\Carbon::parse($employee->date_of_discontinuation)->format('j F Y') }}
                                                                    @endif
                                                                </span>
                                                            </span>
                                                        </div>

                                                    </div>
                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Reason of Discontinuation:
                                                                <span class="employee-profile-text">
                                                                    {{ $employee->reason_of_discontinuation }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <script>

        $("input[name='others']").change(function(){
            alert( "Handler for .click() called." );

        });


        //        <script>
        $(document).on('click','.open_modal',function(){
            var id = $("#salary_grade_modal").val();
            var url = $('#salary_grade_url').val();
            var urlid=url+'/'+id;
            var formData = {
                id: $(this).val(),
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                type: "GET",
                url: urlid,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    $('#employeesalarymodaledit').modal('show');
                    $.each(data, function (i, item) {
                        var empid=document.getElementById('emp_idsss').value= item.emp_id;
                        var acid=document.getElementById('actual_id').value= item.default_id;
                        var gradeid=document.getElementById('current_grade_id').value= item.grades_id;
                        var empname=document.getElementById('emp_name').innerHTML=item.empFirstName+item.empLastName;
                        var gradename=document.getElementById('current_grade').value= item.grade_name;
                        var basic=document.getElementById('basic').value= item.basic_salary;
                        var house=document.getElementById('house_rant').value= item.house_rant;
                        var medical=document.getElementById('medical').value= item.medical;
                        var transport=document.getElementById('transport').value= item.transport;
                        var food=document.getElementById('foods').value= item.food;
                        var others=document.getElementById('others').value= item.default_others;
                        var total_salary =document.getElementById('total_gross_salary').innerHTML= item.total_employee_salary;
                    });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });


        $(document).ready(function(){
            $("#grade_employee_select").change(function(){
                var url = $('#grade_url').val();
                var id=$("#grade_employee_select").val();
                var urlid=url+'/'+id;

                var formData = {
                    id: $('#grade_employee_select').val(),
                }
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })
                $.ajax({
                    type: "GET",
                    url: urlid,
                    data: formData,
                    dataType: 'json',
                    success: function (data) {
                        $.each(data, function (i, item) {
                            var gradeid=document.getElementById('current_grade_id').value= item.id;
                            var gradename=document.getElementById('current_grade').value= item.grade_name;
                            var basic=document.getElementById('basic').value= item.basic_salary;
                            var house=document.getElementById('house_rant').value= item.house_rant;
                            var medical=document.getElementById('medical').value= item.medical;
                            var transport=document.getElementById('transport').value= item.transport;
                            var food=document.getElementById('foods').value= item.food;
                            var others=document.getElementById('others').value= item.others;
                        });
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });




        });




        {{--</script>--}}
$('.show-nominee-delete').click(function (event) {
            event.preventDefault();
//            alert('fuck');
            var url=$(this).attr('href');

            $.ajax({
                url:url,
                method:'get',
                success:function(response){
                    $('#nominee-delete-form').html(response);

                }
            });
            $('#deleteNominee').modal('show');


        });


        $('.show-nominee-edit').click(function (event) {
            event.preventDefault();
            var url=$(this).attr('href');
//            alert(url);
            $.ajax({
                url:url,
                method:'get',
                dataType:'html',
                success:function (response) {
                    $('#nominee-edit-form').html(response);
                }

            });
            $('#editNomineeModal').modal('show');

        });

        $('.show-other-edit').click(function (event) {
            event.preventDefault();
            var url=$(this).attr('href');

            $.ajax({
                url:url,
                dataType:'html',
                success:function (response) {
                    $('#other-edit-form').html(response);

                }
            });

            $('#editOther').modal('show');

        });

        $('.show-other-delete').click(function (event) {
            event.preventDefault();
            var url=$(this).attr('href');
            $.ajax({
                url:url,
                dataType:'html',
                success:function (response) {
                    $('#other-attachment-delete-form').html(response);

                }

            });
            $('#deleteOther').modal('show');

        });

        $('.show-delete-we-modal').click(function (event) {
            event.preventDefault();
//            alert('something');
            var url=$(this).attr('href');
//            console.log(url);
//            var method='DELETE';
            $.ajax({
                url:url,
//                method:method,
                dataType:'html',
                success:function (response) {
                    $('#working-experience-delete-form').html(response);

                }

            });
            $('#deleteWorkingExperience').modal('show');

        });

        $('.show-edit-we-modal').click(function (event) {
            event.preventDefault();
//            alert("c");
            var url=$(this).attr('href');
//            console.log(url);
            $.ajax({
                url:url,
                dataType:'html',
                success:function (response) {
                    $('#working-experience-edit-form').html(response);

                },
                error:function (xhr) {
                    console.log(xhr);

                }
            });
            $('#editWorkingExperience').modal('show');

        });


        //        $('#update-working-experience').click(function (event) {
        //            event.preventDefault();
        //             $.ajaxSetup({
        //                headers: {
        //                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //                }
        //             });
        //            var form=$('#working-experience-edit-form').find('form'),
        //                method="PATCH";
        //            var url=form.attr('action');
        ////            var formData = new FormData(form);
        ////            formData.append('file', $('input[type=file]')[0].files[0]);
        ////            console.log(url);
        //            $.ajax({
        //                url:url,
        //                method:method,
        //                async:false,
        ////                data:form.serialize(),
        //                data:new FormData($("#editFrom")[0]),
        //                processData: false,
        //                contentType: false,
        //                success:function (response) {
        //                    console.log(response);
        //
        //                },
        //                error:function (xhr) {
        //
        //                }
        //
        //            });
        //
        ////            alert('hello world');
        //
        //
        //        });

        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>



    @include('include.copyright')

@endsection




