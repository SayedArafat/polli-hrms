<div class="modal fade" id="educationalInfo" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>New Educational Information</strong></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST','action'=>['EmployeeEducationInfoController@store'],'files'=>true]) !!}
                {!! Form::hidden('emp_id',$emp_id) !!}

                <div class="form-group">
                    {!! Form::label('empExamTitle','Examination Name:') !!}
                    {!! Form::text('empExamTitle',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter Examination Name']) !!}

                </div>
                <div class="form-group">
                    {!! Form::label('empInstitution','Institution:') !!}
                    {!! Form::text('empInstitution',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter InstitutionName']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('empResult','Exam Result:') !!}
                    {!! Form::text('empResult',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter Result']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('empScale','Scale:') !!}
                    {!! Form::text('empScale',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter Grade Scale']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('empPassYear','Passing Year:') !!}
                    {!! Form::text('empPassYear',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter Passing Year']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('empCertificate','Attachment:') !!} (Max Size:5mb)
                    {!! Form::file('empCertificate', null, ['class'=>'form-control-file','required type'=>'text']) !!}

                </div>


            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-primary">Add New</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>