@extends('layouts.master')
@section('title', 'Attendance File List')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">

                    <div class="panel-header">
                        <h3>File<strong> List</strong></h3>
                    </div>
                    <a href="{{route('attendance.file.create')}}" class="btn custom-btn btn-xs ">Add New</a>


                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Date</th>
                                <th>File Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $csv_data)
                                <tr>
                                    <td>{{$csv_data->id}}</td>
                                    <td>{{$csv_data->comment}}</td>
                                    <td>{{$csv_data->date}}</td>
                                    <td>{{$csv_data->attachment}}</td>
                                    <td>

                                        {{--<button style="background: #703a71;" type="button" name="file_name" class="btn btn-success btn-xs import-btn">Download</button>--}}
                                        {{--<button type="button" name="file_name" class="btn btn-success btn-xs import-btn">Import</button>--}}

                                        <a href="{{asset('file')."/".$csv_data->attachment}}" class="btn btn-blue btn-sm" title="download attachment"><i class="fa fa-download"></i></a>
                                        <a href="{{route('file.show.delete',$csv_data->id)}}" class="btn btn-danger btn-sm show-delete-file-btn" title="delete file"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>

                        {{--{{Form::open(array('url' => 'csv_upload','method' => 'post','files' =>true))}}--}}
                        {{--<label>Upload File</label>--}}
                        {{--<div class="form-group">--}}
                        {{--<input type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" class="form-control" name="upload-file">--}}
                        {{--</div>--}}
                        {{--<input type="hidden" name="cur_date" value="{{date("d/m/Y")}}">--}}
                        {{--<button id="salary_check" type="submit" class="btn btn-primary">Upload</button>--}}
                        {{--{{ Form::close() }}--}}
                    </div>
                </div>
            </div>
        </div>
            @include('attendance.modal.deleteFile')

        @include('include.copyright')

    </div>

    <script>
        $(document).ready(function(){
            $('.show-delete-file-btn').click(function (event) {
                event.preventDefault();
//                alert('clicked');
                var url=$(this).attr('href')
                console.log(url);
                $.ajax({
                    url:url,
                    dataType:'html',
                    success:function (resopnse) {
                        $('#file-delete-form').html(resopnse);
                        
                    }

                });
                $('#deleteFile').modal('show');

            });
        });
    </script>

@endsection