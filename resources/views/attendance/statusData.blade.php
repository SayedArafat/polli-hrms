<div class="panel">
    <div class="panel-header panel-controls">
        <h3><i class="fa fa-table"></i> <strong>Employee </strong> List</h3>
    </div>
    <div class="panel-content pagination2 table-responsive">
        <table class="table table-hover table-dynamic ">
            <thead>
            <tr>
                <th>Order</th>
                <th>Name</th>
                <th>Employee Id</th>
                <th>Designation</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($applications as $key=>$a)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$a->empFirstName." ".$a->empLastName}}</td>
                    <td>{{$a->employeeId}}</td>
                    <td>{{$a->designation}}</td>
                    <td class="red-text">Absent</td>
                    <td>
                        <a href="{{route('employee.attendance.add',[$a->id,$date])}}" class="btn btn-default btn-sm show-attendance-add"><i class="fa fa-plus"></i></a>
                        {{--<button type="button" class="btn btn-info btn-xs show-edit-modal" data-toggle="modal" data-target="edit{{$a->id}}">Open</button>--}}
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>
    $('.show-attendance-add').click(function (event) {
        event.preventDefault();
        var url=$(this).attr('href');

//        alert(url);
        $.ajax({
            url:url,
            method:'GET',
            dataType:'html',
            success:function (response) {
                $('#show-attendance-form').html(response);

            }
        })

        $('#showAttendance').modal('show');

    });

</script>