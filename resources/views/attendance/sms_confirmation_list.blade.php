@extends('layouts.master')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                        <div class="col-md-6">
                            <h3 ><i class="fa fa-envelope "></i> <strong> SMS Confirmation</strong></h3>
                        </div>
                       
                        </div>

                    </div>
                    <div class="panel-content">

                        @if(Session::has('message'))
                            <p class="alert alert-danger btn-lg"><b>{{ Session::get('message') }}</b></p><br /><br />
                        @endif

                            @php $i=1; @endphp
                            @foreach($employees as $employee)
                                @if($employee['status']==0)
                                    <p class="btn btn-danger">{{$i}}. SMS notification failed to sent <b>{{$employee['employeeName']}} ( {{$employee['employeeId']}})</b> -> Phone: {{$employee['empPhone']}}</p>
                                    <br />
                                @endif
                                @if($employee['status']==1)
                                    <p class="btn btn-success">{{$i}}. SMS notification successfully sent to <b>{{$employee['employeeName']}} ( {{$employee['employeeId']}})</b> -> Phone: {{$employee['empPhone']}} for  <b>{{$employee['absentDays']}} days</b> consecutive absences.</p>
                                    <br />
                                @endif
                                @if($employee['status']==2)
                                    <p class="btn btn-danger">{{$i}}. No mobile number available for <b>{{$employee['employeeName']}} ( {{$employee['employeeId']}})</b></p>
                                    <br />
                                @endif
                                @php $i++; @endphp
                            @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
    <script>
      $(document).ready(function(){
          setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
      });
    </script>
@endsection