{!! Form::hidden('id',$time->id) !!}

<div class="required form-group">
    <label class="control-label">In Time(HH:MM am/pm)</label>
    <div class="append-icon">
        <input type="time" id="in_time" name="stime" class="form-control" value="{{$time->in_time}}" required>
    </div>
</div>
<div class="required form-group">
    <label class="control-label">Out Time(HH:MM am/pm)</label>
    <div class="append-icon">
        <input type="time" id="out_time" name="etime" class="form-control" value="{{$time->out_time}}" required>
    </div>
</div>