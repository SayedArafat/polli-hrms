@extends('layouts.master')
@section('title', 'Attendance Setup')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3>Office<strong> Hour</strong></h3>
                    </div>
                    <div class="panel-content">
                        <table class="table-responsive table">
                            <thead>
                            <tr>
                                <th>Order</th>
                                <th>Entry Time</th>
                                <th>Max: Entry Time</th>
                                <th>Exit Time</th>
                                <th>Min Exit Time</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $order=0; @endphp
                            @foreach($data as $setup)
                                @php $order++; @endphp
                            <tr class="warning">
                                <td>{{$order}}</td>
                                <td>{{date("g:i A",strtotime($setup->entry_time))}}</td>
                                <td>{{date("g:i A",strtotime($setup->max_entry_time))}}</td>
                                <td>{{date("g:i A",strtotime($setup->exit_time))}}</td>
                                <td>{{date("g:i A",strtotime($setup->min_exit_time))}}</td>
                                <td><a data-toggle="modal" data-target="#{{$setup->id}}" class="btn btn-success btn-sm" type="submit"><i class="fa fa-edit"></i></a></td>
                            </tr>
                            <!-- Modal -->
                            <div class="modal fade" id="{{$setup->id}}" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Update</h4>
                                        </div>
                                        <div class="modal-body">
                                            {{Form::open(array('url' => 'attendance/settings/update','method' => 'post'))}}
                                            <div class="form-group">
                                                <label for="entryTime">Entry Time</label>
                                                <input type="time" name="entry_time" class="form-control form-white" required value="{{$setup->entry_time}}" id="entryTime" placeholder="Enter Entry Time " >
                                            </div>
                                            <div class="form-group">
                                                <label for="entryTime">Max: Entry Time</label>
                                                <input type="time" name="max_entry_time" class="form-control form-white" required value="{{$setup->max_entry_time}}" id="entryTime" placeholder="Enter Maximum Entry Time " >
                                            </div>
                                            <div class="form-group">
                                                <label for="exitTime">Exit Time</label>
                                                <input type="time" name="exit_time" class="form-control form-white" required value="{{$setup->exit_time}}" id="exitTime" placeholder="Enter Exit Time " >

                                            </div>
                                            <div class="form-group">
                                                <label for="minexitTime">Minimum Exit Time</label>
                                                <input type="time" name="min_exit_time" class="form-control form-white" required value="{{$setup->min_exit_time}}" id="minexitTime" placeholder="Enter Minimum Exit Time " >

                                            </div>
                                            <input type="hidden" name="settings_id" value="{{$setup->id}}">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            </tbody>
                        </table>
                        @if($data->isEmpty())
                        <div class="panel-content pagination2 table-responsive">
                            {{Form::open(array('url' => 'attendance/settings/store','method' => 'post'))}}
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="pwd">Entry Time:</label>
                                <input type="time" name="entry_time" class="form-control form-white" placeholder="only number" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="pwd">Maximum Entry Time:</label>
                                <input type="time" name="max_entry_time" class="form-control form-white" placeholder="only number" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="pwd">Exit Time:</label>
                                <input type="time" name="exit_time" class="form-control form-white" placeholder="only number" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="pwd">Minimum Exit Time:</label>
                                <input type="time" name="min_exit_time" class="form-control form-white" placeholder="only number" required>
                            </div>
                        </div>

                            <button type="submit" class="btn btn-success">Create</button>
                            {{ Form::close() }}
                        </div>
                       @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection