@extends('layouts.master')
@section('title', 'Manual Attendance')
@section('content')
    <div class="page-content">
        <div class="row">
            @if(Session::has('message'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
            @endif
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Create Manual </strong>Attendance</h2>
                    </div>
                    <div class="panel-body bg-white">
                        <div class="">
                            {!! Form::open(['method'=>'POST', 'action'=>'AttendanceController@manualAttendance']) !!}
                                <div class="required form-group">
                                    <label class="control-label">Employee Name (Id)</label>
                                    <select name="emp_id" id="emp_id" class="form-control" data-search="true">
                                        @foreach($employees as $emp)
                                            <option value="{{$emp->id}}">{{$emp->empFirstName." (".$emp->employeeId.")"}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="required form-group">
                                    <label class="control-label">Date</label>
                                    <div class="append-icon">
                                        <input type="text" name="date" autocomplete="off" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                                <div class="required form-group">
                                    <label class="control-label">In Time(HH:MM am/pm)</label>
                                    <div class="append-icon">
                                        <input type="time" name="stime" class="form-control" required>
                                    </div>
                                </div>
                                <div class="required form-group">
                                    <label class="control-label">Out Time(HH:MM am/pm)</label>
                                    <div class="append-icon">
                                        <input type="time" name="etime" class="form-control" required>
                                    </div>
                                </div>
                                <div class="text-center  m-t-20">
                                    <button type="submit" class="btn btn-embossed btn-primary">Create</button>
                                    <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                                </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);

    </script>
    @include('include.copyright')

@endsection
