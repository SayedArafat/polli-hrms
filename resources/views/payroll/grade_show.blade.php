@extends('layouts.master')
@section('title', 'Grade List')
@section('content')
    <style>
        .pagination li a {
            color: #A2A2A2;
            font-size: 12px;
            background: darkblue;
            color: #ffffff;
        }
    </style>
    <div class="page-content">
      @if(Session::has('message'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif
      @if(Session::has('failedMessage'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('failedMessage') }}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-table"></i> <strong>Grade </strong> List</h3>
                        <a href="{{url('payroll/grade')}}" class="btn btn-success btn-sm">Add More</a>
                    </div>
                    <div class="panel-content table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Grade</th>
                                <th>Basic</th>
                                <th>House</th>
                                <th>Medical</th>
                                <th>Transportation</th>
                                <th>Food</th>
                                <th>Others</th>
                                <th>Total</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $itemgrade)
                                    <tr>
                                        <td>{{$itemgrade->id}}</td>
                                        <td>{{$itemgrade->grade_name}}</td>
                                        <td>{{$itemgrade->basic_salary}}</td>
                                        <td>{{$itemgrade->house_rant}}</td>
                                        <td>{{$itemgrade->transport}}</td>
                                        <td>{{$itemgrade->medical}}</td>
                                        <td>{{$itemgrade->food}}</td>
                                        <td>{{$itemgrade->others}}</td>
                                        <td>{{$itemgrade->total_salary}}</td>
                                        <td>
                                            <a data-toggle="modal" data-target="#{{$itemgrade->id}}" title="Edit" class="btn btn-success btn-sm" type="submit"><i class="fa fa-edit"></i></a>
                                            <a href="{{ url('/grade_delete/'.$itemgrade->id) }}" onclick="return confirm('Are you sure to delete?')" title="Delete" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <!-- Modal -->
                                <div class="modal fade" id="{{$itemgrade->id}}" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Update Grade</h4>
                                            </div>
                                            <div class="modal-body">
                                           {{Form::open(array('url' => 'grade_update','method' => 'post'))}}
										<div class="form-group">
											<label for="gradename">Grade Name</label>
											<input type="text" name="grade_name" class="form-control form-white" id="gradename" value="{{$itemgrade->grade_name}}" placeholder="Enter Grade Name" required>
										</div>
										<div class="form-group">
											<label for="basicamount">Basic</label>
											<input type="number" name="basic_amount" step="any" class="form-control form-white" id="basicamount" value="{{$itemgrade->basic_salary}}" placeholder="Amount" required>
										</div>
									   <div class="form-group">
										   <label for="rantamount">House Rent</label>
										   <input type="number" name="house_amount" step="any" class="form-control form-white" id="rantamount" value="{{$itemgrade->house_rant}}" placeholder="Amount" required>
									   </div>
									   <div class="form-group">
										   <label for="medicalamount">Medical</label>
										   <input type="number" name="medical_amount" step="any" class="form-control form-white" id="medicalamount" value="{{$itemgrade->transport}}" placeholder="Amount" required>
									   </div>
									   <div class="form-group">
										   <label for="transportamount">Transportation</label>
										   <input type="number" name="transport_amount" step="any" class="form-control form-white" id="transportamount" value="{{$itemgrade->medical}}" placeholder="Amount" required>
									   </div>
									   <div class="form-group">
										   <label for="foodamount">Food</label>
										   <input type="number" name="food_amount" step="any" class="form-control form-white" id="foodamount" value="{{$itemgrade->food}}" placeholder="Amount" required>
									   </div>
									   <div class="form-group">
										   <label for="othersamount">Others</label>
										   <input type="number" name="others_amount" step="any" class="form-control form-white" id="othersamount" value="{{$itemgrade->others}}" placeholder="Amount" required>
									   </div>
									  <input type="hidden" name="user_name" value="{{auth::user()->name}}">
									  <input type="hidden" name="grade_id" value="{{$itemgrade->id}}">
									  <button type="submit" class="btn btn-primary">Update</button>
                                     {{ Form::close() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                        {{$data->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    @include('include.copyright')
@endsection