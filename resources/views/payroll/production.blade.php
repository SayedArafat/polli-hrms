@extends('layouts.master')
@section('title', 'Production Bonus')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('productionamount'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('productionamount') }}</p>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading">
                Production Bonus
            </div>
            <div class="panel-body">
                {{Form::open(array('url' => 'production/store','method' => 'post'))}}
                <div class="col-md-6 fde">
                    <div class="form-group">
                        <label>Select Employee</label>
                        <select name="emp_id" class="form-control"  data-search="true">
                            <option value="">Please Select</option>
                            @foreach($data as $employee)
                                <option value="{{$employee->id}}">{{$employee->empFirstName}} {{$employee->employeeId}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Enter Amount</label>
                        <input type="text" name="production_amount" class="form-control" placeholder="Enter Amount" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <button type="submit" class="btn btn-success">Add</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection