@extends('layouts.master')
@section('title', 'Payslip Generate')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3>Employee<strong> PaySlip</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="panel-content pagination2 table-responsive">
                            <table class="table table-hover table-dynamic">
                                <p class="text-center">Total day in month : <?php echo date('t'); ?></p>
                                <thead>
                                <tr>
                                    <th>Sn</th>
                                    <th>EmpId</th>
                                    <th>Name</th>
                                    <th>Designation</th>
                                    <th>Grade</th>
                                    <th>Basic</th>
                                    <th>House</th>
                                    <th>Medical</th>
                                    <th>Food</th>
                                    <th>Transport</th>
                                    <th>Gross</th>
                                    <th>Cl</th>
                                    <th>Sl</th>
                                    <th>el</th>
                                    <th>ml</th>
                                    <th>work days</th>
                                    <th>absent days</th>
                                    <th>weekend</th>
                                    <th>holiday</th>
                                    <th>total pay days</th>
                                    {{--<th>deduction adv</th>--}}
                                    <th>deduction absent</th>
                                    <th>gross pay</th>
                                    <th>Hrs overtime</th>
                                    <th>rat overtime</th>
                                    <th>amt overtime</th>
                                    {{--<th>att bonus</th>--}}
                                    {{--<th>special allow</th>--}}
                                    <th>net wages</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $order=0; @endphp
                                @foreach($data as $payslip)
                                    @php
                                        $order++;
                                        $cl=1;
                                        $sl=0;
                                        $el=0;
                                        $ml=0;
                                        $current_month_days=date('t');
                                        $weekend=5;
                                        $absent=2;
                                        $holiday=2;
                                        $hours_overtime=41;
                                        $company_working_days=$current_month_days-$weekend-$holiday;
                                        $total_amount_overtime=1793;
                                        $total_leave=$cl+$sl+$el+$ml;
                                        $basic=$payslip->basic_salary;
                                        $final_house_rant=$basic/100*40;
                                        $gross=$payslip->basic_salary+$final_house_rant+$payslip->medical+$payslip->food+$payslip->transport;
                                        $employee_working_days=$current_month_days-$weekend-$total_leave-$absent-$holiday;
                                        $employee_total_payable_days=$employee_working_days+$weekend+$holiday+$total_leave;
                                        $total_deduction_absent=$basic/$current_month_days*$absent;
                                        $gross_pay=$gross-$total_deduction_absent;
                                        $net_wages=$gross_pay+$total_amount_overtime;
                                    @endphp
                                    <tr>
                                        <td>{{$order}}</td>
                                        <td>{{$payslip->employeeId}}</td>
                                        <td>{{$payslip->empFirstName}}</td>
                                        <td>{{$payslip->designation}}</td>
                                        <td>{{$payslip->grade_name}}</td>
                                        <td>{{$payslip->basic_salary}}</td>
                                        <td>{{$final_house_rant}}</td>
                                        <td>{{$payslip->medical}}</td>
                                        <td>{{$payslip->food}}</td>
                                        <td>{{$payslip->transport}}</td>
                                        <td>{{$gross}}</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>{{$employee_working_days}}</td>
                                        <td>2</td>
                                        <td>5</td>
                                        <td>2</td>
                                        <td>{{$employee_total_payable_days}}</td>
                                        {{--<td>0</td>--}}
                                        <td>{{$total_deduction_absent}}</td>
                                        <td>{{$gross_pay}}</td>
                                        <td>41</td>
                                        <td>43.73</td>
                                        <td>1793</td>
                                        {{--<td>0</td>--}}
                                        {{--<td>0</td>--}}
                                        <td>{{$net_wages}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection