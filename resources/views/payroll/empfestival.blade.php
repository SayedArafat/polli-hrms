@extends('layouts.master')
@section('title', 'Festival Bonus')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('fbonus'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('fbonus') }}</p>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading">
                Festival Bonus
            </div>
            <div class="panel-body">
                {{Form::open(array('url' => 'designation/employee/bonus/store','method' => 'post'))}}
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" name="festival_title" placeholder="Bonus Title" required>
                    </div>
                </div>

                <div class="col-md-6 fde">
                    <div class="form-group">
                        <label>Select Designation</label>
                        <select id="designation_id" name="designation_id" class="form-control"  data-search="true">
                            <option>Please Select</option>
                            <option value="0">All Designation</option>
                            @foreach($designation as $designations)
                                <option value="{{$designations->id}}">{{$designations->designation}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 fe">
                    <div class="form-group">
                        <label>Select Department</label>
                        <select id="department_id" name="department_id" class="form-control"  data-search="true">
                            <option>Please Select</option>
                            <option value="0">All Department</option>
                            @foreach($department as $departments)
                                <option value="{{$departments->id}}">{{$departments->departmentName}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <img style="display: none;text-align: center;" id="designation_load" width="100px" height="100px" src="../hrm_script/images/designation.gif">
                <img style="display: none;text-align: center;" id="department_load" width="100px" height="100px" src="../hrm_script/images/designation.gif">
                <input type="hidden" name="designation_url" id="designation_url" value="{{URL::to('/designation/employee/')}}">
                <input type="hidden" name="designation_url_all" id="designation_url_all" value="{{URL::to('/tests')}}">
                <input type="hidden" name="department_url" id="department_url" value="{{URL::to('/department/employee')}}">
                <input type="hidden" name="department_url_all" id="department_url_all" value="{{URL::to('/testsone')}}">
                <table id="designation_tbl" style="display:none" class="table table-bordered">
                    <div class="col-md-3 p">
                         <div class="form-group">
                            <label style="display: none"  id="label_percent">Enter Percent:</label>
                            <input style="display: none"  type="text" class="percent_value form-control" id="percent_value" >
                         </div>
                    </div>
                    <div class="col-md-3 m">
                      <div class="form-group">
                        <label style="display:none" id="label_amount">Enter Amount:</label>
                        <input style="display:none" type="text" class="amount_value form-control" id="amount_value">
                      </div>
                    </div>
                    <button style="display: none;margin-top: 21px;" id="designation_button" type="submit" class="btn btn-success">Save</button>
                    <thead>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <table id="department_tbl" style="display:none" class="table table-bordered">
                    <div class="col-md-3 p">
                        <div class="form-group">
                            <label style="display: none"  id="label_percent">Enter Percent:</label>
                            <input style="display: none"  type="text" class="percent_value form-control" id="percent_value" >
                        </div>
                    </div>
                    <div class="col-md-3 m">
                        <div class="form-group">
                            <label style="display:none" id="label_amount">Enter Amount:</label>
                            <input style="display:none" type="text" class="amount_value form-control" id="amount_value">
                        </div>
                    </div>
                    <button style="display: none;margin-top: 21px;" id="designation_button" type="submit" class="btn btn-success">Save</button>
                    <thead>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                {{ Form::close() }}
                {{--<table class="table table-bordered">--}}
                    {{--<thead>--}}
                    {{--<tr>--}}
                        {{--<th>First Name</th>--}}
                        {{--<th>Last Name</th>--}}
                        {{--<th>Username</th>--}}
                    {{--</tr>--}}
                    {{--</thead>--}}
                    {{--<tbody>--}}
                    {{--<tr>--}}
                        {{--<td>Mark</td>--}}
                        {{--<td>Otto</td>--}}
                        {{--<td>@mdo</td>--}}
                    {{--</tr>--}}
                    {{--</tbody>--}}
                {{--</table>--}}
                {{--@foreach($festivalBonus as $fes)--}}
                    {{--{{$fes->id}}--}}
                    {{--@endforeach--}}
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#designation_id").change(function(){
                $(".fe").hide();
                var all=$("#designation_id").val()
                var urls=$("#designation_url_all").val();
                if(all==0){
                    $("#designation_tbl").show();
                    $("#label_percent").show();
                    $("#percent_value").show();
                    $("#amount_value").show();
                    $("#label_amount").show();
                    $("#designation_load").show();
                    $.ajax({
                        type: "GET",
                        url: urls,
                        data: $("#designation_id").val(),
                        dataType: 'json',
                        success: function (data) {
                            $("#designation_load").hide();
                            var designation='';
                            designation+="<tr><th>Employee</th><th>Designation</th><th>Current Salary</th> <th>Percent</th> <th>Amount</th></tr>";
                            $.each(data, function (i, item) {
                                if(item.total_employee_salary==null){
                                    item.total_employee_salary=0;
                                }else{
                                    item.total_employee_salary
                                }
                                designation += '<tr> <td>'+item.empFirstName+'<input type="hidden" name="emp_id[]" value='+item.id+'>'+'</td>  <td>'+item.designation+'</td>  <td>'+'<input type="text" name="total_salary[]" class="form-control" value='+item.total_employee_salary+'>'+'</td> <td>'+'<input class="percent form-control" id="percent" type="text" name="percent[]">'+'</td>  <td>'+'<input class="amount form-control" id="amount" type="text" name="amount[]">'+'</td> </tr>';
                            });
                            $('#designation_tbl').html(designation);
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
                else{
                    $("#designation_tbl").show();
                    $("#label_percent").show();
                    $("#percent_value").show();
                    $("#amount_value").show();
                    $("#label_amount").show();
                    $("#designation_load").show();
                    var id = $(this).val();
                    var url = $('#designation_url').val();
                    var urlid=url+'/'+id;
                    var formData = {
                        id: $(this).val(),
                    }
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    })
                    $.ajax({
                        type: "GET",
                        url: urlid,
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            $("#designation_load").hide();
                            var designation='';
                            designation+="<tr><th>Employee</th><th>Designation</th><th>Current Salary</th> <th>Percent</th> <th>Amount</th></tr>";
                            $.each(data, function (i, item) {
                                if(item.total_employee_salary==null){
                                    item.total_employee_salary=0;
                                }else{
                                    item.total_employee_salary
                                }
                                designation += '<tr> <td>'+item.empFirstName+'<input type="hidden" name="emp_id[]" value='+item.id+'>'+'</td>  <td>'+item.designation+'</td>  <td>'+'<input type="text" name="total_salary[]" class="form-control" value='+item.total_employee_salary+'>'+'</td> <td>'+'<input class="percent form-control" id="percent" type="text" name="percent[]">'+'</td>  <td>'+'<input class="amount form-control" id="amount" type="text" name="amount[]">'+'</td> </tr>';
                            });
                            $('#designation_tbl').html(designation);
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
               });
            $('#percent_value').keyup(function () {
                $(".m").hide();
                $("#designation_button").show();
                var input = document.getElementsByClassName("percent");
                for (var i = 0; i < input.length; i++){
                    input[i].value = document.getElementById('percent_value').value;
                }
            });
            $('#amount_value').keyup(function () {
                $(".p").hide();
                $("#designation_button").show();
                var input = document.getElementsByClassName("amount");
                for (var i = 0; i < input.length; i++){
                    input[i].value = document.getElementById('amount_value').value;
                }
            });
            $("#designation_button").click(function(){
                return confirm('are you sure??');
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#department_id").change(function(){
                $(".fde").hide();
                var all=$("#department_id").val()
                var urls=$("#department_url_all").val();
                if(all==0){
                    $("#department_tbl").show();
                    $("#label_percent").show();
                    $("#percent_value").show();
                    $("#amount_value").show();
                    $("#label_amount").show();
                    $("#department_load").show();
                    $.ajax({
                        type: "GET",
                        url: urls,
                        data: $("#department_id").val(),
                        dataType: 'json',
                        success: function (data) {
                            $("#department_load").hide();
                            var department='';
                            department+="<tr><th>Employee</th><th>Department</th><th>Current Salary</th> <th>Percent</th> <th>Amount</th></tr>";
                            $.each(data, function (i, item) {
                                if(item.total_employee_salary==null){
                                    item.total_employee_salary=0;
                                }else{
                                    item.total_employee_salary
                                }
                                department += '<tr> <td>'+item.empFirstName+'<input type="hidden" name="emp_id[]" value='+item.id+'>'+'</td>  <td>'+item.departmentName+'</td>  <td>'+'<input type="text" name="total_salary[]" class="form-control" value='+item.total_employee_salary+'>'+'</td> <td>'+'<input class="percent form-control" id="percent" type="text" name="percent[]">'+'</td>  <td>'+'<input class="amount form-control" id="amount" type="text" name="amount[]">'+'</td> </tr>';
                            });
                            $('#department_tbl').html(department);
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
                else{
                    $("#department_tbl").show();
                    $("#label_percent").show();
                    $("#percent_value").show();
                    $("#amount_value").show();
                    $("#label_amount").show();
                    $("#department_load").show();
                    var id = $(this).val();
                    var url = $('#department_url').val();
                    var urlid=url+'/'+id;
                    var formData = {
                        id: $(this).val(),
                    }
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    })
                    $.ajax({
                        type: "GET",
                        url: urlid,
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            $("#department_load").hide();
                            var department='';
                            department+="<tr><th>Employee</th><th>Department</th><th>Current Salary</th> <th>Percent</th> <th>Amount</th></tr>";
                            $.each(data, function (i, item) {
                                if(item.total_employee_salary==null){
                                    item.total_employee_salary=0;
                                }else{
                                    item.total_employee_salary
                                }
                                department += '<tr> <td>'+item.empFirstName+'<input type="hidden" name="emp_id[]" value='+item.id+'>'+'</td>  <td>'+item.departmentName+'</td>  <td>'+'<input type="text" name="total_salary[]" class="form-control" value='+item.total_employee_salary+'>'+'</td> <td>'+'<input class="percent form-control" id="percent" type="text" name="percent[]">'+'</td>  <td>'+'<input class="amount form-control" id="amount" type="text" name="amount[]">'+'</td> </tr>';
                            });
                            $('#department_tbl').html(department);
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });
            $('#percent_value').keyup(function () {
                $(".m").hide();
                $("#department_button").show();
                var input = document.getElementsByClassName("percent");
                for (var i = 0; i < input.length; i++){
                    input[i].value = document.getElementById('percent_value').value;
                }
            });
            $('#amount_value').keyup(function () {
                $(".p").hide();
                $("#department_button").show();
                var input = document.getElementsByClassName("amount");
                for (var i = 0; i < input.length; i++){
                    input[i].value = document.getElementById('amount_value').value;
                }
            });
        });
    </script>
    @include('include.copyright')
@endsection