@extends('layouts.master')
@section('title', 'Advance Deduction')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('loanme'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('loanme') }}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Employee Loan
                    </div>
                    <div class="panel-body">
                        {{Form::open(array('url' => '/loan/store','method' => 'post'))}}
                        <div class="col-md-3">
                          <div class="form-group">
                             <label>Select Employee</label>
                                <select name="emp_id" class="form-control"  data-search="true">
                                    <option>Select Employee</option>
                                    @foreach($data as $emp)
                                        <option value="{{$emp->id}}">{{$emp->empFirstName}} {{$emp->employeeId}}</option>
                                    @endforeach
                                 </select>
                         </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="loan">Loan Amount:</label>
                                <input type="text" id="loan_amount" name="loan_amount" class="form-control" id="loan" placeholder="loan amount">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="loan">Month Wise Deduction Amount:</label>
                                <input type="text" id="month_wise_deduction_amount" name="month_wise_deduction_amount"  onkeyup="cal()" class="form-control" id="loan" placeholder="Month Wise Deduction">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label>Loan complete total month:</label>
                            <input type="text" class="form-control" id="total_month" name="total_months" value=""  onkeyup="cal2()" >
                        </div>

                        <div class="col-md-6">
                            <button type="submit" class="btn btn-success btn-square">Add</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="text" name="country" value="Norway" readonly>
    <script>
        $("#month_wise_deduction_amount").change(function(){
            var loan_amount=$("#loan_amount").val();
            var deduction_amount=$("#month_wise_deduction_amount").val();
        });

        function cal(){
            var loan_amount=$("#loan_amount").val();
            var deduction_amount=$("#month_wise_deduction_amount").val();

            var result =Math.ceil(loan_amount/deduction_amount);
            $("#total_month").val(result);
        }
        function cal2(){
            var loan_amount=$("#loan_amount").val();
            var total_month=$("#total_month").val();

            var result = loan_amount/total_month;
            if(result) {
                $("#month_wise_deduction_amount").val(result);
            }else{
                $("#month_wise_deduction_amount").val('0');
            }
        }
    </script>
    @include('include.copyright')
@endsection