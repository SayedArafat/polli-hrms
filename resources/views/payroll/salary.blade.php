@extends('layouts.master')
@section('title', 'Employee Salary')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3>Pay<strong>roll/Salary</strong></h3>
                    </div>
                    <div class="panel-content">
                        {{Form::open(array('url' => 'salary_store','method' => 'post'))}}
                        <div class="form-group">
                        <input type="hidden" name="emp_select" value="{{session('emp_id')}}">
                        <input type="hidden" name="current_month" value="{{date('m-Y')}}">
                        <div class="form-group">
                            <label for="">Select Grade</label>
                            <select required name="grade_select" id="grade_select" class="form-control">
							    <option value="grade_null">Select Grade</option>
                                @foreach($data as $geade)
                                 <option value="{{$geade->id}}">{{$geade->grade_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="basic">Basic</label>
                            <input type="text" name="basic" class="form-control form-white" id="basic" placeholder="Amount" required>
                        </div>
                        <div class="form-group">
                            <label for="house">House Rent</label>
                            <input type="text" name="houserant" class="form-control form-white" id="house" placeholder="Amount" required>
                        </div>
                        <div class="form-group">
                            <label for="medical">Medical</label>
                            <input type="text" name="medical" class="form-control form-white" id="medical" placeholder="Amount" required>
                        </div>
                        <div class="form-group">
                            <label for="transport">Transportation</label>
                            <input type="text" name="transport" class="form-control form-white" id="transport" placeholder="Amount" required>
                        </div>
                        <div class="form-group">
                            <label for="food">Food</label>
                            <input type="text" name="foods" class="form-control form-white" id="food" placeholder="Amount" required>
                        </div>
                        <div class="form-group">
                            <label for="other">Others</label>
                            <input type="text" name="others" class="form-control form-white" id="other" placeholder="Amount" required>
                        </div>
                        <button id="salary_check" type="submit" class="btn btn-primary">Save</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            setTimeout(function() {
                $('#alert_message').fadeOut('fast');
            }, 5000);
            $("#salary_check").click(function(){
                var emp = document.getElementById("emp_select");
                var grade = document.getElementById("grade_select");
                if(emp.value == "emp_null") {
                    alert('Select Employee');
                    return false;
                }
                if(grade.value == "grade_null"){
                    alert('Select Grade');
                    return false;
                }
            });
            $('select[name="grade_select"]').on('change', function() {
                var gradeid = $(this).val();
                var grade = document.getElementById("grade_select");
                if(grade.value=='grade_null'){
                    document.getElementById('basic').value='';
                    document.getElementById('house').value='';
                    document.getElementById('medical').value='';
                    document.getElementById('transport').value='';
                    document.getElementById('food').value='';
                    document.getElementById('other').value='';
                }
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })
                $.ajax({
                    type: 'POST',
                    url: 'gradeajax'+'/'+gradeid,
                    success:function(data) {
                        $.each(data, function(key, value) {
                            var basic_salary=value.basic_salary;
                            var house_rant=value.house_rant;
                            var transport=value.transport;
                            var medical=value.medical;
                            var food=value.food;
                            var others=value.others;
                            document.getElementById('basic').value=basic_salary;
                            document.getElementById('house').value=house_rant;
                            document.getElementById('medical').value=transport;
                            document.getElementById('transport').value=medical;
                            document.getElementById('food').value=food;
                            document.getElementById('other').value=others;
                        });
                    }
                });
            });
        });
    </script>
@endsection