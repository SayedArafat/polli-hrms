@extends('layouts.master')
@section('title', 'Deduction')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('deductionnormalmessage'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('deductionnormalmessage') }}</p>
        @endif
        @if(Session::has('ndeduction'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('ndeduction') }}</p>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading">
                Employee Deduction
            </div>
            <div class="panel-body">
                {{Form::open(array('url' => '/deduction/store','method' => 'post'))}}

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Select Employee</label>
                        <select name="emp_id" class="form-control"  data-search="true">
                            <option>Select Employee</option>
                            @foreach($data as $emp)
                                <option value="{{$emp->id}}">{{$emp->empFirstName}} {{$emp->employeeId}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Enter Amount</label>
                        <input type="text" name="normal_deduction_amount" class="form-control" placeholder="Enter Amount">
                    </div>
                </div>

                <div class="col-md-12">
                    <button type="submit" class="btn btn-success btn-square">Add</button>
                </div>

                {{ Form::close() }}
            </div>
            <p style="margin-left: 4px">Update Existing Deduction</p>
           <hr>
            <div class="col-md-2">
                <label>Type Amount</label>
                <input style="margin: 5px 0;" class="form-control" type="text" name="amount" id="deduction_amount">
            </div>
            {{Form::open(array('url' => 'normal/deduction/update','method' => 'post'))}}
            <div class="col-md-10">
                <button id="deductionbutton" style="margin-top: 27px;display: none" type="submit" class="btn btn-success btn-square">Update</button>
            </div>
            <table class="table table-bordered posts">
                <thead>
                <tr>
                    <th>Employee</th>
                    <th>Employee Id</th>
                    <th>Deduction Amount</th>
                </tr>
                </thead>
                <tbody>
                @foreach($deductiondata as $de)
                <tr>
                    <input type="hidden" name="emp_id[]" value="{{$de->emp_id}}">
                    <td>{{$de->empFirstName}}</td>
                    <td>#{{$de->employeeId}}</td>
                    <td><input class="form-control percent" type="text" name="deduction_amounts[]" value="{{$de->normal_deduction_amount}}"></td>
                </tr>
                @endforeach
                </tbody>
            </table>
            {{ Form::close() }}
        </div>
    </div>
    <script>
        $('#deduction_amount').keyup(function () {
            $("#deductionbutton").show();
            var input = document.getElementsByClassName("percent");
            for (var i = 0; i < input.length; i++){
                input[i].value = document.getElementById('deduction_amount').value;
            }
        });
    </script>
    @include('include.copyright')
@endsection