@extends('layouts.master')
@section('title', 'Notice Details')
@section('content')
	<div class="page-content">
		<div class="row">

			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="panel panel-default no-bd">
					<div class="panel-header bg-dark">
						<h3 class="panel-title"><strong>Notice Details</strong></h3>
					</div>
					<div class="panel-body bg-white">
						<div class="">
                            <div class="form-group">
                                <label class="control-label">Notice Starting Date</label>
                                <div class="append-icon">
                                    {{$notices->startDate}}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Notice Ending Date</label>
                                <div class="append-icon">
                                    {{$notices->endDate}}
                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Notice</label>
                                <div class="append-icon">
                                    {{$notices->noticeDescription}}
                                    
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label">Notice Status</label>
                                <div class="append-icon">
                                    @if($notices->status=='0')
                                        <span class="red-text">Inactive</span>

                                    @else
                                        <span class="green-text">Active</span>

                                    @endif
                                </div>
                            </div>
						
						</div>

                       

					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
<script>
    setTimeout(function() {
        $('#alert_message').fadeOut('fast');
    }, 5000);
</script>
@endsection
