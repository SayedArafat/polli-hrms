@extends('layouts.master')
@section('title', 'Assign Leave')
@section('content')
    <div class="page-content">
        <div class="row">
            @if(Session::has('applySuccess'))
                <p id="alert_message" class="alert alert-success">{{Session::get('applySuccess')}}</p>
            @endif
           {{-- @if(Session::has('success2'))
                <p id="alert_message" class="alert alert-success">{{Session::get('success2')}}</p>
            @endif --}}
            <div class="col-md-12">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Manually Assign Leave</strong></h2>
                    </div>
                    <div class="panel-body bg-white">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                {!! Form::open(['method'=>'POST','url' =>'leave/assign/mannual_assign', 'files'=>true]) !!}
                                <div class="row">
                                    
                                        <div class="col-sm-6">
                                            <div class="required form-group">
                                                
                                                {!! Form::label('employee_id','Employees',['class'=>'control-label']) !!}
                                                <div class="option-group">
                                                    {!! Form::select('employee_id',[''=>'Select Employee']+$employees,null,['class' => 'form-control employee_id','data-search'=>"true" ,'onchange'=>'interviewCheck()'])!!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="required form-group">
                                                <label class="control-label">Choose Leave Type</label>
                                                <select onchange="interviewCheck();" required name="leave_type_id" id="leave_type_id" class="form-control">
                                                    <option value="">Select leave</option>
                                                    @foreach($leaveType as $leave)
                                                        <option value="{{$leave->id}}">{{$leave->leave_type}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                    <div id="CD1">
                                    <div class="col-sm-4">
                                        <div class="required form-group">
                                            <label> Leave Start Date </label>
                                            <div class="">
                                                <input type="text" class="date-picker form-control" required name="startDate"   placeholder="Click here for pick start date ..." />
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div id="CD2">  
                                        <div class="col-sm-4">
                                            <div class="required form-group">
                                                <label> Leave End Date </label>
                                                <div class="">
                                                    <input type="text"  class="date-picker form-control" required name="endDate"  value={{date("m/d/Y")}}  placeholder="Click here for pick end date ..." />
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Attachment</label>
                                            <div class="">
                                                <input type="file" name="attachment" step="any" class="form-control form-white" id="attachment"  placeholder="Upload Attachment (If any)">
                                                
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label> Reason for Leave</label>
                                            <div class="">
                                                    <textarea class="form-control" rows="4" name="leaveReason" placeholder="Enter Leave Reason..."></textarea>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                                </div>
                                <div class="row">
                                        <div id="table">
                                            <div class="col-sm-6 portlets">
                                                @if(isset($data))
                                                    @include('leave.employeeLeaveAjazTable')
                                                @elseif(isset($leaveData))
                                                    @include('leave.employeeLeaveAjazTable2')
                                                @endif
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-7 center-block">
                                        <label class="  form-label"></label>
                                            <div class="prepend-icon">
                                                <button type="submit" class="btn btn-primary center-block">Add Application</button>
                                            </div>
                                        </div> 
                                    
            
                                {!! Form::close() !!}
                            </div>       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')

    <script>
            function interviewCheck(val) {
                
                if($("#leave_type_id").val()!=1){
                    //alert('accepted');
                   document.getElementById('CD1').style.display="block";
                    document.getElementById('CD2').style.display="block";
                }
                else{
                    //alert('accepted');
                     document.getElementById('CD1').style.display="block";
                     document.getElementById('CD2').style.display="none";
                 }
                    var empID = $("#employee_id").val();
                    var div = $("#leave_type_id").val();
                    var endDate = $("#endDate").val();
                    var concate=empID+',' + div;
                    //var concate=empID+',' + div+','endDate;
                    //alert(concate);

                    $.ajax({
                        url:"employee_leave_history/"+concate, 
                        method:'GET',
                        dataType:'html',
                        success: function (data) {
                            $('#table').html(data);
        
                        },
                        error:function (xhr) {
                            console.log('failed');    
                        }
                    });
                    
                   
            }
            
           
        </script>
@endsection