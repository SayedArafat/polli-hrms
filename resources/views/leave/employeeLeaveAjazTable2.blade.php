
        
        <div class="panel-content pagination2 table-responsive">
                <div class="panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Employee Leave details</strong></h3>
                    </div>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Employee ID</th>
                    <th>Designation</th>
                    <th>Leave type</th>
                    <th>Total Days</th>
                    <th>Available Days</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employeeData as $a)
                    <tr>
                        <td>{{$a->empFirstName}} {{$a->empLastName}}</td>
                        <td>{{$a->employeeId}}</td>
                        <td>{{$a->designation}}</td>
                @endforeach
                @foreach($leaveData as $b)
                        <td>{{$b->leave_type}}</td>
                        <td>{{$b->total_days}}</td>
                        <td>{{$b->total_days}}</td>
                    
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>



