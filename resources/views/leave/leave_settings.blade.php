@extends('layouts.master')
@section('title', 'Leave Settings')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-header">
                        <h3> <strong>Leave Settings</strong></h3>
                    </div>
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#tab2_1" data-toggle="tab"><i class="icon-user"></i>Leaves</a></li>
                            <li><a href="#tab2_2" data-toggle="tab"><i class="icon-settings"></i>Weekends & Festival</a></li>
                        </ul>
                        {{--//leave request start--}}

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab2_1">
                                <h3><i class="fa fa-table"></i> <strong>Leave List</strong></h3>

                                <div class="panel-content pagination2 table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Leave Type</th>
                                            <th>Total Days</th>
                                            <th>Policy</th>
                                            <th>Modified By</th>
                                            <th>Modified at</th>
                                            <th>Update/Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $leavetype)
                                            <tr>
                                                <td>{{$leavetype->leave_type}}</td>
                                                <td>{{$leavetype->total_days}}</td>
                                                <td>{{$leavetype->policy}}</td>
                                                <td>{{$leavetype->modified_by}}</td>
                                                <td>{{$leavetype->updated_at}}</td>
                                                <td>
                                                    <a data-toggle="modal" data-target="#{{$leavetype->id}}" class="btn btn-success btn-sm" type="submit"><i class="fa fa-edit"></i></a>
                                                    <a href="{{ url('/leave_delete/'.$leavetype->id) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>

                                            {{--//table end--}}


                                            {{--//table edit modal start--}}
                                            <div class="modal fade" id="{{$leavetype->id}}" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Update Info</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            {{Form::open(array('url' => 'leave_update','method' => 'post'))}}
                                                            <div class="form-group">
                                                                <div class="form-group">
                                                                    <label for="leavename">Leave Name</label>
                                                                    <input type="text" name="leave_type" class="form-control form-white" id="leavename" value="{{$leavetype->leave_type}}" placeholder="Enter Leave Name" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="totaldays">Total Days</label>
                                                                    <input type="number" name="total_days" step="any" class="form-control form-white" id="totaldays" value="{{$leavetype->total_days}}" placeholder="Enter Number of Days" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="policy">Policy</label>
                                                                    <input type="text" name="policy" step="any" class="form-control form-white" id="policy" value="{{$leavetype->policy}}" placeholder="Enter Policy" required>
                                                                </div>
                                                                <input type="hidden" name="user_name" value="{{auth::user()->name}}">
                                                                <input type="hidden" name="leave_id" value="{{$leavetype->id}}">
                                                                <button type="submit" class="btn btn-primary">Update</button>
                                                                {{ Form::close() }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                {{--//table edit modal end--}}

                                {{--//add new leave modal start--}}

                                <div class="panel-content">
                                    <div class="form-group">
                                        <a data-toggle="modal" data-target="#addLeave" class="btn btn-success btn-md" type="submit"><i class="fa fa-edit"></i>Add Another Leave</a>
                                        <div class="modal fade" id="addLeave" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        {{Form::open(array('url' => 'leave_settings','method' => 'post'))}}
                                                        <div class="form-group">
                                                            <label for="leavename">Leave Name</label>
                                                            <input type="text" name="leave_type" class="form-control form-white" id="leavename"  placeholder="Enter Leave Name" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="totaldays">Total Days</label>
                                                            <input type="number" name="total_days" step="any" class="form-control form-white" id="totaldays"  placeholder="Enter Number of Days" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="policy">Policy</label>
                                                            <input type="text" name="policy" step="any" class="form-control form-white" id="policy"  placeholder="Enter Policy" required>
                                                        </div>
                                                        <input type="hidden" name="user_name" value="{{auth::user()->name}}">
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                        </div>
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--//leave request end--}}
                            <div class="tab-pane fade" id="tab2_2">
                                <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label>Select Weekly leave days</label>
                                        <select onchange=weekLeave(this) multiple class="form-control"  placeholder="{{$placeholder}}">
                                            <option value="6">Friday</option>
                                            <option value="7">Saturday</option>
                                            <option value="1">Sunday</option>
                                            <option value="2">Monday</option>
                                            <option value="3">Tuesday</option>
                                            <option value="4">Wednesday</option>
                                            <option value="5">Thursday</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                    <div id='wkLeave'>
                                        <div class="form-group">
                                            @if(isset($week)) 
                                                @include('leave.weekLeave')
                                            @else 
                                            <h5><strong>Currently setted weekends are:</strong><p class="text-success">
                                                    @foreach($week2 as $data9)
                                                    <strong>{{$data9->day}}</strong>,
                                                    @endforeach
                                                    </p>
                                                </h5>
                                            @endif
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>

                                <h3><i class="fa fa-table"></i> <strong>Festival Leave List</strong></h3>

                                <div class="panel-content pagination2 table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            
                                            <th>Start Day</th>
                                            <th>End Day</th>
                                            <th>Purpose</th>
                                            <th>Update/Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($festival_data as $festive_leave)
                                            
                                        @php
                                        $startDate=$festive_leave->start_date;
                                        $endDate=$festive_leave->end_date;

                                        $leaveStart=strtotime($startDate);
                                        $leaveEnd=strtotime($endDate);
                                        @endphp
                                    <tr>
                                        
                                        <td>{{date("d F", $leaveStart)}}</td>
                                        <td>{{date("d F", $leaveEnd)}}</td>
                                        <td>{{$festive_leave->purpose}}</td>
                                        <td>
                                            <a data-toggle="modal" data-target="#{{$festive_leave->id}}" class="btn btn-success btn-sm" type="submit"><i class="fa fa-edit"></i></a>
                                            <a href="{{ url('/festival_leave_delete/'.$festive_leave->id) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                        
                                        {{--  <tr>
                                                <td>{{$festive_leave->year}}</td>
                                                <td>{{$festive_leave->start_date}}</td>
                                                <td>{{$festive_leave->end_date}}</td>
                                                <td>{{$festive_leave->purpose}}</td>
                                                <td>
                                                    <a data-toggle="modal" data-target="#{{$festive_leave->id}}" class="btn btn-success btn-sm" type="submit"><i class="fa fa-edit"></i></a>
                                                    <a href="{{ url('/festival_leave_delete/'.$festive_leave->id) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>  --}}


                                            {{--//table end--}}


                                            {{--//table edit modal start--}}
                                            <div class="modal fade" id="{{$festive_leave->id}}" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Update Festival Leave</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            {{Form::open(array('url' => 'festival_leave_update','method' => 'post'))}}
                                                            <div class="form-group">

                                                                <div class="form-group">
                                                                    <label> Vacation Start Date </label>
                                                                    <div class="">
                                                                        <input type="text" class="date-picker form-control" required name="startDate" value="{{$festive_leave->start_date}}"  placeholder="Click here for pick start date ..." />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label> Vacation End Date </label>
                                                                    <div class="">
                                                                        <input type="text" class="date-picker form-control" required name="endDate" value="{{$festive_leave->end_date}}" placeholder="Click here for pick end date ..." />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label> Vacation Purpose </label>
                                                                    <div class="">
                                                                        {{--<textarea name="purpose"  value="{{$festive_leave->purpose}}" class="form-control"></textarea>--}}
                                                                        <input type="text" name="purpose" step="any" class="form-control form-white" id="purpose" value="{{$festive_leave->purpose}}" placeholder="Enter Policy".Policy>
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" name="user_name" value="{{auth::user()->name}}">
                                                                <input type="hidden" name="festival_leave_id" value="{{$festive_leave->id}}">
                                                                <button type="submit" class="btn btn-primary">Update</button>
                                                                {{ Form::close() }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div>
                                    <a data-toggle="modal" data-target="#addFestiveLeave" class="btn btn-success btn-md" type="submit"><i class="fa fa-edit"></i>Add Festive Leave</a>
                                    <div class="modal fade" id="addFestiveLeave" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">Add Festival</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    {{Form::open(array('url' => 'add_festival_leave','method' => 'post'))}}
                                                    <form action="save_vacation.php" method="POST">
                                                        
                                                        <div class="form-group">
                                                            <label> Vacation Start Date </label>
                                                            <div class="">
                                                                <input type="text" class="date-picker form-control" required name="startDate"  placeholder="Click here for pick start date ..." />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label> Vacation End Date </label>
                                                            <div class="">
                                                                <input type="text" class="date-picker form-control" required name="endDate" placeholder="Click here for pick end date ..." />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label> Vacation Purpose </label>
                                                            <div class="">
                                                                <input type="text" name="purpose" step="any" class="form-control form-white" id="purpose"  placeholder="Enter Purpose" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label></label>
                                                            <div class="">
                                                                <input type="submit" class="btn btn-primary"  value="Save" />
                                                            </div>
                                                        </div>
                                                    </form>
                                                    {{ Form::close() }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
                                @include('include.copyright')

                                <script>
                                    function weekLeave(that){ 
                                        const weekLeave= $(that).val();
                                        //alert(weekLeave);

                                        $.ajax({
                                            url:"leave/addWeekends/"+weekLeave, 
                                            method:'GET',
                                            dataType:'html',
                                            success: function (data) {
                                            $('#wkLeave').html(data);
                                            //alert("Weekly off Days Succesfully updated")

                                            },
                                            error:function (xhr) {
                                                console.log('failed');    
                                            }
                                        });
                                    }

                                </script>
@endsection