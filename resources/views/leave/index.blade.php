@extends('layouts.master')
@section('title', 'Leave Request Management')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3> <strong>Leave Request Management</strong></h3>
                    </div>
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#tab2_1" data-toggle="tab"><i class="fa fa-pause"></i>Pending Request</a></li>
                            <li><a href="#tab2_2" data-toggle="tab"><i class="fa fa-check"></i>Approved</a></li>
                            <li><a href="#tab2_3" data-toggle="tab"><i class="fa fa-times"></i>Rejected</a></li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab2_1">
                                @if(count($data)!=0)
                                <h3><i id="pendingTable1" class="fa fa-table"></i> <strong>Pending List</strong></h3>

                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                        
                                            <th>Previous Leave</th>
                                            <th>Employee Id</th>
                                            <th>Name</th>
                                            <th>Designation</th>
                                            <th>Leave type</th>
                                            <th>Leave reason</th>
                                            <th>Attachment</th>
                                            <th>Period</th>
                                            <th>Total Days</th>
                                            <th>Available</th>
                                            <th  scope="col" colspan="2">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $leavereq)
                                    {{--{!! Form::open(['method'=>'POST','url' => 'leave_accepted', 'files'=>true]) !!}--}}

                                            @php
                                            if($leavereq->leave_available){
                                            $available=$leavereq->leave_available;
                                            }
                                            else{   
                                            $available=\App\Http\Controllers\leaveController::totalLeave($leavereq->leave_type_id); 
                                            
                                            }
                                            

                                            $date1=$leavereq->leave_starting_date;
                                            $date2=$leavereq->leave_ending_date;
                                            $interval1=1+round(abs(strtotime($date1)-strtotime($date2))/86400);

                                            $leaveStart=strtotime($date1);
                                                $leaveEnd=strtotime($date2);
                                            
                                            if(($leavereq->status)==0){
                                                $status='Pending';
                                            }
                                            elseif(($leavereq->status)==1){
                                                $status='Approved';
                                            }
                                            @endphp
                                        <tr>
                                            {!! Form::hidden('id',$leavereq->id) !!}
                                           
                                            <td><button value="{{$leavereq->eID}}" onclick="dosomething(this)" id="empID" type="button" class="btn btn-success btn-sm fa fa-eye"></button></td>   
                                            <td>{{$leavereq->employeeId}}    
                                            </td>
                                            <td>{{$leavereq->empFirstName}} {{$leavereq->empLastName}}</td>
                                            <td>{{$leavereq->designation}}</td>
                                            <td>{{$leavereq->leave_type}}</td>
                                            <td>
                                                @if($leavereq->description)
                                                {{$leavereq->description}}
                                                @else    
                                                    <h5>N/A</h4>
                                                @endif
                                                

                                            </td>
                                            <td>
                                                    
                                                @if($leavereq->attachment)
                                                    <a target="_blank" href="{{asset('Leave_request_attachment/'.$leavereq->attachment)}}"><p><button type="button"class="btn btn-custom-download btn-sm"><i class="fa fa-download"></i> Download</button></p></a>
                                                @else    
                                                    <h5>N/A</h4>
                                                @endif
                                                      
                                            </td>
                                            <td>{{date("d F", $leaveStart)}} -to- {{date("d F Y", $leaveEnd)}}</td>
                                            <td>{{$leavereq->actual_days}}</td>
                                            <td>{{$available}} </td>
                                            <td>
                                                <a href= "{{url('leave_accepted/'.$leavereq->id)}}"><button type="submit" class="btn btn-success btn-sm">Accept</button></a>
                                            </td>
                                            <td>
                                                <a href= "{{url('leave_declined2/'.$leavereq->id)}}"><button type="button" class="btn btn-danger btn-sm" >Reject</button>
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="{{$leavereq->employeeId}}" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Employee Leave History</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                       
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        @endforeach
                                        </tbody>
                                    {{--{!! Form::close() !!}--}}
                                    </table>
                                    @include('leave.employee_leave_history_modal')
                                    <input type="hidden" name="user_name" value="{{auth()->user()->name}}">
                                    <input type="hidden" name="user_id" value="{{auth()->user()->id}}">                                </div>
                            </div>
                            {{--//leave request end--}}
                            @else
                            <h3>No pending Leave Request</h3>
                            @endif
                            <div class="tab-pane fade" id="tab2_2">
                                @if(count($data2)!=0)
                                    <h3><i class="fa fa-table"></i> <strong>Approved List</strong></h3>

                                    {{--<div class="panel-content pagination2 table-responsive">--}}
                                        {{--<table class="table table-hover table-dynamic">--}}
    
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                            
                                                <th>Employee Id</th>
                                                <th>Name</th>
                                                <th>Designation</th>
                                                <th>Leave type</th>
                                                <th>Period</th>
                                                <th>Working Days</th>
                                                <th>Approved By</th>
                                                <th>Change Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($data2 as $leavereq)
                                        {{--{!! Form::open(['method'=>'POST','url' => 'leave_accepted', 'files'=>true]) !!}--}}
    
                                                @php
                                                $date1=$leavereq->leave_starting_date;
                                                $date2=$leavereq->leave_ending_date;
                                                //$interval=1+round(abs(strtotime($date1)-strtotime($date2))/86400);

                                                $leaveStart=strtotime($date1);
                                                $leaveEnd=strtotime($date2);
                                                
                                                if(($leavereq->status)==0){
                                                    $status='Pending';
                                                }
                                                elseif(($leavereq->status)==1){
                                                    $status='Approved';
                                                }
                                                @endphp
                                            <tr>
                                                {!! Form::hidden('id',$leavereq->id) !!}
                                               
                                                <td>{{$leavereq->employeeId}}</td>
                                                <td>{{$leavereq->empFirstName}} {{$leavereq->empLastName}}</td>
                                                <td>{{$leavereq->designation}}</td>
                                                <td>{{$leavereq->leave_type}}</td>
                                                <td>{{date("d F", $leaveStart)}} -to- {{date("d F", $leaveEnd)}}</td>
                                                <td>{{$leavereq->actual_days}}</td>
                                                <td>{{$leavereq->approved_by}}</td>
                                                <td>
                                                    <a href= "{{url('leave_declined/'.$leavereq->id)}}"><button type="button" class="btn btn-danger btn-sm" >Cancel</button>
                                                </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        {{--{!! Form::close() !!}--}}
                                        </table>
                                        <input type="hidden" name="user_name" value="{{auth()->user()->name}}">
                                        <input type="hidden" name="user_id" value="{{auth()->user()->id}}">                                </div>
                                </div>
                                {{--//leave request end--}}
                                @else
                                <h3>No Accepted Request</h3>
                                 @endif

                            <div class="tab-pane fade" id="tab2_3">
                               
                                @if(count($data3)!=0)
                                    <h3><i class="fa fa-table"></i> <strong>Declined List</strong></h3>

                                    {{--<div class="panel-content pagination2 table-responsive">--}}
                                        {{--<table class="table table-hover table-dynamic">--}}
    
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                            
                                                <th>Employee Id</th>
                                                <th>Name</th>
                                                <th>Designation</th>
                                                <th>Leave type</th>
                                                <th>Period</th>
                                                <th>Total Days</th>
                                                <th>Declined By</th>
                                                <th>Change Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($data3 as $leavereq)
                                        {{--{!! Form::open(['method'=>'POST','url' => 'leave_accepted', 'files'=>true]) !!}--}}
    
                                                @php
                                                $date1=$leavereq->leave_starting_date;
                                                $date2=$leavereq->leave_ending_date;
                                                $interval=1+round(abs(strtotime($date1)-strtotime($date2))/86400);

                                                $leaveStart=strtotime($date1);
                                                $leaveEnd=strtotime($date2);
                                                
                                                if(($leavereq->status)==0){
                                                    $status='Pending';
                                                }
                                                elseif(($leavereq->status)==1){
                                                    $status='Approved';
                                                }
                                                @endphp
                                            <tr>
                                                {!! Form::hidden('id',$leavereq->id) !!}
                                               
                                                <td>{{$leavereq->employeeId}}</td>
                                                <td>{{$leavereq->empFirstName}} {{$leavereq->empLastName}}</td>
                                                <td>{{$leavereq->designation}}</td>
                                                <td>{{$leavereq->leave_type}}</td>
                                                <td>{{date("d F", $leaveStart)}} -to- {{date("d F", $leaveEnd)}}</td>
                                                <td>{{$leavereq->actual_days}}</td>
                                                <td>{{$leavereq->approved_by}}</td>
                                                <td>
                                                    <a href= "{{url('leave_accepted/'.$leavereq->id)}}"><button type="submit" class="btn btn-success btn-sm">Approve</button></a>
                                                    
                                                </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        {{--{!! Form::close() !!}--}}
                                        </table>
                                        @else
                                    <h3>No Declined Request</h3>
                                    @endif
                                        <input type="hidden" name="user_name" value="{{auth()->user()->name}}">
                                        <input type="hidden" name="user_id" value="{{auth()->user()->id}}">                                </div>
                                </div>
                                {{--//leave request end--}}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')

    <script>
            function dosomething(that){
                const data= $(that).val();
                //alert(data);
    
                $.ajax({
                    url:"leave/employee/leave_history/"+data, 
                    method:'GET',
                    dataType:'json',
                   
                    success: function(data){
                        $("#modalshow1").modal('show');
                        //var line = "<h4>"+ val.empFirstName+' '+val.empLastName+"</h4>";
                        var row = "<tr>";
                        
                            row += " <td>" + "LEAVE PERIOD" + "</td>";
                       
                            row += " <td>" + "LEAVE TYPE" + "</td>";
                            row += " <td>" + "REASON" + "</td>";
                            row += "</tr>"
                            $.each(data, function(i, val) {
                                row += " <td>" + val.leave_starting_date+' to '+val.leave_ending_date + "</td>";
                                row += "<td>" + val.leave_type+ "</td>";
                                row += "<td>" + val.description + "</td>";
                                row += "</tr>";
                               
                            } );
    
                        $("#tableID").html(row);
                            
                                                   
    
                    }
                    {{-- error:function (something) {
                        console.log('failed');    
                    }
                     --}}
                });
              }
    </script>
    
@endsection