@extends('layouts.master')
@section('title', 'Training History')
@section('content')
    <div class="page-content">
        <div class="row">
            @if(Session::has('success1'))
                <p id="alert_message" class="alert alert-success">{{Session::get('success1')}}</p>
            @endif
            @if(Session::has('success2'))
                <p id="alert_message" class="alert alert-success">{{Session::get('success2')}}</p>
            @endif
            <div class="col-md-12">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong> Training History </strong></h2>
                    </div>
                    @if(count($history)==0)
                     
                    <h3><p class="text-center">No Employee Training History Available</p></h3>
                    @else
                    <div class="row">
                        <div class="col-md-12 portlets">
                            
                            <div class="panel-header">
                                <h3><i class="fa fa-table"></i> <strong>Training</strong> History</h3>
                            </div>
                            <div class="panel-content pagination2 table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                        <th>Tarining Name</th>
                                        <th>Period</th>
                                        <th>Attendents</th>
                                        <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($history as $his)
                                        @php
                            
                                        if(strtotime($his->training_starting_date)<=strtotime("now") && strtotime($his->training_ending_date)>=strtotime("now")){
                                            $status='Active';
                                        }
                                        else if(strtotime($his->training_starting_date)<strtotime("now") && strtotime($his->training_ending_date)<strtotime("now")){
                                            $status='Completed';
                                        }
                                        else if(strtotime($his->training_starting_date)>strtotime("now") && strtotime($his->training_ending_date)>strtotime("now")){
                                            $status='Upcoming';
                                        }
                
                                     
                                        @endphp
                                        <tr>
                                            <td>{{$his->training_name}}</td>
                                            <td>{{date("d M",strtotime($his->training_starting_date))}} to {{date("d M Y",strtotime($his->training_ending_date))}}</td>
                                            <td>
                                                <button value="{{$his->training_starting_date.','.$his->training_ending_date.','.$his->tid}}" onclick="dosomething(this)" id="testid" type="button" class="btn btn-success btn-sm">See List</button>
                                                {{-- <button value={{$his->training_starting_date.','.$his->training_ending_date.','.$his->tid}} class="btn btn-success btn-sm" onClick="dosomething(this)"> See List</button> --}}
                                            </td>
                                            <td>{{$status}}</td>    
                                        </tr> 
                                        @endforeach  
                                    </tbody>
                                </table>
                                
                                {{-- @if(isset($employee)) --}}
                                    @include('training.modal')
                                {{-- @endif  --}}
                                @endif
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>

@include('include.copyright')

<script>
        function dosomething(that){
            const data= $(that).val();
            //alert(data);

            $.ajax({
                url:"training/history/attendent/"+data, 
                method:'GET',
                dataType:'json',
               
                success: function(data){
                    $("#modalshow1").modal('show');
                    var row = "<tr>";
                    
                        row += " <td>" + "Sl No." + "</td>";
                        row += " <td>" + "EMPLOYEE ID" + "</td>";
                        row += " <td>" + "NAME" + "</td>";
                        row += " <td>" + "DESIGNATION" + "</td>";
                        row += " <td>" + "CONTACT NO" + "</td>";
                        row += " <td>" + "EMAIL" + "</td>";
                        row += "</tr>"
                        $.each(data, function(i, val) {
                            row += " <td>" +(i+1)+ "</td>";
                            row += " <td>" + val.employeeId + "</td>";
                            row += "<td>" + val.empFirstName +' '+ val.empLastName + "</td>";
                            row += "<td>" + val.designation + "</td>";
                            row += "<td>" + val.empPhone + "</td>";
                            row += "<td>" + val.empEmail + "</td>";
                            row += "</tr>";
                           
                        } );

                    $("#tableID").html(row);
                        
                                               

                }
                {{-- error:function (something) {
                    console.log('failed');    
                }
                 --}}
            });
          }
</script>

@endsection