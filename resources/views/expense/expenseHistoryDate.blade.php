@extends('layouts.master')
@section('title', 'Expense History')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Select Date Range of expense</strong> </h3>
                    </div>
                    <div class="panel-content">
                        <div class="row">
                        
                         {{Form::open(array('url' => '/expense/history','method' => 'post'))}}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required form-label">Date From</label>
                                    <div class="prepend-icon">
                                        <input type="text" id="expense_start_date" name="expense_start_date" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required form-label">Date To</label>
                                    <div class="prepend-icon">
                                        <input type="text" id="expense_end_date" name="expense_end_date" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2"><button name="tests" type="submit" value="vacancy_report_generate" class="btn btn-embossed btn-info">See History</button></div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection