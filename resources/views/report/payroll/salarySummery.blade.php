@extends('layouts.master')
@section('title', 'Employee Salary Summary Report')
@section('content')
    <div class="page-content">
        <div class="panel panel-default">
            <div class="panel-heading">Employee Month wise Salary summery </div>
            <div class="panel-body">
                {{Form::open(array('url' => 'report/employees/salarysummery','method' => 'post'))}}
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">Select Month</label>
                        <div class="prepend-icon">
                            <input type="text" name="emp_sal_month" id="emp_sal_month" class="form-control format_date" required>
                            <i class="icon-calendar"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">Group By</label>
                        <select id="group_by_select_monthwise" class="form-control" name="salary_group_by">
                            <option value="nodata">Please Select</option>
                            <option value="department">Department</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                <input type="submit" id="salsummerymonth" name="salarymontwise" class="btn btn-info" value="Generate Salary">
                <input type="submit" id="salsummerymonths" name="salarymontwise" class="btn btn-info" value="Generate Pdf">
                </div>
                {{ Form::close() }}
               </div>
          </div>
     </div>
    </div>
    <script>
        $("#salsummerymonth").click(function(){
            var groupby =  document.getElementById("group_by_select_monthwise");
            if (groupby.value == "nodata") {
                alert('please select group');
                return false;
            }
        });
        $("#salsummerymonths").click(function(){
            var groupby =  document.getElementById("group_by_select_monthwise");
            if (groupby.value == "nodata") {
                alert('please select group');
                return false;
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#emp_sal_month').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });
        });
    </script>
    @include('include.copyright')
@endsection