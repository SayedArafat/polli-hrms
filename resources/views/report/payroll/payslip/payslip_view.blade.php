@extends('layouts.master')
@section('content')
    <style>
        body {
            width: 100vw;
            height: 100vh;
            display: flex;
            justify-content: center;
            padding: 20px;
            height: 100%;
        }
        #payslip {
            width: calc( 8.5in - 240px );
            height: calc( 6in - 48px );
            background: #fff;
            padding: 10px 20px;
        }
        #scope {
            border-top: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            padding: 1px 0 2px 0;
            display: flex;
            justify-content: space-around;
        }
        #scope{
            text-align: center;
        }
        #scope > .scope-entry > .value {
            font-size: 14px;
            font-weight: 700;
        }
        .content {
            display: flex;
            border-bottom: 1px solid #ccc;
            height: 880px;
        }
        .content .left-panel {
            border-right: 1px solid #ccc;
            min-width: 180px;
            padding: 15px 12px 0 0;
        }
        .content .right-panel {
            width: 100%;
            padding: 4px 0 0 10px;
        }
        #employee {
            text-align: center;
            margin-bottom: 20px;
        }
        #employee #name {
            font-size: 15px;
            font-weight: 700;
        }
        #employee {
            font-size: 11px;
            font-weight: 300;
        }
        .details{
            margin-bottom: 20px;
        }
        .details .entry, .contributions .entry, .ytd .entry {
            display: flex;
            justify-content: space-between;
            margin-bottom: 6px;
        }
        .details .entry .value, .contributions .entry .value, .ytd .entry .value {
            font-weight: 700;
            max-width: 130px;
            text-align: right;
        }
        .gross .entry .value {
            font-weight: 700;
            text-align: right;
            font-size: 16px;
        }
        .content .right-panel .details {
            width: 100%;
        }
        .content .right-panel .details .entry {
            display: flex;
            padding: 0 10px;
            margin: 6px 0;
        }
        .content .right-panel .details{
            font-weight: 700;
            width: 120px;
        }
        .content .right-panel .details{
            font-weight: 600;
            width: 130px;
        }
        .content .right-panel .details{
            font-weight: 400;
            width: 80px;
            font-style: italic;
            letter-spacing: 1px;
        }
        .content .right-panel .details{
            text-align: right;
            font-weight: 700;
            width: 90px;
        }
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 2px;
        }
    </style>
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                @php
                $order=0;
                $currencyprefix = "Taka";
                $currencysuffix = "";
                @endphp
                @foreach($data as $payslip_report)
                @php
                $order++;
                @endphp
                <div id="payslip">
                    <h4 style="font-weight: bold;" class="text-center">{{$companyInformation->company_name}}</h4>
                    <p class="text-center">Payslip</p>
                    <div id="scope">
                        <div class="col-md-4">
                            Order:{{$order}}
                        </div>
                        <div class="col-md-4">
                            <div class="scope-entry">
                                <div class="title">MONTH</div>
                                <div class="value">{{date('F-Y',strtotime($payslip_report->month))}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="scope-entry">
                                <div class="title">DATE</div>
                                <div class="value">{{date('F-d-Y')}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="content">
                        <div class="left-panel">
                            <div id="employee">
                                <div id="name">
                                   ID:#{{$payslip_report->employeeId}}
                                </div>
                            </div>
                            <div class="details">
                                <div class="entry">
                                    <label>Employee</label>
                                    <div class="value">{{$payslip_report->empFirstName}}</div>
                                </div>
                                <div class="entry">
                                    <label>Grade</label>
                                    <div class="value">{{$payslip_report->grade_name}} </div>
                                </div>
                                <div class="entry">
                                    <label>Section</label>
                                    <div class="value">{{$payslip_report->empSection}}</div>
                                </div>
                                <div class="entry">
                                    <label>Designation</label>
                                    <div class="value">{{$payslip_report->designation}}</div>
                                </div>
                                <div class="entry">
                                    <label>Join Date</label>
                                    <div class="value">{{date('d-m-Y',strtotime($payslip_report->empJoiningDate))}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="right-panel">
                            <table>
                                <tr>
                                    <th>Basic</th>
                                    <td>House</td>
                                    <td>Medical</td>
                                    <td>Transport</td>
                                    <th>Food</th>
                                </tr>
                                <tr>
                                    <td>{{$currencyprefix}} {{$payslip_report->basic_salary}} {{$currencysuffix}}</td>
                                    <td>{{$currencyprefix}} {{$payslip_report->house_rant}} {{$currencysuffix}}</td>
                                    <td>{{$currencyprefix}} {{$payslip_report->medical}} {{$currencysuffix}}</td>
                                    <td>{{$currencyprefix}} {{$payslip_report->transport}} {{$currencysuffix}}</td>
                                    <td>{{$currencyprefix}} {{$payslip_report->food}} {{$currencysuffix}}</td>
                                </tr>
                                <tr>
                                    <th>Gross</th>
                                    <td>Overtime</td>
                                    <td>Rate</td>
                                    <td>Amount</td>
                                    <th style="font-size: 11px;">Festival Bonus</th>
                                </tr>
                                <tr>
                                    <td>{{$currencyprefix}} {{$payslip_report->gross}}  {{$currencysuffix}}</td>
                                    <td>@if($payslip_report->overtime=='') 0/hr @else {{$payslip_report->overtime}}/hr @endif</td>
                                    <td>{{$payslip_report->overtime_rate}}</td>
                                    <td>{{$currencyprefix}} {{$payslip_report->overtime_amount}} {{$currencysuffix}}</td>
                                    <td>{{$currencyprefix}} {{$payslip_report->festival_bonus_amount+$payslip_report->	festival_bonus_percent}}  {{$currencysuffix}}</td>
                                </tr>
                                <tr>
                                    <th>Special</th>
                                    <td>Gross Pay</td>
                                    <td>Leave</td>
                                    <td>Deduction</td>
                                    <th>Adv/Deduction</th>
                                </tr>
                                <tr>
                                    <td>@if($payslip_report->increment_bonus_amount=='' && $payslip_report->increment_bonus_percent=='') 0 @else{{$currencyprefix}} {{$payslip_report->	increment_bonus_amount+$payslip_report->increment_bonus_percent}} {{$currencysuffix}} @endif</td>
                                    <td>{{$currencyprefix}} {{$payslip_report->gross_pay}}{{$currencysuffix}}</td>
                                    <td>@if($payslip_report->total=='') 0 @else {{$payslip_report->total}} @endif</td>
                                    <td>{{$currencyprefix}} {{$payslip_report->absent_deduction_amount}} {{$currencysuffix}}</td>
                                    <td>@if($payslip_report->advanced_deduction=='') 0 @else {{$payslip_report->advanced_deduction}} @endif</td>
                                </tr>
                                <tr>
                                    <th style="font-size: 11px;">Work Day</th>
                                    <td>Weekend</td>
                                    <td>Absent</td>
                                    <td style="font-size: 11px;">Normal Deduction</td>
                                    <td>Net Amount</td>
                                </tr>
                                <tr>
                                    <td>{{$payslip_report->working_day}}</td>
                                    <td>{{$payslip_report->weekend}}</td>
                                    <td>{{$payslip_report->absent}}</td>
                                    <td>@if($payslip_report->normal_deduction=='') 0 @else {{$payslip_report->normal_deduction}} @endif</td>
                                    <td>{{$currencyprefix}} {{$payslip_report->net_amount}}{{$currencysuffix}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <br>
               @endforeach
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection
