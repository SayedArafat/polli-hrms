@extends('layouts.master')
@section('title', 'Payslip')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel panel-default">
                    <div class="panel-heading">Employee Month wise payslip</div>
                    <div class="panel-body">
                        {{Form::open(array('url' => '/report/payslip/generate','method' => 'post'))}}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-label">Select Month <span style="color:red;">*</span></label>
                                <div class="prepend-icon">
                                    <input type="text" name="payslip_generate_report" id="payslip_generate_report" class="form-control format_date" required>
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <input type="submit" value="Generate Payslip"  name="payslipgenerate" class="btn btn-success margin-top-10">
                              </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#payslip_generate_report').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });
        });
    </script>
    @include('include.copyright')
@endsection
