@extends('layouts.master')
@section('title', 'Employee Deduction Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel panel-default">
                    <div class="panel-heading"><b>Employee Penalty Report</b></div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            {{Form::open(array('url' => '/report/employee/attendance/bonus/pdf','method' => 'post'))}}
                            <div class="form-group">
                                <label class="form-label">Please Select Month</label>
                                <div class="prepend-icon">
                                    <input type="text" name="year_bonus_month" id="year_bonus_month" class="form-control format_date">
                                    <i class="icon-calendar"></i>
                                </div>
                                <input type="hidden" id="year_bonus" value="{{URL::to('/report/penalty/show')}}">
                            </div>
                            <button type="button" id="year_bonus_report" class="btn btn-primary">Generate Report</button>
                            <button type="submit" id="year_bonus_report" class="btn btn-success">Download pdf</button>
                            {{--<button type="button" id="year_bonus_report" class="btn btn-primary">Download csv</button>--}}
                            {{ Form::close() }}
                            <div class="year_bonus_tbl" style="display: none;">
                                <table id="year_bonus_display" class="table table-bordered">
                                    <h4 style="text-align: center;font-weight: bold;">{{$companyInformation->company_name}}</h4>
                                    <h5 style="text-align: center;font-weight: bold;">Employee Penalty Report</h5>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    $(document).ready(function(){
    $("#year_bonus_report").click(function(){
    $(".year_bonus_tbl").show();
    var url = $('#year_bonus').val();

    var formData = {
    year_bonus_month: $('#year_bonus_month').val(),
    }
    if(formData.year_bonus_month==''){
    $(".year_bonus_tbl").hide();
    alert('Please Select Month');
    }
    else{
    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    })
    $.ajax({
    type: "POST",
    url: url,
    data: formData,
    dataType: 'json',
    success: function (data) {
    console.log(data);
    var year_bonus = '';
    year_bonus+='  <thead> <tr> <th>Order</th> <th>Employee</th> <th>Designation</th> <th>Amount</th> </tr> </thead>';
    var count=0;
    $.each(data, function (i, item) {
    if(item.normal_deduction_amount==null){
    item.normal_deduction_amount=0;
    }
    count++;
    year_bonus += '<tr> <td>'+count+'</td> <td>' + item.empFirstName + '</td> <td>'+item.designation+'</td> <td>'+item.normal_deduction_amount+'Taka'+'</td>  </tr>';
    });
    $('#year_bonus_display').html(year_bonus);
    },
    error: function (data) {
    console.log('Error:', data);
    }
    });
    }
    });
    });
    </script>
    <script>
        $(document).ready(function() {
            $('#year_bonus_month').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });
        });
    </script>
    @include('include.copyright')
@endsection
