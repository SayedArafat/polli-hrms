@extends('layouts.master')
@section('title', 'Employee Advance Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel panel-default">
                    <div class="panel-heading"><b>Employee Loan Report</b></div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            {{Form::open(array('url' => '/report/employee/attendance/bonus/pdf','method' => 'post'))}}
                            <div class="form-group">
                                <label>Select Employee</label>
                                <select name="loan_emp_id" id="loan_emp_id" class="form-control"  data-search="true">
                                    <option>Please Select</option>
                                    @foreach($employee as $emp)
                                        <option value="{{$emp->emp_id}}">{{$emp->empFirstName}} {{$emp->empPhone}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" name="loan_url" id="loan_url" value="{{URL::to('/loan')}}">
                            {{--<button type="button" id="year_bonus_report" class="btn btn-primary">Download csv</button>--}}
                            {{ Form::close() }}
                            <table style="display: none;" id="year_bonus_display" class="table table-bordered">
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document ).ready(function() {
            $("#loan_emp_id").change(function(){
                $('#year_bonus_display').show();
                var id = $(this).val();
                var url = $('#loan_url').val();
                var urlid=url+'/'+id;
                var formData = {
                    id: $(this).val(),
                }
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })
                $.ajax({
                    type: "GET",
                    url: urlid,
                    data: formData,
                    dataType: 'json',
                    success: function (data) {
                        var loan_bonus = '';
                        var count=0;
                        loan_bonus+='<thead><tr><th>Order</th><th>Employee</th><th>Designation</th><th>Loan Amount</th> <th>Deduction per month</th> <th>Complete Month</th> <th>Month Duration</th><th>Year Duration</th><th>Loan Given</th></tr></thead>'
                        $.each(data, function (i, item) {
                            count++;
                            loan_bonus += '<tr><td>'+count+'</td> <td>'+item.empFirstName + item.empLastName + '</td> <td>'+item.designation+'</td> <td>'+item.loan_amount+'</td>  <td>'+item.monthwise_deduction+'</td> <td>'+item.complete_month+'Month'+'</td>  <td>'+item.month_duration+'</td> <td>'+item.year_duration+'</td> <td>'+item.advance_given+'</td> </tr>';
                        });
                        $('#year_bonus_display').html(loan_bonus);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });
        });
    </script>
    @include('include.copyright')
@endsection
