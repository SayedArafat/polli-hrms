@extends('layouts.master')
@section('title', 'Salary Sheet')
@section('content')
    <div class="page-content">
        <div class="panel panel-default">
            <div class="panel-heading">
                Generate Employee Month Wise Salary Sheet
            </div>
            <div class="panel-body">
                {{Form::open(array('url' => '/report/employee/salary/monthwise','method' => 'post'))}}
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">SELECT MONTH <span style="color: darkred;font-size: 16px;">*</span></label>
                        <div class="prepend-icon">
                            <input type="text" name="emp_sal_month" id="emp_sal_month" class="form-control format_date" required>
                            <i class="icon-calendar"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">SELECT PAYABLE DATE<span style="color: darkred;font-size: 16px;">*</span></label>
                        <div class="prepend-icon">
                            <input type="text" name="salary_pay_date" id="salary_pay_date" class="form-control date-picker" required>
                            <i class="icon-calendar"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="form-label">GROUP BY <span style="color: darkred;font-size: 16px;">*</span></label>
                        <select id="group_by_select_monthwise" class="form-control" name="salary_group_by">
                            <option value="nodata">Please Select</option>
                            <option value="department">Department</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Generate Salary</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#emp_sal_month').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });
        });
    </script>

    <script>
        $("#please_select_group_monthwise").click(function(){
            var groupby =  document.getElementById("group_by_select_monthwise");
            if (groupby.value == "nodata") {
                alert('Select all required field');
                return false;
            }
        });
    </script>
    @include('include.copyright')
@endsection