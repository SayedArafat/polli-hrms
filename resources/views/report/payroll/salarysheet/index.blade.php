@extends('layouts.master')
@section('title', 'Proccess Salary')
@section('content')
    <style>
        .load_image img{
            width: 100%;
            height: 275px;
        }
    </style>
    <div class="page-content">
        <div class="panel panel-default">
            <div class="panel-heading">Process Employee Salary</div>
            <div class="panel-body">
                {{Form::open(array('url' => '/report/employee/salarysheet','method' => 'post'))}}
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="form-label">Select Month</label>
                        <div class="prepend-icon">
                            <input type="text" name="salary_sheet_month" id="select_salary_sheet_month" class="form-control format_date" required>
                            <i class="icon-calendar"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <button type="submit" name="salary_sheet_monthsss" class="btn btn-primary btn-lg" id="salary_sheet_generate">Process</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#select_salary_sheet_month').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm-dd', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm-dd', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });
        });
    </script>
    <div class="modal fade" data-keyboard="false" data-backdrop="static" id="modelWindow" role="dialog">
        <div class="modal-dialog modal-sm vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <a target="_blank" href="{{url('/')}}">Dashboard</a>
                    <h6 class="modal-title">Please wait</h6>
                    <p>Don't close the window</p>
                </div>
                <div class="modal-body load_image">
                    {{ Html::image("hrm_script/images/designation.gif", "Logo") }}
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#salary_sheet_generate').click(function(){
            $('#modelWindow').modal('show');
        });
    </script>
    @include('include.copyright')
@endsection