@php
use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Payroll Report')
@section('content')
<div class="page-content ">
  <div class="row panel"  style="border:1px solid #999">
      <div class="col-xlg-12 col-lg-12  col-sm-12">
          <div class="text-center" >
              <h2><b>Payroll Report</b></h2>
              <hr>
          </div>
      </div>
      <div class="col-xlg-3 col-lg-3 col-sm-3">
          <a target="_BLANK" href="{{url('/report/payroll/employee/salary/sheet')}}">
              <div class="panel">
                  <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(0,137,123);color:white;">
                      <center>
                          <div class="row" >
                              <div class="">
                                  <center><i class="fa fa-users f-40"></i></center>
                              </div>
                              <br>
                              <div class="">
                                  <center>
                                      <span class="f-14"><b>Salary Sheet</b></span>
                                  </center>
                              </div>
                          </div>
                      </center>
                  </div>
              </div>
          </a>
      </div>
      <div class="col-xlg-3 col-lg-3 col-sm-3">
          <a target="_BLANK" href="{{url('/report/payslip')}}">
              <div class="panel">
                  <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(106,27,154);color:white;">
                      <center>
                          <div class="row" >
                              <div class="">
                                  <center><i class="fa fa-file-text-o f-40"></i></center>
                              </div>
                              <br>
                              <div class="">
                                  <center>
                                      <span class="f-14"><b>Payslip</b></span>
                                  </center>
                              </div>
                          </div>
                      </center>
                  </div>
              </div>
          </a>
      </div>
      <div class="col-xlg-3 col-lg-3 col-sm-3">
        <a target="_BLANK" href="{{url('/report/bonus')}}">
        <div class="panel">
          <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:steelblue;color:white;">
              <center>
                <div class="row" >
                  <div class="">
                      <center><i class="fa fa-money f-40"></i></center>
                  </div>
                  <br>
                  <div class="">
                  <center>
                    <span class="f-14"><b>Bonus</b></span>
                    </center>
                  </div>
                </div>
              </center>
          </div>
        </div>
        </a>
      </div>
      <div class="col-xlg-3 col-lg-3 col-sm-3">
          <a target="_BLANK" href="{{url('/report/advanced')}}">
              <div class="panel">
                  <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:green;color:white;">
                      <center>
                          <div class="row" >
                              <div class="">
                                  <center><i class="fa fa-money f-40"></i></center>
                              </div>
                              <br>
                              <div class="">
                                  <center>
                                      <span class="f-14"><b>Advance</b></span>
                                  </center>
                              </div>
                          </div>
                      </center>
                  </div>
              </div>
          </a>
      </div>
      <div class="col-xlg-3 col-lg-3 col-sm-3">
          <a target="_BLANK" href="{{url('/report/penalty')}}">
              <div class="panel">
                  <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:darkred;color:white;">
                      <center>
                          <div class="row" >
                              <div class="">
                                  <center><i class="fa fa-times-circle f-40"></i></center>
                              </div>
                              <br>
                              <div class="">
                                  <center>
                                      <span class="f-14"><b>Penalty</b></span>
                                  </center>
                              </div>
                          </div>
                      </center>
                  </div>
              </div>
          </a>
      </div>
  </div>
</div>
@include('include.copyright')
@endsection