@extends('layouts.master')
@section('title', 'Employee Festival Bonus Report')
@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12 portlets">
            <div class="panel panel-default">
                <div class="panel-heading"><h4><b>Festival Bonus Report</b></h4></div>
                <div class="panel-body">
                    <div class="col-md-12">
                        {{Form::open(array('url' => '/report/employee/festival/bonus/pdf','method' => 'post'))}}
                        <div class="form-group">
                            <label class="form-label">Please Select Month</label>
                            <div class="prepend-icon">
                                <input type="text" name="year_bonus_month" id="year_bonus_month" class="form-control format_date">
                                <i class="icon-calendar"></i>
                            </div>
                            <input type="hidden" id="year_bonus" value="{{URL::to('/report/employee/festival/bonus')}}">
                        </div>
                        <button type="button"  id="year_bonus_report" class="btn btn-primary">Generate Report</button>
                        {{--<button type="submit"  class="btn btn-success">Download pdf</button>--}}
                        {{--<button type="button"  class="btn btn-primary">Download csv</button>--}}
                        {{ Form::close() }}
                        <div class="year_bonus_tbl" style="display: none;">
                            <table id="year_bonus_display" class="table table-bordered">
                                <h4 style="text-align: center;font-weight: bold;">{{$companyInformation->company_name}}</h4>
                                <h5 style="text-align: center;font-weight: bold;">Festival bonus for the month of</h5>
                                <p class="text-center" id="justs"></p>
                                <thead>
                                <tr>
                                    <th>Order</th>
                                    <th>Employee</th>
                                    <th>Department</th>
                                    <th>Designation</th>
                                    <th>Gross</th>
                                    <th>Gross after bonus</th>
                                    <th>Percent wise amount</th>
                                    <th>Amount</th>
                                    <th>Percent</th>
                                    <th>Title</th>
                                    <th>Given by</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#year_bonus_report").click(function(){
            $(".year_bonus_tbl").show();
            $("#festival_load").show();
            var url = $('#year_bonus').val();

            var formData = {
                year_bonus_month: $('#year_bonus_month').val(),
            }
            if(formData.year_bonus_month==''){
                $(".year_bonus_tbl").hide();
                alert('Please Select Month');
            }
            else{
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })
                $.ajax({
                    type: "POST",
                    url: url,
                    data: formData,
                    dataType: 'json',
                    success: function (data) {
                        var count=0;
                        var festival_bonus = '';
                        $.each(data, function (i, item) {
                            count++;
                            if(item.emp_total_percent==null){
                                item.emp_total_percent=0;
                            }
                            if(item.emp_amount==null){
                                item.emp_amount=0;
                            }
                            var a = parseInt(item.emp_gross);
                            var b = parseInt(item.emp_bonus);
                            var c = parseInt(item.emp_amount);
                            var total_gross=a+b+c;

                            if(item.is_permission==1){
                                var role='Admin'
                            }
                            if(item.is_permission==2){
                                var role='Hr'
                            }
                            if(item.is_permission==4){
                                var role='Executive'
                            }
                            if(item.is_permission==5){
                                var role='Accountant'
                            }
                            const monthNames = ["January", "February", "March", "April", "May", "June",
                                "July", "August", "September", "October", "November", "December"
                            ];
                            const d = new Date(item.created_at);
                            var month= monthNames[d.getMonth()]+'-'+ d.getDate() +'-'+ d.getFullYear();
                            document.getElementById('justs').innerHTML=month;
                            festival_bonus += '<tr><td>'+count+'</td> <td>' + item.empFirstName + item.empLastName + '</td>  <td>'+item.departmentName+'</td><td>'+item.designation+'</td><td>'+item.emp_gross+'Taka'+'</td> <td>'+total_gross+'Taka'+'</td> <td>'+item.emp_bonus+'Taka'+'</td>  <td>'+item.emp_amount+'Taka'+'</td> <td>'+item.emp_total_percent+'%'+'</td> <td>'+item.bonus_title+'</td> <td>'+item.name+'</td> </tr>';
                        });
                        $('#year_bonus_display').append(festival_bonus);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('#year_bonus_month').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm",
            showButtonPanel: true,
            currentText: "This Month",
            onChangeMonthYear: function (year, month, inst) {
                $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
            },
            onClose: function(dateText, inst) {
                var month = $(".ui-datepicker-month :selected").val();
                var year = $(".ui-datepicker-year :selected").val();
                $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
            }
        }).focus(function () {
            $(".ui-datepicker-calendar").hide();
        });
    });
</script>
@include('include.copyright')
@endsection
