@extends('layouts.master')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Training </strong> Report</h3>
                    </div>
                    <div class="panel-content">
                      <a href="{{url('training/report')}}" class="btn btn-sm btn btn-default"><i class="fa fa-arrow-left"> Back</i></a>
                        <button type="button" class="btn btn-success">Download Report</button>
                        <h1 class="text-center">Training Report</h1>
                        <p class="text-center">Current Month <?php echo date('M-Y'); ?></p>
                        <p class="text-center">Total day in month : <?php echo date('t'); ?></p>
                        <table id="leave_display" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Order</th>
                                <th>Training</th>
                                <th>Duration</th>
                                <th>Attendents</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $order=0; @endphp
                            @foreach($training as $data)
                            @php
                            if(($data->training_starting_date)>=date("d/m/Y")&&($data->training_ending_date)<=date("d/m/Y")){
                                $status='Active';
                            }
                            else if(($data->training_starting_date)<=date("d/m/Y")&&($data->training_ending_date)<=date("d/m/Y")){
                                $status='Completed';
                            }
                            else if(($data->training_starting_date)>=date("d/m/Y")&&($data->training_ending_date)>=date("d/m/Y")){
                                $status='Upcoming';
                            }
                            else if(($data->training_starting_date)<=date("d/m/Y")&&($data->training_ending_date)>=date("d/m/Y")){
                                $status='Active';
                            }
                            @endphp
                            @php $order++; @endphp
                            <tr>
                            <td>{{$order}}</td>
                            <td>{{$data->training_name}}</td>
                            <td>{{$data->training_starting_date}} to {{$data->training_ending_date}}</td>
                            <td>{{$data->employee_name}}</td>
                            <td>{{$status}}</td>
                            </tr>
                           @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection