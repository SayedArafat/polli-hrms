@extends('layouts.master')
@section('title', 'On Leave Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Employee on Leave </strong></h3>
                    </div>
                    <div class="panel-content">
                      <a href="{{url('report/leave')}}" class="btn btn btn btn-default"><i class="fa fa-arrow-left"> Back</i></a>
                       
                        @if(count($onLeaveEmployee)==0)
                        
                            <h3><p class="text-center">No Employee Currently on Leave Today</p></h3>
                        
                        @else
                        <a href="{{url('report/leave/onLeaveListPDF')}}" class="btn btn-success"><i class="fa fa-arrow-down"> Download Report</i></a>
                        <h1 class="text-center">Employee on Leave Report</h1>
                        <p class="text-center">Current Date: <strong><?php echo date('d-M-Y'); ?></p></strong>
                        <p class="text-center">Total day in month : <?php echo date('t'); ?></p>
            
                        

                            <table id="leave_display" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Order</th>
                                    <th>Employee</th>
                                    <th>Employee ID</th>
                                    <th>Designation</th>
                                    <th>Leave Type</th>
                                    <th>Leave Duration</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                @php $order=0; @endphp
                                @foreach($onLeaveEmployee as $item)
                                @php 
                                
                                $leaveStart=strtotime($item->leave_starting_date);
                                $leaveEnd=strtotime($item->leave_ending_date);
                                $order++; 
                                @endphp
                                <tr>
                                <td>{{$order}}</td>
                                <td>{{$item->empFirstName}} {{$item->empLastName}}</td>
                                <td>{{$item->employeeId}}</td>
                                <td>{{$item->designation}}</td>
                                <td>{{$item->leave_type}}</td>
                                <td><strong>{{date("d F", $leaveStart)}}</strong>  to  <strong>{{date("d F Y", $leaveEnd)}}</strong></td>

                                </tr>
                            @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection