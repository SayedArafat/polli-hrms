@php
use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Leave Report')
@section('content')
    <div class="page-content ">
        <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Leave Report</b></h2>
                    <hr>
                </div>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="leave/onLeaveReport">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(0,137,123);color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-th f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>On Leave Employee List</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href='leave/dateWiseReport'>
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(57,73,171);color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-history f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Date Wise Leave Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            {{--  <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a href="leave/availableLeaveReport">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(106,27,154);color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-exclamation-triangle f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Available Leave for Employee</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>  --}}

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="leave/earnLeaveReport">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#c0392b;color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-users f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>  Earn Leave Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="leave/maternityLeaveReport">
                <div class="panel">
                    <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#5a002a;color:white;">
                      <center>
                        <div class="row" >
                          <div class="">
                              <center><i class="fa fa-th f-40"></i></center>
                          </div>
                          <br>
                          <div class="">
                          <center>
                            <span class="f-14"><b>Maternity Leave Employee List</b></span>
                            </center>
                          </div>
                        </div>
                      </center>
                  </div>
                </div>
              </a>
            </div>
        </div>

    </div>
 
@include('include.copyright')
@endsection