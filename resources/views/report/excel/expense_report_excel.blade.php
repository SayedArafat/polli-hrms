<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Expense Report</title>
</head>
<body>

<div>
    <table>
        <tbody>
        <tr><td></td><td colspan="4" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_address1}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_email}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_phone}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center">Expense report from <b>{{\Carbon\Carbon::parse($request->start_date)->format('d M Y')}} </b> to <b> {{\Carbon\Carbon::parse($request->end_date)->format('d M Y')}} </b></td></tr>

        </tbody>
    </table>
    <center>

        @if(count($data)!=0)
            <table>
                <thead>
                <tr>
                    <th>Order</th>
                    <th>Title</th>
                    <th>Amount</th>
                    <th>Reference</th>
                    <th>Date</th>
                    <th>Category</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                @php $order=0; @endphp
                @foreach($data as $item)
                    <?php
                        $total_expense=0;
                        $total_expense+=$item->amount;
                    ?>
                    <tr>
                        <td align="center">{{++$order}}</td>
                        <td>{{$item->title}}</td>
                        <td>{{$item->amount}} </td>
                        <td align="center">{{$item->reference}}</td>
                        <td>{{\Carbon\Carbon::parse($item->expenseDate)->format('d M Y')}}</td>
                        <td>{{$item->categoryName}}</td>
                        <td>{{$item->description}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div style="padding-top: 15px; font-size: 12px;">
                <table>
                    <tbody>
                        <tr>
                            <td colspan="4"><span><b>Total Expense -</b> {{ $total_expense }} Taka Only</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>

        @else
            <hr>
            <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @endif

</div>

</body>
</html>




