<?php

$absent=0;
$working_days=0;
$present=0;
$late=0;
$onleave=0;
$endTime=\App\Http\Controllers\OvertimeController::overtime_end();
$startTime=\App\Http\Controllers\OvertimeController::overtime_start();
$attendance_settings=\Illuminate\Support\Facades\DB::table('attendance_setup')->first();


?>
        <!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Job Card</title>
</head>
<body>

<div>
    <table>
        <tbody>
        <tr><td></td><td colspan="4" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_address1}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_email}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_phone}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"> Job Card Report Month From <b>{{\Carbon\Carbon::parse($start_date)->format('d-M-Y')}}</b> to <b>{{\Carbon\Carbon::parse($end_date)->format('d-M-Y')}} </b></td></tr>


        </tbody>
    </table>
    @if(isset($datas[0][0]))
        <table>
            <tr>
                <td><b>Employee ID :</b> {{$datas[0][0]->employeeId}}</td>
                <td><b>Name : </b> {{$datas[0][0]->empFirstName." ".$datas[0][0]->empLastName }}</td>
                <td><b>Joining Date :</b> {{\Carbon\Carbon::parse($datas[0][0]->empJoiningDate)->format('d-M-Y')}}</td>
            </tr>
            <tr>
                <td><b>Designation :</b> {{$datas[0][0]->designation}}</td>
                <td><b>Department : </b> {{$datas[0][0]->departmentName}}</td>
                <td><b> Section : </b> {{$datas[0][0]->empSection}}</td>
            </tr>

        </table>

        <table>
            <thead>
            <tr>
                <th>Date</th>
                <th>Day</th>
                <th>In Time </th>
                <th>Status</th>
                <th>Late Time</th>
                <th>Out Time</th>
                <th>OT</th>
            </tr>
            </thead>
            <tbody>
            @foreach($datas as $d)
                <?php
                $working_days++;
                ?>
                <tr>
                    <td>{{\Carbon\Carbon::parse($d[1])->format('d-M-Y')}}</td>
                    <td>{{\Carbon\Carbon::parse($d[1])->format('l')}}</td>
                    <td>
                        @if(isset($d[0]->in_time))
                            <?php
                            $present++;
                            ?>

                            {{ date('G:i', strtotime($d[0]->in_time)) }}

                        @endif
                    </td>
                    <td>
                        @if(isset($d[0]->in_time))
                            @if($d[0]->in_time>$attendance_settings->max_entry_time)
                                <?php $late++; ?>
                                <span>Late</span>
                            @else
                                <span>Present</span>
                            @endif
                        @else
                            @if($d[2]=='weekend' || $d[2]=='Festival Holiday' || $d[2]=='On Leave')
                                @if($d[2]!='On Leave')
                                    <?php $working_days--; ?>
                                @else
                                    <?php $onleave++; ?>
                                @endif
                                <span>{{$d[2]}}</span>
                            @else
                                <span style="color:#FF0000">Absent</span>

                            @endif

                        @endif
                    </td>
                    <td>
                        @if(isset($d[0]->in_time))
                            @if($d[0]->in_time>$attendance_settings->max_entry_time)
                                {{ date('G:i:s', strtotime($d[0]->in_time) - strtotime($attendance_settings->max_entry_time)) }}
                            @endif

                        @endif
                    </td>
                    <td>
                        @if(isset($d[0]->out_time))
                            @if($d[0]->in_time==$d[0]->out_time)
                                <span style="color: #FF0000;">Not Given</span>


                            @else

                                {{ date('G:i:s', strtotime($d[0]->out_time)) }}

                            @endif

                        @endif
                    </td>
                    <td>
                        @if(isset($d[0]->out_time) && $d[0]->in_time!=$d[0]->out_time)
                            @if($d[0]->out_time>$startTime)
                                {{ date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)) }}
                            @endif

                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

</div>


<div>

        <table>
            <tr>
                <td><b>Total Working Days: </b>{{ $working_days }} Days</td>
                <td><b>Total Present Days: </b> {{ $present }} Days</td>
            </tr>
            <tr>
                <td><b>Total Leave :</b> {{ ($onleave) }} Days</td>
                <td><b>Total Absent :</b> {{ ($working_days-($present+$onleave)) }} Days</td>
            </tr>

        </table>
</div>

@else
    <center><h4 style="color: #FF0000">No data found for the given date</h4></center>

@endif



</body>
</html>




