@php
    $out_time=\App\Http\Controllers\AttendanceController::out_time();
    $overtime_end=\App\Http\Controllers\OvertimeController::overtime_end();
@endphp
@extends('layouts.master')
@section('title', 'Daily Attendance Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 ><i class="fa fa-users "></i> Attendance Report For <strong>{{ \Carbon\Carbon::parse($request->date)->format('d M Y (l)') }}</strong></h3>

                            </div>

                        </div>

                    </div>
                    {{--<div style="padding: 20px;">--}}
                        {{--<td  width="50%"><b>Total Employee: </b> {{ $countAbsent+$countPresent+$totalL }}</td><br>--}}
                        {{--<td class="padTop10"><b>Total Present :</b> {{ ($countPresent) }}</td><br>--}}
                        {{--<td class="padTop10"><b>Total Absent :</b> {{ $countAbsent }}</td><br>--}}
                        {{--<td class="padTop10"><b>On Leave :</b> {{ $totalL }}</td><br>--}}
                        {{--<td class="padTop10"><b>Total Late :</b> {{ $Tlate }}</td>--}}

                    {{--</div>--}}
                    <div class="panel-content pagination2">
                        <table class="table table-hover table-bordered" id="data-table">
                            <thead>
                                <tr>
                                    <th>Emp. ID</th>
                                    <th>Name</th>
                                    <th>Department</th>
                                    <th>Designation</th>
                                    <th>Entry Time</th>
                                    <th>Late Time</th>
                                    <th>Exit Time</th>
                                    <th>Status</th>

                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        {{--$(function() {--}}
            {{--$('#data-table').DataTable({--}}
                {{--processing: true,--}}
                {{--serverSide: true,--}}
                {{--ajax: '{!! route('report.public.daily_attendance_data') !!}',--}}
                {{--columns: [--}}
                    {{--{ data: 'id', name: 'id' },--}}
                    {{--{ data: 'empFirstName', name: 'empFirstName' },--}}
                    {{--{ data: 'empLastName', name: 'empLastName' },--}}
                    {{--{ data: 'employeeId', name: 'employeeId' },--}}
                    {{--{ data: 'empEmail', name: 'empEmail' }--}}
                {{--],--}}
                {{--pageLength: 25--}}
            {{--});--}}
        {{--});--}}

        $(document).ready( function () {
            $('#data-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '{!! route('report.public.daily_attendance_data') !!}',
                "columns": [
                    { "data": "employeeId" },
                    { "data": "name" },
                    { "data": "departmentName" },
                    { "data": 'designation' },
                    { "data": "in_time" },
                    { "data": "late_time" },
                    { "data": "out_time" },
                    { "data": "action" }
                ]
            });
        });


    </script>

    @include('include.copyright')
@endsection