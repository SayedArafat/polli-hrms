@extends('layouts.master')
@section('title', 'Daily Attendance Summary')
@section('content')
    <div class="page-content ">
        <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Daily Attendance Summary</b></h2>
                    <hr>
                </div>
            </div>
            @if(Session::has('message'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
            @endif
            {!! Form::open(['method'=>'POST','action'=>'ReportController@daily_attendance_summary_data','target'=>'_blank']) !!}
            <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Select Date<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <input type="text" name="date" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required placeholder="Select a date...">
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Group By<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select class="form-control" name="reportType">
                            <option  value="1">Department Wise</option>
                            <option  value="2">Section Wise</option>
                            {{--<option  value="3">Floor Line Wise</option>--}}
                            <option  value="4">Designation Wise</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Page Size<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select class="form-control" name="pagesize">
                            <option selected value="A4">A4</option>
                            <option value="Legal">Legal</option>
                            <option value="Letter">Letter</option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Page Orientation<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select class="form-control" name="pageOrientation">
                            <option selected value="Portrait">Portrait</option>
                            <option value="Landscape">Landscape</option>
                        </select>
                    </div>
                </div>




            <div class="form-group">
                <div class="input-form-gap"></div>
                <label class="col-md-3">Excel Extension<span class="clon">:</span></label>
                <div class="col-md-9">
                    <select class="form-control" name="extensions">
                        <option selected value="xls">XLS</option>
                        <option value="xlsx">XLSX</option>
                    </select>
                </div>
            </div>



            <div class="form-group padding-bottom-30-percent">
                <div class="col-md-9 col-md-offset-3">
                    <hr>
                    <input type="submit" value="Preview" name="viewType" class="btn btn-success margin-top-10">


                    <span class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Generate/Export
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><button type="submit" value="Generate PDF" name="viewType" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> &nbsp; Generate PDF </button></li>
                                    <li><button type="submit" value="Generate Excel" name="viewType" class="btn btn-blue"><i class="fa fa-file-excel-o"></i> &nbsp; Generate Excel </button></li>
                                </ul>
                            </span>


                    <input type="reset" value="Clear" class="btn btn-warning margin-top-10">
                </div>
            </div>

            {!! Form::close() !!}
        </div>

    </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>



    @include('include.copyright')
@endsection