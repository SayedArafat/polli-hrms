@php(
    $totalEmployee=\Illuminate\Support\Facades\DB::table('employees')->where('empAccStatus','=','1')->count()
)

@extends('layouts.master')
@section('title', 'Attendance Summery')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 ><i class="fa fa-users "></i> Attendance Summary From <strong>{{ \Carbon\Carbon::parse($request->date)->format('d M Y') }}</strong> to <strong>{{ \Carbon\Carbon::parse($request->endDate)->format('d M Y') }}</strong></h3>
                            </div>

                        </div>

                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-dynamic table-hover table-striped table-bordered">
                            <h5><b>Total Active Employee = {{ $totalEmployee }}</b></h5>
                            <thead style="font-size:15px;font-weight:bold;">
                            <tr>
                                <th>Date</th>
                                <th>Day</th>
                                <th>Total Present</th>
                                <th>Total Late</th>
                                <th>Total Absent</th>
                                <th>On Leave</th>
                                {{--<th>O.T.</th>--}}

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($totalAttendance as $t)
                                <tr>
                                    <td>{{\Carbon\Carbon::parse($t->date)->format('d M Y')}}</td>
                                    <td>{{\Carbon\Carbon::parse($t->date)->format('l')}}</td>
                                    @if($t->key==2)
                                        <td class="red-text">Weekend</td>
                                        <td class="red-text">Weekend</td>
                                        <td class="red-text">Weekend</td>
                                        <td class="red-text">Weekend</td>
                                    @elseif($t->key==1)
                                        <td class="green-text">Festival Holiday</td>
                                        <td class="green-text">Festival Holiday</td>
                                        <td class="green-text">Festival Holiday</td>
                                        <td class="green-text">Festival Holiday</td>
                                    @else
                                    <td>{{$t->attendance}}</td>
                                    <td>{{$t->late}}</td>
                                    <td>{{$totalEmployee-($t->attendance+$t->leaves)}}</td>
                                    <td>{{$t->leaves}}</td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <hr>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection