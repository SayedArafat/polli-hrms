@php
use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Attendance Report')
@section('content')
    <div class="page-content ">
        <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Attendance Report</b></h2>
                    <hr>
                </div>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{route('report.public.daily_attendance')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(0,137,123);color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-th f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Daily Attendance Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a  target="_BLANK" href="{{route('report.daily.late.present')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(57,73,171);color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-history f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Daily Late Present</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{route('report.daily_absent_report')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(106,27,154);color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-exclamation-triangle f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Daily Absent Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{route('report.daily_present_report')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#c0392b;color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-users f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Daily Present Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
                <a target="_BLANK" href="{{route('report.daily_overtime_report')}}">
                    <div class="panel">
                        <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#5a002a;color:white;">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-line-chart f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Daily Overtime Report</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
                <a target="_BLANK" href="{{route('report.daily_attendance_exception')}}">
                    <div class="panel">
                        <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(177,63,115);color:white;">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Daily Attendance Exception</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{route('report.daily_attendance_summery')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#880e4f;color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-list f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Daily Attendance Summary</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{route('report.date_wise_late')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#732424;color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-history f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Date Wise Late Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{route('report.date_wise_absent')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#382626;color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-exclamation-circle f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Date Wise Absent Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{route('report.public.date_wise_present')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#009e47;color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-users f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Date Wise Present Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
                <a target="_BLANK" href="{{route('report.date_wise_overtime')}}">
                    <div class="panel">
                        <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#5a002a;color:white;">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-line-chart f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Date Wise Overtime Report</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>


            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{route('report.attendance_summery')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#3949ab;color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-calendar f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Date Wise Attendance Summary</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>


            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{route('report.public.attendance_card')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(94,53,177);color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-file-text f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Job Card</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>


            <div class="col-xlg-3 col-lg-3 col-sm-3">
                <a target="_BLANK" href="{{route('report.date_wise_attendance_exception')}}">
                    <div class="panel">
                        <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(135,68,177);color:white;">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Date Wise Attendance Exception</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            @if(checkPermission(['admin']))
            <div class="col-xlg-3 col-lg-3 col-sm-3">
                <a target="_BLANK" href="{{route('report.daily_attendance')}}">
                    <div class="panel">
                        <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(0,137,123);color:white;">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-th f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Daily Attendance Report (Private)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xlg-3 col-lg-3 col-sm-3">
                <a target="_BLANK" href="{{route('report.date_wise_present')}}">
                    <div class="panel">
                        <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#009e47;color:white;">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-users f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b> Datewise Present Report (Private)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
                <a target="_BLANK" href="{{route('report.admin_date_wise_overtime')}}">
                    <div class="panel">
                        <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#880e4f;color:white;">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-list f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Date Wise Overtime (Private)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
                <a target="_BLANK" href="{{url('/report/attendance/card')}}">
                    <div class="panel">
                        <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(94,53,177);color:white;">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Job Card (Private)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>
            @endif


        <div class="col-md-12"><hr></div>
        <div class="col-md-6">
       <div class="panel panel-default">
           <div class="panel-header">
               <h5><i class="fa fa-area-chart"></i> <strong> Attendance Statistic (Last 15 Days)</strong></h5>
           </div>
           <div class="panel-body">
               <canvas id="attendancestatistic" style="max-width:100%;"></canvas>
           </div>
       </div>
     </div>

     <div class="col-md-6">
       <div class="panel panel-default">
           <div class="panel-header">
               <h5><i class="icon-list"></i> <strong>Absent Statistic (Last 15 Days)</strong></h5>
           </div>
           <div class="panel-body">
               <canvas id="absentstatistic" style="max-width:100%;"></canvas>
           </div>
       </div>
     </div>

        </div>

    </div>
  

   @php

    function getLastNDays($days, $format = 'd/m'){
         date_default_timezone_set('Asia/Dhaka');

          $m = date("m"); 
          $de= date("d"); 
          $y= date("Y");
          $dateArray = array();
          for($i=0; $i<=$days-1; $i++){
              $dateArray[] = date($format, mktime(0,0,0,$m,($de-$i),$y)) ; 
          }
          return array_reverse($dateArray);
      }

    $days=getLastNDays(15,'Y-m-d');

    $dataPointsAttendenceStatistic = array();
    $dataSetsAttendenceStatistic = array();
    $dataSetsAbsentStatistic = array();

    foreach($days as $day){
      array_push($dataPointsAttendenceStatistic,$day);
      array_push($dataSetsAttendenceStatistic,dashboardcontroller::getAttendanceValueByDate($day));
      array_push($dataSetsAbsentStatistic,($employee-dashboardcontroller::getAttendanceValueByDate($day)));
    } 


      date_default_timezone_set('Asia/Dhaka');
      $datapointPresentAbsent=array();
      $present=dashboardcontroller::getAttendanceValueByDate(date("Y-m-d"));
      array_push($datapointPresentAbsent,$present);
      array_push($datapointPresentAbsent,($employee-$present));

   @endphp


     <script>
         window.onload = function() {

             var ctx = document.getElementById("attendancestatistic").getContext('2d');
             var myChart = new Chart(ctx, {
                 type: 'bar',
                 data: {
                    
                     labels: <?php echo json_encode($dataPointsAttendenceStatistic, JSON_NUMERIC_CHECK); ?>,
                     datasets: [{
                         label: '#',
                         data: <?php echo json_encode($dataSetsAttendenceStatistic, JSON_NUMERIC_CHECK); ?>,
                         backgroundColor: 
                             'rgba( 0, 200, 83, 0.8)',
                         borderColor: 
                             'rgba(1, 1, 1, 0.7)',
                         borderWidth: 1
                     }]
                 },
                 options: {
                     scales: {
                         yAxes: [{
                             ticks: {
                                 beginAtZero:true
                             }
                         }]
                     }
                 }
             });

             var ctx = document.getElementById("absentstatistic").getContext('2d');
             var myChart = new Chart(ctx, {
                 type: 'line',
                 data: {
                    
                     labels: <?php echo json_encode($dataPointsAttendenceStatistic, JSON_NUMERIC_CHECK); ?>,
                     datasets: [{
                         label: '#',
                         data: <?php echo json_encode($dataSetsAbsentStatistic, JSON_NUMERIC_CHECK); ?>,
                         backgroundColor: 
                             'rgba(229,57,47, 0.7)',
                         borderColor: 
                             'rgba(1 , 1, 1, 0.7)',
                         borderWidth: 1
                     }]
                 }, 
                 options: { 
                      scales: {
                         yAxes: [{
                             ticks: {
                                 beginAtZero:true
                             }
                         }]
                      }
                  },
             });
        }
     </script>
    @include('include.copyright')
@endsection