@php
    $out_time=\App\Http\Controllers\AttendanceController::out_time();

@endphp

@extends('layouts.master')
@section('title', 'Daily Present Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 ><i class="fa fa-users "></i>Present Report For <strong>{{ \Carbon\Carbon::parse($request->date)->format('d M Y (l)') }}</strong></h3>

                            </div>

                        </div>

                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-dynamic table-hover table-striped table-bordered">
                            <thead style="font-size:15px;font-weight:bold;">
                            <tr>
                                <th>id</th>
                                <th>Name</th>
                                @if(!empty($request->coldesignation))
                                    <th>Designation</th>
                                @endif
                                @if(!empty($request->coldepartment))
                                    <th>Department</th>
                                @endif
                                @if(!empty($request->coljoiningdate))
                                    <th>Joining Date</th>
                                @endif
                                @if(!empty($request->colgender))
                                    <th>Gender</th>
                                @endif
                                @if(!empty($request->colunit))
                                    <th>Branch</th>
                                @endif
                                @if(!empty($request->colsection))
                                    <th>Section</th>
                                @endif
                                @if(!empty($request->colstatus))
                                    <th>Status</th>
                                @endif
                                <th>In Time</th>
                                @if(!empty($request->collateTime))
                                    <th>Late Time</th>
                                @endif
                                <th>Out Time</th>
                                @if(!empty($request->colOverTime))
                                    <th>O.T.</th>
                                @endif
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($present as $employee)
                                <tr>
                                    <td>{{$employee->employeeId}}</td>
                                    <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                                    @if(!empty($request->coldesignation))
                                        <td>{{$employee->designation}}</td>
                                    @endif
                                    @if(!empty($request->coldepartment))
                                        <td>{{$employee->departmentName}}</td>
                                    @endif
                                    @if(!empty($request->coljoiningdate))
                                        <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                                    @endif
                                    @if(!empty($request->colgender))
                                        <td>
                                            @if($employee->empGenderId==1)
                                                Male
                                            @endif
                                            @if($employee->empGenderId==2)
                                                Female
                                            @endif
                                            @if($employee->empGenderId==3)
                                                Other
                                            @endif
                                        </td>
                                    @endif
                                    @if(!empty($request->colunit))
                                        <td>{{$employee->unitName}}</td>
                                    @endif
                                    @if(!empty($request->colsection))
                                        <td>{{$employee->empSection}}</td>
                                    @endif
                                    @if(!empty($request->colstatus))
                                        <td>
                                            @if($employee->empAccStatus==1)
                                                Active
                                            @elseif($employee->empAccStatus==0)
                                                Inactive
                                            @endif
                                        </td>
                                    @endif
                                    <td>

                                        {{$employee->in_time}}

                                    </td>
                                    @if(!empty($request->collateTime))
                                        @if($employee->in_time>$lateTime)
                                            <td>
                                                <span class="red-text">{{ date('H:i:s', strtotime($employee->in_time) - strtotime($lateTime)) }}</span>
                                            </td>
                                        @else
                                            <td></td>
                                        @endif
                                    @endif
                                    <td>
                                        @if($employee->in_time==$employee->out_time)
                                            <span class="red-text">Not Given</span>

                                        @else
                                            {{$employee->out_time}}
                                        @endif
                                    </td>
                                    @if(!empty($request->colOverTime))
                                        <td>
                                            @if(isset($employee->out_time))
                                                @if($employee->out_time>\App\Http\Controllers\OvertimeController::overtime_start())
                                                    {{ date('H:i:s', strtotime($employee->out_time) - strtotime($out_time)) }}
                                                @endif
                                            @endif
                                        </td>
                                    @endif
                                    <td>
                                        @if($employee->in_time>$lateTime)
                                            <span class="late-text">Late</span>
                                        @else
                                            <span class="green-text">Present</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <hr>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection