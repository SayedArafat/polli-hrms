@extends('layouts.master')
@section('title', 'Daily Attendance Summary')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3><i class="fa fa-users "></i> Attendance Summery For <strong>{{ \Carbon\Carbon::parse($request->date)->format('d M Y (l)') }}</strong></h3>
                            </div>
                        </div>
                    </div>
                    <div class="padLeft20 padTop10">
                        Total Employee={{$totalE}}<br>
                        Total Present={{$totalP}}<br>
                        Total Leave={{$totalL}}<br>
                        Total Absent={{$totalE-($totalL+$totalP)}}<br>
                        Total Late={{ $Tlate }}<br>
                    </div>


                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-dynamic table-hover table-striped table-bordered">
                            <thead style="font-size:15px;font-weight:bold;">
                            <tr>
                                @if($request->reportType==1)
                                    <th>Department</th>
                                @elseif($request->reportType==2)
                                    <th>Section</th>
                                @elseif($request->reportType==3)
                                    <th>Floor</th>
                                    <th>Line</th>
                                @elseif($request->reportType==4)
                                    <th>Designation</th>
                                @endif
                                <th>Total</th>
                                <th>Present</th>
                                <th>Absent</th>
                                <th>Late</th>
                                <th>Leave</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $d)
                                <tr>
                                    @if($request->reportType==3)
                                        <td>{{\Illuminate\Support\Facades\DB::table('floors')->where('id','=',$d['designationId'])->select('floor')->first()->floor}}</td>
                                    @endif
                                    <td>{{$d['designationName']}}</td>
                                    <td>{{$d['total']}}</td>
                                    <td>{{ $d['present']  }}</td>
                                    <td>{{ $d['total']-($d['present']+$d['leave']) }}</td>
                                    <td>{{ $d['late'] }}</td>
                                    <td>{{ $d['leave'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <hr>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection
