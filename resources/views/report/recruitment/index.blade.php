@php
use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Recruitment Report')
@section('content')
    <div class="page-content ">
        <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Recruitment Report</b></h2>
                    <hr>
                </div>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="vacancy">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#d32f2f;color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-th f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b> Vacancy List</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="recruitment/dailyRecruitmentList">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#43A047;color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-line-chart f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Daily Recruitment List</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="vacancy/applicantList">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(57,73,171);color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-users f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Applicant List</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="vacancy/acceptedList">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#0097A7;color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-check-circle f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Accepted Employee List</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

        <div class="col-md-12"><hr></div>

        </div>

    </div>
@include('include.copyright')
@endsection