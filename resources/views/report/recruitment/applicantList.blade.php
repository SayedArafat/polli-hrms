@php
use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Applicant List Report')
@section('content')
  <div class="page-content ">
      <div class="row panel"  style="border:1px solid #999">
        
            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Select Applicants</b></h2>
                    <hr>
                </div>
            </div>
            

              {!! Form::open(['method'=>'POST','action'=>'ReportController@applicantListShow','target'=>"_blank"]) !!}
                <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">

                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Select Position<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <select name="vacId" class="form-control"  data-search="true">
                              <option value="0">All</option>
                              @foreach($applicantList as $applicantList)
                                <option value="{{$applicantList->vID}}">{{$applicantList->vacTitle}}</option>
                              @endforeach
                            </select>
                        </div>
                    </div>
  
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Select Priority<span class="clon">:</span></label>
                        <div class="col-md-9">
                          <select name="priority" class="form-control"  data-search="true">
                            <option value="0">All</option>
                            <option value="High">High</option>
                            <option value="Regular">Regular</option>
                            <option value="Low">Low</option>
                            
                          </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Eligible for Interview<span class="clon">:</span></label>
                        <div class="col-md-9">
                          <select name="elgInterview" class="form-control"  data-search="true">
                            <option value="0">All</option>
                            
                              <option value="Accepted">Accepted</option>
                              <option value="Rejected">Rejected</option>
                            
                          </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Job Status<span class="clon">:</span></label>
                        <div class="col-md-9">
                          <select name="jobStatus" class="form-control"  data-search="true">
                            <option value="0">All</option>
                            <option value="Confirm">Confirmed</option>
                            <option value="Reject">Rejected</option>
                            <option value="Waiting">Waiting</option>
                            
                          </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Vacancy Status<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select class="form-control" name="vacStatus">
                          <option value="3">All</option>
                          <option value="1">Active</option>
                          <option value="0">InActive</option>
                        </select>
                    </div>

                    <div class="form-group">
                      <div class="input-form-gap"></div>
                      <div class="input-form-gap"></div>
                      <label class="col-md-3">Printable Column<span class="clon">:</span></label>
                      <div class="col-md-9">
                        <div class="form-control">
                          
                          <input type="checkbox" checked name="applicationDateCheck" value="1"> Application Date  &nbsp;&nbsp;
                          <input type="checkbox" checked name="vacStatusCheck" value="1"> Vacancy Status   &nbsp;&nbsp;
                          <input type="checkbox" checked name="priorityCheck" value="1"> Priority  &nbsp;&nbsp;
                          
                          <input type="checkbox" checked checked name="interviewDateCheck" value="1"> Interview Date  &nbsp;&nbsp; 
                          <input type="checkbox" checked name="jobStatusCheck" value="1"> Job Status  &nbsp;&nbsp;<br><br>
                          <input type="checkbox" checked name="eligibilityCheck" value="1"> Eligible for Interview  &nbsp;&nbsp;
                            
                        </div>  
                      </div>
                    </div>
                                    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                      <div class="input-form-gap"></div>
                      <label class="col-md-3">Page Size<span class="clon">:</span></label>
                      <div class="col-md-9">
                          <select class="form-control" name="pagesize">
                            <option selected value="A4">A4</option>
                            <option value="Legal">Legal</option>
                            <option value="Letter">Letter</option>
                          </select>
                      </div>
                    </div>
    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                      <label class="col-md-3">Page Orientation<span class="clon">:</span></label>
                      <div class="col-md-9">
                          <select class="form-control" name="pageOrientation">
                            <option selected value="Portrait">Portrait</option>
                            <option value="Landscape">Landscape</option>
                          </select>
                      </div>  
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                          <hr>
                          <input type="submit" value="Preview" name="viewType" class="btn btn-success margin-top-10">
        
                          <span class="dropdown">
                              <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Generate PDF
                              <span class="caret"></span></button>
                              <ul class="dropdown-menu">
                                  <li><button type="submit" value="pdfenglish" name="viewType" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> &nbsp; English </button></li>
                                  <li><button type="submit" value="pdfbangla" name="viewType" class="btn btn-blue"><i class="fa fa-file-excel-o"></i> &nbsp; বাংলা </button></li>
                              </ul>
                          </span>
                          <hr>
                          <hr>
                        </div>
                    </div>

                </div>
                </div>
             {!! Form::close() !!}
      </div>
  </div>



@include('include.copyright')
@endsection