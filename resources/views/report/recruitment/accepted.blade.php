@extends('layouts.master')
@section('title', 'Accepted Vacancy Application List Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Accepted Employee List </strong></h3>
                    </div>
                    <div class="panel-content">
                      <a href="{{url('report/recruitment')}}" class="btn btn btn btn-default"><i class="fa fa-arrow-left"> Back</i></a>
                      
                        @if(count($acceptedList)==0)
                        
                            <h3><p class="text-center">No Applicants found</p></h3>
                        
                        @else
                       
                        
                        <span class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Generate PDF
                            <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <a href="{{url('report/recruitment/acceptedListPDF')}}" class="btn btn-success"><i class="fa fa-file-excel-o"></i> &nbsp; English</i></a>
                                <a href="{{url('report/recruitment/acceptedListPDFBangla')}}" class="btn btn-blue"><i class="fa fa-file-excel-o"></i> &nbsp; বাংলা </button></i></a>
                            </ul>
                        </span>
                        

                        <h1 class="text-center">Applicants List</h1>
                        <p class="text-center">Current Date: <strong><?php echo date('d-M-Y'); ?></p></strong>
                        
                            <table id="leave_display" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Order</th>
                                    <th>Applicant Name</th>
                                    <th>Contact No.</th>
                                    <th>Position Applied</th>
                                    <th>Application Date</th>
                                    
                                    <th>Interview Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $order=0; @endphp
                                @foreach($acceptedList as $item)
                                @php 
                                if(($item->interviewDate))
                                    $status='Approved';
                                
                                else
                                    $status='Rejected';

                                if($item->interviewDate)
                                    $interViewDate=date("d F Y",strtotime($item->interviewDate));
                                else 
                                $interViewDate='Not Available';
                                
                                $applyDate=strtotime($item->submittedDate);
                                
                               
                                $order++; 
                                @endphp
                                <tr>
                                <td>{{$order}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->phone}}</td>
                                <td>{{$item->vacTitle}}</td>
                                <td>{{date("d F Y", $applyDate)}}</td>
                                
                                <td>{{$interViewDate}}</td>
                                </tr>
                            @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection