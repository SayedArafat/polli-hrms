@extends('layouts.master')
@section('title', 'Error')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Error 404</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div style="background: #cc0000;color: #ffffff;" class="alert alert-danger">
                            <strong>Error!</strong> Sorry there is no data in this month.
                            <a href="{{url('report/payroll/employee/salary/summery')}}">Go Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection