@php(
    $totalEmployee=\Illuminate\Support\Facades\DB::table('employees')->where('empAccStatus','=','1')->count()
)


        <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Attendance Summery</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
            text-align: center;
            border:1px;
            font-size: 12px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            border: 1px solid #ddd;
            text-align: center !important;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: left;

        }

        #customers th {
            text-align: left;
            padding: 5px;
            background:#eee;

        }

        table td {
            padding: 2px;
            margin: 0;
        }
        table td p{
            margin: 0px;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }
    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
        <p class="reportDateRange">Attendance summery report from <b>{{\Carbon\Carbon::parse($request->date)->format('d M Y')}} </b> to <b>{{\Carbon\Carbon::parse($request->endDate)->format('d M Y')}} </b> </p>

    </div>



    <center>

        @if(!empty($totalAttendance))
            <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Day</th>
                    <th>Total Present</th>
                    <th>Total Late</th>
                    <th>Total Absent</th>
                    <th>On Leave</th>
                </tr>
                </thead>
                <tbody>
                @foreach($totalAttendance as $t)
                    <tr>
                        <td>{{\Carbon\Carbon::parse($t->date)->format('d M Y')}}</td>
                        <td>{{\Carbon\Carbon::parse($t->date)->format('l')}}</td>
                        @if($t->key==2)
                            <td style="color:#FF0000;">Weekend</td>
                            <td style="color:#FF0000;">Weekend</td>
                            <td style="color:#FF0000;">Weekend</td>
                            <td style="color:#FF0000;">Weekend</td>

                        @elseif($t->key==1)
                            <td style="color:#00aa00;">Festival Holiday</td>
                            <td style="color:#00aa00;">Festival Holiday</td>
                            <td style="color:#00aa00;">Festival Holiday</td>
                            <td style="color:#00aa00;">Festival Holiday</td>

                        @else
                            <td>{{$t->attendance}}</td>
                            <td>{{$t->late}}</td>
                            <td>{{$totalEmployee-($t->attendance+$t->leaves)}}</td>
                            <td>{{$t->leaves}}</td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <hr>
            <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @endif

</div>

</body>
</html>




