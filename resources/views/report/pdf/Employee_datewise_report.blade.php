<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Employee Leave Report</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
            text-align: center;
            border:1px;
            font-size: 12px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            border: 1px solid #ddd;
            text-align: center !important;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: left;

        }

        #customers th {
            text-align: left;
            padding: 5px;
            background:#eee;
            font-size: 9px;

        }

        table td {
            padding: 2px;
            margin: 0;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
          font-size: 18px !important;
          
        }
    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h1 class="reportHeaderCompany">{{$companyInformation->company_name}}</h1>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
    </div>
    <center>
            <div class="col-md-6">
                <h4><strong>Employee Leave Report</strong></h4> 
                <h5><strong>Report showing from {{date("d-M-Y",strtotime($request->leave_start))}} to {{date("d-M-Y",strtotime($request->leave_end))}}</strong></h5> 

            </div>

    @if(!empty($data))
    <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
        <thead>
        <tr>
            <th>Order</th>
            <th>Employee</th>
            <th>Designation</th>
            <th>Leave Type</th>
            <th>Leave Taken</th>
            <th>Leave Available</th>
            <th>Leave Date</th>
            {{--<th>Date</th>--}}
        </tr>
        </thead>
        <tbody>
        @php $order=0; @endphp
        @foreach($data as $item)
        @php $order++; @endphp
        <tr>
        <td>{{$order}}</td>
        <td>{{$item->empFirstName}} {{$item->empLastName}}</td>
        <td>{{$item->designation}}</td>
        <td>
            @foreach($tb_leave_type as $all_leave_type)
            {{$all_leave_type->leave_type}} ({{$all_leave_type->total_days}})
            @endforeach
        </td>
        <td>
        {{$item->names}}
        </td>
        
        <td>
            {{$item->leave_available}}
        </td>

        <td>
        Leave Start Date: {{$item->leave_sdays}} <br> Leave End Date:{{$item->leave_edays}}
        </td>

        {{--<td>--}}
            {{--{{\Carbon\Carbon::parse($item->report_date)->format('j F Y') }}--}}
        {{--</td>--}}
        </tr>
    @endforeach
        </tbody>
    </table>
    @else
        <hr>
        <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @endif

</div>

</body>
</html>




