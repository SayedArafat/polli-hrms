<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daily Attendance Summery</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
            text-align: center;
            border:1px;
            font-size: 12px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            border: 1px solid #ddd;
            text-align: center !important;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: left;

        }

        #customers th {
            text-align: left;
            padding: 5px;
            background:#eee;

        }

        table td {
            padding: 2px;
            margin: 0;
        }
        table td p{
            margin: 0px;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }
    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
        <p class="reportDateRange">Attendance summary for <b>{{\Carbon\Carbon::parse($request->date)->format('d M Y (l)')}} </b></p>

    </div>



    <center>

        @if(!empty($data))
            <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
                <thead>
                <tr>
                    @if($request->reportType==1)
                        <th>Department</th>
                    @elseif($request->reportType==2)
                        <th>Section</th>
                    @elseif($request->reportType==3)
                        <th>Floor</th>
                        <th>Line</th>
                    @elseif($request->reportType==4)
                        <th>Designation</th>
                    @endif
                    <th>Total</th>
                    <th>Present</th>
                    <th>Absent</th>
                    <th>Late</th>
                    <th>Leave</th>

                </tr>
                </thead>
                <tbody>
                @foreach($data as $d)
                    <tr>
                        @if($request->reportType==3)
                            <td>{{\Illuminate\Support\Facades\DB::table('floors')->where('id','=',$d['designationId'])->select('floor')->first()->floor}}</td>
                        @endif
                        <td>{{$d['designationName']}}</td>
                        <td>{{$d['total']}}</td>
                        <td>{{ $d['present']  }}</td>
                        <td>{{ $d['total']-($d['present']+$d['leave']) }}</td>
                        <td>{{ $d['late'] }}</td>
                        <td>{{ $d['leave'] }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div style="float:left;padding-top: 15px; font-size: 11px; text-align: left;">
                <span><b>Total Employee: </b> {{$totalE}}</span><br>
                <span><b>Total Present :</b> {{$totalP}}</span><br>
                <span><b>Total Absent Employee :</b> {{ $totalE-($totalL+$totalP) }}</span><br>
                <span><b>Total Late Employee :</b> {{ $Tlate }}</span><br>
                <span><b>Total Employee On Leave :</b> {{ $totalL }}</span><br>

            </div>

        @else
            <hr>
            <h4 style="color:red;"><center> No Absent data found.</center></h4>
    @endif

</div>

</body>
</html>




