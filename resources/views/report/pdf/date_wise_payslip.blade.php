<!DOCTYPE html>
<html>
<head>
    <title>Page Title</title>
    <style>
        .left-div{
            float: left;
            width:50%
        }
        p{
            margin: auto !important;
            line-height: 20px !important;
        }
        .payslip_generate{
            margin: 10px 10px;
        }
        .salary{
            padding-top:15px ;
        }
        .salary p{
            border-bottom:1px solid #888;
        }

        .column {
            float: left;
            padding-left:24px;
        }

        .left, .right {
            width: 50%;
            padding-right: 10px;
        }

        .column.middle {
            width: 25%;
        }
        .signature{
            font-weight: bold;
            font-size: 16px;
        }
        .office{
            text-align: center;
            font-size: 14px;
        }
        .receipient{
            padding-left: 420px;
        }
    </style>
</head>
<body>
@php
    $currencyprefix = "Taka";
    $currencysuffix = "";
        $order=0;
          function dayCalculator($d1,$d2){
              $date1=strtotime($d1);
              $date2=strtotime($d2);
              $interval1=1+round(abs($date1-$date2)/86400);
              $festivalLeave = DB::table('tb_festival_leave')->get();
              $dcount=0;
              foreach($festivalLeave as $fleave){
              if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
              $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
              }
              else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
              $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
              }
              else{
              continue;
              }
              }
              $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
              $start_date=date('Y-m-d', $date1);
              $end_date=date('Y-m-d', $date2);
              $key=0;
              for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
              $dates = $i->format('Y-m-d');
              $timestamp = strtotime($dates);
              $day = date('l', $timestamp);
              for($j=0;$j<count($weekends);$j++)
              {
              if($day==$weekends[$j]){
              $key++;
              }
              }
              }
              $interval=(int)$interval1-((int)$dcount+$key);
              return $interval;
              }
          function weekdayCalculator($d1,$d2){
              $date1=strtotime($d1);
              $date2=strtotime($d2);
              $interval1=1+round(abs($date1-$date2)/86400);
              $festivalLeave = DB::table('tb_festival_leave')->get();
              $dcount=0;
              foreach($festivalLeave as $fleave){
              if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
              $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
              }
              else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
              $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
              }
              else{
              continue;
              }
              }
              $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
              $start_date=date('Y-m-d', $date1);
              $end_date=date('Y-m-d', $date2);
              $key=0;
              for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
              $dates = $i->format('Y-m-d');
              $timestamp = strtotime($dates);
              $day = date('l', $timestamp);
              for($j=0;$j<count($weekends);$j++)
              {
              if($day==$weekends[$j]){
              $key++;
              }
              }
              }
              $interval=(int)$interval1-((int)$dcount+$key);
              return $key;
              }
@endphp
@foreach($data as $payslip_report)
    @php
        $order++;
        $gross=round($payslip_report->basic_salary+$payslip_report->house_rant+$payslip_report->medical+$payslip_report->food+$payslip_report->transport+$payslip_report->other,2);
        $overtime_rate=$payslip_report->basic_salary/208*2;
        $actual_overtime_rate=round($overtime_rate,2);
        $overtime_amount=$actual_overtime_rate*$payslip_report->overtime;
        $special_bonus=round($payslip_report->special_bonus,2);
        $attendance_bonus=round($payslip_report->attendance_bonus,2);
        $current_month_day=date('t');
        $current_month_first=date('Y-m-01');
        $current_month_last=date('Y-m-t');
        $working_day=dayCalculator($current_month_first,$current_month_last);
        $weekend=weekdayCalculator($current_month_first,$current_month_last);
        $absentday=$working_day-$payslip_report->total_leaves_taken-$payslip_report->total_present;
        $deduction=$payslip_report->basic_salary/$current_month_day*$absentday;
        $actual_deduction=round($deduction,2);
        $gross_pay=$gross-$deduction;
        $actual_gross_pay=round($gross_pay,2);
        $net_wages=$gross_pay+$payslip_report->special_bonus+$payslip_report->attendance_bonus+$overtime_amount;
        $actual_net_wages=round($net_wages,2);
    @endphp
    <div class="left-div">
        <div class="payslip_generate" style=" border: 1px solid #000000;">
            <h4 style="text-align: center;font-weight: bold;">{{$companyInformation->company_name}}</h4>
            <p style="font-weight: bold;text-align: center;font-size: 16px;padding: 0 4px;">Pay Slip</p>
            <hr>
            <div class="row">
                <div class="column left">
                    <p style="padding: 0px;">SL. NO: {{$order}}</p>
                    <p style="padding: 0px;">Month:  {{date('M-Y')}}</p>
                    <p style="padding: 0px;">Id No:  {{$payslip_report->employeeId}}</p>
                    <p style="padding: 0px;">Grade:  {{$payslip_report->grade_name}}</p>
                    <p style="padding: 0px;">Name:   {{$payslip_report->empFirstName}} {{$payslip_report->empLastName}}</p>
                </div>
                <div class="column right">
                    <p style="padding: 0px;">Date:        {{date('d/m/Y')}}</p>
                    <p style="padding: 0px;">Section:     {{$payslip_report->empSection}}</p>
                    <p style="padding: 0px;">Line:        {{$payslip_report->line_no}}</p>
                    <p style="padding: 0px;">Designation: {{$payslip_report->designation}}</p>
                    <p style="padding: 0px;">Join Date:   {{date('d-m-Y',strtotime($payslip_report->empJoiningDate))}}</p>
                </div>
            </div>
            <div class="row salary">
                <div class="column left">
                    <p style="padding: 0px;">Basic:         {{$currencyprefix}} {{$payslip_report->basic_salary}} {{$currencysuffix}}</p>
                    <p style="padding: 0px;">House Rant:    {{$currencyprefix}} {{$payslip_report->house_rant}} {{$currencysuffix}}</p>
                    <p style="padding: 0px;">Medical:       {{$currencyprefix}} {{$payslip_report->medical}} {{$currencysuffix}}</p>
                    <p style="padding: 0px;">Gross:         {{$currencyprefix}} {{$gross}} {{$currencysuffix}}</p>
                    <p style="padding: 0px;">Att Bonus      {{$currencyprefix}} {{$attendance_bonus}} {{$currencysuffix}}</p>
                    <p style="padding: 0px;">Gross Pay:     {{$currencyprefix}} {{$actual_gross_pay}} {{$currencysuffix}}</p>
                    <p style="padding: 0px;">Deduction:     {{$currencyprefix}} {{$actual_deduction}} {{$currencysuffix}}</p>
                    <p style="padding: 0px;">Net Wages:     {{$currencyprefix}} {{$actual_net_wages}} {{$currencysuffix}}</p>
                </div>
                <div class="column right">
                    <p style="padding: 0px;">Overtime Rat:  {{$currencyprefix}} {{$actual_overtime_rate}} {{$currencysuffix}}</p>
                    <p style="padding: 0px;">Transport:     {{$currencyprefix}} {{$payslip_report->transport}} {{$currencysuffix}}</p>
                    <p style="padding: 0px;">Overtime Hour:
                        @if($payslip_report->overtime=='')
                            0 /hr
                        @else
                            {{$payslip_report->overtime}} /hr
                        @endif
                    </p>
                    <p style="padding: 0px;">Amt overtime:   {{$currencyprefix}} {{$overtime_amount}} {{$currencysuffix}}</p>
                    <p style="padding: 0px;">Special Bonus:  {{$currencyprefix}} {{$special_bonus}} {{$currencysuffix}}</p>
                    <p style="padding: 0px;">Food:           {{$currencyprefix}} {{$payslip_report->food}} {{$currencysuffix}}</p>
                    <p style="padding: 0px;">Leave:
                        @if($payslip_report->total_leaves_taken=='')
                            0
                        @else
                            {{$payslip_report->total_leaves_taken}}
                        @endif
                    </p>
                    <p style="padding: 0px;">Workdays:       {{$working_day}}</p>
                    <p style="padding: 0px;">Week Leave:     {{$weekend}}</p>
                    <p style="padding: 0px;">Absent:         {{$absentday}}</p>
                </div>
            </div>
            <div class="signature">Authorities</div>
            <div class="office">Office Copy</div>
            <div class="receipient">Recipient</div>
        </div>
    </div>
@endforeach
















</body>
</html>