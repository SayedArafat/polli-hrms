<!DOCTYPE html>
<html>
<head>
    <title>Attendance Bonus</title>
    <style>
        table, th, td {
            border: 1px solid black;
            font-size: 12px;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
<h4 style="text-align: center;font-weight: bold;">{{$companyInformation->company_name}}</h4>
<h5 style="text-align: center;font-weight: bold;">Attendance bonus for the month of</h5>
<p style="text-align: center">
    @foreach($month_name as $month)
        {{date('F-d-Y',strtotime($month->created_at))}}
    @endforeach
</p>
<table style="width:100%">
    <tr>
        <th>Order</th>
        <th>Employee</th>
        <th>Gross</th>
        <th>Gross after bonus</th>
        <th>Percent wise amount</th>
        <th>Amount</th>
        <th>Percent</th>
        <th>Given by</th>
        <th>Role</th>
        <th>Email</th>
    </tr>
    @php
        $order=0;
    @endphp
    @foreach($data as $attendance)
        @php
            $order++
        @endphp
        <tr>
            <td>{{$order}}</td>
            <td>{{$attendance->empFirstName}} {{$attendance->empLastName}}</td>
            <td>{{$attendance->emp_gross}}</td>
            <td>{{$attendance->emp_gross+$attendance->bonus_amount+$attendance->bonus_percent}}</td>
            <td>{{$attendance->bonus_amount}}</td>
            <td>{{$attendance->emp_total_amount}}</td>
            <td>
                @if($attendance->emp_total_percent=='')
                    0%
                @else
                    {{$attendance->emp_total_percent}}%
                @endif
            </td>
            <td>{{$attendance->name}}</td>
            <td>
                @if($attendance->is_permission==1)
                    Admin
                @endif
                @if($attendance->is_permission==2)
                    Hr
                @endif
                @if($attendance->is_permission==4)
                    Executive
                @endif
                @if($attendance->is_permission==5)
                    Accountant
                @endif
            </td>
            <td>{{$attendance->email}}</td>
        </tr>
    @endforeach
</table>
</body>
</html>