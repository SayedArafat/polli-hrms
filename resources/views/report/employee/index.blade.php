@php
use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Employee Report')
@section('content')
    <div class="page-content ">
        <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Employee Report</b></h2>
                    <hr>
                </div>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{route('report.search_employee')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(57,73,171);color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-search f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Search Employee Profile</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{url('/report/employee_list')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(0,137,123);color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-users f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b> Employee List</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{url('/report/employee_list_status')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#c0392b;color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-exclamation-circle f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Employee List By Account Status</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{url('/report/employee_list_gender')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#C51162;color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-mars-double f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Employee List By Gender</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{url('/report/employee_list_department')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#6D4C41;color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-server f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>  Department Wise Employee List </b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{url('/report/employee_list_designation')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(106,27,154);color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-list-ul f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>  Designation Wise Employee List </b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>


            {{--<div class="col-xlg-3 col-lg-3 col-sm-3">--}}
              {{--<a target="_BLANK" href="{{url('report/floor_line_employee_list')}}">--}}
              {{--<div class="panel">--}}
                {{--<div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#546E7A;color:white;">--}}
                    {{--<center>--}}
                      {{--<div class="row" >--}}
                        {{--<div class="">--}}
                            {{--<center><i class="fa fa-exchange f-40"></i></center>--}}
                        {{--</div>--}}
                        {{--<br>--}}
                        {{--<div class="">--}}
                        {{--<center>--}}
                          {{--<span class="f-14"><b>  Floor/Line Wise Employee List </b></span>--}}
                          {{--</center>--}}
                        {{--</div>--}}
                      {{--</div>--}}
                    {{--</center>--}}
                {{--</div>--}}
              {{--</div>--}}
              {{--</a>--}}
            {{--</div>--}}


            <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a target="_BLANK" href="{{url('report/section_employee_list')}}">
              <div class="panel">
                <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#303F9F;color:white;">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-codepen f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b> Section wise Employee List</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

        <div class="col-md-12">
          <hr>
        </div>
        </div>
    </div>
  
    @include('include.copyright')
@endsection