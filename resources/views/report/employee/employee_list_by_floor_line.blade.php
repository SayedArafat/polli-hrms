@php
use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Employee List By Floor/Line')
@section('content')
  <div class="page-content ">
      <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Employee Report By Floor/Line</b></h2>
                    <hr>
                </div>
            </div>


              {!! Form::open(['method'=>'POST','action'=>'ReportController@viewEmployeeListByFloorLine']) !!}
                <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">
                  
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Floor<span class="clon">:</span></label>
                         <div class="col-md-9">
                            <select name="floorId" class="form-control floor"  data-search="true">
                              <option value="0">All</option>
                              @foreach($floors as $floor)
                                <option value="{{$floor->id}}">{{$floor->floor}}</option>
                              @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Line<span class="clon">:</span></label>
                         <div class="col-md-9">
                            <select class="form-control floor_lines"  data-search="true"></select>
                        </div>
                    </div>

                    <div class="form-group">
                      <div class="input-form-gap"></div>
                      <div class="input-form-gap"></div>
                      <label class="col-md-3">Printable Column<span class="clon">:</span></label>
                      <div class="col-md-9">
                        <div class="form-control">
                          
                          <input type="checkbox" checked name="coldepartment" value="1"> Department  &nbsp;&nbsp;
                          <input type="checkbox" checked name="coldesignation" value="1"> Designation  &nbsp;&nbsp;
                          <input type="checkbox" name="colgender" value="1"> Gender  &nbsp;&nbsp;
                          <input type="checkbox" name="coljoiningdate" value="1"> Joiningdate  <br><br>
                          <input type="checkbox" name="colunit" value="1"> Unit  &nbsp;&nbsp;
                          <input type="checkbox" checked name="colfloor" value="1"> Floor &nbsp;&nbsp;
                          <input type="checkbox" checked name="colline" value="1"> Line &nbsp;&nbsp;
                          <input type="checkbox" name="colsection" value="1"> Section &nbsp;&nbsp;
                          <input type="checkbox" checked name="colstatus" value="1"> Status &nbsp;&nbsp;
                            
                        </div>  
                      </div>
                    </div>
                                    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                      <div class="input-form-gap"></div>
                      <label class="col-md-3">Page Size<span class="clon">:</span></label>
                      <div class="col-md-9">
                          <select class="form-control" name="pagesize">
                            <option selected value="A4">A4</option>
                            <option value="Legal">Legal</option>
                            <option value="Letter">Letter</option>
                          </select>
                      </div>
                    </div>
    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                      <label class="col-md-3">Page Orientation<span class="clon">:</span></label>
                      <div class="col-md-9">
                          <select class="form-control" name="pageOrientation">
                            <option selected value="Portrait">Portrait</option>
                            <option value="Landscape">Landscape</option>
                          </select>
                      </div>  
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                          <hr>
                          <button type="submit" value="Preview" name="viewType" class="btn btn-success"><i class="fa fa-list"></i> &nbsp;Preview</button>
                          <button type="submit" value="Generate PDF" name="viewType" class="btn btn-primary"><i class="fa fa-download"></i> &nbsp;Download as PDF</button>
                          <hr>
                        </div>
                    </div>

                </div>
                </div>
             {!! Form::close() !!}
      </div>
  </div>

<script>
  $(document).ready(function () {
      $("select.floor").change(function () {
          var floor= $(".floor option:selected").val();
          if(!floor){
              var rr='<select class="form-control floor_lines" name="line_id">'+
              '<option value="">All</option>'+

              '</select>';
              $('.floor_lines').html(rr);
          }
          else {
              $.ajax({
                  url: 'floor/line/' + floor,
                  method: 'get',
                  dataType: 'html',
                  success: function (response) {
                      $('.floor_lines').html(response);
                  }
              });
          }
      });
  });
</script>
@include('include.copyright')
@endsection