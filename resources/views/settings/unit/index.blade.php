@extends('layouts.master')
@section('title', 'Branches')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">

                        <h3><i class="fa fa-table"></i> <strong>Branch </strong> List</h3>

                        <a data-toggle="modal" data-target="#usercreate" class="btn btn-default btn-sm"><i class="fa fa-plus"></i>Add</a>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Phone Number</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                                @foreach($units as $unit)
                                    <tr>
                                        <td>{{$unit->name}}</td>
                                        <td>{{$unit->phone_no}}</td>
                                        <td>{{$unit->email}}</td>
                                        <td>
                                            <a href="{{route('unit.show',$unit->id)}}" title="View" class="btn btn-primary btn-sm view-unit"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('unit.edit',$unit->id)}}" title="Edit" class="btn btn-default btn-sm edit-unit"><i class="fa fa-edit"></i></a>
                                            <a href="{{route('unit.delete.show',$unit->id)}}" title="Delete Data" class="btn btn-danger btn-sm delete-unit"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Add New Modal -->
    <div class="modal fade" id="usercreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>Add New Branch</strong>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>

                </div>
                <div id="create-new" class="modal-body">
                    {!! Form::open(['method'=>'POST', 'action'=>'UnitController@store']) !!}
                    <div class="form-group">
                        {!! Form::label('name','Branch Name:') !!}
                        {!! Form::text('name',null,['class'=>'form-control','required'=>'', 'placeholder'=>'Enter Branch Name']) !!}

                    </div>

                    <div class="form-group">
                        {!! Form::label('phone_no','Phone Number:') !!}
                        {!! Form::text('phone_no',null,['class'=>'form-control','required'=>'', 'placeholder'=>'Enter Phone Number']) !!}

                    </div>
                    <div class="form-group">
                        {!! Form::label('email','Email Address:') !!}
                        {!! Form::email('email',null,['class'=>'form-control','required'=>'', 'placeholder'=>'Enter Email Address']) !!}

                    </div>
                    <div class="form-group">
                        {!! Form::label('address','Full Address:') !!}
                        {!! Form::textarea('address', null,['class'=>'form-control','required'=>'', 'placeholder'=>'Enter Address']) !!}

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="Submit" id="addUnit" class="btn btn-primary">Add New</button>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>







    <div class="modal fade" id="edit-unit-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                    <h4 class="modal-title"><strong>Edit Branch</strong> Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="edit-form col-md-12 col-sm-12 col-xs-12">


                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="delete-unit-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                    <h4 class="modal-title"><strong>Confirm Delete</strong></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="delete-form col-md-12 col-sm-12 col-xs-12">


                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="view-unit-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                    <h4 class="modal-title"><strong>Branch Details</strong></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="view-form col-md-12 col-sm-12 col-xs-12">


                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <script>

        $('.document').ready(function () {
            $(document.body).on('click','.view-unit',function (event) {
                event.preventDefault();
                var url=$(this).attr('href'),
                    method='GET';

                $.ajax({
                    url:url,
                    method:method,
                    dataType:'html',
                    success:function (response) {
                        $('.view-form').html(response);


                    },
                    error:function (xhr) {

                    }
                })
                $('#view-unit-modal').modal('show');

            });
            $(document.body).on('click','.edit-unit',function (event) {
                event.preventDefault();
                var url=$(this).attr('href');
                $.ajax({
                    url:url,
                    method:'GET',
                    dataType:'html',
                    success:function (response) {
                        $('.edit-form').html(response);

                    },
                    error:function (xhr) {
                        console.log(xhr);

                    }
                });


                $('#edit-unit-modal').modal('show');

            });

            $(document.body).on('click','.delete-unit',function (event) {
                event.preventDefault();
                var url=$(this).attr('href');
//                alert(url);
                $.ajax({
                    url:url,
                    method:'get',
                    dataType:'html',
                    success:function (response) {
                        $('.delete-form').html(response);

                    },
                    error:function (xhr) {

                    }
                });
                $('#delete-unit-modal').modal('show');


            });

        });


        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);


    </script>


    @include('include.copyright')
@endsection