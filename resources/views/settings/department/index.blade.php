@extends('layouts.master')
@section('title', 'Departments')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <a data-toggle="modal" data-target="#usercreate" class="btn btn-default btn-sm"><i class="fa fa-plus"></i>Add</a>
                        <h3><i class="fa fa-table"></i> <strong>Department </strong> List</h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Department Name</th>
                                <th>Description</th>
                                <th>Employees</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($departments as $department)
                                <tr>
                                    <td>{{$department->id}}</td>
                                    <td>{{$department->departmentName}}</td>
                                    <td>{{$department->departmentDescription}}</td>
                                    <td>{{\Illuminate\Support\Facades\DB::table('employees')->where('empDepartmentId','=',$department->id)->count()}}</td>
                                    <td>
                                        <a data-toggle="modal" title="Edit" data-target="#{{$department->id}}" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a>
                                        <a data-toggle="modal" title="Delete Data" data-target="#{{"delete".$department->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>







                                <!-- Edit Modal -->
                                <div class="modal fade" id="{{$department->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Update Info</strong></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                {!! Form::open(['method'=>'PATCH', 'action'=>['DepartmentController@update', $department->id]]) !!}
                                                <div class="form-group">
                                                    {!! Form::label('name','Name') !!}
                                                    {{--{!! Form::text('name',"Value", ['class'=>'form-control']) !!}--}}
                                                    {{--<label for="username">Name</label>--}}
                                                    <input type="text" class="form-control" required="" id="name" aria-describedby="emailHelp" name="name" value="{{$department->departmentName}}">
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('description', 'Description') !!}
                                                    <textarea name='description' class="form-control" required="" rows="5" id="description">{{$department->departmentDescription}}</textarea>

                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Update Changes</button>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                {{--@endif--}}

                                <!--End of Edit Model-->





                                <!-- Delete Modal -->
                                <div class="modal fade" id="{{"delete".$department->id}}" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Confirm Delete</strong></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Do you want to delete <strong>{{$department->departmentName}}</strong> Department? </p>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::open(['method'=>'DELETE', 'action'=>['DepartmentController@destroy', $department->id]]) !!}
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-danger">Confirm</button>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--Delete Modal Ended-->

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Add New Modal -->
    <div class="modal fade" id="usercreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>New Department</strong>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>

                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST', 'action'=>'DepartmentController@store']) !!}
                    <div class="form-group">
                        {!! Form::label('name','Department Name:') !!}
                        {!! Form::text('name',null,['class'=>'form-control','required'=>'', 'placeholder'=>'Enter Department Name']) !!}

                    </div>
                    <div class="form-group">
                        {!! Form::label('description','Description:') !!}
                        {!! Form::textarea('description', null,['class'=>'form-control','required'=>'', 'placeholder'=>'Enter Description']) !!}

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="Submit" class="btn btn-primary">Add New</button>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>


    @include('include.copyright')
@endsection