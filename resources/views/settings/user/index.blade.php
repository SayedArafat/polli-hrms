@extends('layouts.master')
@section('title', 'User List')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-table"></i> <strong>User </strong> List</h3>
                    </div>



                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($userData as $user)
                                @if($user->is_permission=='1')
                                @else
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>
                                            @if($user->is_permission=='2')
                                                Hr
                                            @elseif($user->is_permission=='3')
                                                Employee
                                            @elseif($user->is_permission=='4')
                                                Executive
                                            @elseif($user->is_permission=='5')
                                                Accountant
                                            @endif
                                        </td>
                                        <td>
                                            <a data-toggle="modal" data-target="#{{$user->id}}" class="btn btn-success btn-sm" type="submit"><i class="fa fa-edit"></i></a>
                                        </td>
                                    </tr>
                                @endif
                                <!-- Modal -->
                                <div class="modal fade" id="{{$user->id}}" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Update Password</h4>
                                            </div>
                                            <div class="modal-body">
                                                {{Form::open(array('url' => 'user_update','method' => 'post'))}}
                                                <div class="form-group">
                                                    <label for="pwd">Password:</label>
                                                    <input type="password" name="user_password" class="form-control form-white" required>
                                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                                </div>
                                                <button onclick="return confirm('are you sure?')" type="submit" class="btn btn-success">Update</button>
                                                {{ Form::close() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection