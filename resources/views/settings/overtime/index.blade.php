@extends('layouts.master')
@section('title', 'Overtime Settings')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <a data-toggle="modal" data-target="#usercreate" class="btn btn-default btn-sm"><i class="fa fa-plus"></i>Add</a>
                        <h3><i class="fa fa-table"></i> <strong>Overtime </strong> Settings</h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Overtime Count Start</th>
                                <th>Overtime End Time</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($overtime as $o)
                                <tr class="warning">
                                    <td>{{$o->id}}</td>
                                    <td>{{$o->min_overtime}}</td>
                                    <td>{{$o->max_overtime}}</td>
                                    <td>
                                        <a data-toggle="modal" title="Edit" data-target="#{{$o->id}}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>

                                <!-- Edit Modal -->
                                <div class="modal fade" id="{{$o->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Update Overtime Settings</strong></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                {!! Form::open(['method'=>'PATCH', 'action'=>['OvertimeController@update', $o->id]]) !!}
                                                <div class="required form-group">
                                                    <label class="control-label">Overtime Start(HH:MM am/pm)</label>
                                                    <div class="append-icon">
                                                        <input type="time" name="min_overtime" class="form-control" value="{{$o->min_overtime}}" required>
                                                    </div>
                                                </div>
                                                <div class="required form-group">
                                                    <label class="control-label">Overtime End(HH:MM am/pm)</label>
                                                    <input type="time" name="max_overtime" class="form-control" value="{{$o->max_overtime}}" required>



                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Update Changes</button>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>


    @include('include.copyright')
@endsection