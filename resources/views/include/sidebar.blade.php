<div class="sidebar">
    <div class="logopanel">
        <h1>
            <a href="{{url('/dashboard')}}"></a>
        </h1>
    </div>
    <div class="sidebar-inner">
        <div class="sidebar-top">
            <div class="userlogged clearfix">
                <i class="icon icons-faces-users-01"></i>
                <div class="user-details">
                    <h4>{{ Auth::user()->name }}</h4>
                    <div class="dropdown user-login">
                        <button class="btn btn-xs dropdown-toggle btn-rounded" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" data-delay="300">
                            <i class="online"></i><span>Online</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <ul  id="nav" class="nav nav-sidebar">
            <li class=""><a href="{{url('/dashboard')}}"><i class="icon-home"></i><span>Dashboard</span></a></li>
            @if(checkPermission(['admin']))
                <li class="nav-parent ">
                    <a href="#"><i class="fa fa-users"></i><span>Employee</span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/employee')}}"> Employee List</a></li>
                        <li><a href="{{route('employee.create')}}"> Create Employee</a></li>
                    </ul>
                </li>

                <li class="nav-parent">
                    <a href="#"><i class="fa fa-suitcase"></i><span>Recruitment</span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('vacancy.index')}}"> Vacancy List</a></li>
                        <li><a href="{{route('vacancy.create')}}"> Vacancy Announcement</a></li>
                        <li><a href="{{route('application.create')}}"> New Job Application</a></li>
                        <li><a href="{{route('application.index')}}"> Job Application List</a></li>
                        <li><a href="{{route('recruitment.status')}}"> Daily Recruitment Status</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-credit-card"></i><span>Payroll</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/grade')}}">Grade</a></li>
                        <li><a href="{{url('/salary_view')}}">Salary</a></li>
                        <li><a href="{{url('/bonus')}}">Bonus</a></li>
                        <li><a href="{{url('/advance')}}">Advance</a></li>
                        <li><a href="{{url('/penalty')}}">Penalty</a></li>
                        <li><a href="{{url('/report/payroll/employee/salary/sheet/save')}}">Process Salary</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-calendar"></i><span>Attendance</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/attendance')}}">Upload</a></li>
                        <li><a href="{{url('/attendance/settings')}}">Setup</a></li>
                        <li><a href="{{route('files.show')}}">Attendance Files</a></li>
                        <li><a href="{{route('report.daily_absent_report')}}">Daily Absent Report</a></li>
                        <li><a href="{{route('report.public.daily_attendance')}}">Daily Attendance Sheet</a></li>
                        <li><a href="{{route('attendance.manual.create')}}">Manual Attendance</a></li>
                        <li><a href="{{route('attendance.manual.edit')}}">Manual Edit Attendance</a></li>
                        <li><a href="{{route('report.daily_attendance_summery')}}">Daily Attendance Summary</a></li>
                        {{--<li><a href="{{route('employee.continued_absent_list')}}"> Continued Absent List</a></li>--}}
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-support"></i><span>Leave</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/leave/assign_leave')}}">Manually Asssign Leave</a></li>
                        <li><a href="{{url('/request_leave')}}">Apply for Leave</a></li>
                        <li><a href="{{url('/leave')}}"> Leave Requests</a></li>
                        <li><a href="{{url('/leave/maternityLeaveDetails')}}">Maternity Leave Details</a></li>
                        <li><a href="{{url('/leave/earnLeaveDetails')}}">Earn Leave Details</a></li>
                        <li><a href="{{url('/leave_sett')}}"> Leave Settings</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="icon-note"></i><span>Training</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/add_training')}}">Add Training</a></li>
                        <li><a href="{{url('/assign/training')}}">Assign Training</a></li>
                        <li><a href="{{url('/training_history')}}">Training History</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-comments"></i><span>Meetings</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('meeting.index')}}"> Meeting List</a></li>
                        <li><a href="{{route('meeting.create')}}"> Create Meeting</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-bullhorn"></i><span>Notices</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('notices.index')}}"> Notices List</a></li>
                        <li><a href="{{route('notices.create')}}"> Create New Notice</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-money"></i><span>Expenses</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/expense/index')}}">Add Expenses</a></li>
                        <li><a href="{{url('/expense/categories')}}">Expense Categories</a></li>
                        <li><a href="{{url('/expense/selectDate')}}">History</a></li>
                        <li><a href="{{route('expense.report')}}">Expense Report</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-comments"></i><span>Complains</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('complains.index')}}">Complain list</a></li>
                        <li><a href="{{route('complains.create')}}">Add New Complain</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-file"></i><span>Report</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/report/employee')}}">Employee</a></li>
                        <li><a href="{{url('/report/payroll')}}">Payroll</a></li>
                        <li><a href="{{url('/report/attendance')}}">Attendance</a></li>
                        <li><a href="{{url('/report/leave')}}">Leave</a></li>
                        <li><a href="{{url('/report/training')}}">Training</a></li>
                        <li><a href="{{url('/report/recruitment')}}">Recruitment</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-cog"></i><span>Settings</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/user_settings')}}">User</a></li>
                        <li><a href="{{route('overtime.index')}}">Overtime</a></li>
                        <li><a href="{{route('unit.index')}}">Branches</a></li>
                        <li><a href="{{route('country.index')}}">Country List</a></li>
                        <li><a href="{{route('maritialstatus.index')}}">Marital Status</a></li>
                        <li><a href="{{route('department.index')}}">Departments</a></li>
                        <li><a href="{{route('designation.index')}}">Designation</a></li>
                        <li><a href="{{url('/company/information')}}">Company Information</a></li>
                        <li><a href="{{url('/system/settings')}}">Logo & Copyright</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-envelope"></i><span>Quota</span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('quota.district_list')}}">District List</a></li>
                        <li><a href="{{route('quota.settings')}}">Quota Settings</a></li>
                        <li><a href="{{route('quota.population')}}">Population</a></li>
                        {{--<li><a href="{{url('/manual_email')}}">Send Email</a></li>--}}
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-envelope"></i><span>Manual SMS/Email</span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/manual_sms')}}">Send SMS</a></li>
                        <li><a href="{{url('/manual_bulk_sms')}}">Send Bulk SMS</a></li>
                        <li><a href="{{url('/manual_email')}}">Send Email</a></li>
                    </ul>
                </li>

            @endif
            @if(checkPermission(['hr']))
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-users"></i><span>Employee</span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/employee')}}"> Employee List</a></li>
                        <li><a href="{{route('employee.create')}}"> Create Employee</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-suitcase"></i><span>Recruitment</span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('vacancy.index')}}"> Vacancy List</a></li>
                        <li><a href="{{route('vacancy.create')}}"> Vacancy Announcement</a></li>
                        <li><a href="{{route('application.create')}}"> New Job Application</a></li>
                        <li><a href="{{route('application.index')}}"> Job Application List</a></li>
                        <li><a href="{{route('recruitment.status')}}"> Daily Recruitment Status</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-credit-card"></i><span>Payroll</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/payroll/grade')}}">Grade</a></li>
                        <li><a href="{{url('/salary_view')}}">Salary</a></li>
                        <li><a href="{{url('/payslip_generate')}}">Payslip</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-credit-card"></i><span>Attendance</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/attendance')}}">Upload</a></li>
                        <li><a href="{{url('/attendance/settings')}}">Setup</a></li>
                        <li><a href="{{route('files.show')}}">Files</a></li>
                        <li><a href="{{route('attendance.manual.edit')}}">Manual Edit Attendance</a></li>
                        <li><a href="{{route('report.daily_attendance_summery')}}">Daily Attendance Summery</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="icon-note"></i><span>Leave</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                            <li><a href="{{url('/request_leave')}}">Apply Leave</a></li>
                        <li><a href="{{url('/leave')}}"> Leave Requests</a></li>
                        <li><a href="{{url('/leave_sett')}}"> Leave Settings</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="icon-note"></i><span>Training</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/add_training')}}">Add Training</a></li>
                        <li><a href="{{url('/training_history')}}">Training History</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-comments"></i><span>Meetings</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('meeting.index')}}"> Meeting List</a></li>
                        <li><a href="{{route('meeting.create')}}"> Create Meeting</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-money"></i><span>Expenses</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/expense/index')}}">Add Expenses</a></li>
                        <li><a href="{{url('/expense/categories')}}">Expense Categories</a></li>
                        <li><a href="{{url('/expense/selectDate')}}">History</a></li>
                        <li><a href="{{route('expense.report')}}">Expense Report</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-file"></i><span>Report</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="medias-image-croping.html"> Images Croping</a></li>
                        <li><a href="medias-gallery-sortable.html"> Gallery Sortable</a></li>
                        <li><a href="medias-hover-effects.html"> <span class="pull-right badge badge-primary">12</span> Hover Effects</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-cog"></i><span>Settings</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/user_settings')}}">User</a></li>
                        <li><a href="{{route('country.index')}}">Country List</a></li>
                        <li><a href="{{route('maritialstatus.index')}}">Marital Status</a></li>
                        <li><a href="{{route('department.index')}}">Departments</a></li>
                        <li><a href="{{route('designation.index')}}">Designation</a></li>
                    </ul>
                </li>
            @endif
            @if(checkPermission(['employee']))
                <li class="nav-parent">
                    <a href="#"><i class="icon-note"></i><span>Leave</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/request_leave')}}">Request Leave</a></li>
                        <li><a href="{{url('/leave_history')}}"> Leave History</a></li>
                    </ul>
                </li>
            @endif
            @if(checkPermission(['executive']))
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-users"></i><span>Employee</span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/employee')}}"> Employee List</a></li>
                        <li><a href="{{route('employee.create')}}"> Create Employee</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-suitcase"></i><span>Recruitment</span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('vacancy.index')}}"> Vacancy List</a></li>
                        <li ><a href="{{route('vacancy.create')}}"> Vacancy Announcement</a></li>
                        <li><a href="{{route('application.create')}}"> New Job Application</a></li>
                        <li><a href="{{route('application.index')}}"> Job Application List</a></li>
                        <li><a href="{{route('recruitment.status')}}"> Daily Recruitment Status</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="icon-note"></i><span>Leave</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/request_leave')}}"> Request Leave</a></li>
                        <li><a href="{{url('/leave_history')}}"> Leave History</a></li>
                    </ul>
                </li>
            @endif
            @if(checkPermission(['accountant']))
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-suitcase"></i><span>Accountant</span> <span class="fa arrow"></span></a>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-money"></i><span>Expenses</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/expense/index')}}">Add Expenses</a></li>
                        <li><a href="{{url('/expense/categories')}}">Expense Categories</a></li>
                        <li><a href="{{url('/expense/selectDate')}}">History</a></li>
                        <li><a href="{{route('expense.report')}}">Expense Report</a></li>
                    </ul>
                </li>
            @endif
        </ul>
        <!-- SIDEBAR WIDGET FOLDERS -->
        <div class="sidebar-widgets">
            <p class="menu-title widget-title"> &nbsp;<span class="pull-right"><a href="{{url('/logout')}}" class="new-folder"> </a></span></p>
        </div>
        <div class="sidebar-footer clearfix">

        </div>
    </div>
</div>