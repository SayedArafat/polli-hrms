<link rel="shortcut icon" href="{{url('hrm_script/images/favicon.png')}}" type="image/png">
{{ Html::script('hrm_script/js/j-query.js') }}
{{ Html::style('hrm_script/css/bootstrap-select.css') }}
{{ Html::script('hrm_script/js/bootstrap-select.js') }}
{{ Html::script('hrm_script/js/chart.js') }}
{{ Html::style('hrm_script/css/style.css') }}
{{ Html::style('hrm_script/css/theme.css') }}
{{ Html::style('hrm_script/css/ui.css') }}
{{ Html::style('hrm_script/css/layout.css') }}
{{ Html::script('hrm_script/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js') }}



