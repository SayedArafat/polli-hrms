<div class="footer">
    <div class="copyright">
        <p class="pull-left sm-pull-reset">
            
			
            <span>
			@foreach($copyright as $data)
			{{$data->copyright}}
			@endforeach
			
			</span>.
            
        </p>
        <p class="pull-right sm-pull-reset">
            <!-- <span><a href="#" class="m-r-10">Support</a> | <a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span> -->
        </p>
    </div>
</div>