@extends('layouts.master')
@section('title', 'Meeting List')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('createMeeting'))
            <p id="alert_message" class="alert alert-success">{{Session::get('createMeeting')}}</p>
        @endif
        @if(Session::has('meetingUpdate'))
            <p id="alert_message" class="alert alert-success">{{Session::get('meetingUpdate')}}</p>
        @endif
        @if(Session::has('meetingdeleted'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('meetingdeleted')}}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-table"></i> <strong>Meeting </strong> List</h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Meeting Subject</th>
                                <th>Date</th>
                                <th>Starting Time</th>
                                <th>Ending Time</th>
                                <th>Venue</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($meetings as $meeting)
                                <tr>
                                    <td>
                                        {{$loop->iteration}}
                                    </td>
                                    <td>
                                        {{$meeting->msub}}
                                    </td>
                                    <td>
                                        {{\Carbon\Carbon::parse($meeting->date)->format('Y-m-d')}}
                                    </td>
                                    <td>
                                        {{$meeting->stime}}
                                    </td>
                                    <td>
                                        {{$meeting->etime}}
                                    </td>
                                    <td>
                                        {{$meeting->venue}}
                                    </td>
                                    <td>
                                        <a href="#"></a>
                                        <a href="#" class="btn btn-default btn-sm" data-toggle="modal" data-target="{{'#examplemodal'.$meeting->id}}"><i class="fa fa-edit"></i></a>
                                        <a href="#" class="btn btn-danger btn-sm" data-toggle="modal" data-target="{{'#deletemodel'.$meeting->id}}"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>

                                <div class="modal fade" id="{{'examplemodal'.$meeting->id}}" role="dialog">
                                    <div class="modal-dialog">

                                      <!-- Modal content-->
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Edit Meeting Information</h4>
                                      </div>
                                      <div class="modal-body">

                                          {!! Form::open(['method'=>'PATCH','action'=>['MeetingController@update',$meeting->id]]) !!}
                                          <div class="required form-group">
                                            <label class="control-label">Meeting Subject</label>
                                            <div class="append-icon">
                                                <input type="text" name="msub" class="form-control" value="{{$meeting->msub}}" minlength="3" placeholder="Minimum 3 characters..." required>
                                                <i class="icon-user"></i>
                                            </div>
                                        </div>
                                        <div class="required form-group">
                                            <label class="control-label">Meeting Date</label>
                                            <div class="append-icon">
                                                <input type="text" name="date" class="date-picker form-control" value="{{Carbon\Carbon::parse($meeting->date)->format('Y-m-d')}}" required>
                                            </div>
                                        </div>
                                        <div class="required form-group">
                                            <label class="control-label">Starting Time(HH:MM am/pm)</label>
                                            <div class="append-icon">
                                                <input type="time" name="stime" class="form-control" value="{{$meeting->stime}}" required>
                                            </div>
                                        </div>
                                        <div class="required form-group">
                                            <label class="control-label">Ending Time(HH:MM am/pm)</label>
                                            <div class="append-icon">
                                                <input type="time" name="etime" class="form-control" value="{{$meeting->etime}}" required>
                                            </div>
                                        </div>
                                        <div class="required form-group">
                                            <label class="control-label">Meeting Venue</label>
                                            <div class="append-icon">
                                                <input type="text" name="venue" placeholder="Enter meeting venue..." class="form-control" value="{{$meeting->venue}}" required>
                                                <i class="icon-user"></i>
                                            </div>
                                        </div>
                                        <div class="required form-group">
                                            <label class="control-label">Description</label>
                                            <div class="append-icon">
                                                <textarea rows="8" name="descrip" class="form-control" placeholder="Minimum 100 Character..." required>{{$meeting->descrip}}</textarea>
                                            </div>
                                        </div>
                                        <div class="text-center modal-footer">
                                            <button type="submit" class="btn btn-default">Update</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>

                                        {!! Form::close() !!}
                                    </div>

                                </div>

                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="{{'deletemodel'.$meeting->id}}" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Delete Meeting</h4>
                              </div>
                              <div class="modal-body">
                                  <p>Are you sure then press OK! or else CLOSE!</p>

                                  {!! Form::open(['method'=>'DELETE','action'=>['MeetingController@destroy',$meeting->id]]) !!}

                                  <div class="text-center modal-footer">
                                    <button type="submit" class="btn btn-default">Ok!<!-- <a href="{{ url('/meeting/{id}'.$meeting->id) }}">Ok!</a> --></button>

                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close!</button>
                                </div>

                                {!! Form::close() !!} 

                            </div>

                        </div>
                    </div>
  




                                @endforeach
                            </tbody>                     
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>







  @include('include.copyright')
@endsection