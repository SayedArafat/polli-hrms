@extends('layouts.master')
@section('title', 'Meeting Details')
@section('content')
	<div class="page-content">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="panel panel-default no-bd">
					<div class="panel-header bg-dark">
						<h3 class="panel-title"><strong>Meeting</strong> Details</h3>
					</div>
					<div class="panel-body bg-white">
						<div class="">
							<div class="form-group">
                                    <label class="control-label">Meeting Subject</label>
                                    <div class="append-icon">
                                        {{$meeting->msub}}
                                	</div>
                             </div>
                             <div class="required form-group">
                                    <label class="control-label">Meeting Date</label>
                                    <div class="append-icon">
                                        {{$meeting->date}}
                                	</div>
                             </div>
                             <div class="form-group">
                                    <label class="control-label">Description</label>
                                    <div class="append-icon">
                                        {{$meeting->descrip}}
                                	</div>
                             </div>

                            <div class="required form-group">
                                <label class="control-label">Meeting Venue</label>
                                <div class="append-icon">
                                    {{$meeting->venue}}
                                    
                                </div>
                            </div>
                            
						</form>
						</div>

                       

					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>

@endsection