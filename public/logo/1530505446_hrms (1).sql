-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2018 at 07:28 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hrms`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_time` time DEFAULT NULL,
  `out_time` time DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `emp_id`, `in_time`, `out_time`, `date`, `created_at`, `updated_at`) VALUES
(1, '9', '09:42:43', '18:12:34', '2018-04-10 18:00:00', NULL, NULL),
(2, '9', '10:36:35', '10:36:35', '2018-04-14 18:00:00', NULL, NULL),
(3, '9', '08:58:00', '23:59:53', '2018-04-15 18:00:00', NULL, NULL),
(4, '9', '09:11:10', '09:11:10', '2018-04-16 18:00:00', NULL, NULL),
(5, '9', '09:08:39', '09:08:39', '2018-04-17 18:00:00', NULL, NULL),
(6, '9', '08:56:05', '17:45:10', '2018-04-18 18:00:00', NULL, NULL),
(7, '9', '08:53:06', '08:53:06', '2018-04-21 18:00:00', NULL, NULL),
(8, '9', '10:06:12', '10:06:12', '2018-04-23 18:00:00', NULL, NULL),
(9, '9', '08:57:20', '13:25:59', '2018-04-24 18:00:00', NULL, NULL),
(10, '9', '10:04:25', '18:48:16', '2018-04-25 18:00:00', NULL, NULL),
(11, '9', '09:12:37', '09:12:37', '2018-04-27 18:00:00', NULL, NULL),
(12, '9', '10:12:45', '10:12:45', '2018-04-28 18:00:00', NULL, NULL),
(13, '9', '09:11:32', '09:11:32', '2018-04-29 18:00:00', NULL, NULL),
(14, '9', '09:11:42', '12:50:35', '2018-05-02 18:00:00', NULL, NULL),
(15, '9', '11:31:21', '17:15:46', '2018-05-04 18:00:00', NULL, NULL),
(16, '9', '15:33:16', '14:39:46', '2018-05-16 09:18:24', NULL, NULL),
(17, '9', '07:51:45', '07:51:45', '2018-05-06 18:00:00', NULL, NULL),
(18, '9', '02:09:29', '02:09:29', '2018-05-07 18:00:00', NULL, NULL),
(19, '9', '10:07:41', '10:07:41', '2018-05-08 18:00:00', NULL, NULL),
(20, '9', '09:19:13', '09:19:13', '2018-05-09 18:00:00', NULL, NULL),
(21, '9', '16:26:23', '16:26:23', '2018-05-12 18:00:00', NULL, NULL),
(22, '9', '09:50:26', '09:50:26', '2018-05-13 18:00:00', NULL, NULL),
(23, '4', '16:52:53', '18:53:23', '2018-04-03 18:00:00', NULL, NULL),
(24, '4', '16:04:26', '16:37:51', '2018-04-04 18:00:00', NULL, NULL),
(25, '4', '08:59:05', '17:43:59', '2018-04-08 18:00:00', NULL, NULL),
(26, '4', '08:57:11', '18:33:12', '2018-04-09 18:00:00', NULL, NULL),
(27, '4', '08:55:42', '18:04:59', '2018-04-10 18:00:00', NULL, NULL),
(28, '4', '09:12:45', '18:06:52', '2018-04-11 18:00:00', NULL, NULL),
(29, '4', '08:56:09', '18:20:32', '2018-04-14 18:00:00', NULL, NULL),
(30, '4', '08:52:11', '17:59:24', '2018-04-15 18:00:00', NULL, NULL),
(31, '4', '09:06:54', '18:02:05', '2018-04-16 18:00:00', NULL, NULL),
(32, '4', '09:06:23', '18:53:36', '2018-04-17 18:00:00', NULL, NULL),
(33, '4', '08:59:05', '17:44:51', '2018-04-18 18:00:00', NULL, NULL),
(34, '4', '08:57:14', '18:16:54', '2018-04-21 18:00:00', NULL, NULL),
(35, '4', '08:49:16', '18:07:45', '2018-04-22 18:00:00', NULL, NULL),
(36, '4', '09:02:11', '18:13:30', '2018-04-23 18:00:00', NULL, NULL),
(37, '4', '08:49:54', '17:59:28', '2018-04-24 18:00:00', NULL, NULL),
(38, '4', '08:59:16', '17:41:38', '2018-04-25 18:00:00', NULL, NULL),
(39, '4', '09:03:42', '17:48:17', '2018-04-27 18:00:00', NULL, NULL),
(40, '4', '09:32:04', '18:07:58', '2018-04-28 18:00:00', NULL, NULL),
(41, '4', '09:03:18', '17:54:00', '2018-04-29 18:00:00', NULL, NULL),
(42, '4', '09:01:51', '16:40:09', '2018-05-02 18:00:00', NULL, NULL),
(43, '4', '09:07:03', '18:07:28', '2018-05-04 18:00:00', NULL, NULL),
(44, '4', '08:58:43', '18:14:30', '2018-05-05 18:00:00', NULL, NULL),
(45, '4', '08:56:17', '18:04:51', '2018-05-06 18:00:00', NULL, NULL),
(46, '4', '09:18:18', '18:05:36', '2018-05-07 18:00:00', NULL, NULL),
(47, '4', '09:26:03', '18:40:36', '2018-05-08 18:00:00', NULL, NULL),
(48, '4', '09:00:26', '16:02:33', '2018-05-09 18:00:00', NULL, NULL),
(49, '4', '09:05:19', '18:24:50', '2018-05-12 18:00:00', NULL, NULL),
(50, '4', '09:02:20', '09:02:20', '2018-05-13 18:00:00', NULL, NULL),
(51, '2', '16:15:19', '16:27:03', '2018-04-09 18:00:00', NULL, NULL),
(52, '2', '09:37:52', '17:59:09', '2018-04-10 18:00:00', NULL, NULL),
(53, '2', '09:41:00', '09:41:00', '2018-04-11 18:00:00', NULL, NULL),
(54, '2', '09:29:17', '09:29:17', '2018-04-14 18:00:00', NULL, NULL),
(55, '2', '19:46:36', '19:47:01', '2018-04-15 18:00:00', NULL, NULL),
(56, '2', '09:32:52', '09:32:55', '2018-04-16 18:00:00', NULL, NULL),
(57, '2', '09:51:33', '09:51:33', '2018-04-17 18:00:00', NULL, NULL),
(58, '2', '09:50:46', '09:50:46', '2018-04-18 18:00:00', NULL, NULL),
(59, '2', '09:35:25', '18:05:03', '2018-04-22 18:00:00', NULL, NULL),
(60, '2', '09:24:57', '09:24:57', '2018-04-23 18:00:00', NULL, NULL),
(61, '2', '09:30:47', '09:30:47', '2018-04-25 18:00:00', NULL, NULL),
(62, '2', '10:22:26', '10:22:26', '2018-04-27 18:00:00', NULL, NULL),
(63, '2', '09:28:33', '09:28:33', '2018-04-29 18:00:00', NULL, NULL),
(64, '2', '09:36:30', '09:36:30', '2018-05-02 18:00:00', NULL, NULL),
(65, '2', '09:26:50', '13:43:55', '2018-05-16 09:18:13', NULL, NULL),
(66, '2', '10:03:46', '10:03:46', '2018-05-07 18:00:00', NULL, NULL),
(67, '2', '09:35:41', '19:03:06', '2018-05-08 18:00:00', NULL, NULL),
(68, '2', '09:28:18', '09:28:18', '2018-05-09 18:00:00', NULL, NULL),
(69, '2', '09:30:31', '18:02:48', '2018-05-12 18:00:00', NULL, NULL),
(70, '2', '09:46:54', '09:46:54', '2018-05-13 18:00:00', NULL, NULL),
(71, '8', '09:42:48', '09:42:56', '2018-04-10 18:00:00', NULL, NULL),
(72, '8', '10:26:16', '10:26:16', '2018-04-14 18:00:00', NULL, NULL),
(73, '8', '08:57:42', '08:57:42', '2018-04-15 18:00:00', NULL, NULL),
(74, '8', '10:17:05', '10:17:05', '2018-04-16 18:00:00', NULL, NULL),
(75, '8', '10:19:35', '10:19:35', '2018-04-17 18:00:00', NULL, NULL),
(76, '8', '09:31:02', '17:45:02', '2018-04-18 18:00:00', NULL, NULL),
(77, '8', '09:26:50', '18:09:34', '2018-04-21 18:00:00', NULL, NULL),
(78, '8', '09:51:08', '18:01:14', '2018-04-22 18:00:00', NULL, NULL),
(79, '8', '09:41:56', '18:03:55', '2018-04-23 18:00:00', NULL, NULL),
(80, '8', '08:56:00', '13:24:31', '2018-04-24 18:00:00', NULL, NULL),
(81, '8', '10:01:29', '19:01:59', '2018-04-25 18:00:00', NULL, NULL),
(82, '8', '09:13:04', '17:58:11', '2018-04-27 18:00:00', NULL, NULL),
(83, '8', '10:03:30', '10:03:30', '2018-04-28 18:00:00', NULL, NULL),
(84, '8', '09:12:00', '14:35:13', '2018-04-29 18:00:00', NULL, NULL),
(85, '8', '09:11:55', '12:50:28', '2018-05-02 18:00:00', NULL, NULL),
(86, '8', '15:42:05', '23:39:49', '2018-05-05 18:00:00', NULL, NULL),
(87, '8', '08:56:33', '17:50:40', '2018-05-06 18:00:00', NULL, NULL),
(88, '8', '18:00:56', '18:00:59', '2018-05-07 18:00:00', NULL, NULL),
(89, '8', '10:37:51', '15:59:10', '2018-05-08 18:00:00', NULL, NULL),
(90, '8', '09:51:22', '17:54:57', '2018-05-09 18:00:00', NULL, NULL),
(91, '8', '10:32:05', '18:05:04', '2018-06-16 09:17:13', NULL, NULL),
(92, '7', '18:56:47', '18:56:47', '2018-04-08 18:00:00', NULL, NULL),
(93, '7', '09:07:26', '18:12:40', '2018-04-09 18:00:00', NULL, NULL),
(94, '7', '09:02:14', '18:12:05', '2018-04-10 18:00:00', NULL, NULL),
(95, '7', '09:07:47', '17:59:35', '2018-04-11 18:00:00', NULL, NULL),
(96, '7', '08:54:33', '08:54:33', '2018-04-14 18:00:00', NULL, NULL),
(97, '7', '09:02:31', '17:58:30', '2018-04-15 18:00:00', NULL, NULL),
(98, '7', '09:40:26', '18:01:00', '2018-04-16 18:00:00', NULL, NULL),
(99, '7', '08:56:57', '18:09:23', '2018-04-17 18:00:00', NULL, NULL),
(100, '7', '09:00:03', '17:58:08', '2018-04-18 18:00:00', NULL, NULL),
(101, '7', '09:06:41', '18:09:29', '2018-04-21 18:00:00', NULL, NULL),
(102, '7', '09:11:21', '17:42:26', '2018-04-22 18:00:00', NULL, NULL),
(103, '7', '09:09:25', '18:12:04', '2018-04-23 18:00:00', NULL, NULL),
(104, '7', '09:14:46', '17:58:47', '2018-04-24 18:00:00', NULL, NULL),
(105, '7', '09:07:58', '18:02:16', '2018-04-25 18:00:00', NULL, NULL),
(106, '7', '09:39:26', '17:51:48', '2018-04-27 18:00:00', NULL, NULL),
(107, '7', '09:31:58', '18:06:20', '2018-04-28 18:00:00', NULL, NULL),
(108, '7', '08:57:54', '19:27:03', '2018-04-29 18:00:00', NULL, NULL),
(109, '7', '10:27:31', '18:08:20', '2018-05-04 18:00:00', NULL, NULL),
(110, '7', '09:01:17', '18:13:40', '2018-05-05 18:00:00', NULL, NULL),
(111, '7', '09:17:01', '18:01:00', '2018-05-06 18:00:00', NULL, NULL),
(112, '7', '09:18:58', '17:59:58', '2018-05-07 18:00:00', NULL, NULL),
(113, '7', '09:12:22', '18:39:41', '2018-05-08 18:00:00', NULL, NULL),
(114, '7', '09:27:27', '14:05:44', '2018-05-09 18:00:00', NULL, NULL),
(115, '7', '09:04:45', '18:04:18', '2018-05-12 18:00:00', NULL, NULL),
(116, '7', '09:17:49', '09:17:49', '2018-05-13 18:00:00', NULL, NULL),
(117, '5', '18:56:40', '18:56:40', '2018-04-08 18:00:00', NULL, NULL),
(118, '5', '09:05:41', '17:54:39', '2018-04-09 18:00:00', NULL, NULL),
(119, '5', '09:07:29', '18:07:22', '2018-04-10 18:00:00', NULL, NULL),
(120, '5', '09:07:33', '09:07:33', '2018-04-11 18:00:00', NULL, NULL),
(121, '5', '09:07:24', '09:07:24', '2018-04-14 18:00:00', NULL, NULL),
(122, '5', '09:10:11', '17:49:51', '2018-04-15 18:00:00', NULL, NULL),
(123, '5', '09:23:05', '17:59:50', '2018-04-16 18:00:00', NULL, NULL),
(124, '5', '08:57:02', '18:11:00', '2018-04-17 18:00:00', NULL, NULL),
(125, '5', '09:19:03', '18:00:01', '2018-04-18 18:00:00', NULL, NULL),
(126, '5', '09:07:04', '18:10:27', '2018-04-21 18:00:00', NULL, NULL),
(127, '5', '08:53:14', '17:59:09', '2018-04-22 18:00:00', NULL, NULL),
(128, '5', '09:04:49', '17:54:29', '2018-04-23 18:00:00', NULL, NULL),
(129, '5', '09:05:45', '17:53:18', '2018-04-24 18:00:00', NULL, NULL),
(130, '5', '08:59:41', '08:59:41', '2018-04-25 18:00:00', NULL, NULL),
(131, '5', '09:08:47', '17:51:41', '2018-04-27 18:00:00', NULL, NULL),
(132, '5', '09:53:04', '18:12:36', '2018-04-28 18:00:00', NULL, NULL),
(133, '5', '09:02:00', '09:02:00', '2018-04-29 18:00:00', NULL, NULL),
(134, '5', '08:51:39', '18:00:42', '2018-05-04 18:00:00', NULL, NULL),
(135, '5', '09:03:20', '18:14:43', '2018-05-05 18:00:00', NULL, NULL),
(136, '5', '09:03:50', '17:56:48', '2018-05-06 18:00:00', NULL, NULL),
(137, '5', '08:58:45', '17:59:53', '2018-05-07 18:00:00', NULL, NULL),
(138, '5', '08:58:19', '18:40:18', '2018-05-08 18:00:00', NULL, NULL),
(139, '5', '09:04:52', '17:41:47', '2018-05-09 18:00:00', NULL, NULL),
(140, '5', '09:00:20', '17:56:01', '2018-05-12 18:00:00', NULL, NULL),
(141, '6', '18:56:45', '18:56:45', '2018-04-08 18:00:00', NULL, NULL),
(142, '6', '09:31:43', '17:54:20', '2018-04-09 18:00:00', NULL, NULL),
(143, '6', '09:07:03', '18:04:44', '2018-04-10 18:00:00', NULL, NULL),
(144, '6', '09:24:12', '18:01:15', '2018-04-11 18:00:00', NULL, NULL),
(145, '6', '09:23:02', '18:15:33', '2018-04-14 18:00:00', NULL, NULL),
(146, '6', '09:21:07', '09:21:07', '2018-04-15 18:00:00', NULL, NULL),
(147, '6', '09:13:53', '18:00:52', '2018-04-16 18:00:00', NULL, NULL),
(148, '6', '09:00:11', '18:06:58', '2018-04-17 18:00:00', NULL, NULL),
(149, '6', '09:29:50', '17:55:14', '2018-04-18 18:00:00', NULL, NULL),
(150, '6', '09:28:03', '18:10:24', '2018-04-21 18:00:00', NULL, NULL),
(151, '6', '09:20:14', '17:59:03', '2018-04-22 18:00:00', NULL, NULL),
(152, '6', '09:22:42', '17:56:44', '2018-04-23 18:00:00', NULL, NULL),
(153, '6', '09:24:26', '17:57:22', '2018-04-24 18:00:00', NULL, NULL),
(154, '6', '09:43:35', '17:49:20', '2018-04-25 18:00:00', NULL, NULL),
(155, '6', '09:27:46', '18:06:06', '2018-05-02 18:00:00', NULL, NULL),
(156, '6', '09:03:23', '18:01:16', '2018-05-04 18:00:00', NULL, NULL),
(157, '6', '09:10:22', '18:09:11', '2018-05-05 18:00:00', NULL, NULL),
(158, '6', '09:16:02', '17:57:53', '2018-05-06 18:00:00', NULL, NULL),
(159, '6', '09:29:49', '17:57:45', '2018-05-07 18:00:00', NULL, NULL),
(160, '6', '09:19:56', '18:39:36', '2018-05-08 18:00:00', NULL, NULL),
(161, '6', '09:12:36', '17:41:09', '2018-05-09 18:00:00', NULL, NULL),
(162, '6', '09:13:30', '17:57:33', '2018-05-12 18:00:00', NULL, NULL),
(163, '6', '09:36:58', '09:36:58', '2018-05-13 18:00:00', NULL, NULL),
(164, '1', '18:56:42', '18:56:42', '2018-04-08 18:00:00', NULL, NULL),
(165, '1', '16:16:43', '17:10:25', '2018-04-09 18:00:00', NULL, NULL),
(166, '1', '15:51:42', '16:36:07', '2018-04-10 18:00:00', NULL, NULL),
(167, '1', '16:02:05', '16:29:25', '2018-04-11 18:00:00', NULL, NULL),
(168, '1', '10:25:00', '10:25:00', '2018-04-14 18:00:00', NULL, NULL),
(169, '1', '15:57:25', '17:47:21', '2018-04-15 18:00:00', NULL, NULL),
(170, '1', '18:18:09', '19:10:23', '2018-04-16 18:00:00', NULL, NULL),
(171, '1', '10:08:20', '11:56:30', '2018-04-21 18:00:00', NULL, NULL),
(172, '1', '14:43:59', '16:25:21', '2018-04-22 18:00:00', NULL, NULL),
(173, '1', '09:06:46', '09:06:46', '2018-04-23 18:00:00', NULL, NULL),
(174, '1', '17:00:55', '17:12:41', '2018-04-24 18:00:00', NULL, NULL),
(175, '1', '18:38:46', '18:38:49', '2018-04-25 18:00:00', NULL, NULL),
(176, '1', '16:37:15', '17:57:50', '2018-04-27 18:00:00', NULL, NULL),
(177, '1', '09:24:59', '14:26:56', '2018-04-29 18:00:00', NULL, NULL),
(178, '1', '12:21:06', '14:08:47', '2018-05-04 18:00:00', NULL, NULL),
(179, '1', '15:41:49', '17:27:26', '2018-05-05 18:00:00', NULL, NULL),
(180, '1', '10:16:16', '18:14:33', '2018-05-12 18:00:00', NULL, NULL),
(181, '3', '18:56:49', '18:56:49', '2018-04-08 18:00:00', NULL, NULL),
(182, '3', '08:59:04', '21:28:58', '2018-04-09 18:00:00', NULL, NULL),
(183, '3', '08:08:55', '20:13:11', '2018-04-10 18:00:00', NULL, NULL),
(184, '3', '08:05:22', '22:20:41', '2018-04-11 18:00:00', NULL, NULL),
(185, '3', '06:21:38', '06:21:38', '2018-04-14 18:00:00', NULL, NULL),
(186, '3', '07:27:13', '23:56:40', '2018-04-15 18:00:00', NULL, NULL),
(187, '3', '08:57:09', '08:57:09', '2018-04-17 18:00:00', NULL, NULL),
(188, '3', '00:08:38', '22:23:08', '2018-04-18 18:00:00', NULL, NULL),
(189, '3', '07:05:42', '22:31:32', '2018-04-21 18:00:00', NULL, NULL),
(190, '3', '07:01:05', '22:02:25', '2018-04-22 18:00:00', NULL, NULL),
(191, '3', '07:03:52', '22:33:19', '2018-04-23 18:00:00', NULL, NULL),
(192, '3', '06:54:14', '22:36:14', '2018-04-24 18:00:00', NULL, NULL),
(193, '3', '07:08:42', '22:07:28', '2018-04-25 18:00:00', NULL, NULL),
(194, '3', '07:05:29', '09:22:08', '2018-06-16 09:19:17', NULL, NULL),
(195, '3', '07:02:46', '23:00:46', '2018-04-28 18:00:00', NULL, NULL),
(196, '3', '07:01:17', '21:22:13', '2018-04-29 18:00:00', NULL, NULL),
(197, '3', '07:10:01', '05:50:05', '2018-05-16 09:18:29', NULL, NULL),
(198, '3', '07:12:07', '22:00:03', '2018-05-04 18:00:00', NULL, NULL),
(199, '3', '07:01:07', '23:39:16', '2018-05-05 18:00:00', NULL, NULL),
(200, '3', '07:08:02', '10:12:43', '2018-05-16 09:18:05', NULL, NULL),
(201, '3', '07:07:09', '07:07:09', '2018-05-07 18:00:00', NULL, NULL),
(202, '3', '07:03:44', '10:56:32', '2018-05-16 09:17:52', NULL, NULL),
(203, '3', '07:01:25', '13:58:59', '2018-05-16 09:17:40', NULL, NULL),
(204, '3', '07:05:09', '02:04:48', '2018-05-16 09:17:32', NULL, NULL),
(205, '3', '07:14:16', '07:14:16', '2018-05-13 18:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attendance_bonus`
--

CREATE TABLE `attendance_bonus` (
  `id` int(10) UNSIGNED NOT NULL,
  `bonus_id` int(11) NOT NULL,
  `bonus_amount` decimal(18,2) DEFAULT NULL,
  `month` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attendance_file`
--

CREATE TABLE `attendance_file` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendance_file`
--

INSERT INTO `attendance_file` (`id`, `date`, `attachment`, `comment`, `created_at`, `updated_at`) VALUES
(46, '15/04/2018', '1523792902_History Report.xls', 'Sheet', '2018-04-15 05:48:22', '2018-04-15 05:48:22'),
(47, '16/04/2018', '1523864116_INNN.xls', 'rewter', '2018-04-16 01:35:16', '2018-04-16 01:35:16'),
(48, '22/04/2018', '1524378259_Attendance Last Report.xls', 'Latest File', '2018-04-22 00:24:19', '2018-04-22 00:24:19'),
(49, '29/04/2018', '1524978972_29-04-2018.xls', '29-04-2018', '2018-04-28 23:16:12', '2018-04-28 23:16:12'),
(50, '29/04/2018', '1524985452_29-04-2018.xls', 'today data', '2018-04-29 01:04:12', '2018-04-29 01:04:12'),
(51, '29/04/2018', '1524990800_29-04-2018.xls', '29-04-2018', '2018-04-29 02:33:20', '2018-04-29 02:33:20'),
(52, '30/04/2018', '1525067469_29-04-2018.xls', 'tytrytry', '2018-04-29 23:51:09', '2018-04-29 23:51:09'),
(53, '09/05/2018', '1525865776_29-04-2018.xls', '2018/05/09', '2018-05-09 05:36:16', '2018-05-09 05:36:16'),
(54, '14/05/2018', '1526292187_History Report.xls', 'Latest File', '2018-05-14 04:03:07', '2018-05-14 04:03:07'),
(55, '14/05/2018', '1526293088_History Report.xls', 'latest data', '2018-05-14 04:18:08', '2018-05-14 04:18:08');

-- --------------------------------------------------------

--
-- Table structure for table `attendance_machine_data`
--

CREATE TABLE `attendance_machine_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `device_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `door` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` timestamp NULL DEFAULT NULL,
  `event_explanation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attendance_file_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attendance_setup`
--

CREATE TABLE `attendance_setup` (
  `id` int(10) UNSIGNED NOT NULL,
  `entry_time` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exit_time` time NOT NULL,
  `max_entry_time` time NOT NULL,
  `over_time_exit_time` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendance_setup`
--

INSERT INTO `attendance_setup` (`id`, `entry_time`, `exit_time`, `max_entry_time`, `over_time_exit_time`, `created_at`, `updated_at`) VALUES
(1, '', '19:00:00', '20:00:00', '', '2018-04-27 18:00:00', '2018-04-27 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `departmentName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departmentDescription` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `departmentName`, `departmentDescription`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 'Production', 'Review the full course description and key learning outcomes and create an account.', 'Admin', 'Admin', '2018-04-10 21:51:50', '2018-04-10 21:52:04'),
(2, 'Research and Development', 'Making the decision to study can be a big step, which is why you\'ll want a trusted University.', 'Admin', 'Admin', '2018-04-10 21:52:35', '2018-04-10 21:52:35'),
(3, 'IT', 'Information Technology', 'Admin', 'Admin', '2018-04-10 22:11:43', '2018-04-10 22:11:43'),
(4, 'IT security', 'Information Technology', 'Admin', 'Admin', '2018-04-10 22:11:43', '2018-04-10 22:11:43');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `designation` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `designation`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Jr.  Software Engineer', NULL, '2018-04-10 21:54:06', '2018-04-10 21:54:06'),
(2, 'It officer', NULL, '2018-04-10 21:54:06', '2018-04-10 21:54:06'),
(3, 'General Officer', NULL, '2018-04-10 21:54:06', '2018-04-10 21:54:06'),
(4, 'minister', NULL, '2018-04-10 21:54:06', '2018-04-10 21:54:06'),
(5, 'it specialist', NULL, '2018-04-10 21:54:06', '2018-04-10 21:54:06'),
(6, 'product manager', NULL, '2018-04-10 21:54:06', '2018-04-10 21:54:06');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `empFirstName` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empLastName` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employeeId` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empDesignationId` int(11) NOT NULL,
  `empGenderId` tinyint(4) NOT NULL,
  `empRole` tinyint(4) NOT NULL,
  `empPassword` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empAccStatus` tinyint(4) NOT NULL DEFAULT '1',
  `empEmail` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empPhone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empParAddress` text COLLATE utf8mb4_unicode_ci,
  `empCurrentAddress` text COLLATE utf8mb4_unicode_ci,
  `empFatherName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empMotherName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empDepartmentId` tinyint(4) NOT NULL,
  `empJoiningDate` date NOT NULL,
  `empDOB` timestamp NULL DEFAULT NULL,
  `empPhoto` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empMaritalStatusId` tinyint(4) NOT NULL,
  `empEcontactName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyPhone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyAddress` text COLLATE utf8mb4_unicode_ci,
  `empReligion` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empNid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empNationalityId` tinyint(4) DEFAULT NULL,
  `empGlobalId` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empSection` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empBloodGroup` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empCardNumber` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `line_id` int(11) DEFAULT NULL,
  `floor_id` int(11) DEFAULT NULL,
  `work_group` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary_type` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_account` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_info` text COLLATE utf8mb4_unicode_ci,
  `payment_mode` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_discontinuation` date DEFAULT NULL,
  `reason_of_discontinuation` text COLLATE utf8mb4_unicode_ci,
  `emergency_contact_relation` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skill_level` varchar(90) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_type` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'None',
  `reference_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `probation_period` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '6'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `empFirstName`, `empLastName`, `employeeId`, `empDesignationId`, `empGenderId`, `empRole`, `empPassword`, `empAccStatus`, `empEmail`, `empPhone`, `empParAddress`, `empCurrentAddress`, `empFatherName`, `empMotherName`, `empDepartmentId`, `empJoiningDate`, `empDOB`, `empPhoto`, `empMaritalStatusId`, `empEcontactName`, `emergencyPhone`, `emergencyAddress`, `empReligion`, `empNid`, `empNationalityId`, `empGlobalId`, `empSection`, `empBloodGroup`, `empCardNumber`, `unit_id`, `line_id`, `floor_id`, `work_group`, `salary_type`, `bank_account`, `bank_info`, `payment_mode`, `date_of_discontinuation`, `reason_of_discontinuation`, `emergency_contact_relation`, `create_by`, `modified_by`, `skill_level`, `reference_type`, `reference_description`, `created_at`, `updated_at`, `probation_period`) VALUES
(1, 'Md. Asaduzzaman', 'Khan', '546456', 3, 1, 6, '$2y$10$qretXtaTdIs8nTBswC8LQurq/BLsun/IsobcDWu5nx9sv./SP89bm', 1, NULL, '43534534', NULL, NULL, NULL, NULL, 1, '2018-04-08', '1990-04-05 18:00:00', NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Swing', NULL, '0005894903', NULL, NULL, 4, NULL, NULL, NULL, NULL, 'Cash', NULL, NULL, NULL, 'Admin', 'Admin', NULL, 'None', NULL, '2018-04-06 20:28:10', '2018-06-03 02:01:46', '6'),
(2, 'Razia Sultana', 'Rimi', '546456', 1, 2, 2, '$2y$10$x4CVpH3U/upvEYm89mIi2O2RU7.2BLvLvEOfkUiy6VlVoN8L4E.US', 1, 'email@email.com', '01826365272', NULL, NULL, NULL, NULL, 1, '2018-04-08', '2018-04-17 18:00:00', '1524134190_2_images.jpg', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '0005691558', NULL, 1, 1, NULL, NULL, NULL, NULL, 'Cash', NULL, NULL, NULL, 'Admin', 'Admin', NULL, 'None', NULL, '2018-04-06 20:31:31', '2018-06-03 02:14:23', '6'),
(3, 'Mr.', 'Mizan', '456456', 1, 1, 4, '$2y$10$8/OFjGQiG9WevwNidNGIreiZ32guHlIKGtMot5XTGQczN.Aa7Nt6W', 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-04-09', '1985-04-06 18:00:00', NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Swing', NULL, '0005899176', NULL, 1, 1, NULL, NULL, NULL, NULL, 'Cash', NULL, NULL, NULL, 'Admin', 'Admin', NULL, 'None', NULL, '2018-04-06 20:32:16', '2018-06-03 02:14:06', '6'),
(4, 'Samsujjaman', 'Bappy', '10003', 3, 1, 1, '$2y$10$vSJD7gBx4.7xbPfUvGN5O.dWUd2rcagk9FhrhGQFLIeaHM7GbkbXO', 1, 'arafat@feits.co', '017762543433', 'Bangladesh, World.', 'Dhaka. Bangladesh', 'Fathers name', 'Mothers Name', 3, '2018-03-01', '1990-04-08 18:00:00', '1523420140__aa-Cover-51j8d2ougs3fkveujhrrv78gn4-20161117145047.Medi.jpeg', 2, 'Abdullah', '01774379178', 'Emergency Contact', 'Muslim', '1224365627782212123', 1, 'G-14274272', 'Swing', 'AB-', '0005677224', 3, 1, 1, 'Worker', 'Monthly', NULL, NULL, 'Rocket', NULL, NULL, 'Grandfather', 'Admin', 'Admin', '7', 'Internal', 'Arafat', '2018-04-09 22:15:40', '2018-06-03 02:12:40', '6'),
(5, 'Sayed Yeamin', 'Arafat', '1009', 1, 1, 1, '$2y$10$HRpmTuyA80p5p4QZYrc5q..milwylcAiVHaJDA2FeKNzo.du6eFEi', 1, 'zim847@gmail.com', '01774379178', 'Nikunja-2, Bangladesh.', 'Nikunja-2, Bangladesh.', 'Fathers Name', 'Mothers Name', 3, '2018-05-01', '1993-08-29 18:00:00', NULL, 2, 'Hasibul', NULL, NULL, NULL, NULL, 1, NULL, 'Swing', NULL, '0005883804', 3, 1, 1, 'Staff', NULL, NULL, NULL, 'Rocket', NULL, NULL, NULL, 'Admin', 'Admin', NULL, 'None', NULL, '2018-04-17 03:55:25', '2018-06-03 02:12:23', '6'),
(6, 'Zubaer', 'Ahmad', '123129', 1, 1, 6, '$2y$10$BGto59e8Jq2SlQlDdmoycecESrmb24.YrWmh0Cv426U4QvCRpuAsG', 1, NULL, NULL, NULL, NULL, NULL, NULL, 3, '2018-04-25', '2018-04-17 18:00:00', NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Swing', NULL, '0005889792', NULL, 1, 1, NULL, NULL, NULL, NULL, 'Cash', NULL, NULL, NULL, 'Admin', 'Admin', NULL, 'None', NULL, '2018-04-17 04:03:34', '2018-06-03 02:13:39', '6'),
(7, 'Partho', 'kar', '3123123', 1, 1, 6, '$2y$10$bzh3tHWnjGufJxW4DaYFBudJw161b4nVZOv4vSo2t7vwLfHRSu3my', 1, 'partho@email.com', '5347612880', NULL, NULL, NULL, NULL, 3, '2018-03-01', '2018-04-20 18:00:00', NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Operator', NULL, '0005876499', 1, NULL, 1, NULL, NULL, NULL, NULL, 'Cash', NULL, NULL, NULL, 'Admin', 'Admin', NULL, 'None', NULL, '2018-04-18 04:50:36', '2018-06-03 02:02:49', '6'),
(8, 'Hasan Ul', 'Ferdous', '12312', 5, 1, 4, '$2y$10$jhhyw7a6UM/wvEwF08KXvuX315gaBW4U0q923DbJC3dLaSbEjE8T6', 1, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-06-01', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Swing', NULL, '0005699519', NULL, 1, 1, NULL, NULL, NULL, NULL, 'Cash', NULL, NULL, NULL, 'Admin', 'Admin', NULL, 'None', NULL, '2018-04-18 04:52:34', '2018-06-03 02:13:12', '6'),
(9, 'Md. Bakhtiar', 'Khan', '1231231', 1, 1, 2, '$2y$10$diDrV9AZO4qLxg8Ea56FvOykf5gWgLjhz8uOqRPP2U19qpQh4weIu', 1, NULL, NULL, NULL, NULL, NULL, NULL, 3, '2017-04-01', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Operator', NULL, '0001758890', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Cash', NULL, NULL, NULL, 'Admin', 'Admin', NULL, 'None', NULL, '2018-04-18 04:54:07', '2018-06-03 02:03:21', '6');

-- --------------------------------------------------------

--
-- Table structure for table `employees_bonus`
--

CREATE TABLE `employees_bonus` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `emp_bonus` decimal(18,2) DEFAULT NULL,
  `date` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees_bonus`
--

INSERT INTO `employees_bonus` (`id`, `emp_id`, `emp_bonus`, `date`, `created_at`, `updated_at`) VALUES
(2, 9, '500.00', '05-2018', '2018-05-17 04:35:26', '2018-05-17 04:35:26');

-- --------------------------------------------------------

--
-- Table structure for table `employee_attachments`
--

CREATE TABLE `employee_attachments` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `empAttachmentTitle` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empAttachmentDescription` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `empAttachment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_attachments`
--

INSERT INTO `employee_attachments` (`id`, `emp_id`, `empAttachmentTitle`, `empAttachmentDescription`, `empAttachment`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 4, 'AIUB Programming Champion', 'Participated in the AIUB programming contest.', '1523422110_4_users.query.txt', 'Admin', 'Admin', '2018-04-10 22:48:30', '2018-04-10 22:48:30'),
(2, 2, 'Gaming Contest', 'Was first in AIUB gaming contest.', '1523426401_2_Note.txt', 'Admin', 'Admin', '2018-04-11 00:00:01', '2018-04-11 00:00:01');

-- --------------------------------------------------------

--
-- Table structure for table `employee_deduction`
--

CREATE TABLE `employee_deduction` (
  `id` int(10) UNSIGNED NOT NULL,
  `employees_id` int(11) NOT NULL,
  `absent` int(11) NOT NULL,
  `overtime` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_education_infos`
--

CREATE TABLE `employee_education_infos` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `empExamTitle` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empInstitution` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empResult` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empScale` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empPassYear` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empCertificate` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_education_infos`
--

INSERT INTO `employee_education_infos` (`id`, `emp_id`, `empExamTitle`, `empInstitution`, `empResult`, `empScale`, `empPassYear`, `empCertificate`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 4, 'Secondary School Certificate', 'AUSSNSCG', '4.00', '5.00', '2018', '1523422032_4_E_CERT_dbmsb_hrm.zip', 'Admin', 'Admin', '2018-04-10 22:47:12', '2018-04-10 22:47:12'),
(2, 2, 'SSC', 'UTTARA', '5.00', '5.00', '2010', '1523526464_2_E_CERT_slider3bg.jpg', 'Admin', 'Admin', '2018-04-12 03:47:44', '2018-04-12 03:47:44');

-- --------------------------------------------------------

--
-- Table structure for table `employee_overtime`
--

CREATE TABLE `employee_overtime` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_ids` int(11) DEFAULT NULL,
  `overtime_hour` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `month` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_working_histories`
--

CREATE TABLE `employee_working_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `empCompanyName` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empJobTile` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empJoiningDate` timestamp NULL DEFAULT NULL,
  `empLeaveDate` timestamp NULL DEFAULT NULL,
  `empWHDescription` text COLLATE utf8mb4_unicode_ci,
  `empWHattachment` text COLLATE utf8mb4_unicode_ci,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_working_histories`
--

INSERT INTO `employee_working_histories` (`id`, `emp_id`, `empCompanyName`, `empJobTile`, `empJoiningDate`, `empLeaveDate`, `empWHDescription`, `empWHattachment`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 3, 'rtyry', 'tryrty', '2018-04-09 12:00:00', '2018-04-10 12:00:00', 'tryrtytryrty', NULL, 'Admin', 'Admin', '2018-04-09 22:14:49', '2018-04-09 22:14:49'),
(2, 4, 'Storrea LTD', 'Web Developer', '2017-07-31 18:00:00', '2018-02-28 18:00:00', 'This was a great opportunity.', '1523422069_4_project note .txt', 'Admin', 'Admin', '2018-04-10 22:47:49', '2018-04-10 23:13:34');

-- --------------------------------------------------------

--
-- Table structure for table `floors`
--

CREATE TABLE `floors` (
  `id` int(10) UNSIGNED NOT NULL,
  `floor` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `if_needed` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `floors`
--

INSERT INTO `floors` (`id`, `floor`, `if_needed`, `created_at`, `updated_at`) VALUES
(1, 'floor1', NULL, NULL, NULL),
(2, 'floor2', NULL, NULL, NULL),
(3, 'floor3', NULL, NULL, NULL),
(4, 'floor4', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leave_of_employee`
--

CREATE TABLE `leave_of_employee` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `leave_type_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `month` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marital_statuses`
--

CREATE TABLE `marital_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` tinyint(4) DEFAULT NULL,
  `modified_by` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marital_statuses`
--

INSERT INTO `marital_statuses` (`id`, `name`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 'Unmarried', 1, 1, '2018-04-07 20:27:04', '2018-04-10 22:59:52'),
(2, 'Married', 1, 1, '2018-04-10 21:50:59', '2018-04-10 21:50:59');

-- --------------------------------------------------------

--
-- Table structure for table `meetings`
--

CREATE TABLE `meetings` (
  `id` int(10) UNSIGNED NOT NULL,
  `msub` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `stime` time NOT NULL,
  `etime` time NOT NULL,
  `descrip` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(29, '2014_10_12_000000_create_users_table', 1),
(30, '2014_10_12_100000_create_password_resets_table', 1),
(33, '2018_03_15_092600_create_tb_training_table', 1),
(34, '2018_03_15_094044_create_tb_employee_training_table', 1),
(35, '2018_03_15_100908_create_departments_table', 1),
(36, '2018_03_15_101606_create_designations_table', 1),
(37, '2018_03_15_102147_create_employee_education_infos_table', 1),
(38, '2018_03_15_103638_create_employee_attachments_table', 1),
(39, '2018_03_15_104326_create_employee_working_histories_table', 1),
(40, '2018_03_15_105854_create_maritial_statuses_table', 1),
(41, '2018_03_15_111646_create_nationalities_table', 1),
(42, '2018_03_18_034529_create_country_table', 1),
(43, '2018_03_18_060625_grade', 1),
(45, '2018_03_21_031534_attendanceattachment', 1),
(49, '2018_03_25_034045_create_tb_festival_leave_table', 1),
(52, '2018_03_29_060834_create_vacancies_table', 1),
(53, '2018_04_01_040928_create_vacancy_application', 1),
(54, '2018_04_08_062032_create_meetings_table', 1),
(56, '2018_04_10_071437_attendence_machine_data', 1),
(58, '2018_04_15_041255_system_settings', 2),
(60, '2018_04_17_052247_create_temporary_training_table', 3),
(63, '2018_04_23_104132_create_week_leave_table', 4),
(64, '2018_04_22_071041_deduction_overtime', 5),
(68, '2018_03_19_095406_payroll_salary', 7),
(70, '2018_03_21_031623_attendance', 8),
(72, '2018_03_21_031715_attendancesetup', 10),
(74, '2018_04_26_114601_create_units_table', 12),
(75, '2018_04_30_033459_attendance_bonus', 12),
(76, '2018_04_08_081037_bonus', 13),
(80, '2018_05_07_040744_create_floors_table', 16),
(81, '2018_05_07_102552_tbcompany_information', 16),
(82, '2018_04_29_053122_create_lines_table', 17),
(83, '2018_05_10_062108_create_tbexpensecategory_table', 18),
(84, '2018_05_10_062611_create_tbexpenselist_table', 18),
(88, '2018_05_13_102315_create_nominees_table', 19),
(94, '2018_05_17_100230_create_tb_overtime_table', 21),
(98, '2018_05_22_043625_add_probation_period_to_employees_table', 23),
(105, '2018_03_15_083346_create_employees_table', 25),
(107, '2018_03_24_054907_create_tb_leave_type_table', 26),
(108, '2018_05_13_090525_create_tb_maternity_allowance_table', 26),
(109, '2018_03_15_080022_create_tb_employee_leave_table', 27),
(110, '2018_05_16_050043_create_tb_leave_application_table', 27),
(113, '2018_04_28_072336_employee_overtime', 29),
(114, '2018_05_14_032329_total_present', 30),
(115, '2018_04_30_125242_total_employee_leave', 31),
(116, '2018_05_20_071817_tb_leave_of_employee', 32),
(118, '2018_05_16_061804_salary_sheet_history', 33);

-- --------------------------------------------------------

--
-- Table structure for table `nationalities`
--

CREATE TABLE `nationalities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` tinyint(4) DEFAULT NULL,
  `updated_by` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nationalities`
--

INSERT INTO `nationalities` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Bangladesh', 1, 1, NULL, NULL),
(2, 'India', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `nominees`
--

CREATE TABLE `nominees` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `nominee_name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominee_phone` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominee_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominee_attachments` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominee_details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `off_day`
--

CREATE TABLE `off_day` (
  `id` int(10) UNSIGNED NOT NULL,
  `weekend` int(11) NOT NULL,
  `holiday` int(11) NOT NULL,
  `cur_month` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `off_day`
--

INSERT INTO `off_day` (`id`, `weekend`, `holiday`, `cur_month`, `created_at`, `updated_at`) VALUES
(1, 5, 1, '2018-04-08', '2018-04-07 20:30:37', '2018-04-07 20:30:37'),
(2, 5, 2, '2018-04-12', '2018-04-12 03:58:53', '2018-04-12 03:58:53'),
(3, 6, 6, '2018-04-12', '2018-04-12 04:01:16', '2018-04-12 04:01:16');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_grade`
--

CREATE TABLE `payroll_grade` (
  `id` int(10) UNSIGNED NOT NULL,
  `grade_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `basic_salary` decimal(8,2) NOT NULL,
  `house_rant` decimal(18,2) NOT NULL,
  `transport` decimal(18,2) NOT NULL,
  `medical` decimal(18,2) NOT NULL,
  `food` decimal(18,2) NOT NULL,
  `others` decimal(18,2) NOT NULL,
  `total_salary` decimal(18,2) NOT NULL,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modified_by` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payroll_grade`
--

INSERT INTO `payroll_grade` (`id`, `grade_name`, `basic_salary`, `house_rant`, `transport`, `medical`, `food`, `others`, `total_salary`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 'Grade1', '4500.00', '1800.00', '250.00', '650.00', '200.00', '0.00', '6600.00', 'Admin', 'Admin', '2018-05-19 20:14:13', '2018-05-19 20:14:13'),
(3, 'Grade2', '4548.00', '1819.00', '250.00', '650.00', '200.00', '0.00', '7467.00', 'Admin', 'Admin', '2018-04-28 23:07:24', '2018-04-28 23:07:24');

-- --------------------------------------------------------

--
-- Table structure for table `payroll_salary`
--

CREATE TABLE `payroll_salary` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `basic_salary` decimal(18,2) NOT NULL,
  `house_rant` decimal(18,2) NOT NULL,
  `medical` decimal(18,2) NOT NULL,
  `transport` decimal(18,2) NOT NULL,
  `food` decimal(18,2) NOT NULL,
  `other` decimal(18,2) NOT NULL,
  `total_employee_salary` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_month` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payroll_salary`
--

INSERT INTO `payroll_salary` (`id`, `emp_id`, `grade_id`, `basic_salary`, `house_rant`, `medical`, `transport`, `food`, `other`, `total_employee_salary`, `current_month`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '0.00', '', '2018-06-03 18:00:00', '2018-06-03 18:00:00'),
(2, 2, 1, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '0.00', '', '2018-06-03 18:00:00', '2018-06-03 18:00:00'),
(3, 3, 1, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '0.00', '', '2018-06-03 18:00:00', '2018-06-03 18:00:00'),
(4, 4, 1, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '0.00', '', '2018-06-03 18:00:00', '2018-06-03 18:00:00'),
(5, 5, 1, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '0.00', '', '2018-06-03 18:00:00', '2018-06-03 18:00:00'),
(6, 6, 1, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '0.00', '', '2018-06-03 18:00:00', '2018-06-03 18:00:00'),
(7, 7, 1, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '0.00', '', '2018-06-03 18:00:00', '2018-06-03 18:00:00'),
(8, 8, 1, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '0.00', '', '2018-06-03 18:00:00', '2018-06-03 18:00:00'),
(9, 9, 1, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '0.00', '', '2018-06-03 18:00:00', '2018-06-03 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copyright` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbcompany_information`
--

CREATE TABLE `tbcompany_information` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbcompany_information`
--

INSERT INTO `tbcompany_information` (`id`, `company_name`, `company_phone`, `company_email`, `company_address1`, `company_address2`) VALUES
(1, 'FEITS', '01824168996', 'info@feits.co', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbexpensecategory`
--

CREATE TABLE `tbexpensecategory` (
  `id` int(10) UNSIGNED NOT NULL,
  `categoryName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categoryDescription` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbexpenselist`
--

CREATE TABLE `tbexpenselist` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categoryId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expenseDate` date NOT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblines`
--

CREATE TABLE `tblines` (
  `id` int(10) UNSIGNED NOT NULL,
  `line_no` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `floor_id` int(11) DEFAULT NULL,
  `lineDescription` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblines`
--

INSERT INTO `tblines` (`id`, `line_no`, `floor_id`, `lineDescription`, `created_at`, `updated_at`) VALUES
(1, 'line1', 1, NULL, NULL, NULL),
(2, 'line2', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_leave`
--

CREATE TABLE `tb_employee_leave` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL,
  `leave_type_id` int(11) NOT NULL,
  `leave_taken` int(11) NOT NULL,
  `leave_available` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_training`
--

CREATE TABLE `tb_employee_training` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL,
  `training_id` int(11) NOT NULL,
  `training_starting_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `training_ending_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_employee_training`
--

INSERT INTO `tb_employee_training` (`id`, `employee_id`, `training_id`, `training_starting_date`, `training_ending_date`, `attachment`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '0000-00-00', '0000-00-00', '', '2018-04-08 15:39:54', '2018-04-08 15:39:54'),
(2, 1, 1, '04/12/2018', '04/12/2018', '', '2018-04-12 03:56:48', '2018-04-12 03:56:48');

-- --------------------------------------------------------

--
-- Table structure for table `tb_festival_leave`
--

CREATE TABLE `tb_festival_leave` (
  `id` int(10) UNSIGNED NOT NULL,
  `year` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purpose` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_leave_application`
--

CREATE TABLE `tb_leave_application` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL,
  `leave_type_id` int(11) NOT NULL,
  `leave_starting_date` date NOT NULL,
  `leave_ending_date` date NOT NULL,
  `actual_days` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `approved_by` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `attachment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_leave_application`
--

INSERT INTO `tb_leave_application` (`id`, `employee_id`, `leave_type_id`, `leave_starting_date`, `leave_ending_date`, `actual_days`, `status`, `approved_by`, `description`, `attachment`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2018-05-05', '2018-05-07', 3, 1, 'Admin', NULL, NULL, '2018-06-04 18:00:00', '2018-06-05 18:00:00'),
(2, 1, 1, '2018-05-05', '2018-05-07', 3, 1, 'Admin', NULL, NULL, '2018-06-04 18:00:00', '2018-06-05 18:00:00'),
(3, 1, 2, '2018-06-07', '2018-06-07', 1, 0, 'Pending', NULL, NULL, '2018-06-06 23:06:32', '2018-06-06 23:06:32');

-- --------------------------------------------------------

--
-- Table structure for table `tb_leave_type`
--

CREATE TABLE `tb_leave_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `leave_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_days` int(11) NOT NULL,
  `policy` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modified_by` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_leave_type`
--

INSERT INTO `tb_leave_type` (`id`, `leave_type`, `total_days`, `policy`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 'Maternity Leave', 120, 'Not more than 2 times', 'Admin', 'Admin', '2018-05-12 12:20:17', '2018-05-12 12:20:17'),
(2, 'Casual Leave', 12, 'Must be notified before 24 hour', 'Admin', 'Admin', '2018-04-04 02:04:05', '2018-04-04 02:04:05'),
(3, 'Sick Leave', 10, 'Must provide Doctor\'s Certificate', 'Admin', 'Admin', '2018-05-23 18:04:48', '2018-05-23 18:04:48'),
(4, 'Earn Leave', 20, 'Can be exchanged with salary', 'Admin', 'Admin', '2018-05-14 17:21:45', '2018-05-14 17:21:45');

-- --------------------------------------------------------

--
-- Table structure for table `tb_maternity_allowance`
--

CREATE TABLE `tb_maternity_allowance` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `leave_starting_date` date NOT NULL,
  `leave_ending_date` date NOT NULL,
  `previous_three_month_salary` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_working_days` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_payable_amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `per_instalment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `other2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `other3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `first_installment_pay` date NOT NULL,
  `second_installment_pay` date NOT NULL,
  `approved_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_maternity_allowance`
--

INSERT INTO `tb_maternity_allowance` (`id`, `employee_id`, `leave_starting_date`, `leave_ending_date`, `previous_three_month_salary`, `total_working_days`, `total_payable_amount`, `per_instalment`, `other2`, `other3`, `created_at`, `updated_at`, `first_installment_pay`, `second_installment_pay`, `approved_by`) VALUES
(4, '2', '2018-05-03', '2018-08-31', '16632', '74', '26970.81081081081', '13485.405405405405', '64', '11', '2018-05-29 16:41:13', '2018-05-29 17:13:45', '2018-05-30', '2018-05-30', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `tb_overtime`
--

CREATE TABLE `tb_overtime` (
  `id` int(10) UNSIGNED NOT NULL,
  `min_overtime` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `max_overtime` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_salary_history`
--

CREATE TABLE `tb_salary_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `grade_id` int(11) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `basic_salary` decimal(18,2) DEFAULT NULL,
  `house_rant` decimal(18,2) DEFAULT NULL,
  `medical` decimal(18,2) DEFAULT NULL,
  `transport` decimal(18,2) DEFAULT NULL,
  `food` decimal(18,2) DEFAULT NULL,
  `other` decimal(18,2) DEFAULT NULL,
  `gross` decimal(18,2) DEFAULT NULL,
  `overtime` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `overtime_rate` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `overtime_amount` decimal(18,2) DEFAULT NULL,
  `present` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `absent` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `absent_deduction_amount` decimal(18,2) DEFAULT NULL,
  `gross_pay` decimal(18,2) DEFAULT NULL,
  `one_year_bonus` decimal(18,2) DEFAULT NULL,
  `attendance_bonus` decimal(18,2) DEFAULT NULL,
  `working_day` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weekend` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `holiday` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leave` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `net_amount` decimal(18,2) DEFAULT NULL,
  `month` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_salary_history`
--

INSERT INTO `tb_salary_history` (`id`, `emp_id`, `grade_id`, `designation_id`, `basic_salary`, `house_rant`, `medical`, `transport`, `food`, `other`, `gross`, `overtime`, `overtime_rate`, `overtime_amount`, `present`, `absent`, `absent_deduction_amount`, `gross_pay`, `one_year_bonus`, `attendance_bonus`, `working_day`, `weekend`, `holiday`, `leave`, `total`, `net_amount`, `month`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 3, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '6600.00', NULL, '43.27', '0.00', '3', '14', '2032.26', '4567.74', NULL, NULL, '23', '8', '0', 'Casual Leave3,Maternity Leave3', '6', '4567.74', '2018-05-31', 0, '2018-06-06 03:52:09', '2018-06-06 03:52:09'),
(2, 2, 1, 1, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '6600.00', NULL, '43.27', '0.00', '7', '16', '2322.58', '4277.42', NULL, NULL, '23', '8', '0', NULL, NULL, '4277.42', '2018-05-31', 0, '2018-06-06 03:52:09', '2018-06-06 03:52:09'),
(3, 3, 1, 1, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '6600.00', '7', '43.27', '302.89', '9', '14', '2032.26', '4567.74', NULL, NULL, '23', '8', '0', NULL, NULL, '4870.63', '2018-05-31', 0, '2018-06-06 03:52:09', '2018-06-06 03:52:09'),
(4, 4, 1, 3, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '6600.00', NULL, '43.27', '0.00', '9', '14', '2032.26', '4567.74', NULL, NULL, '23', '8', '0', NULL, NULL, '4567.74', '2018-05-31', 0, '2018-06-06 03:52:09', '2018-06-06 03:52:09'),
(5, 5, 1, 1, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '6600.00', NULL, '43.27', '0.00', '7', '16', '2322.58', '4277.42', NULL, NULL, '23', '8', '0', NULL, NULL, '4277.42', '2018-05-31', 0, '2018-06-06 03:52:09', '2018-06-06 03:52:09'),
(6, 6, 1, 1, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '6600.00', NULL, '43.27', '0.00', '9', '14', '2032.26', '4567.74', NULL, NULL, '23', '8', '0', NULL, NULL, '4567.74', '2018-05-31', 0, '2018-06-06 03:52:09', '2018-06-06 03:52:09'),
(7, 7, 1, 1, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '6600.00', NULL, '43.27', '0.00', '8', '15', '2177.42', '4422.58', NULL, NULL, '23', '8', '0', NULL, NULL, '4422.58', '2018-05-31', 0, '2018-06-06 03:52:09', '2018-06-06 03:52:09'),
(8, 8, 1, 5, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '6600.00', '4', '43.27', '173.08', '6', '17', '2467.74', '4132.26', NULL, NULL, '23', '8', '0', NULL, NULL, '4305.34', '2018-05-31', 0, '2018-06-06 03:52:09', '2018-06-06 03:52:09'),
(9, 9, 1, 1, '4500.00', '1000.00', '650.00', '250.00', '200.00', '0.00', '6600.00', NULL, '43.27', '0.00', '9', '14', '2032.26', '4567.74', '500.00', NULL, '23', '8', '0', NULL, NULL, '5067.74', '2018-05-31', 0, '2018-06-06 03:52:09', '2018-06-06 03:52:09');

-- --------------------------------------------------------

--
-- Table structure for table `tb_total_present`
--

CREATE TABLE `tb_total_present` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_present` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `month` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_training`
--

CREATE TABLE `tb_training` (
  `id` int(10) UNSIGNED NOT NULL,
  `training_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `attachment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_training`
--

INSERT INTO `tb_training` (`id`, `training_name`, `description`, `duration`, `attachment`, `created_at`, `updated_at`) VALUES
(2, 'test Training', 'This is it training', 10, '', '2018-04-08 12:00:00', '2018-04-08 12:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `temporary_training`
--

CREATE TABLE `temporary_training` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `temporary_training`
--

INSERT INTO `temporary_training` (`id`, `employee_id`, `designation_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2018-06-06 23:08:50', '2018-06-06 23:08:50');

-- --------------------------------------------------------

--
-- Table structure for table `total_employee_leave`
--

CREATE TABLE `total_employee_leave` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_idss` int(11) DEFAULT NULL,
  `total_leaves_taken` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `month` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `phone_no`, `email`, `address`) VALUES
(1, 'Unit1', '43534534', 'unit1@gmail.com', 'sdfsdfsdfds'),
(2, 'Unit 2', '435435435', 'unit2@gmail.com', 'dsafdsfsdfsdfsdf'),
(3, 'Unit3', '324535435', 'unit3@gmail.com', 'fdsgdfgdfg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_permission` tinyint(4) NOT NULL DEFAULT '3',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `emp_id`, `name`, `email`, `password`, `remember_token`, `is_permission`, `created_at`, `updated_at`) VALUES
(1, 0, 'Admin', 'admin@email.com', '$2y$10$BiZqC5t9pNg/QZWRUCwmxuavnC1IRZF3DVqmKVdmMKd9DJ8ASiQyy', 'd3jhpM4PJsOnIRrCNgGh93rQ6mCVUvnksHgUNPbR9vPSb3dPZXOdhnD4tYRg', 1, '2018-04-07 20:23:12', '2018-04-07 20:23:12'),
(2, 3, 'Mr.', NULL, '$2y$10$8p/mQ0besZys8ZLrYTONEebpoF/7pAZRV/RkukOzH4gDCN3OynTE6', 'TLbKKPTSiQAUX9mvGeDzh5bjEVKcYV5Xwv6ETuNdy6HPII67XuIHWaoxhd8Z', 4, '2018-04-07 20:28:10', '2018-06-03 02:14:06'),
(3, 2, 'Razia Sultana', 'email@email.com', '$2y$10$ul5x.UuozQl6TfW/aoCEPu.XAHMMxvDeLjSKrEgw/MakDMkLcLxT.', NULL, 2, '2018-04-07 20:31:31', '2018-06-03 02:14:23'),
(4, 3, 'Mr.', NULL, '$2y$10$QHQMbEdx2fcjVGDV75.WaeJtC14ZVTBHSd8tLmurSchINE62TTwhm', NULL, 4, '2018-04-07 20:32:16', '2018-06-03 02:14:06'),
(5, 4, 'Samsujjaman', 'arafat@feits.co', '$2y$10$4xe8KX2wixPzO1OVVN5uFeg3YPYHKjgAxO5LlJB607z/HhZOh8PLe', NULL, 1, '2018-04-10 22:15:40', '2018-06-03 02:12:40'),
(6, 10, 'ttttttttttttttttt', NULL, '$2y$10$i.TzJHoQ9PbGq8ognBRVTO26FKCZP1SnkorNjK0YvxuQ1.SfotK8W', NULL, 2, '2018-04-25 02:05:32', '2018-04-25 02:05:32'),
(7, 11, 'rrrrrrrrrrrrrrrrrrrrrr', NULL, '$2y$10$Kb8yFw6Qe3B8jEwMzxeO9eJV7F4AeDVwHgD5uyPtzKQPl3ZQrmAFG', NULL, 2, '2018-04-25 02:32:24', '2018-04-25 02:32:24'),
(8, 10, 'reterterterter436546456', NULL, '$2y$10$b.6FTVLIrdMQSEM0EeyP3OLiYCZ2L1M1DM.nmdZf9KvqA8lRol/Ie', NULL, 2, '2018-04-28 23:03:18', '2018-04-28 23:03:18'),
(9, 5, 'Sayed Yeamin', 'zim847@gmail.com', '$2y$10$usYyo7Nlw048zJQVq6tq4.VuT5JUWbBYIBcJP2cKuJFewbxNMEMX2', NULL, 1, NULL, '2018-06-03 02:12:23'),
(10, 8, 'Hasan Ul', NULL, '$2y$10$fCNXeg2.WunC/EAlmVoP5uBuRmmzqtjwj4VhnO7QH43wm0vI6J/R6', NULL, 4, NULL, '2018-06-03 02:13:12'),
(11, 9, 'Md. Bakhtiar', NULL, '$2y$10$ZCn61kvqAqsdNRvUqfMJIe98WCGF2kpkwTbKGRpEPOGvwhq6Eqtz.', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vacancies`
--

CREATE TABLE `vacancies` (
  `id` int(10) UNSIGNED NOT NULL,
  `vacTitle` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacDescription` text COLLATE utf8mb4_unicode_ci,
  `vacNumber` int(11) NOT NULL,
  `VacAnnounceStartingDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `vacAnnounceEndingDate` timestamp NULL DEFAULT NULL,
  `vacJoiningDate` timestamp NULL DEFAULT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vacStatus` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vacancies`
--

INSERT INTO `vacancies` (`id`, `vacTitle`, `vacDescription`, `vacNumber`, `VacAnnounceStartingDate`, `vacAnnounceEndingDate`, `vacJoiningDate`, `created_by`, `updated_by`, `vacStatus`, `created_at`, `updated_at`) VALUES
(1, 'Sr. Software Engineer', 'Nothing', 5, '2018-04-11 18:00:00', '2018-04-11 18:00:00', '2018-04-30 18:00:00', 'Admin', 'Admin', 1, '2018-04-12 03:51:59', '2018-04-12 03:51:59');

-- --------------------------------------------------------

--
-- Table structure for table `vacancy_application`
--

CREATE TABLE `vacancy_application` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vacancy_id` int(11) NOT NULL,
  `submittedDate` timestamp NULL DEFAULT NULL,
  `interviewStatus` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `interviewDate` timestamp NULL DEFAULT NULL,
  `jobStatus` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `interviewNote` text COLLATE utf8mb4_unicode_ci,
  `priorityStatus` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `week_leave`
--

CREATE TABLE `week_leave` (
  `id` int(10) UNSIGNED NOT NULL,
  `day-id` int(11) NOT NULL,
  `day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `week_leave`
--

INSERT INTO `week_leave` (`id`, `day-id`, `day`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Sunday', 0, NULL, '2018-04-23 23:47:18'),
(2, 2, 'Monday', 0, NULL, '2018-04-23 23:00:15'),
(3, 3, 'Tuesday', 0, NULL, '2018-04-23 16:42:01'),
(4, 4, 'Wednesday', 0, NULL, '2018-04-23 16:51:09'),
(5, 5, 'Thursday', 0, NULL, '2018-04-23 16:50:36'),
(6, 6, 'Friday', 1, NULL, '2018-06-06 23:08:15'),
(7, 7, 'Saturday', 0, NULL, '2018-06-06 23:08:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendance_bonus`
--
ALTER TABLE `attendance_bonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendance_file`
--
ALTER TABLE `attendance_file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendance_machine_data`
--
ALTER TABLE `attendance_machine_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendance_setup`
--
ALTER TABLE `attendance_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees_bonus`
--
ALTER TABLE `employees_bonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_attachments`
--
ALTER TABLE `employee_attachments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_deduction`
--
ALTER TABLE `employee_deduction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_education_infos`
--
ALTER TABLE `employee_education_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_overtime`
--
ALTER TABLE `employee_overtime`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_working_histories`
--
ALTER TABLE `employee_working_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `floors`
--
ALTER TABLE `floors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_of_employee`
--
ALTER TABLE `leave_of_employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marital_statuses`
--
ALTER TABLE `marital_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nationalities`
--
ALTER TABLE `nationalities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nominees`
--
ALTER TABLE `nominees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `off_day`
--
ALTER TABLE `off_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payroll_grade`
--
ALTER TABLE `payroll_grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll_salary`
--
ALTER TABLE `payroll_salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbcompany_information`
--
ALTER TABLE `tbcompany_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbexpensecategory`
--
ALTER TABLE `tbexpensecategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbexpenselist`
--
ALTER TABLE `tbexpenselist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblines`
--
ALTER TABLE `tblines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_employee_leave`
--
ALTER TABLE `tb_employee_leave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_employee_training`
--
ALTER TABLE `tb_employee_training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_festival_leave`
--
ALTER TABLE `tb_festival_leave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_leave_application`
--
ALTER TABLE `tb_leave_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_leave_type`
--
ALTER TABLE `tb_leave_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_maternity_allowance`
--
ALTER TABLE `tb_maternity_allowance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_overtime`
--
ALTER TABLE `tb_overtime`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_salary_history`
--
ALTER TABLE `tb_salary_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_total_present`
--
ALTER TABLE `tb_total_present`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_training`
--
ALTER TABLE `tb_training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temporary_training`
--
ALTER TABLE `temporary_training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `total_employee_leave`
--
ALTER TABLE `total_employee_leave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vacancies`
--
ALTER TABLE `vacancies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vacancy_application`
--
ALTER TABLE `vacancy_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `week_leave`
--
ALTER TABLE `week_leave`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;

--
-- AUTO_INCREMENT for table `attendance_bonus`
--
ALTER TABLE `attendance_bonus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attendance_file`
--
ALTER TABLE `attendance_file`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `attendance_machine_data`
--
ALTER TABLE `attendance_machine_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attendance_setup`
--
ALTER TABLE `attendance_setup`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `employees_bonus`
--
ALTER TABLE `employees_bonus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employee_attachments`
--
ALTER TABLE `employee_attachments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employee_deduction`
--
ALTER TABLE `employee_deduction`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_education_infos`
--
ALTER TABLE `employee_education_infos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employee_overtime`
--
ALTER TABLE `employee_overtime`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_working_histories`
--
ALTER TABLE `employee_working_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `floors`
--
ALTER TABLE `floors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `leave_of_employee`
--
ALTER TABLE `leave_of_employee`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `marital_statuses`
--
ALTER TABLE `marital_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `meetings`
--
ALTER TABLE `meetings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `nationalities`
--
ALTER TABLE `nationalities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `nominees`
--
ALTER TABLE `nominees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `off_day`
--
ALTER TABLE `off_day`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payroll_grade`
--
ALTER TABLE `payroll_grade`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payroll_salary`
--
ALTER TABLE `payroll_salary`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbcompany_information`
--
ALTER TABLE `tbcompany_information`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbexpensecategory`
--
ALTER TABLE `tbexpensecategory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbexpenselist`
--
ALTER TABLE `tbexpenselist`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblines`
--
ALTER TABLE `tblines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_employee_leave`
--
ALTER TABLE `tb_employee_leave`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_employee_training`
--
ALTER TABLE `tb_employee_training`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_festival_leave`
--
ALTER TABLE `tb_festival_leave`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_leave_application`
--
ALTER TABLE `tb_leave_application`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_leave_type`
--
ALTER TABLE `tb_leave_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_maternity_allowance`
--
ALTER TABLE `tb_maternity_allowance`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_overtime`
--
ALTER TABLE `tb_overtime`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_salary_history`
--
ALTER TABLE `tb_salary_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_total_present`
--
ALTER TABLE `tb_total_present`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_training`
--
ALTER TABLE `tb_training`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `temporary_training`
--
ALTER TABLE `temporary_training`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `total_employee_leave`
--
ALTER TABLE `total_employee_leave`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `vacancies`
--
ALTER TABLE `vacancies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vacancy_application`
--
ALTER TABLE `vacancy_application`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `week_leave`
--
ALTER TABLE `week_leave`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
