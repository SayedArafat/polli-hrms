<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route for home login page and redirect to dashboard


Route::get('/','dashboardcontroller@dashboard_view');
Route::get('/dashboard','dashboardcontroller@dashboard_view');
Auth::routes();
//all necessary command
Route::get('commands', function () {
    \Artisan::call('migrate');
    return Redirect::back();
});

Route::get('/404', function () {
    return view('404_error');
});

Route::get('/info', function () {
    return view('phpinfo');
});

Route::get('/error', function () {
    return view('404');
});

Route::get('/500', function () {
    return view('404_error');
});

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/employee/details/{id}','EmployeeController@show');
//Route::get('/leave_show','leaveController@leave_type_show');
//Role wise permission route
Route::group(['middleware'=>'auth'], function () {
    //test
    Route::get('/arafat/up',['uses'=>'CountryController@up']);

    //quota
    Route::get('/quota_district_list',['middleware'=>'check-permission:admin|hr|executive','uses'=>'QuotaController@district_list'])->name('quota.district_list');
    Route::get('/quota_settings',['middleware'=>'check-permission:admin|hr|executive','uses'=>'QuotaController@quota_settings'])->name('quota.settings');
    Route::post('/quota_settings',['middleware'=>'check-permission:admin|hr|executive','uses'=>'QuotaController@store_district']);
    Route::post('/store_vacancy',['middleware'=>'check-permission:admin|hr|executive','uses'=>'QuotaController@store_vacancy']);
    Route::patch('/quota_settings/{id}',['middleware'=>'check-permission:admin|hr|executive','uses'=>'QuotaController@update_district']);
    Route::get('/quota_population',['middleware'=>'check-permission:admin|hr|executive','uses'=>'QuotaController@population'])->name('quota.population');
    Route::post('/quota_population',['middleware'=>'check-permission:admin|hr|executive','uses'=>'QuotaController@store_population']);
    Route::patch('/quota_population/{id}',['middleware'=>'check-permission:admin|hr|executive','uses'=>'QuotaController@update_population']);


    //all training route start
    Route::get('/add_training',['middleware'=>'check-permission:admin|hr|executive','uses'=>'TrainingController@index']);
    Route::post('/add_training',['middleware'=>'check-permission:admin|hr|executive','uses'=>'TrainingController@storeTraining']);
    Route::post('/training_update',['middleware'=>'check-permission:admin|hr|executive','uses'=>'TrainingController@trainingUpdate']);
    Route::post('/employee_training',['middleware'=>'check-permission:admin|hr|executive','uses'=>'TrainingController@employeeTraining']);
    Route::get('/training_delete/{id}',['middleware'=>'check-permission:admin|hr|executive','uses'=>'TrainingController@training_delete']);
    Route::get('/training_history',['middleware'=>'check-permission:admin|hr|executive','uses'=>'TrainingController@training_history']);
    Route::get('/assign/training',['middleware'=>'check-permission:admin|hr','uses'=>'TrainingController@training_assign']);
    Route::get('/assign/training/des_table/{designation}', 'TrainingController@DesignationTable');
    Route::get('/training/history/attendent/{id}', 'TrainingController@AttendentList');
    Route::get('/assign/training/addEmployee/{empid}', 'TrainingController@EmployeeTable');
    Route::get('/assign/training/delete/{del}', 'TrainingController@DeleteFromnTable');
    Route::post('/training/assign',['middleware'=>'check-permission:admin|hr','uses'=>'TrainingController@training_assign_request']);
    //training route end

    //Dashboard 
    Route::get('/meetingdetails/{id}','WorkingExperienceController@showDelete')->name('workexperience.delete.show');

    //Employee route start
    Route::get('/grid_view',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@grid_view_index']);
    Route::POST('/employee_grid_filter',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@grid_filter']);
    Route::POST('/employee_grid_search',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@grid_employee_search']);

    Route::POST('/employee_filter',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@filter']);
    Route::POST('/employee_search',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@employee_search']);

    Route::get('/active_employee',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@active_employee_list']);
    Route::POST('/active_employee_filter',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@filter_active_employee']);
    Route::POST('/active_employee_search',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@active_employee_search']);


    Route::get('/inactive_employee',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@inactive_employee_list']);
    Route::POST('/inactive_employee_filter',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@filter_inactive_employee']);
    Route::POST('/inactive_employee_search',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@inactive_employee_search']);


    Route::get('/male_employee',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@male_employee_list']);
    Route::POST('/male_employee_filter',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@filter_male_employee']);
    Route::POST('/male_employee_search',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@male_employee_search']);


    Route::get('/female_employee',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@female_employee_list']);
    Route::POST('/female_employee_filter',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@filter_female_employee']);
    Route::POST('/female_employee_search',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@female_employee_search']);



    Route::post('/employee_discontinuation',['middleware'=>'check-permission:admin|hr','uses'=>'EmployeeController@UpdateResignationDate']);
    Route::resource('/employee','EmployeeController');
    Route::get('/user/profile',['middleware'=>'check-permission:admin|hr|executive|accountant','uses'=>'dashboardcontroller@user_profile'])->name('user.profile');

    Route::resource('/employee/education','EmployeeEducationInfoController',['except'=>['index','show','edit']]);
    Route::resource('/employee/training','EmployeeTrainingController',['except'=>['index','show']]);
    Route::resource('/employee/nominee','NomineeController',['except'=>['index','show']]);
    Route::resource('/employee/workexpericence','WorkingExperienceController',['except'=>['show']]);
    Route::resource('/employee/skill','SkillTestController',['except'=>['show']]);
    Route::get('/employee/workexperience/delete/{id}','WorkingExperienceController@showDelete')->name('workexperience.delete.show');
    Route::get('/employee/skill/delete/{id}','SkillTestController@showDelete')->name('skill.delete.show');
    Route::get('/employee/nominee/delete/{id}','NomineeController@showDelete')->name('nominee.delete.show');
    Route::get('/employee/training/delete/{id}','EmployeeTrainingController@showDelete')->name('training.delete.show');
    Route::resource('/employee/attachments','OtherAttachmentController',['except'=>['create','index']]);
    Route::patch('/employee/password/{id}','EmployeeController@UpdatePassword')->name('employee.password.update');
    Route::patch('/employee/photo/{id}','EmployeeController@UpdatePhoto');
//    Route::get('/employee/floor/line/{id}','EmployeeController@floor_line');
    Route::get('/employeelist/continuedAbsentList',['middleware'=>'check-permission:admin|hr','uses'=>'EmployeeController@getContinueAbsentList'])->name('employee.continued_absent_list');
    Route::post('/report/continuedAbsentListNotification',['middleware'=>'check-permission:admin|hr','uses'=>'EmployeeController@continueAbsentEmployeeNotification']);
    Route::get('section-autocomplete-ajax',array('as'=>'employee.section.autocomplete.ajax','uses'=>'EmployeeController@sectionAjaxData'));
    Route::POST('/store_medical_history',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@store_medical_history']);
    Route::patch('/update_medical_history/{id}','EmployeeController@update_medical_history');
    Route::get('/employee_medical_history_pdf/{id}',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@download_ems'])->name('employee.download_ems');
  
    Route::get('/AbsentEmployees/sendAutoSMS','EmployeeController@sent_auto_sms')->name('employee.continue_absent_list_autosms');
    Route::get('/retire/employee/list',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@retired_list']);
    //Employee Route End
    //Settings route start
    Route::get('/user_settings',['middleware'=>'check-permission:admin|hr','uses'=>'SystemsettingsController@index']);
    Route::post('/user_update',['middleware'=>'check-permission:admin|hr','uses'=>'SystemsettingsController@user_pass_update']);
    Route::resource('/settings/country','CountryController',['except'=>['show','create','edit']]);
    Route::resource('/settings/maritialstatus','MaritialStatusController',['except'=>['show','create','edit']]);
    Route::resource('/settings/department','DepartmentController',['except'=>['show','create','edit']]);
    Route::resource('/settings/designation','DesignationController',['except'=>['show','create','edit']]);
    Route::resource('/settings/unit', 'UnitController',['except'=>['create']]);
    Route::resource('/settings/overtime','OvertimeController',['except'=>['create','store','show','edit','destroy']]);
    Route::get('/settings/unit/delete/show/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'UnitController@showDelete'])->name('unit.delete.show');
    // Settings route end
    //recruitment Route Start
    Route::resource('/recruitment/vacancy','RecruitmentController',['except'=>'edit']);
    Route::get('/recruitment/application/create','RecruitmentController@vacancyApplication')->name('application.create');
    Route::post('/recruitment/application','RecruitmentController@storeVacancyApplication')->name('application.store');
    Route::get('/recruitment/application','RecruitmentController@indexVacancyApplication')->name('application.index');
    Route::get('/recruitment/status','RecruitmentController@dailyRecruitment')->name('recruitment.status');
    Route::get('/recruitment/status/{date}', 'RecruitmentController@recruitmentStatus');
    Route::get('/recruitment/application/{id}', 'RecruitmentController@detailsVacancyApplication')->name('application.details');
    Route::patch('/recruitment/application/{id}','RecruitmentController@applicationUpdate')->name('application.update');
    Route::delete('/recruitment/application/{id}','RecruitmentController@applicationDelete')->name('application.delete');
    //recruitment Route end
    //payroll Route Start
    Route::get('/report/employee/daily/salary',['middleware'=>'check-permission:admin|hr','uses'=>'SalarySheet@employeeDailysalary']);
    Route::post('/report/employee/daily/salary/show',['middleware'=>'check-permission:admin|hr','uses'=>'SalarySheet@employeeDailysalaryshow']);
    Route::get('/employee/grade/salary/update/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'SalaryController@gradeWisesalaryUpdate']);
    Route::get('/employee/salary/grade/modals/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'SalaryController@employeeSalaryEditmodal']);
    Route::post('/deduction_overtime',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@employee_absent_overtime']);
    Route::post('/payroll_grade_store',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@payroll_grade_store']);
    Route::get('/payroll/grade',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@payroll_grade']);
    Route::get('/payslip',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@payslip']);
    Route::get('/grade',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@payroll_grade_show']);
    Route::post('/grade_update',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@payroll_grade_update']);
    Route::get('/grade_delete/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@grade_delete']);
    Route::get('/salary_list',['middleware'=>'check-permission:admin|hr','uses'=>'SalaryController@total_salary_view']);
    Route::get('/salary',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@payroll_salary']);
    Route::post('/gradeajax/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@show']);
    Route::post('/salary_store',['middleware'=>'check-permission:admin|hr','uses'=>'SalaryController@store']);
    Route::get('/salary_view',['middleware'=>'check-permission:admin|hr','uses'=>'SalaryController@index']);
    Route::post('/salary_up',['middleware'=>'check-permission:admin|hr','uses'=>'SalaryController@update']);
    Route::get('/payroll/salary',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@payroll_salary']);
    Route::post('/employee/bonus/store',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@bonus_emloyee']);
    Route::post('/overtime_employee',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@employee_overtime']);
    Route::post('/overtime_employee_store',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@overtime_amount_hour_store']);
    Route::post('/festivalbonus',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@empFestivalbonus']);
    Route::post('/leave_employee',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@leave_employee']);
    Route::post('/pres_employee',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@employee_present_days_count']);
    Route::post('/leave_employee_store',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@leave_employee_store']);
    Route::post('/employee_present_store',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@employee_present_store']);
    Route::get('/report/bonus',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@employee_month_wise_bonus_Report']);
    Route::get('/report/increment/bonus',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@incrementBonus']);
    Route::get('/report/attendance/bonus',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@attendanceBonus']);
    Route::get('/report/festival/bonus',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@festivalBonus']);
    Route::post('/report/employee/increment',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@incrementEmployeeBonusReport']);
    Route::post('/report/employee/attendance/bonus',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@attendanceEmployeeBonusReport']);
    Route::post('/report/employee/festival/bonus',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@festivalEmloyeeBonusReport']);
    Route::post('/report/employee/increment/bonus/pdf',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@incrementreportpdf']);
    Route::post('/report/employee/attendance/bonus/pdf',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@attendancereportpdf']);
    Route::post('/report/employee/festival/bonus/pdf',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@festivalreportpdf']);

    Route::post('/report/year/bonus',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@one_year_bonus_Report']);
    Route::get('/report/payroll/employee/salary/sheet/save',['middleware'=>'check-permission:admin|hr','uses'=>'SalarySheet@view_salary_sheet']);
    Route::get('/report/payroll/employee/salary/summery',['middleware'=>'check-permission:admin|hr','uses'=>'SalarySheet@salarySummery']);
    Route::get('/report/payroll/employee/salary/sheet',['middleware'=>'check-permission:admin|hr','uses'=>'SalarySheet@monthwisesalarysheetview']);
    Route::post('/report/employee/salary/monthwise',['middleware'=>'check-permission:admin|hr','uses'=>'SalarySheet@Monthwiseemployeesalarysheet']);
    Route::post('/report/employee/salarysheet',['middleware'=>'check-permission:admin|hr','uses'=>'SalarySheet@employee_SalarySheet']);
    Route::get('/report/payslip',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@payslipGenerate']);
    Route::post('/report/payslip/generate',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@payslipReportemployees']);
    Route::get('/bonus',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@employee_bonus_view']);
    Route::get('increment/bonus',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@incrementBonusview']);
    Route::get('festival/bonus',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@FestivalBonuspage']);
    //payroll Route end
    Route::get('/leave/addWeekends/{weekLeave}', 'leaveController@addweekLeave');
    Route::get('/leave_sett',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@leave_sett']);
    Route::get('leave/employee/leave_history/{id}', 'leaveController@employeeHistoryModal');
    //Route::get('/leave_sett',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@wkleavealternate']);
    Route::get('/leave/assign_leave',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@assignLeave']);
    Route::post('leave/assign/mannual_assign',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@mannualAssignLeave']);
    Route::get('leave/employee_leave_history/{id}', 'leaveController@employeeLeaveLeft');
    Route::get('/leave',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@index']);
    Route::get('/leave_history',['middleware'=>'check-permission:admin|hr|executive','uses'=>'leaveController@leaveHistory']);
    Route::post('/leave_settings',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@leave_settings']);
    Route::post('/leave_update',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@leave_update']);
    Route::post('/festival_leave_update',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@festival_leave_update']);
    Route::get('/leave_delete/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@leave_delete']);
    Route::get('/leave/request/delete/{id}',['middleware'=>'check-permission:admin|hr|executive','uses'=>'leaveController@leaveRequestDelete']);
    Route::get('/festival_leave_delete/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@festival_leave_delete']);
    Route::post('/add_festival_leave',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@add_festival_leave']);
    Route::get('/attendance',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@index'])->name('attendance.file.create');
    Route::post('/attendance_store',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@store']);
    Route::post('/csv_upload',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@csv_upload']);
    Route::get('/csv_view',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@csv_show']);
    Route::get('/attendance/files',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@filesView'])->name('files.show');
    Route::get('/attendance/files/show/delete/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@showDelete'])->name('file.show.delete');
    Route::delete('/attendance/files/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@destroyFile']);
    Route::get('/attendance/status/absent/{date}',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@absentStatus']);
    Route::get('/attendance/employee/{id}/{date}',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@addEmployee'])->name('employee.attendance.add');
    Route::get('/attendance/present/status/{id}/{date}',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@addEditAttendance'])->name('employee.attendance.add');
    Route::post('/attendance/store',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@storeAttendance']);
    Route::patch('/attendance/update/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@updateAttendance']);
    Route::post('/attendance/present/data/{date}',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@presentAttendance']);
    // Route::get('/report/attendance/daily_attendance',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@presentStatus'])->name('attendance.present.status');
    Route::post('/attendance/manual/store',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@manualAttendance']);
    Route::get('/attendance/present/status',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@presentStatus'])->name('attendance.present.status');
    Route::get('/attendance/create',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@createAttendance'])->name('attendance.manual.create');
    Route::get('/attendance/edit',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@editAttendance'])->name('attendance.manual.edit');
    Route::get('/attendance/data/{value}',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@dataAttendance']);

    //Route::get('/attendance/continiousAbsent',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@continiousAbsent']);
    //see all leave request
    Route::post('/leave_request',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@leave_request']);
    Route::get('/leave/maternityLeaveDetails',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@maternityLeaveHistory']);
    Route::get('/pay_first_installment/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@pay_first_installment']);
    Route::get('/pay_second_installment/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@pay_second_installment']);
    Route::get('/leave/earnLeaveDetails',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@earnLeave']);
    Route::get('/leave_accepted/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@leave_accepted']);
    Route::get('/maternity_leave_benefits_of_person/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@personWiseMaternityLeaveBenefit']);
    //Route::get('/leave_accepted2/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@leave_accepted2']);
    Route::get('/leave_declined/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@leave_declined']);
    Route::get('/leave_declined2/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@leave_declined2']);
    Route::post('/complete_off',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@off_day']);
    //request for leave
    Route::get('/request_leave',['middleware'=>'check-permission:admin|hr|executive','uses'=>'leaveController@request_leave']);
    Route::post('/store_leave_request',['middleware'=>'check-permission:admin|hr|executive','uses'=>'leaveController@store_leave_request']);
    // Route::get('/leave_history',['middleware'=>'check-permission:admin|hr|executive','uses'=>'leaveController@employee_leave_history']);
    Route::get('/attendance/settings',['middleware'=>'check-permission:admin','uses'=>'AttendanceController@attendance_setup']);
    Route::post('/attendance/settings/store',['middleware'=>'check-permission:admin','uses'=>'AttendanceController@settings_store']);
    Route::post('/attendance/settings/update',['middleware'=>'check-permission:admin','uses'=>'AttendanceController@settings_update']);
    //report route start
    Route::get('/report/pdf/employeeTrainingHistory',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@employeeTrainingPDFReport']);
    Route::get('/report/employee',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@employeeReportDashboard']);
    Route::get('/report/employee_list',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@employeeListReport']);
    Route::post('/report/view_employee_list',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@viewEmployeeListReport']);
    Route::get('/report/search_employee',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@search_employee'])->name('report.search_employee');
    Route::post('/report/search_employee',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@showEmployee']);
//    Route::get('report/floor/line/{id}','ReportController@floor_line');
//    Route::get('report/floor/line2/{id}','ReportController@floor_line2');
    Route::get('/report/employee_list_status',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@employeeListByStatus']);
    Route::post('/report/view_employee_list_status',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@viewEmployeeListByStatus']);
    Route::get('/report/employee_list_gender',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@employeeListByGender']);
    Route::post('/report/view_employee_list_gender',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@viewEmployeeListByGender']);
    Route::get('/report/employee_list_department',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@employeeListByDepartment']);
    Route::post('/report/view_employee_list_department',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@viewEmployeeListByDepartment']);
    Route::get('/report/employee_list_designation',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@employeeListByDesignation']);
    Route::post('/report/view_employee_list_designation',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@viewEmployeeListByDesignation']);
    Route::get('/report/floor_line_employee_list',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@employeeListByFloorLine']);
    Route::post('/report/view_floor_line_employee_list',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@viewEmployeeListByFloorLine']);
    Route::get('/report/section_employee_list',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@employeeListBySection']);
    Route::post('/report/view_section_employee_list',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@viewEmployeeListBySection']);
    Route::get('/report/payroll',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@payrollReportDashboard']);
    Route::get('/report/attendance',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@attendanceReportDashboard']);
    Route::get('/report/attendance/date_wise_attendance',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@attendance_report_view']);
    Route::post('/attendance/report/show',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@attendance_report_show']);
    Route::get('/attendance/status',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@attendance_report_daily']);
    Route::post('/reports_daily',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@daily_employee_attendance']);
    Route::get('/report/daily_late_present',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@daily_late_present'])->name('report.daily.late.present');
    Route::post('/report/daily_late_present',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@daily_late_present_data']);
    Route::get('/report/daily_present_report',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@daily_present_report'])->name('report.daily_present_report');
    Route::post('/report/daily_present_report',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@daily_present_report_data']);
    Route::get('/report/daily_absent_report',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@daily_absent_report'])->name('report.daily_absent_report');
    Route::post('/report/daily_absent_report',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@daily_absent_report_data']);
    Route::get('/report/public/daily_attendance',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@public_report_daily_attendance'])->name('report.public.daily_attendance');
    Route::post('/report/public/daily_attendance',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@public_report_daily_attendance_index']);
    Route::get('/report/public/daily_attendance/data',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@public_report_daily_attendance_data'])->name('report.public.daily_attendance_data');
    Route::get('/report/date_wise_late',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@date_wise_late'])->name('report.date_wise_late');
    Route::post('/report/date_wise_late',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@date_wise_late_data']);
    Route::get('/report/public/date_wise_present',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@public_date_wise_present'])->name('report.public.date_wise_present');
    Route::post('/report/public/date_wise_present',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@public_date_wise_present_data']);
    Route::get('/report/date_wise_absent',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@date_wise_absent'])->name('report.date_wise_absent');
    Route::post('/report/date_wise_absent',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@date_wise_absent_data']);
    Route::get('/report/daily_overtime_report',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@daily_overtime_report'])->name('report.daily_overtime_report');
    Route::post('/report/daily_overtime_report',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@daily_overtime_data']);
    Route::get('/report/date_wise_overtime',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@date_wise_overtime'])->name('report.date_wise_overtime');
    Route::post('/report/date_wise_overtime',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@date_wise_overtime_data']);
    Route::get('/report/daily_attendance_exception',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@daily_attendance_exception'])->name('report.daily_attendance_exception');
    Route::post('/report/daily_attendance_exception',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@daily_attendance_exception_data']);
    Route::get('/report/date_wise_attendance_exception',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@date_wise_attendance_exception'])->name('report.date_wise_attendance_exception');
    Route::post('/report/date_wise_attendance_exception',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@date_wise_attendance_exception_data']);
    Route::get('/report/admin_date_wise_overtime',['middleware'=>'check-permission:admin','uses'=>'ReportController@admin_date_wise_overtime'])->name('report.admin_date_wise_overtime');
    Route::post('/report/admin_date_wise_overtime',['middleware'=>'check-permission:admin','uses'=>'ReportController@admin_date_wise_overtime_data']);
    Route::get('/report/daily_attendance',['middleware'=>'check-permission:admin','uses'=>'ReportController@report_daily_attendance'])->name('report.daily_attendance');
    Route::post('/report/daily_attendance',['middleware'=>'check-permission:admin','uses'=>'ReportController@view_report_daily_attendance']);
    Route::get('/report/date_wise_present',['middleware'=>'check-permission:admin','uses'=>'ReportController@date_wise_present'])->name('report.date_wise_present');
    Route::post('/report/date_wise_present',['middleware'=>'check-permission:admin','uses'=>'ReportController@date_wise_present_data']);
    Route::get('/report/attendance/card',['middleware'=>'check-permission:admin','uses'=>'ReportController@attendanceCard']);
    Route::post('/attendance/report/card',['middleware'=>'check-permission:admin','uses'=>'ReportController@attendanceCardReport']);
    Route::get('/report/attendance/summary',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@attendance_summary'])->name('report.attendance_summery');
    Route::post('/report/attendance/summary',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@attendance_summary_data']);
    Route::get('/report/leave',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@leave_report']);
    Route::get('/report/leave/dateWiseReport',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@dateWiseLeaveReport']);
    Route::get('/report/leave/onLeaveReport',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@onLeaveReport']);
    Route::get('/report/leave/onLeaveListPDF',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@onLeaveListPDF']);
    Route::get('/report/leave/earnLeaveReport',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@earnLeave']);
    Route::post('/report/leave/earnLeaveReport',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@viewEarnReport']);
    Route::get('/report/leave/maternityLeaveReport',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@maternityLeave']);
    Route::post('/report/leave/maternityLeaveReport',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@viewMaternityReport']);
    Route::get('/report/leave/availableLeaveReport',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@availableLeaveReport']);
    Route::post('/report/leave/availableLeaveReport',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@viewEmployeeAvailableLeave']);
    Route::get('/training/report',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@training_report']);
    Route::post('/leave/report/dateWiseReport/show',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@showDateWiseLeaveReport']);
    Route::post('/training/report/show',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@training_report_show']);
    Route::get('/report/vacancy',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@vacancyListFirst']);
    Route::post('/report/vacancy/vacancyList',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@vacancyList']);
    Route::get('/report/vacancy/applicantList',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@applicantList']);
    Route::post('/report/vacancy/applicantList/show',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@applicantListShow']);
    Route::get('/report/vacancy/acceptedList',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@acceptedList']);
    Route::get('/report/recruitment/acceptedListPDF',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@acceptedListPDF']);
    Route::get('/report/recruitment/acceptedListPDFBangla',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@acceptedListPDFBangla']);
    Route::get('/report/recruitment/dailyRecruitmentList',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@dailyRecruitmentList']);
    Route::get('/report/recruitment/dailyRecruitmentListPDF',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@dailyRecruitmentListPDF']);
    Route::get('/report/public/attendance/card',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@attendanceCardPublic'])->name('report.public.attendance_card');
//    Route::post('public/attendance/report/card',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@attendanceCardReportPublic']);
    Route::get('/report/leave',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@leave_report']);
    Route::post('/leave/report/show',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@leave_report_employee']);
    Route::get('/report/training',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@trainingReportDashboard']);
    Route::get('/report/training/ongoingTraining',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@ongoing_training_report_show']);
    Route::get('/report/training/completedTraining',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@completed_training_report_show']);
    Route::get('/report/training/completedTrainingPDF',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@completedTrainingPDF']);
    Route::get('/report/training/employeeHistory',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@employee_training_history_report']);
    Route::get('/report/training/trainingwiseEmployee',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@training_wise_employee_report']);
    Route::get('/report/training/trainingWiseEmployeePDF',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@trainingWiseEmployeePDF']);
    Route::get('/report/training/trainingWiseEmployeeAjax', 'ReportController@training_report_show');
    Route::get('/report/recruitment',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@recruitmentReportDashboard']);
    //report route end
    Route::post('/employee/bonus/store',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@bonus_emloyee']);
    Route::post('/year/employee/bonus/update',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@year_employee_bonus']);
    Route::post('/year/employee/bonus/store/multiple',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@year_employee_bonus_multiple']);
    Route::get('/attendance/load',['middleware'=>'check-permission:admin|hr', 'uses'=>'AttendanceController@request_leave']);
    //meeting
//    Route::post('/meeting/create',['uses' => 'MeetingController@store']);
    Route::resource('/meeting', 'MeetingController');
//    Route::get('/create', 'MeetingController@create');
    //end meeting
    //expense routes
    Route::get('/expense/categories',['middleware'=>'check-permission:admin|hr|accountant','uses'=>'expenseController@expenseSettings']);
    Route::post('/expense/updateExpense',['middleware'=>'check-permission:admin|hr|accountant','uses'=>'expenseController@expenseUpdate']);
    Route::post('/expense/settings/addExpense',['middleware'=>'check-permission:admin|hr|accountant','uses'=>'expenseController@expenseAdd']);
    Route::get('/expense/settings/delete/{id}',['middleware'=>'check-permission:admin|hr|accountant','uses'=>'expenseController@expenseDelete']);
    Route::get('/expense/deleteDailyExpense/{id}',['middleware'=>'check-permission:admin|hr|accountant','uses'=>'expenseController@deleteDailyExpense']);
    Route::get('/expense/index',['middleware'=>'check-permission:admin|hr|accountant','uses'=>'expenseController@index']);
    Route::post('/expense/addDailyExpense',['middleware'=>'check-permission:admin|hr|accountant','uses'=>'expenseController@addDailyExpense']);
    Route::post('/expense/history',['middleware'=>'check-permission:admin|hr|accountant','uses'=>'expenseController@expenseHistory']);
    Route::get('/expense/selectDate',['middleware'=>'check-permission:admin|hr|accountant','uses'=>'expenseController@expenseDateSelect']);
    Route::get('/expense/report',['middleware'=>'check-permission:admin|hr|accountant','uses'=>'expenseController@expense_report'])->name('expense.report');
    Route::post('/expense/report',['middleware'=>'check-permission:admin|hr|accountant','uses'=>'expenseController@expense_report_data']);
    //end expense routes
    //settings route start
    Route::get('/system/settings',['middleware'=>'check-permission:admin|hr', 'uses'=>'SettingsController@index']);
    Route::post('/settings_store',['middleware'=>'check-permission:admin|hr', 'uses'=>'SettingsController@settings_store']);
    Route::post('/settings_update',['middleware'=>'check-permission:admin|hr', 'uses'=>'SettingsController@settings_update']);
    Route::get('/settings_delete/{id}',['middleware'=>'check-permission:admin|hr', 'uses'=>'SettingsController@settings_delete']);
    //settings route end
    //PDF VIEW
    Route::get('/attendance/card/pdf',['middleware'=>'check-permission:admin', 'uses'=>'ReportController@pdfAttendanceCard']);
    Route::get('/public/attendance/card/pdf',['middleware'=>'check-permission:admin|hr', 'uses'=>'ReportController@pdfPublicAttendanceCard']);
    Route::get('/report/training/ongoingTraining/pdf',['middleware'=>'check-permission:admin|hr', 'uses'=>'ReportController@pdfOngoingTrainingCard']);
    Route::get('/employee/id_card/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'EmployeeController@id_card'])->name('employee.id_card');
    Route::get('/employee/id_card/{id}',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@id_card'])->name('employee.id_card');
    Route::get('/employee/appointment_latter/{id}',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@appointment_latter'])->name('employee.appointment_latter');
    Route::get('/employee/police_verification/{id}',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@generate_pva'])->name('employee.generate_pva');
    Route::get('/employee/police_verification_application/{id}',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@download_police_varification_application'])->name('employee.download_pva');
    Route::POST('/police_verification_data',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@save_pva_data']);
    Route::get('/employee/resignation_letter/{id}',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@resignation_letter'])->name('employee.resignation_letter');
    Route::get('/employee/final_settlement/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'EmployeeController@final_settlement'])->name('employee.final_settlement');
    Route::get('/salary_data',['middleware'=>'check-permission:admin|hr','uses'=>'SalarySheet@salary_data']);
    Route::get('/jop_application/vacancy/{id}',['middleware'=>'check-permission:admin|hr|executive','uses'=>'RecruitmentController@download_job_application'])->name('download_job_application');
    Route::get('/recruitment_policy',['middleware'=>'check-permission:admin|hr|executive','uses'=>'EmployeeController@recruitment_policy'])->name('company.recruitment_policy');
    //data save in history table
    Route::post('/salary_history_data',['middleware'=>'check-permission:admin|hr|executive','uses'=>'SalarySheet@save_history']);
    Route::get('/daily_attendance_summary',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@daily_attendance_summery'])->name('report.daily_attendance_summery');
    Route::post('/daily_attendance_summary',['middleware'=>'check-permission:admin|hr','uses'=>'ReportController@daily_attendance_summary_data']);
    //Manual SMS/Email
    Route::get('/manual_sms',['middleware'=>'check-permission:admin|hr','uses'=>'messageController@manual_sms']);
    Route::post('/manual_sms',['uses' => 'messageController@sent_sms']);
    Route::get('/manual_email',['middleware'=>'check-permission:admin|hr','uses'=>'messageController@manual_email']);
    Route::post('/manual_email',['uses' => 'messageController@sent_email']);
    //payslip pdf report
    Route::get('/report/payslip/datewise/pdf/generate',['middleware'=>'check-permission:admin|hr|executive','uses'=>'PayrollController@payslip_pdf_report_generate_view']);
    Route::post('/report/payslip/datewise/pdf',['middleware'=>'check-permission:admin|hr|executive','uses'=>'PayrollController@payslip_pdf_report']);
    //company information
    Route::get('/company/information',['middleware'=>'check-permission:admin|hr|executive','uses'=>'SystemsettingsController@companyInformationview']);
    Route::post('/company/information/update',['middleware'=>'check-permission:admin|hr|executive','uses'=>'SystemsettingsController@companyInformationupdate']);
    Route::get('/attbonus/url/{id}',['middleware'=>'check-permission:admin|hr|executive','uses'=>'PayrollController@empGrossShow']);
    Route::post('/deduction/store',['middleware'=>'check-permission:admin|hr|executive','uses'=>'DeductionController@deductionStore']);
    Route::get('/advance',['middleware'=>'check-permission:admin|hr|executive','uses'=>'DeductionController@advancedDeduction']);
    Route::post('/loan/store',['middleware'=>'check-permission:admin|hr|executive','uses'=>'DeductionController@loanStore']);
    
    //notices
    Route::resource('/notices', 'noticeController');

    Route::get('/manual_bulk_sms',['middleware'=>'check-permission:admin|hr|executive','uses'=>'messageController@manual_bulk_sms_view']);
    Route::post('/proccess_bulk_sms',['middleware'=>'check-permission:admin|hr|executive','uses'=>'messageController@proccess_bulk_sms_view']);

     Route::POST('/sent_bulk_sms_to_employee',['middleware'=>'check-permission:admin|hr|executive','uses'=>'messageController@send_manual_bulk_sms']);

     Route::get('/aa',['middleware'=>'check-permission:admin|hr|executive','uses'=>'messageController@test1']);

    //All bonus route are start here
    Route::get('/designation/employee/{id}',['middleware'=>'check-permission:admin|hr|','uses'=>'PayrollController@Designationwiseemployeefestival']);
    Route::get('/tests',['middleware'=>'check-permission:admin|hr|','uses'=>'PayrollController@alldesignationemployee']);
    Route::get('/department/employee/{id}',['middleware'=>'check-permission:admin|hr|','uses'=>'PayrollController@Departmentwiseemployeefestival']);
    Route::get('/testsone',['middleware'=>'check-permission:admin|hr|','uses'=>'PayrollController@alldepartmentemployee']);
    Route::post('/designation/employee/bonus/store',['middleware'=>'check-permission:admin|hr|','uses'=>'PayrollController@designationBonusStore']);
    Route::post('/designation/employee/att/bonus/store',['middleware'=>'check-permission:admin|hr|','uses'=>'PayrollController@attdebonusstore']);
    Route::get('/inc/designation/all',['middleware'=>'check-permission:admin|hr|','uses'=>'PayrollController@incdesignationall']);
    Route::post('/inc/bonusss/multiple/store',['middleware'=>'check-permission:admin|hr|','uses'=>'PayrollController@incBonusStore']);
    Route::get('/production',['middleware'=>'check-permission:admin|hr|','uses'=>'PayrollController@ProductionBonus']);
    Route::post('/production/store',['middleware'=>'check-permission:admin|hr|','uses'=>'PayrollController@ProductionBonusStore']);

    //All bonus route are end here

    //penalty advance report route start
      Route::get('/penalty',['middleware'=>'check-permission:admin|hr|executive','uses'=>'DeductionController@index']);
      Route::get('/report/advanced',['middleware'=>'check-permission:admin|hr|','uses'=>'DeductionController@loanEmployee']);
      Route::get('/report/penalty',['middleware'=>'check-permission:admin|hr|','uses'=>'DeductionController@Employeedeductionreportview']);
      Route::post('/report/penalty/show',['middleware'=>'check-permission:admin|hr|','uses'=>'DeductionController@Employeedeductionreportshow']);
      Route::get('/loan/{id}',['middleware'=>'check-permission:admin|hr|','uses'=>'DeductionController@Employeeloanreportshow']);
      Route::post('/normal/deduction/update',['middleware'=>'check-permission:admin|hr|','uses'=>'DeductionController@normalDeductionUpdate']);
    //penalty advance report route end



      //Start Complains Management 
      Route::resource('/complains', 'ComplainsController');
      Route::get('/complainsList',['middleware'=>'check-permission:admin|hr|executive','uses'=>'ComplainsController@alllist'])->name('complains.list');;
    Route::get('/complains/edit/{id}','ComplainsController@edit')->name('complains.edit');

});