<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbcomplains', function (Blueprint $table) {
            $table->increments('id');
            $table->string('complainerName')->nullable();
            $table->string('complainerPhone',25)->nullable();
            $table->text('complainerAddress')->nullable();
            $table->text('complainMessage')->nullable();
            $table->text('complainRejectReason')->nullable();
            $table->tinyinteger('complainStatus')->nullable();
            $table->date('complainDate')->nullable();
            $table->tinyinteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbcomplains');
    }
}
