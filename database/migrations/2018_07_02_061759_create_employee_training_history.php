<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTrainingHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_training_history',function (Blueprint $table){
            $table->increments('id');
            $table->integer('emp_id');
            $table->index('emp_id');
            $table->string('training_name');
            $table->date('training_start');
            $table->date('training_end');
            $table->text('training_description')->nullable();
            $table->text('training_institution')->nullable();
            $table->string('training_attachment')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_training_history');
    }
}
