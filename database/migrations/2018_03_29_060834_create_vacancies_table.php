<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vacTitle',50);
            $table->text('vacDescription')->nullable();
            $table->integer('vacNumber');
            $table->timestamp('VacAnnounceStartingDate');
            $table->timestamp('vacAnnounceEndingDate')->nullable();
            $table->timestamp('vacJoiningDate')->nullable();
            $table->string('created_by');
            $table->string('updated_by');
            $table->tinyInteger('vacStatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies');
    }
}
