<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeLivingHistoryDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbemployee_living_history_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empId')->nullable();
            $table->string('address')->nullable();
            $table->date('fromAddress')->nullable();
            $table->date('toAddress')->nullable();
            $table->text('remarks')->nullable();
            $table->integer('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbemployee_living_history_data');
    }
}
