<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbFestivalBonus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('festival_bonus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id');
            $table->index('emp_id');
            $table->decimal('emp_gross',18,2)->nullable();
            $table->decimal('emp_bonus',18,2)->nullable();
            $table->decimal('emp_amount',18,2)->nullable();
            $table->string('emp_total_percent','30')->nullable();
            $table->decimal('emp_total_amount',18,2)->nullable();
            $table->integer('bonus_given_id');
            $table->string('bonus_title',30);
            $table->string('month',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('festival_bonus');
    }
}
