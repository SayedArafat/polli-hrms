<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbLeaveTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_leave_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('leave_type','40');
            $table->integer('total_days');
            $table->text('policy')->nullable();
            $table->string('created_by',50);
            $table->string('modified_by',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_leave_type');
    }
}
