<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbEmployeeTrainingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_employee_training', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->index('employee_id');
            $table->integer('training_id');
            $table->index('training_id');
            $table->date('training_starting_date');
            $table->index('training_starting_date');
            $table->date('training_ending_date');
            $table->index('training_ending_date');
            $table->string('attachment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_employee_training');
    }
}
