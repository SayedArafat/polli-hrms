<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AttendanceSetupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('attendance_setup')->insert([
            'entry_time'=>'09:00',
            'exit_time'=>'18:00',
            'max_entry_time'=>'09:15',
            'min_exit_time'=>'17:45',
            'created_at'=>\Carbon\Carbon::now()->toDateString(),
            'updated_at'=>\Carbon\Carbon::now()->toDateString(),

        ]);
    }
}
