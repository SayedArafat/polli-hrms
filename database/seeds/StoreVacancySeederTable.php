<?php

use Illuminate\Database\Seeder;

class StoreVacancySeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('store_vacancy_temp')->insert([
            'designation'=>'Not Given',
            'vacancy_no'=>0,
        ]);
    }
}
